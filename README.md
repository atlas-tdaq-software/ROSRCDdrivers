# TDAQ drivers
This repository contains the source code for the kernel modules listed below.

| Kernel module | User Library| Description | 
|:--------|:--------|:--------|
| robinnp.ko      | [ROSRobinNP](https://gitlab.cern.ch/atlas-tdaq-software/ROSRobinNP)| RobinNP card used by ROS up to Run 3 |    
| robin.ko        | [ROSRobin](https://gitlab.cern.ch/atlas-tdaq-software/ROSRobin)    | Robin card used in Run 1 |
| filar.ko        | [ROSfilar](https://gitlab.cern.ch/atlas-tdaq-software/ROSfilar)    | Filar data generator to emulate a ROD |
| quest.ko        | [ROSsolar](https://gitlab.cern.ch/atlas-tdaq-software/ROSsolar)    | Quest/Dolar data generator to emulate a ROD |
| io_rcc.ko       | [io_rcc](https://gitlab.cern.ch/atlas-tdaq-software/io_rcc)        | User level PCI and PC I/O |
| cmem_rcc.ko     | [cmem_rcc](https://gitlab.cern.ch/atlas-tdaq-software/cmem_rcc)    | Driver to allocate large contigous memory buffers. |
| vme_rcc_tsi.ko  | [vme_rcc](https://gitlab.cern.ch/atlas-tdaq-software/vme_rcc)      | Driver for SBC in VME crates |
| uio_pci_dma.ko  | [vme_rcc](https://gitlab.cern.ch/atlas-tdaq-software/vme_rcc)      |  |
| vme_rcc.ko      | [vme_rcc](https://gitlab.cern.ch/atlas-tdaq-software/vme_rcc)      | Driver for SBC in VME crates |



## Automatic Build
CI pipelines are used to build kernel modules.
Two pipelines exists: one that follows the kernels as they appear in the CERN IT repos, and one that follows the ATLAS TDAQ SysAdmins (ATDS) test repo.
Note that:

* ROSRCDdrivers tags have to respect the format `ROSRCDdrivers-NN-NN-NN` and they are used to name the drivers release,
* kernel modules are build for the latest 4 kernel version found by yum/dnf.
* [- The TDAQ release name is hard-coded in scripts/setup_tags.sh. -]

### Build on push
When a change is pushed to the repo, kernel modules are built with release name `ROSRCDdrivers-99-00-00` and copied to
```
/eos/project/f/felix/www/user/dist/software/tdaq-drivers/el9/ROSRCDdrivers-99-00-00
/eos/project/f/felix/www/user/dist/software/tdaq-drivers/el9_atds/ROSRCDdrivers-99-00-00
```

### Scheduled pipeline
The sheduled build always rebuilds the last tag so to always have it available in the latest kernels at
```
/eos/project/f/felix/www/user/dist/software/tdaq-drivers/el9/<release name>
/eos/project/f/felix/www/user/dist/software/tdaq-drivers/el9_atds/<release name>
```
[+ When a new release is made by creating a new tag the pipeline schedule shall be manually edited to point to the new tag. +]
[+ This can be done using the web interface. From the left panel: CI/CD > Schedules > Pencil button > change "Target branch or tag"  +]


## Manual Build

### Requirements
Building the drivers requires the kernel source to be installed in the system.
To install the necessary packages on RHEL9 and derivates:
```
dnf install kernel kernel-devel grubby
```

### Build procedure

1. Clone this repo
2. Clone submodules
   ```
   git submodule init && git submodule update
   ```
3. Set up version tags using the provided script. Version tags consists in the git tag/hash (output of `git describe`) of this repo and of the library headers included as submodules. These tags are exported to the environment and used in the Makefile. Among the tags is the release name for the build is set by default to `ROSRCDdrivers-99-00-00` and the [- TDAQ release name hard-coded in scripts/setup_tags.sh. -]
   ```
   cd scripts
   source setup_tags.sh
   ```

4.  Define the kernel version to compile against.The kernel version is set to the running kernel unless a different option is passed as argument. It is, in fact, possible to compile against different kernel versions as long as the sources are installed on the system.
    ```
    source setup_kernel.sh
    ```

5. Compile
   ```
   cd ../src
   make
   ```
5. The kernel modules will be placed in the `build/<release name>` directory. Kernel modules contain the kernel version in their filename. 


## RPM build
The RPM is not built by the CI pipeline. Instructions for a manual build are contained in the `rpm_creation` directory.
