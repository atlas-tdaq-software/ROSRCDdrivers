#!/bin/bash


##########################################################################################
#                                                                                        #
# This script populates a directory with TDAQ S/W. It is a precondition for making a RPM #
#                                                                                        #
#  Author: Markus Joos, CERN                                                             #
#                                                                                        #
##########################################################################################


# Note: This is not yet the final version. Some cleanup still has to be done

export SOURCE_PATH=/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-11-02-01

export SPECIAL_PATH=.
export SPECIAL_PATH2=/afs/cern.ch/user/j/joos/DAQ/DataFlow/ROSRCDdrivers/src
export LIB_PATH=/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-11-02-01/installed/x86_64-el9-gcc13-dbg/lib
export BIN_PATH=/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-11-02-01/installed/x86_64-el9-gcc13-dbg/bin


#######################
#create the directories
#######################
rm -rf tdaq_drivers
mkdir -p tdaq_drivers/cmem_rcc
mkdir -p tdaq_drivers/io_rcc
mkdir -p tdaq_drivers/vme_rcc
mkdir -p tdaq_drivers/ROSRobinNP
mkdir -p tdaq_drivers/ROSRobin
mkdir -p tdaq_drivers/ROSGetInput
mkdir -p tdaq_drivers/DFDebug
mkdir -p tdaq_drivers/rcc_error
mkdir -p tdaq_drivers/rcc_time_stamp
mkdir -p tdaq_drivers/src
mkdir -p tdaq_drivers/ROSRCDdrivers
mkdir -p tdaq_drivers/drivers
mkdir -p tdaq_drivers/script
mkdir -p tdaq_drivers/lib64
mkdir -p tdaq_drivers/bin


#############
#Header files
#############
cp -f $SOURCE_PATH/cmem_rcc/cmem_rcc/cmem_rcc_drv.h               tdaq_drivers/cmem_rcc
cp -f $SOURCE_PATH/cmem_rcc/cmem_rcc/cmem_rcc_common.h            tdaq_drivers/cmem_rcc
cp -f $SOURCE_PATH/cmem_rcc/cmem_rcc/cmem_rcc.h                   tdaq_drivers/cmem_rcc

cp -f $SOURCE_PATH/io_rcc/io_rcc/io_rcc_driver.h                  tdaq_drivers/io_rcc
cp -f $SOURCE_PATH/io_rcc/io_rcc/io_rcc_common.h                  tdaq_drivers/io_rcc
cp -f $SOURCE_PATH/io_rcc/io_rcc/io_rcc.h                         tdaq_drivers/io_rcc

cp -f $SOURCE_PATH/vme_rcc/vme_rcc/cct_berr.h                     tdaq_drivers/vme_rcc
cp -f $SOURCE_PATH/vme_rcc/vme_rcc/tsi148.h                       tdaq_drivers/vme_rcc
cp -f $SOURCE_PATH/vme_rcc/vme_rcc/universe.h                     tdaq_drivers/vme_rcc
cp -f $SOURCE_PATH/vme_rcc/vme_rcc/vme_rcc_common.h               tdaq_drivers/vme_rcc
cp -f $SOURCE_PATH/vme_rcc/vme_rcc/vme_rcc_driver.h               tdaq_drivers/vme_rcc
cp -f $SOURCE_PATH/vme_rcc/vme_rcc/vme_rcc_lib.h                  tdaq_drivers/vme_rcc
cp -f $SOURCE_PATH/vme_rcc/vme_rcc/vme_rcc.h                      tdaq_drivers/vme_rcc

cp -f $SOURCE_PATH/ROSRobinNP/ROSRobinNP/RobinNP_common.h         tdaq_drivers/ROSRobinNP
cp -f $SOURCE_PATH/ROSRobinNP/ROSRobinNP/RobinNPRegisters.h       tdaq_drivers/ROSRobinNP
cp -f $SOURCE_PATH/ROSRobinNP/ROSRobinNP/RobinNPRegistersCommon.h tdaq_drivers/ROSRobinNP

cp -f $SOURCE_PATH/ROSRobin/ROSRobin/robindriver_common.h         tdaq_drivers/ROSRobin
cp -f $SOURCE_PATH/ROSRobin/ROSRobin/robindriver.h                tdaq_drivers/ROSRobin

cp -f $SOURCE_PATH/rcc_error/rcc_error/rcc_error.h                tdaq_drivers/rcc_error

cp -f $SOURCE_PATH/rcc_time_stamp/rcc_time_stamp/tstamp.h         tdaq_drivers/rcc_time_stamp

cp -f $SOURCE_PATH/ROSGetInput/ROSGetInput/get_input.h            tdaq_drivers/ROSGetInput
cp -f $SOURCE_PATH/DFDebug/DFDebug/DFDebug.h                      tdaq_drivers/DFDebug
cp -f $SOURCE_PATH/DFDebug/DFDebug/GlobalDebugSettings.h          tdaq_drivers/DFDebug

cp -f $SOURCE_PATH/installed/include/ROSRCDdrivers/tdaq_drivers.h tdaq_drivers/ROSRCDdrivers


###################
#Driver source code
###################
cp -f $SPECIAL_PATH2/cmem_rcc.c                                   tdaq_drivers/src
cp -f $SPECIAL_PATH2/io_rcc.c                                     tdaq_drivers/src
cp -f $SPECIAL_PATH2/vme_rcc.c                                    tdaq_drivers/src
cp -f $SPECIAL_PATH2/vme_rcc_tsi.c                                tdaq_drivers/src
cp -f $SPECIAL_PATH2/robinnp.c                                    tdaq_drivers/src
cp -f $SPECIAL_PATH2/robin.c                                      tdaq_drivers/src

#############
#Driver tools
#############
cp -f $SPECIAL_PATH/Makefile_drivers                              tdaq_drivers/src/Makefile
cp -f $SPECIAL_PATH/dkms.conf                                     tdaq_drivers
cp -f $SPECIAL_PATH/drivers_tdaq                                  tdaq_drivers/script
cp -f $SPECIAL_PATH/drivers_tdaq.service                          tdaq_drivers/script
#cp -f $SPECIAL_PATH/vmetab_universe                              tdaq_drivers/script

###############
#tdaq libraries
###############
cp -f $LIB_PATH/librcc_error.so                                   tdaq_drivers/lib64
cp -f $LIB_PATH/librcc_time_stamp.so                              tdaq_drivers/lib64
cp -f $LIB_PATH/libio_rcc.so                                      tdaq_drivers/lib64
cp -f $LIB_PATH/libcmem_rcc.so                                    tdaq_drivers/lib64
cp -f $LIB_PATH/libDFDebug.so                                     tdaq_drivers/lib64
cp -f $LIB_PATH/libgetinput.so                                    tdaq_drivers/lib64
cp -f $LIB_PATH/libvme_rcc.so                                     tdaq_drivers/lib64

#########################################################
# some applications for driver initialisation and testing 
#########################################################
cp -f $BIN_PATH/cmem_rcc_bpa_test                                tdaq_drivers/bin
cp -f $BIN_PATH/test_cmem_rcc                                    tdaq_drivers/bin
cp -f $BIN_PATH/vmeconfig                                        tdaq_drivers/bin
cp -f $BIN_PATH/tsiconfig                                        tdaq_drivers/bin


tar czf  ../tdaq_drivers-11.2.1-src.tar.gz --directory tdaq_drivers .
