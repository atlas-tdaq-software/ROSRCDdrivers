%define module tdaq_drivers
%define version 11.2.1

%define _binaries_in_noarch_packages_terminate_build   0

Summary: tdaq_drivers dkms package
Name: %{module}
Version: %{version}
Release: 2dkms
Vendor: CERN
License: GPL
Packager: Markus Joos <markus.joos@cern.ch>
Group: System Environment/Base
BuildArch: noarch
Requires: dkms >= 1.00
Requires: kernel-devel
Requires: bash
# There is no Source# line for dkms.conf since it has been placed
# into the source tarball of SOURCE0
Source0: %{module}-%{version}-src.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root/

%description
This package contains the tdaq_drivers module wrapped for the DKMS framework.

%prep
rm -rf %{module}-%{version}
mkdir %{module}-%{version}
cd %{module}-%{version}
tar xvzf $RPM_SOURCE_DIR/%{module}-%{version}-src.tar.gz

%install
if [ "$RPM_BUILD_ROOT" != "/" ]; then
   rm -rf $RPM_BUILD_ROOT
fi
mkdir -p $RPM_BUILD_ROOT/usr/src/%{module}-%{version}/
mkdir -p $RPM_BUILD_ROOT/usr/src/%{module}-%{version}/patches
mkdir -p $RPM_BUILD_ROOT/usr/src/%{module}-%{version}/redhat_driver_disk
cp -rf %{module}-%{version}/* $RPM_BUILD_ROOT/usr/src/%{module}-%{version}

mkdir -p $RPM_BUILD_ROOT/usr/include
mkdir -p $RPM_BUILD_ROOT/usr/lib64

install -m644 -t $RPM_BUILD_ROOT/usr/include $RPM_BUILD_DIR/%{name}-%{version}/cmem_rcc/cmem_rcc_drv.h    
install -m644 -t $RPM_BUILD_ROOT/usr/include $RPM_BUILD_DIR/%{name}-%{version}/cmem_rcc/cmem_rcc_common.h 
install -m644 -t $RPM_BUILD_ROOT/usr/include $RPM_BUILD_DIR/%{name}-%{version}/cmem_rcc/cmem_rcc.h    
install -m644 -t $RPM_BUILD_ROOT/usr/include $RPM_BUILD_DIR/%{name}-%{version}/io_rcc/io_rcc_driver.h
install -m644 -t $RPM_BUILD_ROOT/usr/include $RPM_BUILD_DIR/%{name}-%{version}/io_rcc/io_rcc_common.h
install -m644 -t $RPM_BUILD_ROOT/usr/include $RPM_BUILD_DIR/%{name}-%{version}/io_rcc/io_rcc.h     
install -m644 -t $RPM_BUILD_ROOT/usr/include $RPM_BUILD_DIR/%{name}-%{version}/vme_rcc/cct_berr.h 
install -m644 -t $RPM_BUILD_ROOT/usr/include $RPM_BUILD_DIR/%{name}-%{version}/vme_rcc/tsi148.h  
install -m644 -t $RPM_BUILD_ROOT/usr/include $RPM_BUILD_DIR/%{name}-%{version}/vme_rcc/universe.h  
install -m644 -t $RPM_BUILD_ROOT/usr/include $RPM_BUILD_DIR/%{name}-%{version}/vme_rcc/vme_rcc_common.h 
install -m644 -t $RPM_BUILD_ROOT/usr/include $RPM_BUILD_DIR/%{name}-%{version}/vme_rcc/vme_rcc_driver.h
install -m644 -t $RPM_BUILD_ROOT/usr/include $RPM_BUILD_DIR/%{name}-%{version}/vme_rcc/vme_rcc_lib.h  
install -m644 -t $RPM_BUILD_ROOT/usr/include $RPM_BUILD_DIR/%{name}-%{version}/vme_rcc/vme_rcc.h     
install -m644 -t $RPM_BUILD_ROOT/usr/include $RPM_BUILD_DIR/%{name}-%{version}/ROSRobinNP/RobinNP_common.h   
install -m644 -t $RPM_BUILD_ROOT/usr/include $RPM_BUILD_DIR/%{name}-%{version}/ROSRobinNP/RobinNPRegisters.h 
install -m644 -t $RPM_BUILD_ROOT/usr/include $RPM_BUILD_DIR/%{name}-%{version}/ROSRobinNP/RobinNPRegistersCommon.h
install -m644 -t $RPM_BUILD_ROOT/usr/include $RPM_BUILD_DIR/%{name}-%{version}/ROSRobin/robindriver_common.h
install -m644 -t $RPM_BUILD_ROOT/usr/include $RPM_BUILD_DIR/%{name}-%{version}/ROSRobin/robindriver.h
install -m644 -t $RPM_BUILD_ROOT/usr/include $RPM_BUILD_DIR/%{name}-%{version}/rcc_error/rcc_error.h            
install -m644 -t $RPM_BUILD_ROOT/usr/include $RPM_BUILD_DIR/%{name}-%{version}/ROSGetInput/get_input.h         
install -m644 -t $RPM_BUILD_ROOT/usr/include $RPM_BUILD_DIR/%{name}-%{version}/DFDebug/DFDebug.h              
install -m644 -t $RPM_BUILD_ROOT/usr/include $RPM_BUILD_DIR/%{name}-%{version}/DFDebug/GlobalDebugSettings.h 
install -m644 -t $RPM_BUILD_ROOT/usr/include $RPM_BUILD_DIR/%{name}-%{version}/ROSRCDdrivers/tdaq_drivers.h 

install -m644 -t $RPM_BUILD_ROOT/usr/lib64 $RPM_BUILD_DIR/%{name}-%{version}/lib64/librcc_error.so
install -m644 -t $RPM_BUILD_ROOT/usr/lib64 $RPM_BUILD_DIR/%{name}-%{version}/lib64/libio_rcc.so
install -m644 -t $RPM_BUILD_ROOT/usr/lib64 $RPM_BUILD_DIR/%{name}-%{version}/lib64/libcmem_rcc.so
install -m644 -t $RPM_BUILD_ROOT/usr/lib64 $RPM_BUILD_DIR/%{name}-%{version}/lib64/libDFDebug.so
install -m644 -t $RPM_BUILD_ROOT/usr/lib64 $RPM_BUILD_DIR/%{name}-%{version}/lib64/libgetinput.so
install -m644 -t $RPM_BUILD_ROOT/usr/lib64 $RPM_BUILD_DIR/%{name}-%{version}/lib64/libvme_rcc.so

mkdir -p $RPM_BUILD_ROOT/etc/init.d
install -m755 -t $RPM_BUILD_ROOT/etc/init.d $RPM_BUILD_DIR/%{name}-%{version}/script/drivers_tdaq
#install -m755 -t $RPM_BUILD_ROOT/etc/init.d $RPM_BUILD_DIR/%{name}-%{version}/script/vmetab_universe


mkdir -p $RPM_BUILD_ROOT/etc/systemd/system
install -m644 -t $RPM_BUILD_ROOT/etc/systemd/system $RPM_BUILD_DIR/%{name}-%{version}/script/drivers_tdaq.service

%clean
if [ "$RPM_BUILD_ROOT" != "/" ]; then
   rm -rf $RPM_BUILD_ROOT
fi

%files
%defattr(-,root,root)
/usr/src/%{module}-%{version}/
/usr/lib64/librcc_error.so
/usr/lib64/libio_rcc.so
/usr/lib64/libcmem_rcc.so
/usr/lib64/libDFDebug.so
/usr/lib64/libgetinput.so
/usr/lib64/libvme_rcc.so

/usr/include/cmem_rcc_drv.h
/usr/include/cmem_rcc_common.h
/usr/include/cmem_rcc.h
/usr/include/io_rcc_driver.h
/usr/include/io_rcc_common.h
/usr/include/io_rcc.h
/usr/include/cct_berr.h
/usr/include/tsi148.h
/usr/include/universe.h
/usr/include/vme_rcc_common.h
/usr/include/vme_rcc_driver.h
/usr/include/vme_rcc_lib.h
/usr/include/vme_rcc.h
/usr/include/RobinNP_common.h
/usr/include/RobinNPRegisters.h
/usr/include/RobinNPRegistersCommon.h
/usr/include/robindriver.h
/usr/include/robindriver_common.h
/usr/include/rcc_error.h
/usr/include/get_input.h
/usr/include/DFDebug.h
/usr/include/GlobalDebugSettings.h
/usr/include/tdaq_drivers.h
/etc/init.d/drivers_tdaq
#/etc/init.d/vmetab_universe
/etc/systemd/system/drivers_tdaq.service

%pre

%post
dkms add -m %{module} -v %{version} --rpm_safe_upgrade

# If we haven't loaded a tarball, then try building it for the current kernel
if [ -z "$loaded_tarballs" ]; then
   if [ `uname -r | grep -c "BOOT"` -eq 0 ] && [ -e /lib/modules/`uname -r`/build/include ]; then
      dkms build -m %{module} -v %{version}
      	   dkms install -m %{module} -v %{version}
	   elif [ `uname -r | grep -c "BOOT"` -gt 0 ]; then
	   	echo -e ""
		     echo -e "Module build for the currently running kernel was skipped since you"
		     	  echo -e "are running a BOOT variant of the kernel."
			  else
				echo -e ""
				     echo -e "Module build for the currently running kernel was skipped since the"
				     	  echo -e "kernel source for this kernel does not seem to be installed."
					  fi
fi
echo -e "Activating drivers_tdaq"
systemctl enable drivers_tdaq.service
systemctl start drivers_tdaq.service
echo -e "Done."
exit 0

%preun
echo -e
echo -e "Uninstall of %{module} module (version %{version}) beginning:"
dkms remove -m %{module} -v %{version} --all --rpm_safe_upgrade
echo -e "Deleting files that dkms remove may have forgotten"
cd /lib/modules/
echo -e "Listing files that will be deleted"
find . -name cmem_rcc.ko
find . -name io_rcc.ko
find . -name vme_rcc.ko
find . -name vme_rcc_tsi.ko
find . -name robinnp.ko
find . -name robin.ko

rm -f `find . -name cmem_rcc.ko`
rm -f `find . -name io_rcc.ko`
rm -f `find . -name vme_rcc.ko`
rm -f `find . -name vme_rcc_tsi.ko`
rm -f `find . -name robinnp.ko`
rm -f `find . -name robin.ko`
echo -e "Done deleting forgotten files"


exit 0
