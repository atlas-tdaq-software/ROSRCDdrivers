NOTE:
When you run "rpmbuild -v -bb --clean SPECS/tdaq_sw_for_Flx.spec" you may get this error: "ImportError: No module named site"
The root of the problem is that rpmbuild does not find the proper Python environment.
Bad Solution: Run "rpmbuild -v -bb --clean SPECS/tdaq_sw_for_Flx.spec" on lxplus
Good solution: Close all windows on pcepese48 and start from scratch

if "rpm -ihv tdaq_drivers-10.0.0-2dkms.noarch.rpm" returns:
error: Failed dependencies:
	libc.so.6(GLIBC_2.34)(64bit) is needed by tdaq_drivers-10.0.0-2dkms.noarch
you may be installing an RPM that was built for ALMA9 on a CC7 system.
Go to /afs/cern.ch/user/j/joos/rpmbuild/SOURCES/pkgs and edit build-tarball.sh (look for LIB_PATH / BIN_PATH)


Keep these files up to date:
============================
SOURCES/pkgs/build-tarball.sh
SOURCES/pkgs/dkms.conf
SOURCES/pkgs/Makefile_drivers
SPECS/tdaq_drivers.spec 

RPM build process:
(* Open the source code of the drivers and update the version number in the "fill_proc_read_text" function)

* Edit the files listed above.

* Execute: "cd SOURCES/pkgs"

* Execute: "./build-tarball.sh"

* "cd" to your home directory

* rm rpmbuild

* ln -s /afs/cern.ch/user/j/joos/DAQ/DataFlow/ROSRCDdrivers/rpm_creation rpmbuild

* Execute: "cd rpmbuild"

* Execute: "rpmbuild -v -bb --define "_sourcedir /afs/cern.ch/user/j/joos/DAQ/DataFlow/ROSRCDdrivers/rpm_creation/SOURCES" --clean SPECS/tdaq_drivers.spec"

* take the RPM from RPMS/noarch

On the target system:
* if not instlled: yum install dkms

* (as root) "rpm -ihv tdaq_drivers-9.2.1-2dkms.noarch.rpm"

* Check which files are installed from the RPM: rpm -ql tdaq_drivers-9.2.1-2dkms.noarch

* Start the service manually: "systemctl start drivers_tdaq.service"

* Check the status: "systemctl status drivers_tdaq.service"
