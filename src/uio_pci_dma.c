/**
 * @author Dominic Eschweiler <eschweiler@fias.uni-frankfurt.de>
 * @date 2013-04-18
 *
 * @section LICENSE
 * Copyright (C) 2012 Dominic Eschweiler
 * This code is no free software. Please read the LICENSE file in the root of
 * the base directory to get all terms and conditions. Don't use this code if
 * you had not contacted the author before.
 *
 * 27.11.2018 Adaptations to CC7 by Markus Joos
 */

#include <linux/device.h>
#include <linux/kobject.h>
#include <linux/module.h>
#include <linux/pci.h>
#include <linux/slab.h>
#include <linux/sort.h>
#include <linux/sysfs.h>
#include <linux/uio_driver.h>
#include <linux/version.h>

#include "uio_pci_dma.h"

#define DRIVER_NAME     "uio_pci_dma"
#define DRIVER_DESC     "UIO PCI 2.4 driver with DMA support"
#define DRIVER_LICENSE  "Dual BSD/GPL"
#define DRIVER_AUTHOR   "Dominic Eschweiler. Modified by Markus Joos, CERN"

#pragma GCC diagnostic ignored "-Wdeclaration-after-statement"





static int uio_probe(struct pci_dev *pci_device, const struct pci_device_id *id);






struct uio_pci_dma_device
{
  struct uio_info info;
  struct pci_dev *pdev;
  struct kobject *dma_kobj;
};


static inline int uio_pci_dma_allocate_scatter_gather(struct uio_pci_dma_private *priv);
int sort_sg_compare(const void *item1, const void *item2);
void sort_sg_swap(void *item1, void *item2, int size);
static inline int uio_pci_dma_free(struct kobject *kobj);
static inline int uio_pci_dma_free_scatter_gather(struct uio_pci_dma_private *priv);

#if LINUX_VERSION_CODE <= KERNEL_VERSION(3, 2, 0)
bool pci_check_and_mask_intx(struct pci_dev *pdev)
{
  uint32_t csr;
  uint16_t command, status, disable;

  //mj pci_block_user_cfg_access(pdev);
  pci_cfg_access_lock(pdev);

  /* Readout the CSR and split it into command and status part */
  pci_read_config_dword(pdev, PCI_COMMAND, &csr);
  command = csr;
  status  = csr >> 16;

  /* Check that the current device indeed throw the interrupt */
  if(!(status & PCI_STATUS_INTERRUPT) )
  {
    //mj pci_unblock_user_cfg_access(pdev);
    pci_cfg_access_unlock(pdev);
    return true;
  }

  /* Disable interrupt on the device */
  disable = command | PCI_COMMAND_INTX_DISABLE;
  if(disable != command)
  {
      pci_write_config_word(pdev, PCI_COMMAND, disable);
  }

  //mj pci_unblock_user_cfg_access(pdev);
  pci_cfg_access_unlock(pdev);
  return true;
}

//CS9: function renamed to avoid clash with a function of the same name in /usr/src/kernels/5.14.0-165.el9.x86_64/include/linux/pci.h
bool pci_intx_mask_supported_local(struct pci_dev *pdev)
{
  bool     ret        = false;
  uint16_t csr_before = 0, csr_after  = 0;

  //mj pci_block_user_cfg_access(pdev);
  pci_cfg_access_lock(pdev);
  {
    pci_read_config_word(pdev, PCI_COMMAND, &csr_before);
    pci_write_config_word(pdev, PCI_COMMAND, csr_before ^ PCI_COMMAND_INTX_DISABLE);
    pci_read_config_word(pdev, PCI_COMMAND, &csr_after);

    if( (csr_before ^ csr_after) & ~PCI_COMMAND_INTX_DISABLE)
    {
      printk(DRIVER_NAME " : CSR changed (intx masking not supported)!\n");
      goto intx_mask_supported_return;
    }

    if( (csr_before ^ csr_after) & PCI_COMMAND_INTX_DISABLE)
    {
      ret = true;
      pci_write_config_word(pdev, PCI_COMMAND, csr_before);
    }
  }

intx_mask_supported_return:
  //mj pci_unblock_user_cfg_access(pdev);
  pci_cfg_access_unlock(pdev);
  return ret;
}
#endif



/** Device initialization stuff etc. */
static irqreturn_t irqhandler(int irq, struct uio_info *info_uio)
{
  struct uio_pci_dma_device *dev =  container_of(info_uio, struct uio_pci_dma_device, info);

  if(!pci_check_and_mask_intx(dev->pdev) )
    return IRQ_NONE;

  return IRQ_HANDLED;
}



static int uio_probe(struct pci_dev *pci_device, const struct pci_device_id *id)
{
  struct uio_pci_dma_device *dma_device = NULL;
  struct kobject            *dma_kobj = NULL;
  int                        err;
  struct kset               *kset;

  UIO_DEBUG_PRINTF("__devinit ENTER\n");

  /* Define and allocate the struct  attr_bin_request  */
  BIN_ATTR_PDA(request, sizeof(struct uio_pci_dma_private), S_IWUGO, NULL, uio_pci_dma_sysfs_request_buffer_write, NULL);

  /* Define and allocate the struct  attr_bin_free  */
  BIN_ATTR_PDA(free, 0, S_IWUGO, NULL, uio_pci_dma_sysfs_delete_buffer_write, NULL);

  /* Enable the PCI Device */
  err = pci_enable_device(pci_device);
  if(err)
  {
    printk(DRIVER_NAME " : Failed to enable PCI device!\n");
    return err;
  }

  if(!pci_device->irq)
  {
    printk(DRIVER_NAME " : Device has no assigned interrupt\n");
    pci_disable_device(pci_device);
    return -ENODEV;
  }

  if(!pci_intx_mask_supported_local(pci_device) )
  {
    printk(DRIVER_NAME " : Device does not support interrupt masking!\n");
    err = -ENODEV;
    goto err_verify;
  }

  dma_device = kzalloc(sizeof(struct uio_pci_dma_device), GFP_KERNEL);
  if(!dma_device)
  {
    err = -ENOMEM;
    goto err_alloc;
  }

  dma_device->info.name      = DRIVER_NAME;
  dma_device->info.version   = UIO_PCI_DMA_VERSION;
  dma_device->info.irq       = pci_device->irq;
  dma_device->info.irq_flags = IRQF_SHARED;
  dma_device->info.handler   = irqhandler;
  dma_device->pdev           = pci_device;

  /* Set DMA-Master */
  pci_set_master(pci_device);

  /* Set DMA-Mask */
  err = pci_set_dma_mask(pci_device, DMA_BIT_MASK(64) );
  if(err)
  {
    printk(DRIVER_NAME " : Warning: couldn't set 64-bit PCI DMA mask.\n");
    err = pci_set_dma_mask(pci_device, DMA_BIT_MASK(32) );
    if(err)
    {
      printk(DRIVER_NAME " : Can't set PCI DMA mask, aborting!\n");
      goto err_alloc;
    }
  }

  /* Set consistent DMA-Mask */
  err = pci_set_consistent_dma_mask(pci_device, DMA_BIT_MASK(64) );
  if(err)
  {
    printk(DRIVER_NAME " : Warning: couldn't set consistent 64-bit PCI DMA mask.\n");
    err = pci_set_consistent_dma_mask(pci_device, DMA_BIT_MASK(32) );
    if(err)
    {
      printk(DRIVER_NAME " : Can't set consistent PCI DMA mask, aborting\n");
      goto err_alloc;
    }
  }

  /* First generate a new folder */
  kset  = kset_create_and_add("dma", NULL, &pci_device->dev.kobj);
  dma_kobj = &kset->kobj;
  dma_device->dma_kobj = dma_kobj;

  /* Add a sysfs entry for request_buffer, piping into the related file triggers an allocate and the generation of a new <buffer name> entry in sysfs */
  err = sysfs_create_bin_file(dma_kobj, attr_bin_request);
  if(err)
  {
    printk(DRIVER_NAME " : Can't create entry for buffer request!\n");
    goto err_alloc;
  }

  /* Add a sysfs entry for delete_buffer, piping <buffer name> into the related file triggers a deletion of bufferN entry in sysfs */
  err = sysfs_create_bin_file(dma_kobj, attr_bin_free);
  if(err)
  {
    printk(DRIVER_NAME " : Can't create entry for buffer freeing!\n");
    goto err_alloc;
  }

  /* Register device and set the driver specific data */
  if(uio_register_device(&pci_device->dev, &dma_device->info) )
  {
    goto err_alloc;
  }
  pci_set_drvdata(pci_device, dma_device);

  /* Print success feedback into dmesg */
  printk(DRIVER_NAME " : initialized new DMA device (%x %x)\n", pci_device->vendor, pci_device->device);

  UIO_DEBUG_PRINTF("__devinit EXIT\n");
  return 0;

/** Error Handling */
err_alloc:
  if(dma_device)
    kfree(dma_device);

  if(dma_kobj)
    kobject_del(dma_kobj);

  if(attr_bin_request)
    kfree(attr_bin_request);

  if(attr_bin_free)
    kfree(attr_bin_free);

err_verify:
  pci_disable_device(pci_device);
  UIO_DEBUG_PRINTF("__devinit EXIT\n");
  return err;
}


static void remove(struct pci_dev *pci_device)
{
  struct uio_pci_dma_device *dma_device = pci_get_drvdata(pci_device);
  struct kobject            *kobj       = dma_device->dma_kobj;
  struct kobject            *k          = NULL;

  /* Remove all allocated DMA buffers */
  struct kset *kset = to_kset(kobj);

  UIO_DEBUG_PRINTF("remove ENTER\n");

  spin_lock( &kset->list_lock );
  list_for_each_entry(k, &kset->list, entry)
      uio_pci_dma_free(k);

  spin_unlock( &kset->list_lock );

  kobject_del(dma_device->dma_kobj);
  kset_unregister(kset);

  /* Disable and remove device */
  uio_unregister_device(&dma_device->info);
  pci_disable_device(pci_device);
  kfree(dma_device);

  printk(DRIVER_NAME " : removed DMA device (%x %x)\n", pci_device->vendor, pci_device->device);

  UIO_DEBUG_PRINTF("remove EXIT\n");
}


static struct pci_driver driver =
{
  .name     = DRIVER_NAME,
  .id_table = NULL,
  .remove   = remove,
  .probe    = uio_probe,
};


static int __init mod_init(void)
{
  UIO_DEBUG_PRINTF("mod_init ENTER\n");

  printk(DRIVER_DESC " version: " UIO_PCI_DMA_VERSION "\n");

  UIO_DEBUG_PRINTF("mod_init EXIT\n");
  return pci_register_driver(&driver);
}



static void __exit mod_exit(void)
{
  UIO_DEBUG_PRINTF("mod_exit ENTER\n");

  pci_unregister_driver(&driver);

  UIO_DEBUG_PRINTF("mod_exit EXIT\n");
  printk("Unloaded " DRIVER_DESC " version: " UIO_PCI_DMA_VERSION "\n");
}



/** CALLBACKS */

/*! \brief uio_pci_dma_request_buffer_write
 *         Callback function that gets called when someone pipes a
 *         uio_pci_dma_private (see header file) into the "request"
 *         attribute of the DMA subsystem.
 */
BIN_ATTR_WRITE_CALLBACK(request_buffer_write)
{
  int                         err;
  struct kobj_type           *type = NULL;
  struct uio_pci_dma_private *priv = NULL;

  UIO_DEBUG_PRINTF("uio_pci_dma_request_buffer_write ENTER\n");

  struct uio_pci_dma_private *request = (struct uio_pci_dma_private*)buffer;
  struct kobject *device_kobj = kobj->parent;
  struct kset *kset = container_of(kobj, struct kset, kobj);

  /* Define and allocate the struct attr_bin_map */
  BIN_ATTR_PDA(map, request->size, S_IWUGO | S_IRUGO, NULL, NULL, uio_pci_dma_sysfs_map);

  /* Check that really a request struct is passed */
  if(count != sizeof(struct uio_pci_dma_private) )
  {
    printk(DRIVER_NAME " : invalid size of request struct -> aborting!\n");
    goto err_alloc;
  }

  UIO_DEBUG_PRINTF("MB = %zu are requested\n", (request->size/1048576) );

  /* Allocate and innitialize internal bookkeeping */
  priv = kzalloc(sizeof(struct uio_pci_dma_private), GFP_KERNEL);
  if(!priv)
  {
    printk(DRIVER_NAME " : Can't allocate memory -> aborting!\n");
    err = -ENOMEM;
    goto err_alloc;
  }
  memcpy(priv, buffer, count);

  priv->device = container_of(device_kobj, struct device, kobj);

  /* Setup related kernel object */
  type = kzalloc(sizeof(struct kobj_type), GFP_KERNEL);
  if(!type)
  {
    printk(DRIVER_NAME " : Can't allocate memory -> aborting!\n");
    err = -ENOMEM;
    goto err_alloc;
  }

  kobject_init(&priv->kobj, type);
  priv->kobj.kset = kset;
  err = kobject_add(&priv->kobj, NULL, request->name);
  if(err)
  {
    printk(DRIVER_NAME " : Can't create buffer object!\n");
    goto err_alloc;
  }

  /* First try to allocate the memory */
  priv->buffer = NULL;
  priv->sg     = NULL;
  priv->length = 0;
  if(uio_pci_dma_allocate_scatter_gather(priv) != 0)
    goto err_alloc;

  UIO_DEBUG_PRINTF("init priv pointer %p\n", priv);

  /* Define and allocate the struct attr_bin_sg */
  BIN_ATTR_PDA(sg, (priv->length * sizeof(struct scatter) ), S_IRUGO, NULL, NULL, uio_pci_dma_sysfs_map_sg);

  UIO_DEBUG_PRINTF("uio_pci_dma_request_buffer_write size %llu\n", (priv->length * sizeof(struct scatterlist) ) );

  /* Add entry for memory mapping of the DMA buffer */
  err = sysfs_create_bin_file(&priv->kobj, attr_bin_map);
  if(err)
  {
    printk(DRIVER_NAME " : Can't create entry for buffer mapping!\n");
    goto err_alloc_sg;
  }

  kobject_uevent(&priv->kobj, KOBJ_ADD);

  /* Add entry to expose the sg-list of the DMA buffer */
  err = sysfs_create_bin_file(&priv->kobj, attr_bin_sg);
  if(err)
  {
    printk(DRIVER_NAME " : Can't create entry for sg list exposing!\n");
    goto err_alloc_sg;
  }

  UIO_DEBUG_PRINTF("uio_pci_dma_request_buffer_write EXIT\n");
  return sizeof(struct uio_pci_dma_private);

err_alloc_sg:
  if(attr_bin_sg)
    kfree(attr_bin_sg);

err_alloc:
  if(attr_bin_map)
    kfree(attr_bin_map);

  UIO_DEBUG_PRINTF("uio_pci_dma_request_buffer_write EXIT\n");
  return 0;
}

static inline int uio_pci_dma_allocate_scatter_gather(struct uio_pci_dma_private *priv)
{
  uint64_t       pages                  = 0;
  uint64_t       pages_left             = 0;
  uint64_t       pages_nr               = 0;
  uint64_t       i                      = 0;
  uint8_t        order                  = 0;
  struct   page *page                   = NULL;
  size_t         chunk_length           = 0;
  size_t         size                   = priv->size;

  UIO_DEBUG_PRINTF("uio_pci_dma_allocate_scatter_gather ENTER\n");
  if( (priv->size) < 1)
  {
    printk(DRIVER_NAME " : Zero amount of pages requested -> aborting!\n");
    goto err_alloc;
  }

  /* Calculate the amount of pages needed */
  pages = (size/PAGE_SIZE);
  if( (size % PAGE_SIZE) != 0)
  {
    pages++;
    priv->size = pages * PAGE_SIZE;
    UIO_DEBUG_PRINTF("Adjusted page size\n");
  }

  /* Allocate and initialize scatter gather list */
  priv->sg = (struct scatterlist*)vmalloc_user(pages * sizeof(struct scatterlist) );
  if(!(priv->sg) )
  {
    printk(DRIVER_NAME " : Unable to allocate memory for scatter gather list -> aborting! pages = %llu\n", pages);
    goto err_alloc;
  }
  memset(priv->sg, 0, pages * sizeof(struct scatterlist));
  sg_init_table( (priv->sg), pages);

  /* Get the order for chunks which can be allocated in a consecutive buffer.
   * We allocate chunks with the size of MAX_ORDER, which is the maximum that
   * we can get in a consecutive buffer.
   */
  if( (order = get_order(size) ) > (MAX_ORDER - 1) )
    order = (MAX_ORDER - 1);

  UIO_DEBUG_PRINTF("Page order = %u (MAX_ORDER = %u) Pages to allocate = %llu\n", order, MAX_ORDER, pages);

  pages_left = pages;
  priv->length = 0;
  while(pages_left > 0)
  {
    page = alloc_pages( __GFP_HIGHMEM, order );
    if(!page)
    {
      if( (order--) < 0)
      {
        printk(DRIVER_NAME " : Failed to allocate DMA memory!\n");
        goto err_alloc;
      }
      UIO_DEBUG_PRINTF("Reducing page order = %u\n", order);

      continue;
    }

    /* Calculate sizes */
    pages_nr = (1 << order);
    chunk_length = pages_nr * PAGE_SIZE;

    /* Reserve allocated pages */
    for(i = 0; i < pages_nr; i++)
      SetPageReserved(&page[i]);
 
    /* Clear the allocated stuff */
    memset(page_address(page), 0, chunk_length);

    /* Put it into the scatter gather list */
    sg_set_page( &priv->sg[priv->length], page, chunk_length, 0);
    priv->length++;
    pages_left = pages_left - pages_nr;
  }

  /* Map all the stuff to the device!
   * This needs to be done first, because dma_map_sg also inserts bus adresses
   * into the list. Sorting according to these addresses can't be done other-
   * wise.
   */
  if(!dma_map_sg(priv->device, priv->sg, priv->length, priv->dma_direction) )
  {
    printk(DRIVER_NAME " : Mapping of scatter gather list failed, aborting!\n");
    priv->device = NULL;
    goto err_alloc;
  }

  UIO_DEBUG_SG( "Scatter:\n", (priv->sg), priv->length );

  /* Sort the scatter gather list according to the page adresses */
  sort(priv->sg, priv->length, sizeof(struct scatterlist), sort_sg_compare, sort_sg_swap);

  UIO_DEBUG_SG( "Sorted Scatter:\n", (priv->sg), priv->length );

  /* Allocate a private structure to secure mapping into the userspace */
  priv->scatter = (struct scatter*)vmalloc_user(pages * sizeof(struct scatter) );
  if(!(priv->scatter) )
  {
    printk(DRIVER_NAME " : Unable to allocate memory for scatter gather list interface -> aborting!\n");
    goto err_alloc;
  }

  for(i = 0; i < (priv->length); i++)
  {
    priv->scatter[i].page_link   = priv->sg[i].page_link;
    priv->scatter[i].offset      = priv->sg[i].offset;
    priv->scatter[i].length      = sg_dma_len( &(priv->sg[i]) );
    priv->scatter[i].dma_address = sg_dma_address( &(priv->sg[i]) );
  }

  printk(DRIVER_NAME " : %llu pages requested (in %llu entries), buffer %s is allocated\n", pages, priv->length, priv->name);

  UIO_DEBUG_PRINTF("uio_pci_dma_allocate_scatter_gather EXIT\n");
  return 0;

err_alloc:

  uio_pci_dma_free_scatter_gather(priv);

  UIO_DEBUG_PRINTF("uio_pci_dma_allocate_scatter_gather EXIT\n");
  return -1;
}


int sort_sg_compare(const void *item1, const void *item2)
{
  struct scatterlist *sg1 = (struct scatterlist*)item1;
  struct scatterlist *sg2 = (struct scatterlist*)item2;
  return( sg1->dma_address - sg2->dma_address );
}


void sort_sg_swap(void *item1, void *item2, int size)
{
  struct scatterlist sg_tmp;

  memcpy(&sg_tmp, item1, sizeof(struct scatterlist) );
  memcpy(item1, item2, sizeof(struct scatterlist) );
  memcpy(item2, &sg_tmp, sizeof(struct scatterlist) );
}



/*! \brief uio_pci_dma_delete_buffer_write
 *         Callback function that gets called when someone pipes a string
 *         into the "free" attribute of the DMA subsystem. The string should
 *         contain buffer name.
 */
BIN_ATTR_WRITE_CALLBACK( delete_buffer_write )
{
  struct kset         *kset_pointer = to_kset(kobj);
  char                *tmp_string   = kzalloc(count + 1, GFP_KERNEL);
  struct kobject      *kobj_tmp     = NULL;
  struct bin_attribute attrib;

  UIO_DEBUG_PRINTF("uio_pci_dma_delete_buffer_write ENTER\n");

  attrib.attr.name = "map";

  snprintf(tmp_string, count, "%s", buffer);
  KSET_FIND( kset_pointer, tmp_string, kobj_tmp );
  if(kobj_tmp != NULL)
  {
    uio_pci_dma_free(kobj_tmp);

    attrib.attr.name = "map";
    sysfs_remove_bin_file(kobj_tmp, &attrib);

    attrib.attr.name = "sg";
    sysfs_remove_bin_file(kobj_tmp, &attrib);

    kobject_del(kobj_tmp);
  }
  else
  {
    printk(DRIVER_NAME " : freeing of buffer %s failed!\n", tmp_string);
    goto err_free;
  }

err_free:
  kfree(tmp_string);

  UIO_DEBUG_PRINTF("uio_pci_dma_delete_buffer_write EXIT\n");
  return count;
}

static inline int uio_pci_dma_free(struct kobject *kobj)
{
  char                       *kobj_name = NULL;
  struct uio_pci_dma_private *priv_tmp  = NULL;

  if(!kobj)
    goto err_free;

  priv_tmp = container_of(kobj, struct uio_pci_dma_private, kobj);
  if(!priv_tmp)
    goto err_free;

  kobj_name = (char*)kzalloc( (strlen(kobject_name(kobj) ) + 1), GFP_KERNEL);
  strcpy(kobj_name, kobject_name(kobj) );
  if(!kobj_name)
  {
    UIO_DEBUG_PRINTF("Freeing failed!\n");
    goto err_free;
  }

  if(uio_pci_dma_free_scatter_gather(priv_tmp) != 0)
  {
    UIO_DEBUG_PRINTF("Freeing failed!\n");
    goto err_free;
  }

  printk(DRIVER_NAME " : freed buffer %s\n", kobj_name);
  kfree(kobj_name);
  UIO_DEBUG_PRINTF("uio_pci_dma_request_buffer_write EXIT\n");
  return 0;

err_free:
  printk(DRIVER_NAME " : freeing of buffer %s failed!\n", kobj_name);
  if(kobj_name)
    kfree(kobj_name);

  UIO_DEBUG_PRINTF("uio_pci_dma_request_buffer_write EXIT\n");
  return -1;
}



static inline int uio_pci_dma_free_scatter_gather(struct uio_pci_dma_private *priv)
{
  uint64_t              i                          = 0;
  struct   scatterlist *sglist        = priv->sg;
  uint64_t              length        = priv->length;
  struct   page        *page          = NULL;
  size_t                chunk_length  = 0;
  size_t                reserve       = 0;
  struct scatterlist   *sgp           = NULL;

  UIO_DEBUG_PRINTF("uio_pci_dma_free_scatter_gather ENTER\n");

  if(!sglist)
  {
    printk( DRIVER_NAME " : This is no valid scatter gather list!\n");
    return 0;
  }

  if(priv->device)
    dma_unmap_sg(priv->device, sglist, length, priv->dma_direction);

  /* Release and free each entry of the scatter gather list */
  for_each_sg( sglist, sgp, length, i)
  {
    chunk_length = sgp->length;

    page = sg_page(sgp);
    reserve = chunk_length + PAGE_SIZE;
    while(reserve -= PAGE_SIZE)
      ClearPageReserved(page++);

    __free_pages( sg_page(sgp), get_order(chunk_length) );
  }
  priv->length = 0;

  vfree(priv->sg);

  if(priv->scatter)
    vfree(priv->scatter);

  UIO_DEBUG_PRINTF("uio_pci_dma_free_scatter_gather EXIT\n");
  return 0;
}



/*! \brief uio_pci_dma_map
 *         Callback function that gets called when someone mmaps the related
 *         map attribute. The user process needs to map the whole file. The
 *         right size is the file size of the map attribute.
 */
BIN_ATTR_MAP_CALLBACK(map)
{
  UIO_DEBUG_PRINTF("uio_pci_dma_map ENTER\n");

  int                 ret           = 0;
  unsigned long       pfn           = 0;
  unsigned long       start         = vma->vm_start;
  uint64_t            request_size  = 0;
  size_t              mapped_size   = 0;
  size_t              buffer_size   = 0;
  uint64_t            sg_size       = 0;
  struct scatterlist *sgp           = NULL;
  struct page        *page          = NULL;

  struct uio_pci_dma_private *priv = container_of(kobj, struct uio_pci_dma_private, kobj);

  /** Return an error if someone tries to map a buffer with COW attribute */
  if( (vma->vm_flags & (VM_SHARED | VM_MAYWRITE) ) == VM_MAYWRITE)
  {
    printk(DRIVER_NAME " : You try to map COW memory. This can't be done with "   );
    printk(               "this buffer, because the map insertion is scattered. " );
    printk(               "Please try to alter your mmap flags from MAP_PRIVATE " );
    printk(               "to MAP_SHARED or something similar.\n");
    ret = -1;
    goto err_map;
  }

  /** Modulo map the buffer. */
  vma->vm_page_prot = pgprot_noncached(vma->vm_page_prot);
  request_size      = (vma->vm_end - vma->vm_start);
  buffer_size       = priv->size;
  sgp               = priv->sg;
  for(mapped_size = 0; mapped_size<request_size; )
  {
    pfn     =  page_to_pfn( sg_page(sgp) );
    sg_size =  sg_dma_len(sgp);
    page    =  sg_page(sgp);
    ret = io_remap_pfn_range(vma, start, pfn, sg_size, vma->vm_page_prot);
    if(ret < 0)
    {
      printk(DRIVER_NAME " : Buffer mapping failed!\n");
      goto err_map;
    }

    start        = start + sg_size;
    sgp          = sg_next(sgp);
    mapped_size +=sg_size;

    /** Wrap if needed */
    if( (mapped_size % buffer_size) == 0 )
      sgp = priv->sg;
  }

err_map:
  UIO_DEBUG_PRINTF("uio_pci_dma_map EXIT\n");
  return ret;
}



/*! \brief uio_pci_dma_map_sg
 *         Callback function that gets called when someone mmaps the related
 *         sg attribute to get the scatter gather list. The user process needs
 *         to map the whole file. The right size is the file size of the map
 *         attribute.
 */
BIN_ATTR_MAP_CALLBACK( map_sg )
{
  int ret = 0;

  struct uio_pci_dma_private *priv =  container_of(kobj, struct uio_pci_dma_private, kobj);

  UIO_DEBUG_PRINTF("uio_pci_dma_map_sg ENTER\n");

  vma->vm_private_data = priv;
  vma->vm_ops = NULL;

  /* Mapping ranges can only be alligned to page sizes. The test after
   * this one here would fail if we don't round to ceil.
   */
  size_t sg_size = (priv->length) * sizeof(struct scatter);
  if( (sg_size % PAGE_SIZE)  != 0)
    sg_size = ( (sg_size/PAGE_SIZE) + 1) * PAGE_SIZE;

  /* Check if someone tries to map the sg list with the wrong size */
  if( (vma->vm_end - vma->vm_start) != sg_size)
  {
      printk( DRIVER_NAME " : Requested mapping does not have the same size like the sg-list! (%lu != %llu)\n", (vma->vm_end - vma->vm_start), (priv->length * sizeof(struct scatter) ) );
      ret = -1;
      goto err_map;
  }

  ret = remap_vmalloc_range(vma, priv->scatter, 0);
  if(ret != 0)
      printk(DRIVER_NAME " : Unable to map sg-list!\n" );

err_map:
  UIO_DEBUG_PRINTF("uio_pci_dma_map_sg EXIT\n");
  return ret;
}

module_init(mod_init);
module_exit(mod_exit);

MODULE_VERSION(UIO_PCI_DMA_VERSION);
MODULE_LICENSE(DRIVER_LICENSE);
MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_DESCRIPTION(DRIVER_DESC);
