/************************************************************************/
/*									*/
/* File: filar_driver.c							*/
/*									*/
/* driver for the FILAR S-Link interface				*/
/*									*/
/* 25. Jun. 02  MAJO  created						*/
/*									*/
/************ C 2003 - The software with that certain something *********/

/************************************************************************/
/* NOTES:								*/
/* - This driver should work on kernels from 2.4 onwards		*/
/************************************************************************/

/************************************************************************/
/*Open issues:								*/
/*- so far the errors in the ISR only printk to var/log/message. One 	*/
/*  could use a global variable to remember the error and report it 	*/
/*  via the next ioctl() call.						*/
/*- The start and end control word patters passed via the INIT ioctl    */
/*  are not yet used in the ISR for consistency checking.		*/
/*- The FILAR does not return the actual control words via the ACK block*/
/*- The ioctl fuctions copy large arrays into the driver and back to	*/
/*  the user application. This is so because the amount of data in the	*/
/*  arrays is not known. If possible one should only copy the valid 	*/
/*  data. I don't know yet how to do this				*/ 
/************************************************************************/

#include <linux/init.h>           //MJ: for 2.6, p30
#include <linux/module.h>
#include <linux/moduleparam.h>    //MJ: for 2.6, p30
#include <linux/kernel.h>
#include <linux/stat.h>           //MJ: for 2.6, e.g. for module_param
#include <linux/pci.h>
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/mm.h>
#include <linux/cdev.h>           //e.g. for cdev_alloc
#include <linux/slab.h>           //e.g. for kmalloc
#include <linux/delay.h>          //e.g. for udelay
#include <linux/interrupt.h>      //e.g. for request_irq
#include <asm/io.h>
#include <asm/uaccess.h>
#include <asm/system.h>           //e.g. for local_irq_save
#include "ROSfilar/filar_driver.h"
#include "ROSRCDdrivers/tdaq_drivers.h"


/*********/
/*Globals*/
/*********/
static int debug = 0, errorlog = 1;
static u_int opid, scw, ecw, filar_irq_line[MAXCARDS];
static u_int maxcards, channels[MAXCARDS][MAXCHANNELS], served[MAXCARDS][MAXCHANNELS], pci_memaddr[MAXCARDS];
static u_char pci_revision[MAXCARDS];
static u_int irq_in[MAXCARDS], hwfifo[MAXROLS][MAXHWFIFO];
static u_int hwfifow[MAXROLS], hwfifor[MAXROLS], hwfifon[MAXROLS];
static u_int *outfifo, *outfifon;
static u_int *infifo, *infifon;
static u_int irqlist[MAXIRQ], infifor[MAXROLS], outfifow[MAXROLS];
static u_int order, ackadd[MAXCARDS], reqadd[MAXCARDS], *ackbuf[MAXCARDS], *reqbuf[MAXCARDS], swfifobasephys, swfifobase, outfifodatasize, infifodatasize, fifoptrsize, tsize = 0;
static char *proc_read_text;
static dev_t major_minor;
static struct cdev *filar_cdev;
static T_filar_card filarcard[MAXCARDS];

/************/
/*Prototypes*/
/************/
static int req_block(int card, int ackenable);
static int __devinit filarProbe(struct pci_dev *dev, const struct pci_device_id *id);
static int filar_init(void);
static void filar_cleanup(void);
static void filar_vmaOpen(struct vm_area_struct *vma);
static void filar_vmaClose(struct vm_area_struct *vma);
static void filarRemove(struct pci_dev *dev); 
static irqreturn_t filar_irq_handler (int irq, void *dev_id, struct pt_regs *regs); 

MODULE_DESCRIPTION("S-Link FILAR (DMA F/W)");
MODULE_AUTHOR("Markus Joos, CERN/PH");
MODULE_VERSION("2.0");
MODULE_LICENSE("Private: Contact markus.joos@cern.ch");
module_param (debug, int, S_IRUGO | S_IWUSR);  //MJ: for 2.6 p37
MODULE_PARM_DESC(debug, "1 = enable debugging   0 = disable debugging"); 
module_param (errorlog, int, S_IRUGO | S_IWUSR);  //MJ: for 2.6 p37
MODULE_PARM_DESC(errorlog, "1 = enable debugging   0 = disable debugging");

// memory handler functions
static struct vm_operations_struct filar_vm_ops = 
{
  .close = filar_vmaClose,   //mmap-close
  .open  = filar_vmaOpen,    //MJ: Note the comma at the end of the list!
};

// Standard file operations
static struct file_operations fops = 
{
  .owner          = THIS_MODULE,
  .unlocked_ioctl = filar_ioctl,
  .open           = filar_open,    
  .mmap           = filar_mmap,
  .release        = filar_release,
};

// PCI driver structures. See p311
static struct pci_device_id filarIds[] = 
{
  {PCI_DEVICE(PCI_VENDOR_ID_CERN, 0x14)},      
  {0,},
};

//PCI operations
static struct pci_driver filarPciDriver = 
{
  .name     = "filar_dma",
  .id_table = filarIds,
  .probe    = filarProbe,
  .remove   = filarRemove,
};


/*****************************/
/* Standard driver functions */
/*****************************/

/**************************/
static int filar_init(void)
/**************************/
{
  int rol, tfsize, result;
  u_int card, loop;
  struct page *page_ptr;
  struct filar_proc_data_t filar_proc_data;
  static struct proc_dir_entry *filar_file;

  kerror(("FILAR (SC) driver version 3.0\n")); 

  if (alloc_chrdev_region(&major_minor, 0, 1, "filar")) //MJ: for 2.6 p45
  {
    kerror(("filar(filar_init): failed to obtain device numbers\n"));
    result = -FILAR_EIO;
    goto fail1;
  }
  
  proc_read_text = (char *)kmalloc(MAX_PROC_TEXT_SIZE, GFP_KERNEL);
  if (proc_read_text == NULL)
  {
    kerror(("filar(filar_init): error from kmalloc\n"));
    result = -EFAULT;
    goto fail2;
  }

  filar_file = create_proc_entry("filar", 0644, NULL);
  if (filar_file == NULL)
  {
    kerror(("filar(filar_init): error from call to create_proc_entry\n"));
    result = -EFAULT;
    goto fail3;
  }  
  
  strcpy(filar_proc_data.name, "filar");
  strcpy(filar_proc_data.value, "filar");
  filar_file->data = &filar_proc_data;
  filar_file->read_proc = filar_read_procmem;
  filar_file->write_proc = filar_write_procmem;
  filar_file->owner = THIS_MODULE;  
  
  //Clear the list of used interupts 
  for(loop = 0; loop < MAXIRQ; loop++)
    irqlist[loop] = 0;

  kdebug(("filar(filar_init): registering PCIDriver\n"));
  result = pci_register_driver(&filarPciDriver);  //MJ: See P312
  if (result == 0)
  {
    kerror(("filar(filar_init): ERROR! no FILAR cards found!\n"));
    result = -EIO;
    goto fail4;
  }
  else 
  {
    kdebug(("filar(filar_init): Found %d FILAR cards!\n", result));
  }

  // Allocate contiguous memory for the FIFOs and store the base addresses in a global structure
  outfifodatasize = MAXROLS * MAXOUTFIFO * OUTFIFO_ELEMENTS * sizeof(u_int);    //outfifo
  infifodatasize  = MAXROLS * MAXINFIFO * INFIFO_ELEMENTS * sizeof(u_int);      //infifo
  fifoptrsize     = MAXROLS * sizeof(u_int);                                    //outfifo(w/r/n) + infifo(w/r/n)
  kdebug(("filar(filar_init): 0x%08x bytes needed for the OUT FIFO\n", outfifodatasize));
  kdebug(("filar(filar_init): 0x%08x bytes needed for the IN FIFO\n", infifodatasize));
  kdebug(("filar(filar_init): 0x%08x bytes needed for the FIFO pointers\n", fifoptrsize));
  
  tsize = outfifodatasize + infifodatasize + 2 * fifoptrsize;
  kdebug(("filar(filar_init): 0x%08x bytes needed for all FIFOs\n", tsize));

  tfsize = (tsize + 4095) & 0xfffff000;  // round up to next 4K boundary
  tfsize = tfsize >> 12;                 // compute number of pages
  kdebug(("filar(filar_init): %d pages needed for the S/W FIFOs\n", tfsize));
 
  order = 0;
  while(tfsize)
  {
    order++;
    tfsize = tfsize >> 1;                 // compute order
  } 
  kdebug(("filar(filar_init): order = %d\n", order));
  
  swfifobase = __get_free_pages(GFP_ATOMIC, order);
  if (!swfifobase)
  {
    kerror(("filar(filar_init): error from __get_free_pages\n"));
    result = -FILAR_EFAULT;
    goto fail5;
  }
  kdebug(("filar(filar_init): swfifobase = 0x%08x\n", swfifobase));
   
  // Reserve all pages to make them remapable
  page_ptr = virt_to_page(swfifobase);

  for (loop = (1 << order); loop > 0; loop--, page_ptr++)
    set_bit(PG_reserved, &page_ptr->flags);                   //MJ: have a look at the kernel book

  swfifobasephys = virt_to_bus((void *)swfifobase);
  kdebug(("filar(filar_init): swfifobasephys = 0x%08x\n", swfifobasephys));

  // Assign base addresses to the FIFO arrays
  outfifo  = (u_int *)swfifobase;
  infifo   = (u_int *)(swfifobase + outfifodatasize);
  outfifon = (u_int *)(swfifobase + outfifodatasize + infifodatasize);
  infifon  = (u_int *)(swfifobase + outfifodatasize + infifodatasize + fifoptrsize);

  kdebug(("filar(filar_init): outfifo  is at 0x%016lx\n", (u_long)outfifo));
  kdebug(("filar(filar_init): infifo   is at 0x%016lx\n", (u_long)infifo));
  kdebug(("filar(filar_init): outfifon is at 0x%016lx\n", (u_long)outfifon));
  kdebug(("filar(filar_init): infifon  is at 0x%016lx\n", (u_long)infifon));

  //Initialize the FIFOs
  for(rol = 0; rol < MAXROLS; rol++)
  {
    infifor[rol] = 0;
    infifon[rol] = 0;
    hwfifow[rol] = 0;
    hwfifor[rol] = 0;
    hwfifon[rol] = 0;
    outfifow[rol] = 0;
    outfifon[rol] = 0;
  }

  //Allocate the buffers for the REQ and ACK blocks
  //The max size of such a block is: 4 channels * (1 size word + 31 data words) = 128 words = 512 bytes
  for(card = 0; card < maxcards; card++)
  {
    reqbuf[card] = (u_int *)__get_free_page(GFP_ATOMIC);
    if (!reqbuf[card])
    {
      kerror(("filar(init_module): error from __get_free_pages for reqbuf\n"));
      return(-FILAR_EFAULT);
    }
    reqadd[card] = virt_to_bus((void *) reqbuf[card]);
    kdebug(("filar(init_module): reqbuf[%d] = 0x%08x (PCI = 0x%08x)\n", card, (u_int)reqbuf[card], reqadd[card]));
  
    ackbuf[card] = (u_int *)__get_free_page(GFP_ATOMIC);
    if (!ackbuf[card])
    {
      kerror(("filar(init_module): error from __get_free_pages for ackbuf\n"));
      return(-FILAR_EFAULT);
    }
    ackadd[card] = virt_to_bus((void *) ackbuf[card]);
    kdebug(("filar(init_module): ackbuf[%d] = 0x%08x (PCI = 0x%08x)\n", card, (u_int)ackbuf[card], ackadd[card]));
  }

  opid = 0;

  filar_cdev = (struct cdev *)cdev_alloc();       //MJ: for 2.6 p55
  filar_cdev->ops = &fops;
  result = cdev_add(filar_cdev, major_minor, 1);  //MJ: for 2.6 p56
  if (result)
  {
    kdebug(("filar(filar_init): error from call to cdev_add.\n"));
    goto fail6;
  }
  
  
  kdebug(("filar(filar_init): driver loaded; major device number = %d\n", MAJOR(major_minor)));
  return(0);  
  
  fail6:
    page_ptr = virt_to_page(swfifobase);
    for (loop = (1 << order); loop > 0; loop--, page_ptr++)
      clear_bit(PG_reserved, &page_ptr->flags);
    free_pages(swfifobase, order);

  fail5:
    pci_unregister_driver(&filarPciDriver);

  fail4:
    remove_proc_entry("io_rcc", NULL);
    
  fail3:
    kfree(proc_read_text);

  fail2:
    unregister_chrdev_region(major_minor, 1); //MJ: for 2.6 p45

  fail1:
    return(result);  
}


/******************************/
static void filar_cleanup(void)
/******************************/
{
  u_int card, loop;
  struct page *page_ptr;
      
  cdev_del(filar_cdev);                     //MJ: for 2.6 p56
  pci_unregister_driver(&filarPciDriver);
  page_ptr = virt_to_page(swfifobase);      // unreserve all pages used for the S/W FIFOs

  for (loop = (1 << order); loop > 0; loop--, page_ptr++)
    clear_bit(PG_reserved, &page_ptr->flags);

  free_pages(swfifobase, order);            // free the area 

  //Return the buffers for the REQ and ACK blocks
  for(card = 0; card < maxcards; card++)
  {
    free_page((int)reqbuf[card]);
    free_page((int)ackbuf[card]);
  }

  remove_proc_entry("filar", NULL);
  kfree(proc_read_text);
  unregister_chrdev_region(major_minor, 1); //MJ: for 2.6 p45

  kdebug(("filar(filar_cleanup): driver removed\n"));
}


module_init(filar_init);    //MJ: for 2.6 p16
module_exit(filar_cleanup); //MJ: for 2.6 p16


/**********************************************************/
static int filar_open(struct inode *ino, struct file *filep)
/**********************************************************/
{
  return(0);
}


/*************************************************************/
static int filar_release(struct inode *ino, struct file *filep)
/*************************************************************/
{
  opid = 0;
  return(0);
}


/**********************************************************************/
static long filar_ioctl(struct file *file, u_int cmd, unsigned long arg)
/**********************************************************************/
{
  switch (cmd)
  {    
    case OPEN:
    {
      FILAR_open_t params;
      
      kdebug(("filar(ioctl,OPEN): called from process %d\n", current->pid))
      if (!opid)
        opid = current->pid;
      else
      {
      	kerror(("filar(ioctl, OPEN): The FILAR is already used by process %d\n", opid));
  	return(-FILAR_USED);
      }
      	
      params.size = tsize;
      params.physbase = swfifobasephys;
      params.outsize = outfifodatasize;
      params.insize = infifodatasize;
      params.ptrsize = fifoptrsize;
            
      if (copy_to_user((void *)arg, &params, sizeof(FILAR_open_t)) != 0)
      {
	kerror(("filar(ioctl, OPEN): error from copy_to_user\n"));
	return(-FILAR_EFAULT);
      }      
      return(0);
      break;
    }
    
    case CLOSE:
    {
      kdebug(("filar(ioctl,CLOSE): called from process %d\n", current->pid))
      opid = 0;
  
      return(0);
      break;
    }
    case CONFIG:
    { 
      FILAR_config_t params;
      u_int card, channel, data, rol;
      
      kdebug(("filar(ioctl,CONFIG): \n"));
      if (copy_from_user(&params, (void *)arg, sizeof(FILAR_config_t)) !=0 )
      {
	kerror(("filar(ioctl,CONFIG): error from copy_from_user\n"));
	return(-FILAR_EFAULT);
      }
        
      scw = params.scw;
      ecw = params.ecw;
        
      kdebug(("filar(ioctl,CONFIG): params.bswap = %d\n", params.bswap));
      kdebug(("filar(ioctl,CONFIG): params.wswap = %d\n", params.wswap));
      kdebug(("filar(ioctl,CONFIG): params.psize = %d\n", params.psize));
              
      for(card = 0; card < maxcards; card++)
      {
        irq_in[card] = 0;
        data = 0;
        for(channel = 0; channel < MAXCHANNELS; channel++)
        { 
          served[card][channel] = 0;
          rol = (card << 2) + channel;  //works only for 4 channels per card
          kdebug(("filar(ioctl,CONFIG): params.enable[%d] = %d\n", rol, params.enable[rol]));
          if (!params.enable[rol])
          {
            data += (1 << (6 * channel + 9));
            channels[card][channel] = 0;
            kdebug(("filar(ioctl,CONFIG): channels[%d][%d] = %d\n", card, channel, channels[card][channel]));
          }
          else
          {
            channels[card][channel] = 1; 
            kdebug(("filar(ioctl,CONFIG): channels[%d][%d] = %d\n", card, channel, channels[card][channel]));
          }
          kdebug(("filar(ioctl,CONFIG): data = 0x%08x\n", data));
        }
        data += (params.bswap << 1) + (params.wswap << 2) + (params.psize << 3);
        kdebug(("filar(ioctl,CONFIG): Writing 0x%08x to the OCR\n", data));
        filarcard[card].regs->ocr = (data | 0x80);
        filarcard[card].regs->ackadd = ackadd[card];
        if (params.interrupt)
          filarcard[card].regs->imask |= 0x8;  //Enable the ACKBLK_DONE interrupt
      }
      break;
    }
    
    case RESET:
    {
      u_int channel, data, card, rol;
      
      if (copy_from_user(&card, (void *)arg, sizeof(int)) !=0 )
      {
	kerror(("filar(ioctl,RESET): error from copy_from_user\n"));
	return(-FILAR_EFAULT);
      }

      irq_in[card] = 0;
      for(channel = 0; channel < MAXCHANNELS; channel++)
        served[card][channel] = 0;
      
      if (card <= maxcards)
      {
        kdebug(("filar(ioctl,RESET): Resetting card %d\n", card));
        data = filarcard[card].regs->ocr;
        data |= 0x1;
        filarcard[card].regs->ocr = data;
        // Delay for at least one us
        udelay(10);
        data &= 0xfffffffe;
        filarcard[card].regs->ocr = data;

        //reset the FIFOs
        for(channel = 0; channel < MAXCHANNELS; channel++)
        {
          rol = (card << 2) + channel;
          infifor[rol] = 0;
          infifon[rol] = 0;
          outfifow[rol] = 0;
          outfifon[rol] = 0;
          hwfifow[rol] = 0;
          hwfifor[rol] = 0;
          hwfifon[rol] = 0;
        }
      }
      else
      {
       	kerror(("filar(ioctl,RESET): This card is unknown\n"));
	return(-FILAR_NOCARD); 
      }
      
      return(0);
      break;
    }
    
    case LINKRESET:
    {   
      u_int waited, rol, data, card, channel;
 
      if (copy_from_user(&rol, (void *)arg, sizeof(int)) !=0 )
      {
	kerror(("filar(ioctl,LINKRESET): error from copy_from_user\n"));
	return(-FILAR_EFAULT);
      }
      
      card = rol >> 2;      
      if (card <= maxcards)
      {
        channel = rol & 0x3;
        kdebug(("filar(ioctl,LINKRESET): Resetting card %d, channel %d\n", card, channel));
        served[card][channel] = 0;

        // Set the URESET bit
        data = 0x1 << (8 + channel * 6);
        filarcard[card].regs->ocr |= data;
        
        // Delay for at least 30 ms
        mdelay(30);

        // Now wait for LDOWN to come up again
        data = 0x1 << (16 + channel * 4);
        waited = 0;
        while(filarcard[card].regs->osr & data)
        {
          waited++;
          if (waited > 10000)
          {
            kerror(("filar(ioctl,LINKRESET): channel %d of card %d does not come up again\n", channel, card));
            // Reset the URESET bit
            data = ~(0x1 << (8 + channel * 6));
            filarcard[card].regs->ocr &= data;
	    return(-FILAR_STUCK);
          }          
        }

        // Reset the URESET bit
        data = ~(0x1 << (8 + channel * 6));
        filarcard[card].regs->ocr &= data;
      }
      else
      {
       	kerror(("filar(ioctl,LINKRESET): This card is unknown\n"));
	return(-FILAR_NOCARD); 
      }
      
      return(0);
      break;
    }
    
    case LINKSTATUS:
    {   
      u_int isup, data, card, rol, channel;
      
      if (copy_from_user(&rol, (void *)arg, sizeof(int)) !=0 )
      {
	kerror(("filar(ioctl, RESET): error from copy_from_user\n"));
	return(-FILAR_EFAULT);
      }
      
      card = rol >> 2;
      if (card <= maxcards)
      {
        channel = rol & 0x3;
        data = 0x1 << (16 + channel * 4);
        if (filarcard[card].regs->osr & data)
          isup = 0;
        else
          isup = 1;
          
        if (copy_to_user((void *)arg, &isup, sizeof(int)) != 0)
        {
	  kerror(("filar(ioctl, LINKSTATUS): error from copy_to_user\n"));
	  return(-FILAR_EFAULT);
        } 
      }
      else
      {
       	kerror(("filar(ioctl, LINKSTATUS): This card is unknown\n"));
	return(-FILAR_NOCARD); 
      }
      
      return(0);
      break;
    }

    case INFO:
    {
      FILAR_info_t info;
      u_int rol, card, data;
     
      //Initialise the array to 0
      for(rol = 0; rol < MAXROLS; rol++)
      {
        info.channels[rol] = 0;
        info.enabled[rol] = 0;
      }
      info.nchannels = 0;
      
      for(card = 0; card < maxcards; card++)
      {
        data = filarcard[card].regs->osr;
        kdebug(("filar(ioctl, INFO): 0x%08x read from OSR\n", data));
        if (data & 0x00080000)  info.channels[(card << 2)] = 0; 
        else {info.channels[(card << 2)] = 1; info.nchannels++;}
        
        if (data & 0x00800000)  info.channels[(card << 2) + 1] = 0; 
        else {info.channels[(card << 2) + 1] = 1; info.nchannels++;}
        
        if (data & 0x08000000)  info.channels[(card << 2) + 2] = 0; 
        else {info.channels[(card << 2) + 2] = 1; info.nchannels++;}
        
        if (data & 0x80000000)  info.channels[(card << 2) + 3] = 0; 
        else {info.channels[(card << 2) + 3] = 1; info.nchannels++;}
                
        data = filarcard[card].regs->ocr;
        kdebug(("filar(ioctl, INFO): 0x%08x read from OCR\n", data));
        
        if (data & 0x00000200) info.enabled[(card << 2)] = 0;     else info.enabled[(card << 2)] = 1;
        if (data & 0x00008000) info.enabled[(card << 2) + 1] = 0; else info.enabled[(card << 2) + 1] = 1;        
        if (data & 0x00200000) info.enabled[(card << 2) + 2] = 0; else info.enabled[(card << 2) + 2] = 1;        
        if (data & 0x08000000) info.enabled[(card << 2) + 3] = 0; else info.enabled[(card << 2) + 3] = 1;       
      }  

      if (copy_to_user((void *)arg, &info, sizeof(FILAR_info_t)) != 0)
      {
	kerror(("filar(ioctl, INFO): error from copy_to_user\n"));
	return(-FILAR_EFAULT);
      }      
      return(0);
      break;
    }
    
    case FIFOIN:
    { 
      u_int ret, card, channel, rol;
      unsigned long flags;

      if (copy_from_user(&rol, (void *)arg, sizeof(int)) !=0)
      {
	kerror(("filar(ioctl,FIFOIN): error from copy_from_user\n"));
	return(-FILAR_EFAULT);
      }
      card = rol >> 2;
      channel = rol & 0x3; 
      kdebug(("filar(ioctl,FIFOIN): channel = %d   card = %d\n", channel, card));

      //We have to make sure that this code does not get interrupted. Otherwhise there will be
      //inconsistencies in the REQ block which lead to incorrect PCI addresses being written to the FILAR
      save_flags(flags);
      cli();

      ret = req_block(card, 0);
      if (ret)
      {
        kerror(("filar(ioctl,FIFOIN): error from req_block\n"));
	return(-FILAR_EREQ);
      }

      restore_flags(flags);
      return(0);
      break;
    }
    
    case FLUSH:
    { 
      flush_cards();
      break;
    }
  }   
  return(0);
}


/****************************************************************************************************/
static int filar_write_procmem(struct file *file, const char *buffer, unsigned long count, void *data)
/****************************************************************************************************/
{
  struct filar_proc_data_t *fb_data = (struct filar_proc_data_t *)data;
  int len;
  u_int card;

  kdebug(("filar(filar_write_procmem): filar_write_procmem called\n"));

  if(count > 99)
    len = 99;
  else
    len = count;

  if (copy_from_user(fb_data->value, buffer, len)) 
  {
    kerror(("filar(filar_write_procmem): error from copy_from_user\n"));
    return(-FILAR_EFAULT);
  }
 
  kdebug(("filar(filar_write_procmem): len = %d\n", len)); 
  fb_data->value[len - 1] = '\0';
  kdebug(("filar(filar_write_procmem): text passed = %s\n", fb_data->value));
 
  if (!strcmp(fb_data->value, "debug"))
  {
    debug = 1;
    kdebug(("filar(filar_write_procmem): debugging enabled\n")); 
  }
    
  if (!strcmp(fb_data->value, "nodebug"))
  {
    kdebug(("filar(filar_write_procmem): debugging disabled\n")); 
    debug = 0;   
  }
  
  if (!strcmp(fb_data->value, "disable"))
  {
    for(card = 0; card < maxcards; card++)
      filarcard[card].regs->ocr |= 0x08208200;
    kdebug(("filar(filar_write_procmem): All channels disabled\n")); 
  }

  if (!strcmp(fb_data->value, "reset"))
  {
    reset_cards(1);
    kdebug(("filar(filar_write_procmem): All cards reset\n"));
  }
  
  if (!strcmp(fb_data->value, "elog"))
  {
    kdebug(("filar(filar_write_procmem): Error logging enabled\n"))
    errorlog = 1;
  }

  if (!strcmp(fb_data->value, "noelog"))
  {
    kdebug(("filar(filar_write_procmem): Error logging disabled\n"))
    errorlog = 0;
  }
  
  if (!strcmp(fb_data->value, "flush"))
  {
    flush_cards();
    kdebug(("filar(filar_write_procmem): All cards flushed\n"));
  }
  
  if (!strcmp(fb_data->value, "dec"))
  {
    kdebug(("filar(filar_write_procmem): Use count decremented\n")); 
  }
  
  if (!strcmp(fb_data->value, "inc"))
  {
    kdebug(("filar(filar_write_procmem): Use count incremented\n")); 
  }
  
  return len;
}


/***************************************************************************************************/
static int filar_read_procmem(char *buf, char **start, off_t offset, int count, int *eof, void *data)
/***************************************************************************************************/
{
  u_int fdata, rol, ret, card, ocr[MAXROLS], osr[MAXROLS], osr2[MAXROLS], imask[MAXROLS], efstatr[MAXROLS], efstata[MAXROLS], value, value2, channel;
  int loop, nchars = 0;
  static int len = 0;

  kdebug(("filar(filar_read_procmem): Called with buf    = 0x%08x\n", (u_int)buf));
  kdebug(("filar(filar_read_procmem): Called with *start = 0x%08x\n", (u_int)*start));
  kdebug(("filar(filar_read_procmem): Called with offset = %d\n", (u_int)offset));
  kdebug(("filar(filar_read_procmem): Called with count  = %d\n", count));

  if (offset == 0)
  {
    kdebug(("exp(exp_read_procmem): Creating text....\n"));
    len = 0;
  len += sprintf(proc_read_text + len, "\n\n\nFILAR driver (DMA protocol) for release %s (based on CVS tag %s)\n", RELEASE_NAME, CVSTAG);

  if  (opid)
    len += sprintf(proc_read_text + len, "The drivers is currently used by PID %d\n", opid);
    len += sprintf(proc_read_text + len, "=================================================================\n");
    len += sprintf(proc_read_text + len, "Card|IRQ| ACK IRQ|revision|PCI MEM addr.| page size|wswap|bswap|DMA|temperature|ACK address|REQ address|IRQs received\n");
    len += sprintf(proc_read_text + len, "----|---|--------|--------|-------------|----------|-----|-----|---|-----------|-----------|-----------|-------------\n");

    //Read the registers
    for(card = 0; card < maxcards; card++)
    {
      ocr[card]     = filarcard[card].regs->ocr;
      osr[card]     = filarcard[card].regs->osr;
      osr2[card]    = filarcard[card].regs->osr;
      imask[card]   = filarcard[card].regs->imask;
      efstatr[card] = filarcard[card].regs->efstatr;
      efstata[card] = filarcard[card].regs->efstata;
    }

    for(card = 0; card < maxcards; card++)
    {
      len += sprintf(proc_read_text + len, "   %d|", card);
      len += sprintf(proc_read_text + len, "%3d|", filar_irq_line[card]);
      len += sprintf(proc_read_text + len, "%s|", (imask[card] & 0x8)?" enabled":"disabled"); 
      len += sprintf(proc_read_text + len, "    0x%02x|", pci_revision[card]); 
      len += sprintf(proc_read_text + len, "   0x%08x|", pci_memaddr[card]);
      value = (ocr[card] >> 3) & 0x7;
      if (value == 0) len += sprintf(proc_read_text + len, " 256 bytes|");
      if (value == 1) len += sprintf(proc_read_text + len, "   1 Kbyte|");
      if (value == 2) len += sprintf(proc_read_text + len, "  2 Kbytes|");
      if (value == 3) len += sprintf(proc_read_text + len, "  4 Kbytes|");
      if (value == 4) len += sprintf(proc_read_text + len, " 16 Kbytes|");
      if (value == 5) len += sprintf(proc_read_text + len, " 64 Kbytes|");
      if (value == 6) len += sprintf(proc_read_text + len, "256 Kbytes|");
      if (value == 7) len += sprintf(proc_read_text + len, "4 MB - 8 B|");
      len += sprintf(proc_read_text + len, "  %s|", (ocr[card] & 0x4)?"yes":" no");
      len += sprintf(proc_read_text + len, "  %s|", (ocr[card] & 0x2)?"yes":" no");
      len += sprintf(proc_read_text + len, "%s|", (ocr[card] & 0x80)?"yes":" no");
      len += sprintf(proc_read_text + len, " %3d deg. C|", (osr[card] >>8) & 0xff);
      fdata = filarcard[card].regs->ackadd;
      len += sprintf(proc_read_text + len, " 0x%08x|", fdata);
      fdata = filarcard[card].regs->reqadd;
      len += sprintf(proc_read_text + len, " 0x%08x|", fdata);
      fdata = filarcard[card].regs->blkctl & 0xff;
      len += sprintf(proc_read_text + len, "%13d\n", irq_in[card]);
    }

    for(card = 0; card < maxcards; card++)
    {
      len += sprintf(proc_read_text + len, "\nCard %d:\n", card);
      len += sprintf(proc_read_text + len, "=======\n");
      len += sprintf(proc_read_text + len, "       |       |       |          |free      |     |        |       |          |free      |         |fragments\n");
      len += sprintf(proc_read_text + len, "       |       |       |entries in|entries in|     |        |       |entries in|entries in|fragments|per      \n");
      len += sprintf(proc_read_text + len, "Channel|present|enabled|ACK FIFO  |REQ FIFO  |LDOWN|Overflow|   XOFF|OUT FIFO  |IN FIFO   |served   |interrupt\n");
      len += sprintf(proc_read_text + len, "-------|-------|-------|----------|----------|-----|--------|-------|----------|----------|---------|---------\n");

      for(channel  = 0; channel < MAXCHANNELS; channel++)
      {
	len += sprintf(proc_read_text + len, "      %d|", channel);

	value = osr[card] & (1 << (19 + channel * 4));
	len += sprintf(proc_read_text + len, "    %s|", value?" no":"yes");

	value = ocr[card] & (1 << (9 + channel * 6));
	len += sprintf(proc_read_text + len, "    %s|", value?" no":"yes");

	value = (efstata[card] >> (channel * 8)) & 0xff;
	len += sprintf(proc_read_text + len, "        %2d|", value);

	value = (efstatr[card] >> (channel * 8)) & 0xff;
	len += sprintf(proc_read_text + len, "        %2d|", value);

	value = osr[card] & (1 << (16 + channel * 4));
	len += sprintf(proc_read_text + len, "  %s|", value?"yes":" no"); 

	value = osr[card] & (1 << (17 + channel * 4));
	len += sprintf(proc_read_text + len, "     %s|", value?"yes":" no");

	value = osr[card] & (1 << (18 + channel * 4));
	value2 = osr2[card] & (1 << (18 + channel * 4));
	if (!value && !value2)
          len += sprintf(proc_read_text + len, " is off|");
	else if (value && value2)
          len += sprintf(proc_read_text + len, "  is on|");
	else if (value && !value2)
          len += sprintf(proc_read_text + len, " was on|");
	else 
          len += sprintf(proc_read_text + len, "went on|");

	rol = (card << 2) + channel;
	ret = out_fifo_level(rol, &value);
	len += sprintf(proc_read_text + len, "      %4d|", MAXOUTFIFO - value);

	ret = in_fifo_level(rol, &value);
	len += sprintf(proc_read_text + len, "      %4d|", value);

	len += sprintf(proc_read_text + len, " %8d|", served[card][channel]);     

	if (irq_in[card]) 
          len += sprintf(proc_read_text + len, "%f\n", (float)served[card][channel] / (float)irq_in[card]);
	else
          len += sprintf(proc_read_text + len, "undefined\n");
      }
    }

    len += sprintf(proc_read_text + len, " \n");
    len += sprintf(proc_read_text + len, "The command 'echo <action> > /proc/filar', executed as root,\n");
    len += sprintf(proc_read_text + len, "allows you to interact with the driver. Possible actions are:\n");
    len += sprintf(proc_read_text + len, "debug   -> Enable debugging\n");
    len += sprintf(proc_read_text + len, "nodebug -> Disable debugging\n");
    len += sprintf(proc_read_text + len, "elog    -> Log errors to /var/log/message\n");
    len += sprintf(proc_read_text + len, "noelog  -> Do not log errors to /var/log/message\n");
    len += sprintf(proc_read_text + len, "disable -> Disable all FILAR channels\n");
    len += sprintf(proc_read_text + len, "reset   -> Reset all FILAR channels\n");
    len += sprintf(proc_read_text + len, "flush   -> Flush all FILAR channels\n");
    len += sprintf(proc_read_text + len, "dec     -> Decrement use count\n");
    len += sprintf(proc_read_text + len, "inc     -> Increment use count\n");
  }
  kdebug(("filar(filar_read_procmem): number of characters in text buffer = %d\n", len));

  if (count < (len - offset))
    nchars = count;
  else
    nchars = len - offset;
  kdebug(("filar(filar_read_procmem): min nchars         = %d\n", nchars));
  
  if (nchars > 0)
  {
    for (loop = 0; loop < nchars; loop++)
      buf[loop + (offset & (PAGE_SIZE - 1))] = proc_read_text[offset + loop];
    *start = buf + (offset & (PAGE_SIZE - 1));
  }
  else
  {
    nchars = 0;
    *eof = 1;
  }
 
  kdebug(("filar(filar_read_procmem): returning *start   = 0x%08x\n", (u_int)*start));
  kdebug(("filar(filar_read_procmem): returning nchars   = %d\n", nchars));
  return(nchars);
}




/*************************************************************************/
static irqreturn_t filar_irq_handler (int irq, void *dev_id, struct pt_regs *regs)
/*************************************************************************/
{
  u_int card;

  // Note: the FILAR card(s) may have to share an interrupt line with other PCI devices
  // It is therefore important to exit quickly if we are not concerned with an interrupt

  for(card = 0; card < maxcards; card++)
  {
    if (filar_irq_line[card] == (u_int)irq)
    {
      if (filarcard[card].regs->osr & 0x8)
      {
        irq_in[card]++;
        read_ack_and_write_out(card);
        return(IRQ_HANDLED);  //MJ: for 2.6 see p273
      }
    }
  }
  return(IRQ_NONE);     
}

/**********************************************************************************/
static int __devinit filarProbe(struct pci_dev *dev, const struct pci_device_id *id)   //MJ: for _devinit see p31
/**********************************************************************************/
{
  int i, card;
  u_long flags;
  u_int size;

  kdebug(("filar(filarProbe): called for device: 0x%04x / 0x%04x\n", dev->vendor, dev->device));
  kdebug(("filar(filarProbe): Bus: 0x%04x / Devfn 0x%04x\n", dev->bus->number, dev->devfn));

  /* Find a new "slot" for the card */
  card = -1;
  for (i = 0; i < MAXCARDS; i++) 
  {
    if (filarcard[i].pciDevice == NULL) 
    {
      card = i;
      filarcard[card].pciDevice = dev;
      break;
    }
  }

  if (card == -1) 
  {
    kerror(("filar(filarProbe): Could not find space in the filarcard array for this card."));
    return(-FILAR_TOOMANYCARDS);
  }
  
  flags = pci_resource_flags(dev, BAR0);  //MJ: See p317
  if ((flags & IORESOURCE_MEM) != 0) 
  {
    filarcard[card].pci_memaddr = pci_resource_start(dev, BAR0) & PCI_BASE_ADDRESS_MEM_MASK;        //MJ: See p317
    size = pci_resource_end(dev, BAR0) - pci_resource_start(dev, BAR0);                             //MJ: See p317
    kdebug(("filar(filarProbe): Mapping %d bytes at PCI address 0x%08x\n", size, filarcard[card].pci_memaddr));
    
    filarcard[card].regs = (T_filar_regs *)ioremap(filarcard[card].pci_memaddr, size);
    if (filarcard[card].regs == NULL)
    {
      kerror(("filar(filarProbe): error from ioremap for filarcard[%d].regs\n", card));
      filarcard[card].pciDevice = NULL;
      return(-FILAR_IOREMAP);
    }
    kdebug(("filar(filarProbe): filarcard[%d].regs is at 0x%016lx\n", card, (u_long)filarcard[card].regs));
  }
  else 
  {
    kerror(("filar(filarProbe): Bar 0 is not a memory resource"));
    return(-FILAR_EIO);
  }

  // get revision directly from the Filar
  pci_read_config_byte(filarcard[card].pciDevice, PCI_REVISION_ID, &filarcard[card].pci_revision);
  kdebug(("filar(filarProbe): revision = %x \n", filarcard[card].pci_revision));
  if (filarcard[card].pci_revision < MINREV)
  {
    kerror(("filar(filarProbe): Illegal Filar Revision\n"));
    return(-FILAR_ILLREV);
  }


  //Enable the device (See p314) and get the interrupt line
  pci_enable_device(dev);
  filarcard[card].irq_line = dev->irq;
  kdebug(("filar(filarProbe): interrupt line = %d \n", filarcard[card].irq_line));

  //Several FILAR cards may use the same interrupt but we want to install the driver only once
  if (irqlist[filarcard[card].irq_line] == 0) 
  {
    if(request_irq(filarcard[card].irq_line, filar_irq_handler, SA_SHIRQ, "filar", dev))	
    {
      kdebug(("filar(filarProbe): request_irq failed on IRQ = %d\n", filarcard[card].irq_line));
      return(-FILAR_REQIRQ);
    }
    irqlist[filarcard[card].irq_line]++;
    kdebug(("filar(filarProbe): Interrupt %d registered\n", filarcard[card].irq_line)); 
  }  
 
  maxcards++;
  return(0);
}


/******************************************/
static void filarRemove(struct pci_dev *dev) 
/******************************************/
{
  int i, card;

  /* Find the "slot" of the card */
  card = -1;
  for (i = 0; i < MAXCARDS; i++) 
  {
    if (filarcard[i].pciDevice == dev) 
    {
      card = i;
      filarcard[i].pciDevice = 0;
      break;
    }  
  }
  
  if (card == -1) 
  {
    kerror(("filar(filarRemove): Could not find device to remove."));
    return;
  }

  if (--irqlist[filarcard[card].irq_line] == 0)
  {
    kerror(("filar(filarRemove): removing handler for interrupt %d", filarcard[card].irq_line));
    free_irq(filarcard[card].irq_line, dev);	
  }
  
  iounmap((void *)filarcard[card].regs);
  kerror(("filar(filarRemove): Card %d removed", card));
  maxcards--;
}
//------------------
// Service functions
//------------------


/****************************************/
static int hw_fifo_push(int rol, int data)
/****************************************/
{
  if (hwfifon[rol] == MAXHWFIFO)
  {
    kerror(("filar(hw_fifo_push): The HW FIFO is full\n"));
    return(-1);
  }
  hwfifo[rol][hwfifow[rol]] = data;
  hwfifow[rol]++;
  if (hwfifow[rol] == MAXHWFIFO)
    hwfifow[rol] = 0;
  
  hwfifon[rol]++;
  return(0);
}


/****************************************/
static int hw_fifo_pop(int rol, int *data)
/****************************************/
{
  if (hwfifon[rol] == 0)
  {
    kerror(("filar(hw_fifo_pop): The HW FIFO is empty\n"));
    return(-1);
  }
  *data = hwfifo[rol][hwfifor[rol]];
  hwfifor[rol]++;
  if (hwfifor[rol] == MAXHWFIFO)
    hwfifor[rol] = 0;
  
  hwfifon[rol]--;
  return(0);
}


/****************************************/
static int in_fifo_pop(int rol, int *data)
/****************************************/
{
  if (infifon[rol] == 0)
  {
    kdebug(("filar(in_fifo_pop): The IN FIFO is empty\n"));
    return(-1);
  }
  *data = infifo[IN_INDEX(rol, infifor[rol])];

  infifor[rol]++;

  if (infifor[rol] > (MAXINFIFO - 1))
    infifor[rol] = 0;

  infifon[rol]--;

  return(0);
}


/*******************************************/
static int in_fifo_level(int rol, int *nfree)
/*******************************************/
{
  *nfree = MAXINFIFO - infifon[rol];
  return(0);
}


/******************************************************************************************/
static int out_fifo_push(int rol, int pciaddr, int fragsize, int fragstat, int scw, int ecw)
/******************************************************************************************/
{
  int oo;
  
  oo = OUT_INDEX(rol, outfifow[rol]);
  kdebug(("filar(out_fifo_push): oo = %d\n", oo));
  
  if (outfifon[rol] == MAXOUTFIFO)
  {
    kerror(("filar(out_fifo_push): The OUT FIFO is full\n"));
    return(-1);
  }
  
  outfifo[oo] = pciaddr;
  outfifo[oo + 1] = fragsize;
  outfifo[oo + 2] = fragstat;
  outfifo[oo + 3] = scw;
  outfifo[oo + 4] = ecw;

  outfifow[rol]++;
  if (outfifow[rol] == MAXOUTFIFO)
    outfifow[rol] = 0;
  
  outfifon[rol]++;
  return(0);
}


/********************************************/
static int out_fifo_level(int rol, int *nfree)
/********************************************/
{
  *nfree = MAXOUTFIFO - outfifon[rol];
  return(0);
}


/*******************************************/
static int req_block(int card, int ackenable)
/*******************************************/
{
  u_int rol, ret, reqlen, reqfifo, loop, nreqs, navail, nmin, channel, pciaddr, *reqptr;

  reqlen = 0;
  reqptr = reqbuf[card];
  reqfifo = filarcard[card].regs->efstatr;
  for(channel = 0; channel < MAXCHANNELS; channel++)
  {
    rol = (card << 2) + channel;
    nreqs = (reqfifo >> (8 * channel)) & 0xff;
    navail = infifon[rol]; 
    if (nreqs < navail)
      nmin = nreqs;
    else
      nmin = navail;

    kdebug(("filar(req_block): nreqs=%d  navail=%d\n", nreqs, navail));
    *reqptr++ = nmin;
    reqlen++;
    kdebug(("filar(req_block): Wrote nmin=%d to REQ block for channel=%d\n", nmin, channel));
    for(loop = 0; loop < nmin; loop++)
    {
      ret = in_fifo_pop(rol, &pciaddr);
      if (ret)
      {
        kerror(("filar(req_block): error from in_fifo_pop\n"));
        return(-1);
      }
 
      ret = hw_fifo_push(rol, pciaddr);
      if (ret)
      {
        kerror(("filar(req_block): error from hw_fifo_push\n"));
        return(-1);
      }
      *reqptr++ = pciaddr;
      reqlen++;
      kdebug(("filar(req_block): Wrote PCI address 0x%08x to REQ block\n", pciaddr));
    }
  }
  
  if (ackenable)
    filarcard[card].regs->blkctl = 0x60000000 | reqlen;
  else
    filarcard[card].regs->blkctl = 0x20000000 | reqlen;

  kdebug(("filar(req_block): Wrote reqlen=%d to FILAR\n", reqlen));
  filarcard[card].regs->reqadd = reqadd[card];
  kdebug(("filar(req_block): Wrote reqadd=0x%08x to FILAR card=%d\n", reqadd[card], card));
  
  //Wait until the card has read the REQ block
  loop = 0;
  while(1)
  {
    if (++loop == 1000) 
    {
      kerror(("filar(req_block) FILAR refuses to read REQ block\n"));
      return(-1);
    }
    if (filarcard[card].regs->osr & 0x4) break;
  }

  return(0);
}


/***************************************************/
static void filar_vmaOpen(struct vm_area_struct *vma)
/***************************************************/
{ 
  kdebug(("filar(filar_vmaOpen): Called\n"));
}


/****************************************************/
static void filar_vmaClose(struct vm_area_struct *vma)
/****************************************************/
{  
  kdebug(("filar(filar_vmaClose): Virtual address  = 0x%016lx\n",(u_long)vma->vm_start));
  kdebug(("filar(filar_vmaClose): mmap released\n"));
}


/******************************************************************/
static int filar_mmap(struct file *file, struct vm_area_struct *vma)
/******************************************************************/
{
  unsigned long offset, size;

  kdebug(("filar(filar_mmap): function called\n"));
  
  offset = vma->vm_pgoff << PAGE_SHIFT;
  
  size = vma->vm_end - vma->vm_start;

  kdebug(("filar(filar_mmap): offset = 0x%08x\n",(u_int)offset));
  kdebug(("filar(filar_mmap): size   = 0x%08x\n",(u_int)size));

  if (offset & ~PAGE_MASK)
  {
    kerror(("filar(filar_mmap): offset not aligned: %ld\n", offset));
    return -ENXIO;
  }

  // we only support shared mappings. "Copy on write" mappings are
  // rejected here. A shared mapping that is writeable must have the
  // shared flag set.
  if ((vma->vm_flags & VM_WRITE) && !(vma->vm_flags & VM_SHARED))
  {
    kerror(("filar(filar_mmap): writeable mappings must be shared, rejecting\n"));
    return(-EINVAL);
  }

  vma->vm_flags |= VM_RESERVED;
  
  // we do not want to have this area swapped out, lock it
  vma->vm_flags |= VM_LOCKED;

  // we create a mapping between the physical pages and the virtual
  // addresses of the application with remap_page_range.
  // enter pages into mapping of application
  kdebug(("filar(filar_mmap): Parameters of remap_page_range()\n"));
  kdebug(("filar(filar_mmap): Virtual address  = 0x%08x\n",(u_int)vma->vm_start));
  kdebug(("filar(filar_mmap): Physical address = 0x%08x\n",(u_int)offset));
  kdebug(("filar(filar_mmap): Size             = 0x%08x\n",(u_int)size));
  if (remap_page_range(vma, vma->vm_start, offset, size, vma->vm_page_prot))
  {
    kerror(("filar(filar_mmap): remap page range failed\n"));
    return -ENXIO;
  }
  
  vma->vm_ops = &filar_vm_ops;  
  
  kdebug(("filar(filar_mmap): function done\n"));
  return(0);
}


/******************************************/
static void read_ack_and_write_out(int card)
/******************************************/
{
  u_int rol, ret, data, scw = 0, ecw = 0, loop, nacks, channel, fragstat, pciaddr, *ackptr;

  //---> Part 1: process the ACK blocks
  ackptr = ackbuf[card];
  for(channel = 0; channel < MAXCHANNELS; channel++)
  {
    rol = (card << 2) + channel;
    nacks = *ackptr++;
    served[card][channel] += nacks;
    kdebug(("filar(read_ack_and_write_out): card=%d  channel=%d  nacks=%d\n", card, channel, nacks));
    for(loop = 0; loop < nacks; loop++)
    {
      data = *ackptr++;
      kdebug(("filar(read_ack_and_write_out): Analyzing 0x%08x\n", data));
      fragstat = 0;
      
      if (data & 0x50000000)
      {
        scw = *ackptr++;
        ecw = *ackptr++;
      }
  
      if (data & 0x80000000)
      { 
        fragstat |= NO_SCW; 
        kerror(("filar(read_ack_and_write_out): NO_SCW")); 
      }
      else 
      {
        if (data & 0x40000000)
        { 
          fragstat |= BAD_SCW; 
          kerror(("filar(read_ack_and_write_out): BAD_SCW")); 
          kerror(("filar(read_ack_and_write_out): Start Control Word = 0x%08x\n", scw));
          if (scw & 0x1)
          {
            kerror(("filar(read_ack_and_write_out): Data transmission error in SCW\n"));
            fragstat |= SCW_DTE;
          }
          if (scw & 0x2)
          {
            kerror(("filar(read_ack_and_write_out): Control word transmission error in SCW\n"));
            fragstat |= SCW_CTE;
          } 
        }
      }
  
      if (data & 0x20000000)
      { 
        fragstat |= NO_ECW; 
        kerror(("filar(read_ack_and_write_out): NO_ECW")); 
      }
      else
      {
        if (data & 0x10000000)
        { 
          fragstat |= BAD_ECW; 
          kerror(("filar(read_ack_and_write_out): BAD_ECW"));             
          kerror(("filar(read_ack_and_write_out): End Control Word = 0x%08x\n", ecw));

          if (ecw & 0x1)
          {
            kerror(("filar(read_ack_and_write_out): Data transmission error in ECW\n"));
            fragstat |= ECW_DTE;
          }

          if (ecw & 0x2)
          {
            kerror(("filar(read_ack_and_write_out): Control word transmission error in ECW\n"));
            fragstat |= ECW_CTE;
          } 
        }
      }
  
      ret = hw_fifo_pop(rol, &pciaddr);
      if (ret)
      {
        kerror(("filar(read_ack_and_write_out): error from hw_fifo_pop\n"));
        blow_fuse();
        return;
      }
      kdebug(("filar(read_ack_and_write_out): Read PCI address 0x%08x from HW FIFO\n", pciaddr));

      ret = out_fifo_push(rol, pciaddr, data & 0x000fffff, fragstat, 0 ,0);
      if (ret)
      {
        kerror(("filar(read_ack_and_write_out): error from out_fifo_push\n"));
        blow_fuse();
        return;
      }        
    }
  }
      
  //---> Part 2: write REQ blocks
  ret = req_block(card, 1);
  if (ret)
    kerror(("filar(read_ack_and_write_out): error from req_block\n"));

  kdebug(("filar(read_ack_and_write_out): End of ISR\n"));
}


/*************************/
static void blow_fuse(void)
/*************************/
{
  u_int card;

  for(card = 0; card < maxcards; card++)
    filarcard[card].regs->imask = 0;
  kerror(("filar(blow_fuse): fuse blown\n"));
}


/*******************************/
static void reset_cards(int mode)
/*******************************/
{
  u_int card, rol, channel, data;

  kdebug(("filar(reset_cards): maxcards = %d   mode = %d\n", maxcards, mode));
  for(card = 0; card < maxcards; card++)
  {
    data = filarcard[card].regs->ocr;
    data |= 0x1;
    filarcard[card].regs->ocr = data;
    // Delay for at least one us
    udelay(10);
    data &= 0xfffffffe;
    filarcard[card].regs->ocr = data;
    irq_in[card] = 0;
    //reset the FIFOs
    if (mode)
    {
      for(channel = 0; channel < MAXCHANNELS; channel++)
      {
        served[card][channel] = 0;
        rol = (card << 2) + channel;
        infifor[rol] = 0;
        infifon[rol] = 0;
        outfifow[rol] = 0;
        outfifon[rol] = 0;
        hwfifow[rol] = 0;
        hwfifor[rol] = 0;
        hwfifon[rol] = 0;
      }
    }
  }
  kdebug(("filar(reset_cards): Done\n"));
}


/***************************/
static void flush_cards(void)
/***************************/
{
  u_int loop, card, channel;
  unsigned long flags;

  //We have to make sure that this code does not get interrupted.
  //Otherwhise there can be a race condition.
  save_flags(flags);
  cli();

  for(card = 0; card < maxcards; card++)
  {
    filarcard[card].regs->blkctl = 0x80000000;
    loop = 0;
    //MJ: should I sleep a bit to wait for ACKBLK_DONE = 0 ?
    while(1)
    {
      if (++loop == 1000)
      {        
	kerror(("filar(flush_cards): FILAR refuses to send ACK block\n"));
	return;
      }
      if (filarcard[card].regs->osr & 0x8)  break;
    }
    read_ack_and_write_out(card);
  }

  for(channel = 0; channel < MAXCHANNELS; channel++)
  {
    kdebug(("filar(flush_cards): There are %d events in the OUT FIFO of channel %d\n", outfifon[channel], channel));
  }
  
  restore_flags(flags);
}
      
