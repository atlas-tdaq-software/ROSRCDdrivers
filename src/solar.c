/************************************************************************/
/*									*/
/* File: solar_driver.c							*/
/*									*/
/* driver for the SOLAR S-Link interface				*/
/*									*/
/* 2. Apr. 03  MAJO  created						*/
/*									*/
/************ C 2005 - The software with that certain something *********/

/************************************************************************/
/*NOTES:								*/
/*- This driver should work on kernels from 2.6.9 onwards		*/
/************************************************************************/


#include <linux/init.h>           //MJ: for 2.6, p30
#include <linux/module.h>         //p30
#include <linux/moduleparam.h>    //MJ: for 2.6, p30
#include <linux/kernel.h>         //e.g. for printk
#include <linux/stat.h>           //MJ: for 2.6, e.g. for module_param
#include <linux/pci.h>            //e.g. for   pci_find_device
#include <linux/errno.h>          //e.g. for EFAULT
#include <linux/fs.h>             //e.g. for register_chrdev
#include <linux/proc_fs.h>        //e.g. for create_proc_entry
#include <linux/mm.h>             //e.g. for vm_operations_struct
#include <linux/slab.h>           //e.g. for kmalloc
#include <linux/cdev.h>           //e.g. for cdev_alloc
#include <linux/delay.h>
#include <linux/version.h>        
#include <linux/interrupt.h>      //e.g. for request_irq
#include <asm/uaccess.h>          //e.g. for copy_from_user
#include <asm/io.h>               //e.g. for inl
#include <asm/system.h>           //e.g. for wmb
#include "ROSsolar/solar_driver.h"
#include "ROSRCDdrivers/tdaq_drivers.h"

#ifndef PCI_VENDOR_ID_CERN
  #define PCI_VENDOR_ID_CERN 0x10dc
#endif

/*********/
/*Globals*/
/*********/
static int debug = 0, errorlog = 1;
static char *proc_read_text;
static u_int irqlist[MAXIRQ], infifor[MAXCARDS], served[MAXCARDS];
static u_int maxcards, *infifo, *infifon, order, swfifobasephys, infifodatasize, fifoptrsize, tsize = 0;
static u_long swfifobase;
static struct cdev *solar_cdev;
static dev_t major_minor;
static T_solar_card solarcard[MAXCARDS];

/************/
/*Prototypes*/
/************/
static int __devinit solarProbe(struct pci_dev *dev, const struct pci_device_id *id);
static void solar_cleanup(void);
static void solar_vmaOpen(struct vm_area_struct *vma);
static void solar_vmaClose(struct vm_area_struct *vma);
static void solarRemove(struct pci_dev *dev); 
static int solar_init(void);
static irqreturn_t solar_irq_handler (int irq, void *dev_id, struct pt_regs *regs);

/***************************************************************/
/* Use /sbin/modinfo <module name> to extract this information */
/***************************************************************/
MODULE_DESCRIPTION("S-Link SOLAR");
MODULE_AUTHOR("Markus Joos, CERN/PH");
MODULE_VERSION("2.0");
MODULE_LICENSE("Private: Contact markus.joos@cern.ch");
module_param (debug, int, S_IRUGO | S_IWUSR);  //MJ: for 2.6 p37
MODULE_PARM_DESC(debug, "1 = enable debugging   0 = disable debugging"); 
module_param (errorlog, int, S_IRUGO | S_IWUSR);  //MJ: for 2.6 p37
MODULE_PARM_DESC(errorlog, "1 = enable error logging   0 = disable error logging");


//MJ: Not actually required. Just for kdebug
struct vm_operations_struct solar_vm_ops =
{       
  .close = solar_vmaClose,      
  .open  = solar_vmaOpen,      //MJ: Note the comma at the end of the list!
};


static struct file_operations fops = 
{
  .owner   = THIS_MODULE,
  .ioctl   = solar_ioctl,
  .open    = solar_open,    
  .mmap    = solar_mmap,
  .release = solar_release,
};

// PCI driver structures. See p311
static struct pci_device_id solarIds[] = 
{
  {PCI_DEVICE(PCI_VENDOR_ID_CERN, SOLAR_PCI_DEVICE_ID)},      
  {0,},
};

//PCI operations
static struct pci_driver solarPciDriver = 
{
  .name     = "solar",
  .id_table = solarIds,
  .probe    = solarProbe,
  .remove   = solarRemove,
};



/*****************************/
/* Standard driver functions */
/*****************************/

/*************************/
static int solar_init(void)
/*************************/
{
  int card, tfsize, result;
  u_int loop;
  struct page *page_ptr;
  struct solar_proc_data_t solar_proc_data;
  static struct proc_dir_entry *solar_file;

  if (alloc_chrdev_region(&major_minor, 0, 1, "solar")) //MJ: for 2.6 p45
  {
    kerror(("solar(solar_init): failed to obtain device numbers\n"));
    result = -EIO;
    goto fail1;
  }

  proc_read_text = (char *)kmalloc(MAX_PROC_TEXT_SIZE, GFP_KERNEL);
  if (proc_read_text == NULL)
  {
    kerror(("solar(solar_init): error from kmalloc\n"));
    result = -EFAULT;
    goto fail2;
  }

  solar_file = create_proc_entry("solar", 0644, NULL);
  if (solar_file == NULL)
  {
    kerror(("solar(solar_init): error from call to create_proc_entry\n"));
    result = -EFAULT;
    goto fail3;
  }

  strcpy(solar_proc_data.name, "solar");
  strcpy(solar_proc_data.value, "solar");
  solar_file->data = &solar_proc_data;
  solar_file->read_proc = solar_read_procmem;
  solar_file->write_proc = solar_write_procmem;
  solar_file->owner = THIS_MODULE;    
  
  kdebug(("solar(solar_init): registering PCIDriver\n"));
  result = pci_register_driver(&solarPciDriver);  //MJ: See P312
  if (result < 0)
  {
    kerror(("solar(solar_init): ERROR! no SOLAR cards found!\n"));
    result = -EIO;
    goto fail4;
  }
  else 
  {
    kdebug(("solar(solar_init): Found some SOLAR cards!\n"));
  }
    
  // Allocate contiguous memory for the FIFOs and store the base addresses in a global structure
  infifodatasize = MAXCARDS * MAXINFIFO * sizeof(u_int) * 2;  //infifo
  fifoptrsize = MAXCARDS * sizeof(u_int);                     //infifo(w/r/n)
  kdebug(("solar(solar_init): 0x%08x bytes needed for the IN FIFO\n", infifodatasize));
  kdebug(("solar(solar_init): 0x%08x bytes needed for the FIFO pointers\n", fifoptrsize));
  
  tsize = infifodatasize + fifoptrsize;
  kdebug(("solar(solar_init): 0x%08x bytes needed for all FIFOs\n", tsize));

  tfsize = (tsize + 4095) & 0xfffff000;  // round up to next 4K boundary
  tfsize = tfsize >> 12;                 // compute number of pages
  kdebug(("solar(solar_init): %d pages needed for the S/W FIFOs\n", tfsize));
 
  order = 0;
  while(tfsize)
  {
    order++;
    tfsize = tfsize >> 1;                 // compute order
  } 
  kdebug(("solar(solar_init): order = %d\n", order));
  
  swfifobase = __get_free_pages(GFP_ATOMIC, order);
  if (!swfifobase)
  {
    kerror(("rsolar(solar_init): error from __get_free_pages\n"));
    result = -EFAULT;
    goto fail5;
  }
  kdebug(("solar(solar_init): swfifobase = 0x%016lx\n", swfifobase));
   
  // Reserve all pages to make them remapable
  page_ptr = virt_to_page(swfifobase);
  for (loop = (1 << order); loop > 0; loop--, page_ptr++)
    set_bit(PG_reserved, &page_ptr->flags);

  swfifobasephys = virt_to_bus((void *) swfifobase);
  kdebug(("solar(solar_init): swfifobasephys = 0x%08x\n", swfifobasephys));

  // Assign base addresses to the FIFO arrays
  infifo  = (u_int *)swfifobase;
  infifon = (u_int *)(swfifobase + infifodatasize);
  kdebug(("solar(solar_init): infifo   is at 0x%016lx\n", (u_long)infifo));
  kdebug(("solar(solar_init): infifon  is at 0x%016lx\n", (u_long)infifon));

  //Initialize the FIFOs
  for(card = 0; card < MAXCARDS; card++)
  {
    infifor[card] = 0;
    infifon[card] = 0;
  }

  solar_cdev = (struct cdev *)cdev_alloc();       //MJ: for 2.6 p55
  solar_cdev->ops = &fops;
  result = cdev_add(solar_cdev, major_minor, 1);  //MJ: for 2.6 p56
  if (result)
  {
    kerror(("solar(solar_init): error from call to cdev_add.\n"));
    goto fail6;
  }

  kdebug(("solar(solar_init): driver loaded; major device number = %d\n", MAJOR(major_minor)));
  return(0);

  fail6:
    page_ptr = virt_to_page(swfifobase);
    for (loop = (1 << order); loop > 0; loop--, page_ptr++)
      clear_bit(PG_reserved, &page_ptr->flags);
    free_pages(swfifobase, order);
     
  fail5:
    pci_unregister_driver(&solarPciDriver);

  fail4:
    remove_proc_entry("io_rcc", NULL);
    
  fail3:
    kfree(proc_read_text);

  fail2:
    unregister_chrdev_region(major_minor, 1); //MJ: for 2.6 p45

  fail1:
    return(result);    
}


/*****************************/
static void solar_cleanup(void)
/*****************************/
{
  u_int loop;
  struct page *page_ptr;
      
  cdev_del(solar_cdev);                     //MJ: for 2.6 p56
  pci_unregister_driver(&solarPciDriver);

  // unreserve all pages used for the S/W FIFOs
  page_ptr = virt_to_page(swfifobase);
  for (loop = (1 << order); loop > 0; loop--, page_ptr++)
    clear_bit(PG_reserved, &page_ptr->flags);

  // free the area 
  free_pages(swfifobase, order);
  remove_proc_entry("solar", NULL);
  kfree(proc_read_text);
  unregister_chrdev_region(major_minor, 1); //MJ: for 2.6 p45

  kdebug(("solar(solar_cleanup): driver removed\n"));
}


module_init(solar_init);    //MJ: for 2.6 p16
module_exit(solar_cleanup); //MJ: for 2.6 p16


/**********************************************************/
static int solar_open(struct inode *ino, struct file *filep)
/**********************************************************/
{
  kdebug(("solar(solar_open): nothing to be done\n"));
  return(0);
}


/*************************************************************/
static int solar_release(struct inode *ino, struct file *filep)
/*************************************************************/
{
  kdebug(("solar(solar_release): nothing to be done\n"));
  return(0);
}

/***********************************************************************************/
static int solar_ioctl(struct inode *inode, struct file *file, u_int cmd, u_long arg)
/***********************************************************************************/
{
  switch (cmd)
  {    
    case OPEN:
    {
      SOLAR_open_t params;
      
      params.size = tsize;
      params.physbase = swfifobasephys;
      params.insize = infifodatasize;
      kdebug(("solar(ioctl,OPEN): params.size     = 0x%08x\n", params.size));
      kdebug(("solar(ioctl,OPEN): params.physbase = 0x%08x\n", params.physbase));
      kdebug(("solar(ioctl,OPEN): params.insize   = 0x%08x\n", params.insize));
  
      if (copy_to_user((void *)arg, &params, sizeof(SOLAR_open_t)))
      {
	kerror(("solar(ioctl, OPEN): error from copy_to_user\n"));
	return(-SOLAR_EFAULT);
      }      
      return(0);
      break;
    }

    case CONFIG:
    { 
      SOLAR_config_t params;
      u_int card, data;
      
      if (copy_from_user(&params, (void *)arg, sizeof(SOLAR_config_t)))
      {
	kerror(("solar(ioctl,CONFIG): error from copy_from_user\n"));
	return(-SOLAR_EFAULT);
      }
      kdebug(("solar(ioctl,CONFIG): \n"));        
      kdebug(("solar(ioctl,CONFIG): params.bswap  = %d\n", params.bswap));
      kdebug(("solar(ioctl,CONFIG): params.wswap  = %d\n", params.wswap));
      kdebug(("solar(ioctl,CONFIG): params.scw    = 0x%08x\n", params.scw));
      kdebug(("solar(ioctl,CONFIG): params.ecw    = 0x%08x\n", params.ecw));
      kdebug(("solar(ioctl,CONFIG): params.udw    = %d\n", params.udw));
      kdebug(("solar(ioctl,CONFIG): params.clksel = %d\n", params.clksel));
      kdebug(("solar(ioctl,CONFIG): params.clkdiv = %d\n", params.clkdiv));

      for(card = 0; card < maxcards; card++)
      {
        data = 0;  
        solarcard[card].regs->opctrl = (params.udw << 18) + (params.wswap << 2) + (params.bswap << 1);
        solarcard[card].regs->bctrlw = params.scw & 0xfffffffc;
        solarcard[card].regs->ectrlw = params.ecw & 0xfffffffc;
        solarcard[card].regs->opfeat = (params.clksel << 31) + (params.clkdiv << 29);
        
        //The LFF and LDOWN interrupts are not yet supported
        //If enabled the REQ interrupt has a fixed threshold of 32
        //This is to reduce the IRQ frequency
        solarcard[card].regs->intmask = INTERRUPT_THRESHOLD;
        served[card] = 0;
      }
      break;
    }
    
    case RESET:
    {
      u_int data, card;
      
      if (copy_from_user(&card, (void *)arg, sizeof(int)))
      {
	kerror(("solar(ioctl,RESET): error from copy_from_user\n"));
	return(-SOLAR_EFAULT);
      }
     
      kdebug(("solar(ioctl,RESET): Resetting card %d\n", card));
      data = solarcard[card].regs->opctrl;
      data |= 0x1;
      solarcard[card].regs->opctrl = data;
      udelay(2);  // Delay for at least one us
      data &= 0xfffffffe;
      solarcard[card].regs->opctrl = data;

      //reset the FIFOs
      infifor[card] = 0;
      infifon[card] = 0;

      return(0);
      break;
    }
    
    case LINKRESET:
    {   
      u_int waited = 0, card;
 
      if (copy_from_user(&card, (void *)arg, sizeof(int)))
      {
	kerror(("solar(ioctl,LINKRESET): error from copy_from_user\n"));
	return(-SOLAR_EFAULT);
      }
      
      kdebug(("solar(ioctl,LINKRESET): Resetting link of card %d\n", card));
      
      solarcard[card].regs->opctrl |= 0x00020000;        // Set the URESET bit
      udelay(2000);                             // Delay for at least two ms
      while(solarcard[card].regs->opstat & 0x00040000)   // Now wait for LDOWN to come up again
      {
        waited++;
        if (waited > 10000)
        {
          kerror(("solar(ioctl,LINKRESET): card %d does not come up again\n", card));
          solarcard[card].regs->opctrl &= 0xfffdffff;    // Reset the URESET bit
	  return(-SOLAR_STUCK);
        }          
      }

      // Reset the URESET bit
      solarcard[card].regs->opctrl &= 0xfffdffff;
      served[card] = 0;
      return(0);
      break;
    }
    
    case LINKSTATUS:
    {              
      u_long flags;
      SOLAR_status_t params;
      
      if (copy_from_user(&params, (void *)arg, sizeof(SOLAR_status_t)))
      {
	kerror(("solar(ioctl, LINKSTATUS): error from copy_from_user\n"));
	return(-SOLAR_EFAULT);
      }
      
      if (solarcard[params.card].regs->opstat & 0x00040000)
        params.ldown = 1;
      else
        params.ldown = 0;
      
      //We have to make sure that this code does not get interrupted. 
      local_irq_save(flags);  //MJ: for 2.6 see p274 & p275
      params.infifo = infifon[params.card];
      params.reqfifo = REQFIFODEPTH - ((solarcard[params.card].regs->opstat) & 0x3f); 
      kdebug(("solar(ioctl, LINKSTATUS): params.ldown = %d\n", params.ldown));
      kdebug(("solar(ioctl, LINKSTATUS): params.infifo = %d\n", params.infifo));
      kdebug(("solar(ioctl, LINKSTATUS): params.reqfifo = %d\n", params.reqfifo));
      local_irq_restore(flags);  //MJ: for 2.6 see p274 & p275

      if (copy_to_user((void *)arg, &params, sizeof(SOLAR_status_t)))
      {
	kerror(("solar(ioctl, LINKSTATUS): error from copy_to_user\n"));
	return(-SOLAR_EFAULT);
      } 
      
      return(0);
      break;
    }

    case INFO:
    {
      SOLAR_info_t info;
      u_int card;
     
      //Initialise the array to 1 (it is not possible to disable a SOLAR card via a register)
      for(card = 0; card < MAXCARDS; card++)
      {
        info.cards[card] = 0;
        info.enabled[card] = 0;
      }
      
      for(card = 0; card < maxcards; card++)
      {
        info.cards[card] = 1;
        info.enabled[card] = 1;
      }
      
      info.ncards = maxcards;  
      kdebug(("solar(ioctl, INFO): maxcards = %d\n", maxcards));

      if (copy_to_user((void *)arg, &info, sizeof(SOLAR_info_t)))
      {
	kerror(("solar(ioctl, INFO): error from copy_to_user\n"));
	return(-SOLAR_EFAULT);
      }      
      return(0);
      break;
    }
    
    case FIFOIN:
    { 
      u_int card;

      if (copy_from_user(&card, (void *)arg, sizeof(int)) !=0)
      {
	kerror(("solar(ioctl,FIFOIN): error from copy_from_user\n"));
	return(-SOLAR_EFAULT);
      }
      kdebug(("solar(ioctl,FIFOIN): card = %d\n", card));

      // (re)enable the interupt. Once the REQ FIFO is empty the ISR will fill it with the new requests
      solarcard[card].regs->intmask = INTERRUPT_THRESHOLD;

      return(0);
      break;
    }
  }   
  return(0);
}


/***************************************************/
static void solar_vmaOpen(struct vm_area_struct *vma)
/***************************************************/
{
  kdebug(("solar(solar_vmaOpen): Called\n"));
}


/****************************************************/
static void solar_vmaClose(struct vm_area_struct *vma)
/****************************************************/
{  
  kdebug(("solar(solar_vmaClose): Virtual address  = 0x%016lx\n",(u_long)vma->vm_start));
  kdebug(("solar(solar_vmaClose): mmap released\n"));
}

/******************************************************************/
static int solar_mmap(struct file *file, struct vm_area_struct *vma)
/******************************************************************/
{
  u_long offset, size;

  kdebug(("solar(solar_mmap): function called\n"));
  vma->vm_flags |= VM_RESERVED;
  vma->vm_flags |= VM_LOCKED;

  kdebug(("solar(solar_mmap): vma->vm_end    = 0x%016lx\n", (u_long)vma->vm_end));
  kdebug(("solar(solar_mmap): vma->vm_start  = 0x%016lx\n", (u_long)vma->vm_start));
  kdebug(("solar(solar_mmap): vma->vm_offset = 0x%016lx\n", (u_long)vma->vm_pgoff << PAGE_SHIFT));
  kdebug(("solar(solar_mmap): vma->vm_flags  = 0x%08x\n", (u_int)vma->vm_flags));
  
  size = vma->vm_end - vma->vm_start;
  offset = vma->vm_pgoff << PAGE_SHIFT;  

  kdebug(("solar(solar_mmap): offset = 0x%08x\n",(u_int)offset));
  kdebug(("solar(solar_mmap): size   = 0x%08x\n",(u_int)size));

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,18)
  if (remap_page_range(vma, vma->vm_start, offset, size, vma->vm_page_prot))
#else
  if (remap_pfn_range(vma, vma->vm_start, vma->vm_pgoff, size, vma->vm_page_prot))
#endif
  {
    kerror(("solar(solar_mmap): remap page range failed\n"));
    return -ENXIO;
  }
  
  vma->vm_ops = &solar_vm_ops;  
  kdebug(("solar(solar_mmap): function done\n"));
  return(0);
}


/*********************************************************************************************/
static int solar_write_procmem(struct file *file, const char *buffer, u_long count, void *data)
/*********************************************************************************************/
{
  int len;
  struct solar_proc_data_t *fb_data = (struct solar_proc_data_t *)data;

  kdebug(("solar(solar_write_procmem): solar_write_procmem called\n"));

  if(count > 99)
    len = 99;
  else
    len = count;

  if (copy_from_user(fb_data->value, buffer, len))
  {
    kerror(("solar(solar_write_procmem): error from copy_from_user\n"));
    return(-SOLAR_EFAULT);
  }

  kdebug(("solar(solar_write_procmem): len = %d\n", len));
  fb_data->value[len - 1] = '\0';
  kdebug(("solar(solar_write_procmem): text passed = %s\n", fb_data->value));

  if (!strcmp(fb_data->value, "debug"))
  {
    debug = 1;
    kdebug(("solar(solar_write_procmem): debugging enabled\n"));
  }

  if (!strcmp(fb_data->value, "nodebug"))
  {
    kdebug(("solar(solar_write_procmem): debugging disabled\n"));
    debug = 0;
  }
  
  if (!strcmp(fb_data->value, "elog"))
  {
    kdebug(("solar(solar_write_procmem): error logging enabled\n"))
    errorlog = 1;
  }

  if (!strcmp(fb_data->value, "noelog"))
  {
    kdebug(("csolar(solar_write_procmem): error logging disabled\n"))
    errorlog = 0;
  }
  
  return len;
}


/***************************************************************************************************/
static int solar_read_procmem(char *buf, char **start, off_t offset, int count, int *eof, void *data)
/***************************************************************************************************/
{
  u_int card, ocr, osr, imask;
  int loop, nchars = 0;
  static int len = 0;

  kdebug(("solar(solar_read_procmem): Called with buf    = 0x%016lx\n", (u_long)buf));
  kdebug(("solar(solar_read_procmem): Called with *start = 0x%016lx\n", (u_long)*start));
  kdebug(("solar(solar_read_procmem): Called with offset = %d\n", (u_int)offset));
  kdebug(("solar(solar_read_procmem): Called with count  = %d\n", count));

  if (offset == 0)
  {
    kdebug(("exp(exp_read_procmem): Creating text....\n"));
    len = 0;
    len += sprintf(proc_read_text + len, "\n\n\nSOLAR driver for release %s (based on CVS tag %s)\n", RELEASE_NAME, CVSTAG);

    len += sprintf(proc_read_text + len, "==================================================\n");
    len += sprintf(proc_read_text + len, "Card|IRQ line| REQ IRQ|revision|PCI MEM addr.|wswap|bswap|LDOWN|REQ available|REQs posted|\n");
    len += sprintf(proc_read_text + len, "----|--------|--------|--------|-------------|-----|-----|-----|-------------|-----------|\n");
    for(card = 0; card < maxcards; card++)
    {
      ocr = solarcard[card].regs->opctrl;
      osr = solarcard[card].regs->opstat;
      imask = solarcard[card].regs->intmask;
      len += sprintf(proc_read_text + len, "   %d|", card);
      len += sprintf(proc_read_text + len, "      %2d|", solarcard[card].irq_line);
      len += sprintf(proc_read_text + len, "      %2d|", (imask & 0xf)); 
      len += sprintf(proc_read_text + len, "    0x%02x|", solarcard[card].pci_revision); 
      len += sprintf(proc_read_text + len, "   0x%08x|", solarcard[card].pci_memaddr);
      len += sprintf(proc_read_text + len, "  %s|", (ocr & 0x4)?"yes":" no");
      len += sprintf(proc_read_text + len, "  %s|", (ocr & 0x2)?"yes":" no");
      len += sprintf(proc_read_text + len, "  %s|", (osr & 0x00040000)?"yes":" no");
      len += sprintf(proc_read_text + len, "           %2d|", (osr & 0x3f));
      len += sprintf(proc_read_text + len, "%11d|\n", served[card]);
    }

    len += sprintf(proc_read_text + len, " \n");
    len += sprintf(proc_read_text + len, "The command 'echo <action> > /proc/solar', executed as root,\n");
    len += sprintf(proc_read_text + len, "allows you to interact with the driver. Possible actions are:\n");
    len += sprintf(proc_read_text + len, "debug   -> enable debugging\n");
    len += sprintf(proc_read_text + len, "nodebug -> disable debugging\n");
    len += sprintf(proc_read_text + len, "elog    -> Log errors to /var/log/messages\n");
    len += sprintf(proc_read_text + len, "noelog  -> Do not log errors to /var/log/messages\n");
  }
  kdebug(("solar(solar_read_procmem): number of characters in text buffer = %d\n", len));

  if (count < (len - offset))
    nchars = count;
  else
    nchars = len - offset;
  kdebug(("solar(solar_read_procmem): min nchars         = %d\n", nchars));
  
  if (nchars > 0)
  {
    for (loop = 0; loop < nchars; loop++)
      buf[loop + (offset & (PAGE_SIZE - 1))] = proc_read_text[offset + loop];
    *start = buf + (offset & (PAGE_SIZE - 1));
  }
  else
  {
    nchars = 0;
    *eof = 1;
  }
 
  kdebug(("solar(solar_read_procmem): returning *start   = 0x%016lx\n", (u_long)*start));
  kdebug(("solar(solar_read_procmem): returning nchars   = %d\n", nchars));
  return(nchars);
}


/********************************************************************************/
static irqreturn_t solar_irq_handler (int irq, void *dev_id, struct pt_regs *regs)
/********************************************************************************/
{
  u_int card, for_us;

  // Note: the SOLAR card(s) may have to share an interrupt line with other PCI devices.
  // It is therefore important to exit quickly if we are not concerned with an interrupt.
  // For now this is done by looking at the OPSTAT register. There may be a dedicated
  // interrupt status bit in the future.
  
  for_us = 0;
  for(card = 0; card < maxcards; card++)
    for_us += solarcard[card].regs->opstat & 0x3f;

  if (for_us)
  {
    kdebug(("solar(solar_irq_handler): Interrupt received\n"));
    refill_req_fifo();
    return(IRQ_HANDLED);          //MJ: for 2.6 see p273
  }
  else
  {
    kdebug(("solar(solar_irq_handler): This was not a SOLAR interrupt\n"));
    return(IRQ_NONE);     
  }
}



/*****************************/
/* PCI driver functions      */
/*****************************/

/**********************************************************************************/
static int __devinit solarProbe(struct pci_dev *dev, const struct pci_device_id *id)   //MJ: for _devinit see p31
/**********************************************************************************/
{
  int i, card;
  u_long flags;
  u_int size;

  kdebug(("solar(solarProbe): called for device: 0x%04x / 0x%04x\n", dev->vendor, dev->device));
  kdebug(("solar(solarProbe): Bus: 0x%04x / Devfn 0x%04x\n", dev->bus->number, dev->devfn));

  /* Find a new "slot" for the card */
  card = -1;
  for (i = 0; i < MAXCARDS; i++) 
  {
    if (solarcard[i].pciDevice == NULL) 
    {
      card = i;
      solarcard[card].pciDevice = dev;
      break;
    }
  }

  if (card == -1) 
  {
    kerror(("solar(solarProbe): Could not find space in the solarcard array for this card."));
    return(-EIO);
  }
  
  flags = pci_resource_flags(dev, BAR0);  //MJ: See p317
  if ((flags & IORESOURCE_MEM) != 0) 
  {
    solarcard[card].pci_memaddr = pci_resource_start(dev, BAR0) & PCI_BASE_ADDRESS_MEM_MASK;        //MJ: See p317
    size = pci_resource_end(dev, BAR0) - pci_resource_start(dev, BAR0);                             //MJ: See p317
    kdebug(("solar(solarProbe): Mapping %d bytes at PCI address 0x%08x\n", size, solarcard[card].pci_memaddr));
    
    solarcard[card].regs = (T_solar_regs *)ioremap(solarcard[card].pci_memaddr, size);
    if (solarcard[card].regs == NULL)
    {
      kerror(("solar(solarProbe): error from ioremap for solarcard[%d].regs\n", card));
      solarcard[card].pciDevice = NULL;
      return(-SOLAR_IOREMAP);
    }
    kdebug(("solar(solarProbe): solarcard[%d].regs is at 0x%016lx\n", card, (u_long)solarcard[card].regs));
  }
  else 
  {
    kerror(("solar(solarProbe): Bar 0 is not a memory resource"));
    return(-SOLAR_EIO);
  }  

  // get revision directly from the SOLAR
  pci_read_config_byte(solarcard[card].pciDevice, PCI_REVISION_ID, &solarcard[card].pci_revision);
  kdebug(("solar(solarProbe): revision = %x \n", solarcard[card].pci_revision));
  if (solarcard[card].pci_revision < MINREV)
  {
    kerror(("solar(solarProbe): Illegal SOLAR Revision\n"));
    return(-SOLAR_ILLREV);
  }  
  
  //Enable the device (See p314) and get the interrupt line
  pci_enable_device(dev);
  solarcard[card].irq_line = dev->irq;
  kdebug(("solar(solarProbe): interrupt line = %d \n", solarcard[card].irq_line));

  //Several solar cards may use the same interrupt but we want to install the driver only once
  if (irqlist[solarcard[card].irq_line] == 0) 
  {
    if(request_irq(solarcard[card].irq_line, solar_irq_handler, SA_SHIRQ, "solar", dev))	
    {
      kerror(("solar(solarProbe): request_irq failed on IRQ = %d\n", solarcard[card].irq_line));
      return(-SOLAR_REQIRQ);
    }
    irqlist[solarcard[card].irq_line]++;
    kdebug(("solar(solarProbe): Interrupt %d registered\n", solarcard[card].irq_line)); 
  }  
 
  maxcards++;
  return(0);
}


/******************************************/
static void solarRemove(struct pci_dev *dev) 
/******************************************/
{
  int i, card;

  /* Find the "slot" of the card */
  card = -1;
  for (i = 0; i < MAXCARDS; i++) 
  {
    if (solarcard[i].pciDevice == dev) 
    {
      card = i;
      solarcard[i].pciDevice = 0;
      break;
    }  
  }
  
  if (card == -1) 
  {
    kerror(("solar(solarRemove): Could not find device to remove."));
    return;
  }
 
  if (--irqlist[solarcard[card].irq_line] == 0)
  {
    kdebug(("solar(solarRemove): removing handler for interrupt %d", solarcard[card].irq_line));
    free_irq(solarcard[card].irq_line, dev);	
  }
  
  iounmap((void *)solarcard[card].regs);
  kdebug(("solar(solarRemove): Card %d removed", card));
  maxcards--;
}




/*********************/
/* Service functions */
/*********************/

/**********************************************************/
static int in_fifo_pop(int card, u_int *data1, u_int *data2)
/**********************************************************/
{
  int index;
  
  if (infifon[card] == 0)
  {
    kdebug(("solar(in_fifo_pop): The IN FIFO is empty\n"));
    return(-1);
  }
  
  index = IN_INDEX(card, infifor[card]);
  *data1 = infifo[index];
  *data2 = infifo[index + 1];
  kdebug(("solar(in_fifo_pop): index=%d data1=0x%08x  data2=0x%08x\n", index, *data1, *data2));

  infifor[card]++;

  if (infifor[card] > (MAXINFIFO - 1))
    infifor[card] = 0;

  infifon[card]--;

  return(0);
}


/*******************************/
static void refill_req_fifo(void)
/*******************************/
{
  u_int data1 = 0, data2 = 0, loop, card, numfree, numreq, numcopy;

  // We got here because one of the cards generated an interrupt.
  // As other cards may also have space in ther REQ FIFOs we process all of them
  // to reduce the interrupt frequency.

  for(card = 0; card < maxcards; card++)
  {       
    // Is there space in the REQ FIFO?
    numfree = (solarcard[card].regs->opstat) & 0x3f;

    // Have we got new requests?
    numreq = infifon[card];
    kdebug(("solar(refill_req_fifo): numfree=%d  numreq=%d\n", numfree, numreq));

    if (numfree && !numreq)
    {
      //We have to disable the interrupt as we can not clear it by filling the REQ FIFO
      kdebug(("solar(refill_req_fifo): Disabling interrupt\n"));
      solarcard[card].regs->intmask = 0;
    }
    else
    {
      if (numfree < numreq)
        numcopy = numfree;
      else
        numcopy = numreq;
      kdebug(("solar(refill_req_fifo): numcopy=%d \n", numcopy));

      for (loop = 0; loop < numcopy; loop++)
      {
        // As we have checked the number of entries in the IN FIFO we do not have to worry about errors
        in_fifo_pop(card, &data1, &data2);
        solarcard[card].regs->reqfifo1 = data1;
        solarcard[card].regs->reqfifo2 = data2;
        served[card]++;
        kdebug(("solar(refill_req_fifo): data1=0x%08x  data2=0x%08x\n", data1, data2));
      }
    }
  }
}


