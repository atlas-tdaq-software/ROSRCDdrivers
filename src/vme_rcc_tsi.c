// $Id$
/************************************************************************/
/*									*/
/* File: vme_rcc_tsi.c						        */
/*									*/
/* RCC VMEbus driver							*/
/*									*/
/* 17. Jul. 08  MAJO  created						*/
/*									*/
/************ C 2021 - The software with that certain something *********/


//MJ - FIXME - We have to decide what to do with the pid


//new dma questions
//Does the TSI use 64bit PCI addresses (while the Universe only uses 32bit addresses). 
//  if yes: check if u_long is used instead of u_int in the variable declarations



/************************************************************************/
/*NOTES:								*/
/*- This driver should work on kernels from 2.6.9 onwards		*/
/*- This driver requires a TSI148. 					*/
/*- The proper way of accessing the TSI148 would be through 		*/
/*  writel/readl functions. For a first implementation I have chosen to */
/*  use pointer based I/O as this simplifies the re-use of old code	*/ 
/************************************************************************/


// Register the used address ranges in /proc/iomem
// The driver only supports 32-bit PCI and VMEbus addresses
// How can we get BERR information for safe cycles in a synchronous way? I.e. how do we know if a (posted) cycle has been executed? 
// Shoud we get the BERR information from the TSI148 or from the extra logic of the VP426?


//Note: Kernel headers are at: /usr/src/kernels/2.6.32-279.19.1.el6.x86_64/include/linux

#include <linux/init.h>           //MJ: for 2.6, p30
#include <linux/module.h>
#include <linux/delay.h>
#include <linux/moduleparam.h>    //MJ: for 2.6, p30
#include <linux/kernel.h>
#include <linux/stat.h>           //MJ: for 2.6, e.g. for module_param
#include <linux/pci.h>
#include <linux/sched.h>          //MJ: for current->pid
#include <linux/spinlock.h>       //For the spin-lock 
#include <linux/wait.h>           //For the wait queue 
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/mm.h>
#include <linux/slab.h>
#include <linux/cdev.h>           //e.g. for cdev_alloc
#include <linux/interrupt.h>      //e.g. for request_irq
#include <linux/version.h>        
#include <linux/seq_file.h>       //CS9

#if LINUX_VERSION_CODE > KERNEL_VERSION(4,0,0)
  #include <linux/uaccess.h>
  #include <linux/sched/signal.h>
#else
  #include <asm/uaccess.h>
#endif

#include <asm/io.h>
#include "vme_rcc/tsi148.h"
#include "vme_rcc/vme_rcc_driver.h"
#include "ROSRCDdrivers/tdaq_drivers.h"

#ifndef PCI_VENDOR_ID_TUNDRA
  #define  PCI_VENDOR_ID_TUNDRA 0x10e3
#endif


/*********/
/*Globals*/
/*********/
static int berr_interlock = 0, vmetab_ok = 0, debug = 0, errorlog = 1;
static int sem_positive[2] = {0,0};
static char *proc_read_text;
static u_char vme_irq_line;
static u_int maxcard = 0;
static u_int crcsr_mapped = 0;
static VME_MasterMapInt_t mm_crcsr;
static u_int board_type;
static u_int irq_level_table[7];
static u_int irq_sysfail;
static u_int berr_int_count = 0;
static all_mstslvmap_t stmap;
static master_map_t master_map_table;
static link_dsc_table_t link_table;		// link descriptors
static berr_proc_table_t berr_proc_table;	// processes with BERR handling
static sysfail_link_dsc_t sysfail_link;	        // processes with SYSFAIL handling
static berr_dsc_t berr_dsc;			// last BERR
static InterruptCounters_t Interrupt_counters = {0,0,0,0,0,0,0, {0,0,0,0,0,0,0}, 0};
static dev_t major_minor;
static tsi148_regs_t *tsi;

#if LINUX_VERSION_CODE < KERNEL_VERSION(4,0,0)
  static struct timer_list dma_timer;
#endif

static struct cdev *vme_rcc_tsi_cdev;

static u_int dma_done_total = 0, dma_todo_total = 0, dma_todo_write, dma_todo_read, dma_todo_num, dmas_pending = 0, dma_is_free = 1, dma_current_pid;
static u_long dma_current_pciaddr, dma_irq_flags;

//Note: The DMA requests can arrive at any time and from any number of processes. The driver will queue the requests and start them in the order in which they were entered into the queue. 
//      Therefore I am using a FIFO based bechanism
static VME_DMAhandle_todo_t dma_list_todo[VME_DMATODOSIZE];

//Note: Information about completed DMAs will be entered into a random-access array. Using a FIFO is not possible because the user applications may not poll for the end of their DMAs in parallel
//      The absence of polling activity from one process should not compromize the polling of other processes. If an entry in the array is free or busy will be controlled bya flag in the array elements
static VME_DMAhandle_done_t dma_list_done[VME_DMATODOSIZE];

DECLARE_WAIT_QUEUE_HEAD(dma_wait);

static DEFINE_SPINLOCK(dmalock);
static DEFINE_SPINLOCK(slock);


/************/
/*Prototypes*/
/************/
/******************************/
/*Standard function prototypes*/
/******************************/
static int vme_rcc_tsi_open(struct inode *ino, struct file *filep);
static int vme_rcc_tsi_release(struct inode *ino, struct file *filep);
static long vme_rcc_tsi_ioctl(struct file *filep, u_int cmd, u_long arg);
static int vme_rcc_tsi_mmap(struct file *file, struct vm_area_struct *vma);
static void vme_rcc_tsi_vmaOpen(struct vm_area_struct *vma);
static void vme_rcc_tsi_vmaClose(struct vm_area_struct *vma);
static ssize_t vme_rcc_tsi_proc_write(struct file *file, const char *buffer, size_t count, loff_t *startOffset);
static int vme_rcc_tsi_init(void);
static int vme_rcc_tsi_Probe(struct pci_dev *dev, const struct pci_device_id *id);   
static void vme_rcc_tsi_Remove(struct pci_dev *dev); 
static void vme_rcc_tsi_cleanup(void);
int vme_rcc_tsi_proc_open(struct inode *inode, struct file *file);
int vme_rcc_tsi_proc_show(struct seq_file *sfile, void *p);

/*****************************/
/*Special function prototypes*/
/*****************************/
static int berr_check(u_int *addr, u_int *multi, u_int *am);
static int cct_berrInt(void);
static int fill_mstmap(u_int otsau, u_int otsal, u_int oteau, u_int oteal, u_int otofu, u_int otofl, u_int otat, mstslvmap_t *mstslvmap);
static int fill_slvmap(u_int itsau, u_int itsal, u_int iteau, u_int iteal, u_int itofu, u_int itofl, u_int itat, mstslvmap_t *mstslvmap);
static void vme_dma_handler(void);

#if LINUX_VERSION_CODE < KERNEL_VERSION(4,0,0)
  static void vme_intTimeout(u_long arg);
  static void vme_intSysfailTimeout(u_long arg);
#else
  void vme_intSysfailTimeout(struct timer_list *t);
  void vme_intTimeout(struct timer_list *t);
#endif

static void vme_irq_handler(int level);
static void init_cct_berr(void);
static void mask_cct_berr(void);
static void send_berr_signals(void);
static void sysfail_irq_handler(void);
static void read_berr_capture(void);
static irqreturn_t tsi_irq_handler(int irq, void *dev_id);
static u_int bswap(u_int word);
int dma_is_done(VME_DMAhandle_t *dma_poll_params);
void start_next_dma(int src);



/***************************************************************/
/* Use /sbin/modinfo <module name> to extract this information */
/***************************************************************/
MODULE_DESCRIPTION("VMEbus driver for the Tundra Tsi148 on SBCs from CCT");
MODULE_AUTHOR("Markus Joos, CERN/PH");
MODULE_VERSION("1.1");
MODULE_LICENSE("GPL");
module_param (debug, int, S_IRUGO | S_IWUSR);  //MJ: for 2.6 p37
MODULE_PARM_DESC(debug, "1 = enable debugging   0 = disable debugging");
module_param (errorlog, int, S_IRUGO | S_IWUSR);  //MJ: for 2.6 p37
MODULE_PARM_DESC(errorlog, "1 = enable error logging   0 = disable error logging");


//MJ: Not actually required. Just for kdebug
struct vm_operations_struct vme_rcc_tsi_vm_ops =
{       
  .close = vme_rcc_tsi_vmaClose,      
  .open  = vme_rcc_tsi_vmaOpen,      //MJ: Note the comma at the end of the list!
};

static struct file_operations fops = 
{
  .owner          = THIS_MODULE,
  .unlocked_ioctl = vme_rcc_tsi_ioctl,    //MJ: Is unlocked_ioctl correct or should it be something else?
                                          //MJ: It was recommended by Rubini but I am not quite understanding
					  //MJ: why it should be used.  See also mail exchange with Bertrand Martin
  .open           = vme_rcc_tsi_open,    
  .mmap           = vme_rcc_tsi_mmap,
  .release        = vme_rcc_tsi_release,
};

// PCI driver structures. See p311
static struct pci_device_id vme_rcc_tsi_Ids[] = 
{
  {PCI_DEVICE(PCI_VENDOR_ID_TUNDRA, 0x148)},      
  {0,},
};

//PCI operations
static struct pci_driver __refdata vme_rcc_tsi_PciDriver =   //MJ: The need for __refdata is not yet fully understood. The compiler on SLC6 asked for it. See below
{
  .name     = "vme_rcc_tsi",
  .id_table = vme_rcc_tsi_Ids,
  .probe    = vme_rcc_tsi_Probe,
  .remove   = vme_rcc_tsi_Remove,
};


//CS9
//Inspiration taken from: https://stackoverflow.com/questions/64931555/how-to-fix-error-passing-argument-4-of-proc-create-from-incompatible-pointer
//===========================================================
#if LINUX_VERSION_CODE >= KERNEL_VERSION(5,6,0)
static const struct proc_ops vme_rcc_tsi_proc_file_ops = 
{
  .proc_open    = vme_rcc_tsi_proc_open,
  .proc_write   = vme_rcc_tsi_proc_write,
  .proc_read    = seq_read,    
  .proc_lseek   = seq_lseek,	
  .proc_release = single_release
};
#else
static struct file_operations vme_rcc_tsi_proc_file_ops = 
{
  .owner   = THIS_MODULE,
  .open    = vme_rcc_tsi_proc_open,
  .write   = vme_rcc_tsi_proc_write,
  .read    = seq_read,
  .llseek  = seq_lseek,
  .release = single_release
};
#endif
//==============================================================


/****************************/
/* Standard driver function */
/****************************/


/***************************************************************/
int vme_rcc_tsi_proc_open(struct inode *inode, struct file *file) 
/***************************************************************/
{
  return single_open(file, vme_rcc_tsi_proc_show, NULL);
}


/*******************************/
static int vme_rcc_tsi_init(void)
/*******************************/
{
  char *id_ptr;
  int found, result, loop;
  u_int bidloop, i;
  u_long bidaddr;
  static struct proc_dir_entry *vme_rcc_tsi_file;

  kerror(("vme_rcc_tsi(vme_rcc_tsi_init): Error debug is active\n"))

  bidaddr = (u_long)ioremap(0xe0000, 0x20000);
  id_ptr = (u_char *)bidaddr;
  found = 0;
  board_type = VP_UNKNOWN;
  for (bidloop = 0; (bidloop + 7) < 0x20000; bidloop++)
  {
    if ((id_ptr[0] == '$') && (id_ptr[1] == 'S') && (id_ptr[2] == 'H') && (id_ptr[3] == 'A') && (id_ptr[4] == 'A') && (id_ptr[5] == 'N') && (id_ptr[6] == 'T') && (id_ptr[7] == 'I'))
    {
      kdebug(("vme_rcc_tsi(vme_rcc_tsi_init): Board ID table found\n"));
      kdebug(("vme_rcc_tsi(vme_rcc_tsi_init): Board Id Structure Version:%d\n", id_ptr[8]));   //VP717 = 1
      kdebug(("vme_rcc_tsi(vme_rcc_tsi_init): Board Type:%d\n", id_ptr[9]));                   //VP717 = 2
      kdebug(("vme_rcc_tsi(vme_rcc_tsi_init): Board Id:%d,%d\n", id_ptr[10], id_ptr[11]));     //VP717 = 28, VP917 = 31
      found = 1;
      if (id_ptr[8] == 1 && id_ptr[9] == 2 && id_ptr[10] == 28 && id_ptr[11] == 0) 
        board_type = VP_717;   
      if (id_ptr[8] == 1 && id_ptr[9] == 2 && id_ptr[10] == 31 && id_ptr[11] == 0) 
        board_type = VP_917;   
      if (id_ptr[8] == 2 && id_ptr[9] == 2 && id_ptr[10] == 31 && id_ptr[11] == 0) 
        board_type = VP_917;  
    }
    if (found)
      break;
    id_ptr++;
  }
  
  if (board_type == VP_UNKNOWN)
  {
    kerror(("vme_rcc_tsi(vme_rcc_tsi_init): Unable to determine board type\n"))
    return(-VME_UNKNOWN_BOARD);
  } 
  
  if(alloc_chrdev_region (&major_minor, 0, 1, "vme_rcc_tsi")) //MJ: for 2.6 p45
  {
    kdebug(("vme_rcc_tsi(vme_rcc_tsi_init): failed to obtain device numbers\n"))
    result = -EIO;
    goto fail1;
  }
  
  proc_read_text = (char *)kmalloc(MAX_PROC_TEXT_SIZE, GFP_KERNEL);
  kdebug(("vme_rcc_tsi(vme_rcc_tsi_init): %d bytes allocated [proc_read_text](MAX_PROC_TEXT_SIZE)\n", MAX_PROC_TEXT_SIZE))
  if (proc_read_text == NULL)
  {
    kerror(("vme_rcc_tsi(vme_rcc_tsi_init): error from kmalloc\n"))
    result = -ENOMEM;
    goto fail2;
  }

  vme_rcc_tsi_file = proc_create("vme_rcc_tsi", 0644, NULL, &vme_rcc_tsi_proc_file_ops);
  if (vme_rcc_tsi_file == NULL)
  {
    kdebug(("vme_rcc_tsi(vme_rcc_tsi_init): error from call to create_proc_entry\n"))
    result = -EFAULT;
    goto fail3;
  }
  kdebug(("vme_rcc_tsi(vme_rcc_tsi_init): entry in proc directory created\n"))

  kdebug(("vme_rcc_tsi(vme_rcc_tsi_init): registering PCIDriver\n"));
  result = pci_register_driver(&vme_rcc_tsi_PciDriver);  //MJ: See P312
  if (result < 0)
  {
    kerror(("vme_rcc_tsi(vme_rcc_tsi_init): ERROR! no Tsi148 chip found or other initialisation problem! result = %d\n", result))
    result = -EIO;
    goto fail3;
  }
  else 
  {
    kerror(("vme_rcc_tsi(vme_rcc_tsi_init): Found a Tsi148 chip!\n"))
  }
      
  init_cct_berr();	                            // Enable Tsi148 BERR interrupts
  sema_init(&link_table.link_dsc_sem, 1);           // Initialize the Link Descriptor protection semaphore
  
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,0,0)
  init_timer(&dma_timer);                           // Initialise the watchdog timer for DMA transactions
#endif

  for (loop = 0; loop < VME_DMATODOSIZE; loop++)    // Initialize the list of DMA transactions
  {
    dma_list_todo[loop].paddr = 0;
    dma_list_done[loop].paddr = 0;
    dma_list_done[loop].in_use = 0;
  }  
  dma_todo_write = 0; 
  dma_todo_read  = 0;
  dma_todo_num   = 0;
  
  
  for (loop = 0; loop < VME_MAX_MASTERMAP; loop++)  //Initialize the list of active master mappings
  {
    master_map_table.map[loop].pid = 0;
    master_map_table.map[loop].vaddr = 0;
  }
  sema_init(&master_map_table.sem, 1);
  
  for (i = 0; i < VME_VCTSIZE; i++)                 // Reset IRQ link and BERR tables
  {
    link_table.link_dsc[i].pid = 0;
    link_table.link_dsc[i].vct = 0;
    link_table.link_dsc[i].lvl = 0;
    link_table.link_dsc[i].pending = 0;
    link_table.link_dsc[i].total_count = 0;
    link_table.link_dsc[i].group_ptr = 0;
    sema_init(&link_table.link_dsc[i].sem, 0);
    link_table.link_dsc[i].sig = 0;
  }

  for (i = 0; i < VME_MAX_BERR_PROCS; i++)
  {
    berr_proc_table.berr_link_dsc[i].pid = 0;
    berr_proc_table.berr_link_dsc[i].sig = 0;
    sema_init(&berr_proc_table.proc_sem, 1);
  }

  berr_dsc.vmeadd   = 0;
  berr_dsc.am       = 0;
  berr_dsc.iack     = 0;
  berr_dsc.lword    = 0;
  berr_dsc.ds0      = 0;
  berr_dsc.ds1      = 0;
  berr_dsc.wr       = 0;
  berr_dsc.multiple = 0;
  berr_dsc.flag     = 0;

  sysfail_link.pid = 0;
  sysfail_link.sig = 0;
  sema_init(&sysfail_link.proc_sem, 1);

  vme_rcc_tsi_cdev = (struct cdev *)cdev_alloc();       //MJ: for 2.6 p55
  vme_rcc_tsi_cdev->ops = &fops;
  result = cdev_add(vme_rcc_tsi_cdev, major_minor, 1);  //MJ: for 2.6 p56
  if (result)
  {
    kdebug(("vme_rcc_tsi(vme_rcc_tsi_init): error from call to cdev_add.\n"))
    goto fail4;
  }
  
  kdebug(("vme_rcc_tsi(vme_rcc_tsi_init): driver loaded; major device number = %d\n", MAJOR(major_minor)))
  return(0);

  fail4:
    remove_proc_entry("vme_rcc_tsi", NULL);

  fail3:
    kdebug(("vme_rcc_tsi(vme_rcc_tsi_init): Freeing proc_read_text memory\n"))
    kfree(proc_read_text);
    kdebug(("vme_rcc_tsi(vme_rcc_tsi_init): kfree called for proc_read_text\n"))

  fail2:
    kdebug(("vme_rcc_tsi(vme_rcc_tsi_init): Unregistering chrdev\n"))
    unregister_chrdev_region(major_minor, 1); //MJ: for 2.6 p45

  fail1:
    kdebug(("vme_rcc_tsi(vme_rcc_tsi_init): returning from function\n"))
    return(result);
}


/***********************************/
static void vme_rcc_tsi_cleanup(void)
/***********************************/
{
  pci_unregister_driver(&vme_rcc_tsi_PciDriver);
  cdev_del(vme_rcc_tsi_cdev);                     //MJ: for 2.6 p56
  remove_proc_entry("vme_rcc_tsi", NULL);
  kfree(proc_read_text);
  kdebug(("vme_rcc_tsi(vme_rcc_tsi_cleanup): kfree called for proc_read_text\n"))
  unregister_chrdev_region(major_minor, 1);       //MJ: for 2.6 p45
  kdebug(("vme_rcc_tsi(vme_rcc_tsi_cleanup): driver removed\n"))
}


module_init(vme_rcc_tsi_init);    //MJ: for 2.6 p16
module_exit(vme_rcc_tsi_cleanup); //MJ: for 2.6 p16


/****************************************************************/
static int vme_rcc_tsi_open(struct inode *ino, struct file *filep)
/****************************************************************/
{
  int vct, berr, loop;
  private_data_t *pdata;

  pdata = (private_data_t *)kmalloc(sizeof(private_data_t), GFP_KERNEL);
  kdebug(("vme_rcc_tsi(open): 0x%08lx bytes allocated [pdata](sizeof(private_data_t))\n", sizeof(private_data_t)))
  if (pdata == NULL)
  {
    kerror(("vme_rcc_tsi(open): error from kmalloc\n"))
    return(-VME_KMALLOC);
  }

  kdebug(("vme_rcc_tsi(open): pointer to link flags in file descriptor = %016lx\n", (u_long)&pdata->link_dsc_flag[0]))
  for (vct = 0; vct < VME_VCTSIZE; vct++)
    pdata->link_dsc_flag[vct] = 0;

  kdebug(("vme_rcc_tsi(open): pointer to berr flags in file descriptor = %016lx\n", (u_long)&pdata->berr_dsc_flag[0]))
  for (berr = 0; berr < VME_MAX_BERR_PROCS; berr++)
    pdata->berr_dsc_flag[berr] = 0;

  pdata->sysfail_dsc_flag = 0;
  
  for (loop = 0; loop < VME_DMADONESIZE; loop++)
    pdata->dma[loop] = 0;
  
  for (loop = 0; loop < VME_MAX_MASTERMAP; loop++)
    pdata->mastermap[loop] = 0;
  
  filep->private_data = pdata;
  return(0);
}


/*******************************************************************/
static int vme_rcc_tsi_release(struct inode *ino, struct file *filep)
/*******************************************************************/
{
  int vct, no_freed, isave, free_flag, loop;
  u_int i;
  VME_IntHandle_t *group_ptr;
  VME_IntHandle_t **group_ptr_save;
  private_data_t *pdata;
  
  group_ptr_save = (VME_IntHandle_t **)kmalloc(sizeof(VME_IntHandle_t *) * VME_VCTSIZE, GFP_KERNEL);
  kdebug(("vme_rcc_tsi(vme_rcc_tsi_release): 0x%08lx bytes allocated [group_ptr_save](sizeof(VME_IntHandle_t *) * VME_VCTSIZE))\n", sizeof(VME_IntHandle_t *) * VME_VCTSIZE))
  if (group_ptr_save == NULL)
  {
    kerror(("vme_rcc_tsi(vme_rcc_tsi_release): error from kmalloc\n"))
    return(-VME_KMALLOC);
  }
  	    
  pdata = (private_data_t *)filep->private_data;

  down(&link_table.link_dsc_sem);

  no_freed = 0;
  group_ptr_save[0] = (VME_IntHandle_t*)0xffffffff;

  kdebug(("vme_rcc_tsi(release): pid = %d\n", current->pid))

  for (vct = 0; vct < VME_VCTSIZE; vct++)
  {
    if (pdata->link_dsc_flag[vct] == 1)
    {
      if (link_table.link_dsc[vct].group_ptr)	// grouped vector
      {
        group_ptr = link_table.link_dsc[vct].group_ptr;
        kdebug(("vme_rcc_tsi(release): vector = %d, group_ptr = 0x%p\n", vct, group_ptr))
        free_flag = 1;
        for (isave = 0; isave <= no_freed; isave++)
        {
          if (group_ptr == group_ptr_save[isave])
          {
            free_flag = 0;	// already freed
            break;
          }
        }

        kdebug(("vme_rcc_tsi(release): free_flag = %d\n", free_flag))
        if (free_flag)
        {
          kfree(group_ptr);                            //MJ: where is the corresponding kmalloc? In VMELINK?
          kdebug(("vme_rcc_tsi(release): kfree called for group_ptr\n"))
          kdebug(("vme_rcc_tsi(release): group_ptr = 0x%p is now kfree'd\n", group_ptr))
          group_ptr_save[no_freed] = group_ptr;        
          no_freed++;
          kdebug(("vme_rcc_tsi(release): no_freed = %d \n", no_freed))
        }
      }

      link_table.link_dsc[vct].pid = 0;
      link_table.link_dsc[vct].vct = 0;
      link_table.link_dsc[vct].lvl = 0;
      link_table.link_dsc[vct].pending = 0;
      link_table.link_dsc[vct].total_count = 0;
      link_table.link_dsc[vct].group_ptr = 0;
      sema_init(&link_table.link_dsc[vct].sem, 0);
      link_table.link_dsc[vct].sig = 0;

      pdata->link_dsc_flag[vct] = 0;
    }
  }
  up(&link_table.link_dsc_sem);
  kfree(group_ptr_save);
  kdebug(("vme_rcc_tsi(release): kfree called for group_ptr_save\n"))

  // and now the BERR & SYSFAIL links
  down(&berr_proc_table.proc_sem);
  for (i = 0; i < VME_MAX_BERR_PROCS; i++)
  {
    if (pdata->berr_dsc_flag[i] == 1)
    {
      kdebug(("vme_rcc_tsi(release): clearing BERR entry %d \n", i))
      berr_proc_table.berr_link_dsc[i].pid = 0;
      berr_proc_table.berr_link_dsc[i].sig = 0;
      pdata->link_dsc_flag[i] = 0;
    }
  }
  up(&berr_proc_table.proc_sem);
  
  down(&sysfail_link.proc_sem);
  if (pdata->sysfail_dsc_flag)
  {
    kdebug(("vme_rcc_tsi(release): clearing SYSFAIL entry that has been left open by process %d\n", sysfail_link.pid))
    sysfail_link.pid = 0;
    sysfail_link.sig = 0;
    pdata->sysfail_dsc_flag = 0;
  }
  up(&sysfail_link.proc_sem);  

  // Release orphaned master mappings
  down(&master_map_table.sem);
  for (loop = 0; loop < VME_MAX_MASTERMAP; loop++)
  {
    if (pdata->mastermap[loop] == 1)
    {
      kdebug(("vme_rcc_tsi(release): Unmapping orphaned virtual address 0x%016lx for process %d\n", master_map_table.map[loop].vaddr, master_map_table.map[loop].pid))
      iounmap((void *)master_map_table.map[loop].vaddr);
      master_map_table.map[loop].vaddr = 0;
      master_map_table.map[loop].pid = 0;
      pdata->mastermap[loop] = 0;
    }
    if (pdata->mastermap[loop] == 2)
    {
      kdebug(("vme_rcc_tsi(release): Not unmapping orphaned but protected virtual address 0x%016lx (used for CR/CSR access)\n", master_map_table.map[loop].vaddr))
    }
  }
  up(&master_map_table.sem);

  // and finally the DMA lists
  for (loop = 0; loop < VME_DMATODOSIZE; loop++)
  {
    if (dma_list_todo[loop].pid == current->pid)
    {
      kdebug(("vme_rcc_tsi(release)(%d): entry %d in dma_list_todo list released. paddr=0x%016lx\n", current->pid, loop, dma_list_todo[loop].paddr))
      dma_list_todo[loop].paddr = 0;
      dma_list_todo[loop].pid = 0;
    }
    if (dma_list_done[loop].pid == current->pid)
    {
      kdebug(("vme_rcc_tsi(release)(%d): entry %d in dma_list_done list released. paddr=0x%016lx, in_use=%d \n", current->pid, loop, dma_list_done[loop].paddr, dma_list_done[loop].in_use))
      dma_list_done[loop].paddr = 0;
      dma_list_done[loop].pid = 0;
      dma_list_done[loop].in_use = 0;

      dmas_pending--;
    }
  }
  kfree(pdata);
  kdebug(("vme_rcc_tsi(release): kfree called for pdata\n"))
  kdebug(("vme_rcc_tsi(release): kfreed file private data @ 0x%016lx\n", (u_long)pdata))
  return(0);
}


/**********************************************************************/
static long vme_rcc_tsi_ioctl(struct file *filep, u_int cmd, u_long arg)
/**********************************************************************/
{
  private_data_t *pdata;
  u_long irq_flags;

  kdebug(("vme_rcc_tsi(ioctl): filep at 0x%016lx, cmd = 0x%08x\n", (u_long)filep, cmd))

/*  kdebug(("vme_rcc_tsi(ioctl): cmd(u_int) = %u\n", cmd))
  kdebug(("vme_rcc_tsi(ioctl): cmd(int)   = %d\n", cmd))
  
  kdebug(("vme_rcc_tsi(ioctl): VMEDMAPOLL(u_int) = %u\n", (u_int)VMEDMAPOLL))
  kdebug(("vme_rcc_tsi(ioctl): VMEDMAPOLL(int)   = %d\n", (int)VMEDMAPOLL))
  kdebug(("vme_rcc_tsi(ioctl): size of VMEDMAPOLL = %d\n", (int)sizeof(VMEDMAPOLL)))
  kdebug(("vme_rcc_tsi(ioctl): VMEDMAPOLL(u_int46) = %lu\n", VMEDMAPOLL))
  kdebug(("vme_rcc_tsi(ioctl): VMEDMAPOLL(int64)   = %ld\n", VMEDMAPOLL))
  kdebug(("vme_rcc_tsi(ioctl): size of VMEDMAPOLL = %ld\n", sizeof(VMEDMAPOLL)))*/
  
  pdata = (private_data_t *)filep->private_data;

  switch (cmd)
  {    
    case VMEMASTERMAP:   
    {
      int ok2, ok = 0, loop, loop2;
      u_long vaddr;
      VME_MasterMapInt_t *mm;
      
      mm = (VME_MasterMapInt_t *)kmalloc(sizeof(VME_MasterMapInt_t), GFP_KERNEL);
      kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAP): 0x%08lx bytes allocated [mm](sizeof(VME_MasterMapInt_t)))\n", sizeof(VME_MasterMapInt_t)))
      if (mm == NULL)
      {
	kerror(("vme_rcc_tsi(ioctl,VMEMASTERMAP): error from kmalloc\n"));
        return(-VME_KMALLOC);
      }
      
      if (copy_from_user(mm, (void *)arg, sizeof(VME_MasterMapInt_t)))
      {
	kerror(("vme_rcc_tsi(ioctl,VMEMASTERMAP): error from copy_from_user\n"))
	kfree(mm);
        kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAP): kfree called for mm\n"))
	return(-VME_EFAULT);
      }
      kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAP): mm.in.vmebus_address   = 0x%08x\n", mm->in.vmebus_address))
      kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAP): mm.in.window_size      = 0x%08x\n", mm->in.window_size))
      kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAP): mm.in.address_modifier = 0x%08x\n", mm->in.address_modifier))
      kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAP): mm.in.options          = 0x%08x\n", mm->in.options))

      for (loop = 0; loop < 8; loop++)
      {
	if (mm->in.vmebus_address >= stmap.master[loop].vbase && 
           (mm->in.vmebus_address + mm->in.window_size - 1) <= stmap.master[loop].vtop && 
	   stmap.master[loop].enab == 1 && 
	   stmap.master[loop].am == mm->in.address_modifier &&
           stmap.master[loop].options == mm->in.options)
        {
          kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAP): Decoder %d matches parameters\n", loop))
          mm->pci_address = stmap.master[loop].pbase + (mm->in.vmebus_address-stmap.master[loop].vbase); 

          //find a free slot in the master map table
          down(&master_map_table.sem);
          ok2 = 0;
          for (loop2 = 0; loop2 < VME_MAX_MASTERMAP; loop2++)
          {
            if (master_map_table.map[loop2].pid == 0)
            {
              kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAP): Will use entry %d in master_map_table\n", loop2))
              ok2 = 1;
              break;
            }
          }
          if (!ok2)
          {
            kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAP): master_map_table is full\n"))
            up(&master_map_table.sem);
	    kfree(mm);
            kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAP): kfree called for mm\n"))
            return(-VME_NOSTATMAP2);
          }
          kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAP): PCI address = 0x%016llx\n", mm->pci_address))
          kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAP): window size = 0x%08x\n", mm->in.window_size))
	  vaddr = (u_long)ioremap(mm->pci_address, mm->in.window_size);
          if (!vaddr)
          {
            kerror(("vme_rcc_tsi(ioctl,VMEMASTERMAP): error from ioremap\n"))
            up(&master_map_table.sem);
	    kfree(mm);
            kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAP): kfree called for mm\n"))
            return(-VME_EFAULT);
          }
          master_map_table.map[loop2].vaddr = vaddr;
          master_map_table.map[loop2].pid = current->pid;
          pdata->mastermap[loop2] = 1;
          up(&master_map_table.sem);
	  
          kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAP): kernel virtual address = 0x%016lx   pid = %d\n", vaddr, current->pid))
	  
          mm->kvirt_address = vaddr;
	  ok = 1;
	  break;
        }
        else
        {
          kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAP): stmap.master[%d].vbase   = 0x%016llx\n", loop, stmap.master[loop].vbase))
          kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAP): stmap.master[%d].vtop    = 0x%016llx\n", loop, stmap.master[loop].vtop))
          kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAP): stmap.master[%d].enab    = %d\n", loop, stmap.master[loop].enab))
          kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAP): stmap.master[%d].am      = %d\n", loop, stmap.master[loop].am))
          kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAP): stmap.master[%d].options = 0x%08x\n", loop, stmap.master[loop].options))
        }
      }
      if (!ok)
      {
	kfree(mm);
        kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAP): kfree called for mm\n"))
        kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAP): No matching mapping found\n"))
	return(-VME_NOSTATMAP);
      }
      if (copy_to_user((void *)arg, mm, sizeof(VME_MasterMapInt_t)))
      {
	kerror(("vme_rcc_tsi(ioctl,VMEMASTERMAP): error from copy_to_user\n"))
	kfree(mm);
        kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAP): kfree called for mm\n"))
	return(-VME_EFAULT);
      }
      
      kfree(mm);
      kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAP): kfree called for mm\n"))
      break;
    }

    case VMEMASTERMAPCRCSR:   //Get a permanent mapping for generic CR/CSR access
    {
      int ok2, ok = 0, loop, loop2;
      u_long vaddr;
      
      if (!crcsr_mapped)
      {
	mm_crcsr.in.vmebus_address = 0x0;
	mm_crcsr.in.window_size = 0x1000000;
	mm_crcsr.in.address_modifier = 5;
	mm_crcsr.in.options = 0;
	mm_crcsr.used = 2;
        kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAPCRCSR): used set to 2\n"))

	for (loop = 0; loop < 8; loop++)
	{
	  if (mm_crcsr.in.vmebus_address >= stmap.master[loop].vbase && 
             (mm_crcsr.in.vmebus_address + mm_crcsr.in.window_size - 1) <= stmap.master[loop].vtop && 
	     stmap.master[loop].enab == 1 && 
	     stmap.master[loop].am == mm_crcsr.in.address_modifier &&
             stmap.master[loop].options == mm_crcsr.in.options)
          {
            kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAPCRCSR): Decoder %d matches parameters\n", loop))
            mm_crcsr.pci_address = stmap.master[loop].pbase + (mm_crcsr.in.vmebus_address - stmap.master[loop].vbase); 
            kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAPCRCSR): PCI address = 0x%016llx\n", mm_crcsr.pci_address))
            //find a free slot in the master map table
            down(&master_map_table.sem);
            ok2 = 0;
            for (loop2 = 0; loop2 < VME_MAX_MASTERMAP; loop2++)
            {
              if (master_map_table.map[loop2].pid == 0)
              {
        	kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAPCRCSR): Will use entry %d in master_map_table\n", loop2))
        	ok2 = 1;
        	break;
              }
            }
            if (!ok2)
            {
              kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAPCRCSR): master_map_table is full\n"))
              up(&master_map_table.sem);
              return(-VME_NOSTATMAP2);
            }

	    vaddr = (u_long)ioremap(mm_crcsr.pci_address, mm_crcsr.in.window_size);
            if (!vaddr)
            {
              kerror(("vme_rcc_tsi(ioctl,VMEMASTERMAPCRCSR): error from ioremap\n"))
              up(&master_map_table.sem);
              return(-VME_EFAULT);
            }
            master_map_table.map[loop2].vaddr = vaddr;
            master_map_table.map[loop2].pid = current->pid;
            pdata->mastermap[loop2] = 2;
            up(&master_map_table.sem);

            kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAPCRCSR): kernel virtual address = 0x%016lx   pid = %d\n", vaddr, current->pid))
            mm_crcsr.kvirt_address = vaddr;
	    ok = 1;
	    break;
          }
          else
          {
            kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAPCRCSR): stmap.master[%d].vbase   = 0x%08x\n", loop, (u_int)stmap.master[loop].vbase))
            kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAPCRCSR): stmap.master[%d].vtop    = 0x%08x\n", loop, (u_int)stmap.master[loop].vtop))
            kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAPCRCSR): stmap.master[%d].enab    = %d\n", loop, stmap.master[loop].enab))
            kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAPCRCSR): stmap.master[%d].am      = %d\n", loop, stmap.master[loop].am))
            kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAPCRCSR): stmap.master[%d].options = 0x%08x\n", loop, stmap.master[loop].options))
          }
	}
	if (!ok)
	  return(-VME_NOSTATMAP);
	  
	crcsr_mapped = 1;
      }          
      
      kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAPCRCSR): PCI address            = 0x%016llx\n", mm_crcsr.pci_address))
      kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAPCRCSR): kernel virtual address = 0x%016lx\n", mm_crcsr.kvirt_address))
      	
      if (copy_to_user((void *)arg, &mm_crcsr, sizeof(VME_MasterMapInt_t)))
      {
	kerror(("vme_rcc_tsi(ioctl,VMEMASTERMAPCRCSR): error from copy_to_user\n"))
	return(-VME_EFAULT);
      }
      break;
    }

    case VMEMASTERUNMAP:
    {    
      u_int ok, loop;
      u_long mm;

      if (copy_from_user(&mm, (void *)arg, sizeof(u_long)))
      {
	kerror(("vme_rcc_tsi(ioctl,VMEMASTERUNMAP): error from copy_from_user\n"))
	return(-VME_EFAULT);
      }

      down(&master_map_table.sem);
      ok = 0;
      for (loop = 0; loop < VME_MAX_MASTERMAP; loop++)
      {
	kdebug(("vme_rcc_tsi(ioctl,VMEMASTERUNMAP): master_map_table.map[%d].vaddr = 0x%016lx  <-> mm = 0x%016lx\n", loop, master_map_table.map[loop].vaddr, mm));
	kdebug(("vme_rcc_tsi(ioctl,VMEMASTERUNMAP): master_map_table.map[%d].pid = %d <-> current->pid = %d\n", loop, master_map_table.map[loop].pid, current->pid));
      
        if (master_map_table.map[loop].vaddr == mm) 
        {
          kdebug(("vme_rcc_tsi(ioctl,VMEMASTERUNMAP): Entry %d in master_map_table matches\n", loop))
          ok = 1;
          iounmap((void *)mm);
          kdebug(("vme_rcc_tsi(ioctl,VMEMASTERUNMAP): unmapping address 0x%016lx\n", mm))
          master_map_table.map[loop].vaddr = 0;
          master_map_table.map[loop].pid = 0;
          pdata->mastermap[loop] = 0;
          break;
        }
      }
      up(&master_map_table.sem);
  
      if (!ok)
      {
        kerror(("vme_rcc_tsi(ioctl,VMEMASTERUNMAP): No entry in master_map_table matched for address 0x%016lx\n", mm))
        return(-VME_IOUNMAP);
      }
      break;
    }

    case VMEMASTERMAPDUMP:
    {
      char *buf;
      int len, loop;

      buf = (char *)kmalloc(TEXT_SIZE1, GFP_KERNEL);
      kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAPDUMP): %d bytes allocated [buf](TEXT_SIZE1))\n", TEXT_SIZE1))
      if (buf == NULL)
      {
        kerror(("vme_rcc_tsi(ioctl,VMEMASTERMAPDUMP): error from kmalloc\n"))
        return(-VME_KMALLOC);
      }

      len = 0;
      len += sprintf(buf + len, "Master mapping:\n");
      len += sprintf(buf + len, "LSI                  VME address range                  PCI address range   EN   AM\n");
      for (loop = 0; loop < 8; loop++)
	len += sprintf(buf + len, "%d    %016llx-%016llx  %016llx-%016llx    %d    %d\n"
        	      ,loop
	 	      ,stmap.master[loop].vbase
		      ,stmap.master[loop].vtop
		      ,stmap.master[loop].pbase
		      ,stmap.master[loop].ptop
		      ,stmap.master[loop].enab
                      ,stmap.master[loop].am);
      if (copy_to_user((void *)arg, buf, TEXT_SIZE1 * sizeof(char)))
      {
	kerror(("vme_rcc_tsi(ioctl,VMEMASTERMAPDUMP): error from copy_to_user\n"))
	return(-VME_EFAULT);
      }
            
      kfree(buf);
      kdebug(("vme_rcc_tsi(ioctl,VMEMASTERMAPDUMP): kfree called for buf\n"))
      break;
    }
     
    case VMESYSFAILLINK:
    {
      down(&sysfail_link.proc_sem);      //MJ: Rubini recommands down_interruptible()

      if (sysfail_link.pid)              // Already linked by any other process?
      {
        up(&sysfail_link.proc_sem);
        return(-VME_SYSFAILTBLFULL);
      }

      sysfail_link.pid = current->pid;
      pdata->sysfail_dsc_flag = 1;
      sema_init(&sysfail_link.sem, 0);   // initialise semaphore count
      up(&sysfail_link.proc_sem);
      break;
    }

    case VMESYSFAILUNLINK:
    {
      down(&sysfail_link.proc_sem);

      if (pdata->sysfail_dsc_flag == 0)  //Check if the current process (group) has linked it
      {
        up(&sysfail_link.proc_sem);
        return(-VME_SYSFAILTBLNOTLINKED);
      }

      sysfail_link.pid = 0;
      sysfail_link.sig = 0;
      pdata->sysfail_dsc_flag = 0;
      up(&sysfail_link.proc_sem);
      break;
    }

    case VMESYSFAILREGISTERSIGNAL:
    {
      VME_RegSig_t *VME_SysfailRegSig;
      
      VME_SysfailRegSig = (VME_RegSig_t *)kmalloc(sizeof(VME_RegSig_t), GFP_KERNEL);
      kdebug(("vme_rcc_tsi(ioctl,VMESYSFAILREGISTERSIGNAL): 0x%08lx bytes allocated [VME_SysfailRegSig](sizeof(VME_RegSig_t)))\n", sizeof(VME_RegSig_t)))
      if (VME_SysfailRegSig == NULL)
      {
	kerror(("vme_rcc_tsi(ioctl,VMESYSFAILREGISTERSIGNAL): error from kmalloc\n"));
        return(-VME_KMALLOC);
      }

      if (copy_from_user(VME_SysfailRegSig, (void *)arg, sizeof(VME_RegSig_t)))
      {
        kerror(("vme_rcc_tsi(ioctl,VMESYSFAILREGISTERSIGNAL): error from copy_from_user\n"))
	kfree(VME_SysfailRegSig);
        kdebug(("vme_rcc_tsi(ioctl,VMESYSFAILREGISTERSIGNAL): kfree called for VME_SysfailRegSig\n"))
        return(-VME_EFAULT);
      }
      kdebug(("vme_rcc_tsi(ioctl,VMESYSFAILREGISTERSIGNAL): signal # = %d\n", VME_SysfailRegSig->signum))
      down(&sysfail_link.proc_sem);

      if (sysfail_link.pid != current->pid)
      {
        up(&sysfail_link.proc_sem);
	kfree(VME_SysfailRegSig);
        kdebug(("vme_rcc_tsi(ioctl,VMESYSFAILREGISTERSIGNAL): kfree called for VME_SysfailRegSig\n"))
        return(-VME_SYSFAILNOTLINKED);
      }
      if (VME_SysfailRegSig->signum)        // register
        sysfail_link.sig = VME_SysfailRegSig->signum;
      else                                  // unregister
        sysfail_link.sig = 0;
      kfree(VME_SysfailRegSig);
      kdebug(("vme_rcc_tsi(ioctl,VMESYSFAILREGISTERSIGNAL): kfree called for VME_SysfailRegSig\n"))
      up(&sysfail_link.proc_sem);
      break;
    }
    
    case VMESYSFAILWAIT:   
    {
      struct timer_list int_timer;
      int ret, use_timer = 0, result = 0;
      VME_WaitInt_t *VME_WaitInt;
      
      VME_WaitInt = (VME_WaitInt_t *)kmalloc(sizeof(VME_WaitInt_t), GFP_KERNEL);
      kdebug(("vme_rcc_tsi(ioctl,VMESYSFAILWAIT): 0x%08lx bytes allocated [VME_WaitInt](sizeof(VME_WaitInt_t)))\n", sizeof(VME_WaitInt_t)))
      if (VME_WaitInt == NULL)
      {
	kerror(("vme_rcc_tsi(ioctl,VMESYSFAILWAIT): error from kmalloc\n"));
        return(-VME_KMALLOC);
      }

      if (copy_from_user(VME_WaitInt, (void *)arg, sizeof(VME_WaitInt_t)))
      {
        kerror(("vme_rcc_tsi(ioctl,VMESYSFAILWAIT): error from copy_from_user\n"))
 	kfree(VME_WaitInt);
        kdebug(("vme_rcc_tsi(ioctl,VMESYSFAILWAIT): kfree called for VME_WaitInt\n"))
	return(-VME_EFAULT);
      }
      kdebug(("vme_rcc_tsi(ioctl,VMESYSFAILWAIT): timeout = %d\n", VME_WaitInt->timeout))

      if (VME_WaitInt->timeout > 0) 
      {
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,0,0)
        init_timer(&int_timer);
        int_timer.function = vme_intSysfailTimeout;
        int_timer.data = (u_long)VME_WaitInt; 
        int_timer.expires = jiffies + VME_WaitInt->timeout * HZ / 1000;  //convert from ms to kernel-jiffis (pretty pointless for the current 1 ms jiffies)
        add_timer(&int_timer);
#else
        timer_setup(&int_timer, vme_intSysfailTimeout, 0);
        mod_timer(&int_timer, jiffies + VME_WaitInt->timeout * HZ / 1000); //convert from ms to kernel-jiffis (pretty pointless for the current 1 ms jiffies)
        //MJ: it is not yet clear how the VME_WaitInt structure can be communicated to vme_intSysfailTimeout
#endif          
        use_timer = 1;
      }

      if (VME_WaitInt->timeout) 		
      {
        kdebug(("vme_rcc_tsi(ioctl,VMESYSFAILWAIT): Before Down, semaphore = %d\n", sysfail_link.sem.count))
        ret = down_interruptible(&sysfail_link.sem);	// wait ..
        if (signal_pending(current))
        {					// interrupted system call
          kdebug(("vme_rcc_tsi(ioctl,VMESYSFAILWAIT): Interrupted by signal\n"))
          result = -VME_INTBYSIGNAL;		// or ERESTARTSYS ?
        }
        else if ((use_timer == 1) && (VME_WaitInt->timeout == 0))
          result = -VME_TIMEOUT;
      }
      
      if (use_timer == 1)
        del_timer(&int_timer);	// OK also if timer ran out

      if (copy_to_user((void *)arg, VME_WaitInt, sizeof(VME_WaitInt_t)))  //MJ: What are we copying back to the user?
      {
	kerror(("vme_rcc_tsi(ioctl,VMESYSFAILWAIT): error from copy_to_user\n"))
 	kfree(VME_WaitInt);
        kdebug(("vme_rcc_tsi(ioctl,VMESYSFAILWAIT): kfree called for VME_WaitInt\n"))
        return(-VME_EFAULT);
      }

      kfree(VME_WaitInt);
      kdebug(("vme_rcc_tsi(ioctl,VMESYSFAILWAIT): kfree called for VME_WaitInt\n"))
      return (result);
      break;	// dummy ..
    }

    // SYSFAIL is latched in lint_stat (I believe). Therefore, even if SYSFAIL is removed from VMEbus
    // the interrupt status bit has to be cleared in LINT_STAT  mjmj
    // this will only work if SYSFAIL is not on the bus


    case VMESYSFAILREENABLE:
    {
      u_int inteo;

      tsi->intc = bswap(0x200);                                 // clear the interrupt
      inteo = bswap(tsi->inteo) | 0x200;	                // read enabled irq bits
      kdebug(("vme_rcc_tsi(ioctl,VMESYSFAILREENABLE): inteo after masking = 0x%x\n", inteo))
      tsi->inteo = bswap(inteo);	                        // reenable SYSFAIL
      break;
    }

    case VMESYSFAILPOLL:
    {
      u_int flag, vstat;

      vstat = bswap(tsi->vstat);
      kdebug(("vme_rcc_tsi(ioctl,VMESYSFAILPOLL): vstat = 0x%x\n", vstat))
      if (vstat & 0x400)	// the SYSFAIL bit
        flag = 1;
      else
        flag = 0;

      if (copy_to_user((void *)arg, &flag, sizeof(u_int)))
      {
	kerror(("vme_rcc_tsi(ioctl,VMESYSFAILPOLL): error from copy_to_user\n"))
        return(-VME_EFAULT);
      }
      break;
    }
    
    case VMESYSFAILSET:
    {   
      u_int vctrl;

      vctrl = bswap(tsi->vctrl);
      vctrl |= 0x8000;				// bit SYSFAIL
      kdebug(("vme_rcc_tsi(ioctl,VMESYSFAILSET): vctrl = %x\n", vctrl))
      tsi->vctrl = bswap(vctrl);
      break;
    }

    case VMESYSFAILRESET:
    {   
      u_int vctrl;

      vctrl = bswap(tsi->vctrl);
      vctrl &= 0xffff7fff;		       // bit SYSFAIL
      kdebug(("vme_rcc_tsi(ioctl,VMESYSFAILRESET): vctrl = %x\n", vctrl))
      tsi->vctrl = bswap(vctrl);
      break;
    }

    case VMEBERRREGISTERSIGNAL:   
    {
      u_int i;
      int index;
      VME_RegSig_t *VME_BerrRegSig;

      VME_BerrRegSig = (VME_RegSig_t *)kmalloc(sizeof(VME_RegSig_t), GFP_KERNEL);
      kdebug(("vme_rcc_tsi(ioctl,VMEBERRREGISTERSIGNAL): 0x%08lx bytes allocated [VME_BerrRegSig](sizeof(VME_RegSig_t)))\n", sizeof(VME_RegSig_t)))
      if (VME_BerrRegSig == NULL)
      {
	kerror(("vme_rcc_tsi(ioctl,VMEBERRREGISTERSIGNAL): error from kmalloc\n"));
        return(-VME_KMALLOC);
      }

      if (copy_from_user(VME_BerrRegSig, (void *)arg, sizeof(VME_RegSig_t)))
      {
	kerror(("vme_rcc_tsi(ioctl,VMEBERRREGISTERSIGNAL): error from copy_from_user\n"))
	kfree(VME_BerrRegSig);
        kdebug(("vme_rcc_tsi(ioctl,VMEBERRREGISTERSIGNAL): kfree called for VME_BerrRegSig\n"))
	return(-VME_EFAULT);
      }

      kdebug(("vme_rcc_tsi(ioctl,VMEBERRREGISTERSIGNAL): signal # = %d\n", VME_BerrRegSig->signum))
      down(&berr_proc_table.proc_sem);

      if (VME_BerrRegSig->signum)
      {
        // find a free slot
        index = -1;
        for (i = 0; i < VME_MAX_BERR_PROCS; i++)
        {
          if (berr_proc_table.berr_link_dsc[i].pid == 0)
          {
            index = i;
            break;
          }
        }
        if (index == -1)
        {
          up(&berr_proc_table.proc_sem);
	  kfree(VME_BerrRegSig);
          kdebug(("vme_rcc_tsi(ioctl,VMEBERRREGISTERSIGNAL): kfree called for VME_BerrRegSig\n"))
          return(-VME_BERRTBLFULL);
        }

        berr_proc_table.berr_link_dsc[index].pid = current->pid;
        berr_proc_table.berr_link_dsc[index].sig = VME_BerrRegSig->signum;
        berr_dsc.flag = 0; 	                // enable BERR latching via signal
				                // race: could occur while a(nother) bus error is latched and not yet serviced ..
        pdata->berr_dsc_flag[index] = 1;	// remember which file berr belongs to
      }
      else		                        // unregister
      {
        index = -1;
        for (i = 0; i < VME_MAX_BERR_PROCS; i++)
        {
          if (pdata->berr_dsc_flag[i] == 1)  //MJ: Are we sure there is only one "1" in the berr_dsc_flag array?
          {
            index = i;
            break;
          }
        }
        if (index == -1)
        {
          up(&berr_proc_table.proc_sem);
	  kfree(VME_BerrRegSig);
          kdebug(("vme_rcc_tsi(ioctl,VMEBERRREGISTERSIGNAL): kfree called for VME_BerrRegSig\n"))
          return(-VME_BERRNOTFOUND);
        }

        berr_proc_table.berr_link_dsc[index].pid = 0;
        berr_proc_table.berr_link_dsc[index].sig = 0;
        pdata->berr_dsc_flag[index] = 0;
      }

      up(&berr_proc_table.proc_sem);
      kfree(VME_BerrRegSig);
      kdebug(("vme_rcc_tsi(ioctl,VMEBERRREGISTERSIGNAL): kfree called for VME_BerrRegSig\n"))
      break;
    }

    case VMEBERRINFO:
    {
      u_long flags;

      if (berr_dsc.flag == 0)		// slight risk of race with ISR ..
        return(-VME_NOBUSERROR);

      local_irq_save(flags);            //MJ: for 2.6 see p274 & p275

      pdata->VME_BerrInfo.vmebus_address   = berr_dsc.vmeadd;
      pdata->VME_BerrInfo.address_modifier = berr_dsc.am;
      pdata->VME_BerrInfo.multiple         = berr_dsc.multiple;
      pdata->VME_BerrInfo.lword            = berr_dsc.lword;
      pdata->VME_BerrInfo.iack             = berr_dsc.iack;
      pdata->VME_BerrInfo.ds0              = berr_dsc.ds0;
      pdata->VME_BerrInfo.ds1              = berr_dsc.ds1;
      pdata->VME_BerrInfo.wr               = berr_dsc.wr;

      berr_dsc.vmeadd   = 0;
      berr_dsc.am       = 0;
      berr_dsc.multiple = 0;
      berr_dsc.iack     = 0;
      berr_dsc.flag     = 0;
      berr_dsc.lword    = 0;
      berr_dsc.ds0      = 0;
      berr_dsc.ds1      = 0;
      berr_dsc.wr       = 0;

      local_irq_restore(flags);

      if (copy_to_user((void *)arg, &pdata->VME_BerrInfo, sizeof(VME_BusErrorInfo_t)))
      {
	kerror(("vme_rcc_tsi(ioctl,VMEBERRINFO): error from copy_to_user\n"))
	return(-VME_EFAULT);
      }
      break;
    }
		
    case VMESLAVEMAP:   
    {
      int loop, ok = 0;
      VME_SlaveMapInt_t *sm;

      sm = (VME_SlaveMapInt_t *)kmalloc(sizeof(VME_SlaveMapInt_t), GFP_KERNEL);
      kdebug(("vme_rcc_tsi(ioctl,VMESLAVEMAP): 0x%08lx bytes allocated [sm](sizeof(VME_SlaveMapInt_t)))\n", sizeof(VME_SlaveMapInt_t)))
      if (sm == NULL)
      {
	kerror(("vme_rcc_tsi(ioctl,VMESLAVEMAP): error from kmalloc\n"));
        return(-VME_KMALLOC);
      }

      if (copy_from_user(sm, (void *)arg, sizeof(VME_SlaveMapInt_t)))
      {
	kerror(("vme_rcc_tsi(ioctl,VMESLAVEMAP): error from copy_from_user\n"))
	kfree(sm);
        kdebug(("vme_rcc_tsi(ioctl,VMESLAVEMAP): kfree called for sm\n"))
	return(-VME_EFAULT);
      }
      kdebug(("vme_rcc_tsi(ioctl,VMESLAVEMAP): mm.in.system_iobus_address = 0x%016llx\n", sm->in.system_iobus_address))
      kdebug(("vme_rcc_tsi(ioctl,VMESLAVEMAP): mm.in.window_size          = 0x%08x\n", sm->in.window_size))
      kdebug(("vme_rcc_tsi(ioctl,VMESLAVEMAP): mm.in.address_width        = 0x%08x\n", sm->in.address_width))
      kdebug(("vme_rcc_tsi(ioctl,VMESLAVEMAP): mm.in.options              = 0x%08x\n", sm->in.options))

      for(loop = 0; loop < 8; loop++)
      {
	kdebug(("vme_rcc_tsi(ioctl,VMESLAVEMAP): stmap.slave[%d].pbase = 0x%016llx\n", loop, stmap.slave[loop].pbase))
	kdebug(("vme_rcc_tsi(ioctl,VMESLAVEMAP): stmap.slave[%d].ptop  = 0x%016llx\n", loop, stmap.slave[loop].ptop))
	kdebug(("vme_rcc_tsi(ioctl,VMESLAVEMAP): stmap.slave[%d].enab  = 0x%08x\n", loop, stmap.slave[loop].enab))
	kdebug(("vme_rcc_tsi(ioctl,VMESLAVEMAP): stmap.slave[%d].space = 0x%08x\n", loop, stmap.slave[loop].space))
	if (sm->in.system_iobus_address >= stmap.slave[loop].pbase && 
           (sm->in.system_iobus_address + sm->in.window_size) <= stmap.slave[loop].ptop && 
	   stmap.slave[loop].enab == 1 && 
	   stmap.slave[loop].am == sm->in.address_width)
        {
	  kdebug(("vme_rcc_tsi(ioctl,VMESLAVEMAP): Decoder %d matches parameters\n", loop))
	  sm->vme_address = stmap.slave[loop].vbase + (sm->in.system_iobus_address - stmap.slave[loop].pbase);
          kdebug(("vme_rcc_tsi(ioctl,VMESLAVEMAP): VME address = 0x%08x\n", sm->vme_address))
	  ok = 1;
	  break;
        }
      }
      if (!ok)
      {
	kerror(("vme_rcc_tsi(ioctl,VMESLAVEMAP): No matching mapping found\n"))
	kfree(sm);
        kdebug(("vme_rcc_tsi(ioctl,VMESLAVEMAP): kfree called for sm\n"))
	return(-VME_NOSTATMAP);   
      }
      if (copy_to_user((void *)arg, sm, sizeof(VME_SlaveMapInt_t)))
      {
	kerror(("vme_rcc_tsi(ioctl,VMESLAVEMAP): error from copy_to_user\n"))
	kfree(sm);
        kdebug(("vme_rcc_tsi(ioctl,VMESLAVEMAP): kfree called for sm\n"))
	return(-VME_EFAULT);
      }
      kfree(sm);
      kdebug(("vme_rcc_tsi(ioctl,VMESLAVEMAP): kfree called for sm\n"))
      break;
    } 

    case VMESLAVEMAPDUMP: 
    {  
      char *buf;
      int len, loop;

      buf = (char *)kmalloc(TEXT_SIZE1, GFP_KERNEL);
      kdebug(("vme_rcc_tsi(ioctl,VMESLAVEMAPDUMP): 0x%08x bytes allocated [buf](TEXT_SIZE1))\n", TEXT_SIZE1))
      if (buf == NULL)
      {
        kerror(("vme_rcc_tsi(ioctl,VMESLAVEMAPDUMP): error from kmalloc\n"))
        return(-VME_KMALLOC);
      }
      len = 0;
      len += sprintf(buf + len, "Slave mapping:\n");
      len += sprintf(buf + len, "VSI  VME address range  PCI address range   EN   AM\n");
      for (loop = 0 ;loop < 8; loop++)
	len += sprintf(buf + len, "%d    %016llx-%016llx  %016llx-%016llx    %d    %d\n"
          	      ,loop
		      ,stmap.slave[loop].vbase
	  	      ,stmap.slave[loop].vtop
		      ,stmap.slave[loop].pbase
		      ,stmap.slave[loop].ptop
		      ,stmap.slave[loop].enab
		      ,stmap.slave[loop].am);
      if (copy_to_user((void *)arg, buf, TEXT_SIZE1*sizeof(char)))
      {
	kerror(("vme_rcc_tsi(ioctl,VMESLAVEMAPDUMP): error from copy_to_user\n"))
	return(-VME_EFAULT);
      }
      kfree(buf);
      kdebug(("vme_rcc_tsi(ioctl,VMESLAVEMAPDUMP): kfree called for buf\n"))
      break;
    }

    case VMESCSAFE:
    {    
      u_int addr, multi, am, *lptr, ldata;
      u_short *sptr, sdata;
      u_char *bptr, bdata;
      int ret;
      VME_SingleCycle_t *sc;

      sc = (VME_SingleCycle_t *)kmalloc(sizeof(VME_SingleCycle_t), GFP_KERNEL);
      kdebug(("vme_rcc_tsi(ioctl,VMESCSAFE): 0x%08lx bytes allocated [sc](sizeof(VME_SingleCycle_t)))\n", sizeof(VME_SingleCycle_t)))
      if (sc == NULL)
      {
	kerror(("vme_rcc_tsi(ioctl,VMESCSAFE): error from kmalloc\n"));
        return(-VME_KMALLOC);
      }

      if (copy_from_user(sc, (void *)arg, sizeof(VME_SingleCycle_t)))
      {
	kerror(("vme_rcc_tsi(ioctl,VMESCSAFE): error from copy_from_user\n"))
	kfree(sc);
        kdebug(("vme_rcc_tsi(ioctl,VMESCSAFE): kfree called for sc\n"))
	return(-VME_EFAULT);
      }
      kdebug(("vme_rcc_tsi(ioctl,VMESCSAFE): sc.kvirt_address = 0x%016lx\n", sc->kvirt_address))
      kdebug(("vme_rcc_tsi(ioctl,VMESCSAFE): sc.offset        = 0x%08x\n", sc->offset))
      kdebug(("vme_rcc_tsi(ioctl,VMESCSAFE): sc.nbytes        = %d\n", sc->nbytes))
      kdebug(("vme_rcc_tsi(ioctl,VMESCSAFE): sc.rw            = %d\n", sc->rw))
      kdebug(("vme_rcc_tsi(ioctl,VMESCSAFE): pid              = %d\n", current->pid))

      spin_lock_irqsave(&slock, irq_flags); 

      //Disable the CCT BERR logic for the duration of the safe single cycle
      mask_cct_berr();
        
      if (sc->rw == 1)             //read
      {
	if (sc->nbytes == 4)       //D32
	{
	  lptr = (u_int *)(sc->kvirt_address + sc->offset);
	  ldata = *lptr;
	  sc->data = (u_int)ldata;
          kdebug(("vme_rcc_tsi(ioctl,VMESCSAFE): D32 read. Data = 0x%08x = 0x%08x\n", ldata, sc->data))
	}
	else if (sc->nbytes == 2)  //D16
        {
	  sptr = (u_short *)(sc->kvirt_address + sc->offset);
	  sdata = *sptr;
	  sc->data = (u_int)sdata;
          kdebug(("vme_rcc_tsi(ioctl,VMESCSAFE): D16 read. Data = 0x%08x\n", sdata))
	}
	else                      //D8
	{
	  bptr = (u_char *)(sc->kvirt_address + sc->offset);
	  bdata = *bptr;
	  sc->data = (u_int)bdata;
          kdebug(("vme_rcc_tsi(ioctl,VMESCSAFE): D8 read. Data = 0x%08x\n", bdata))
	}
      }
      else                         //write
      {
	if (sc->nbytes == 4)       //D32
	{
	  lptr = (u_int *)(sc->kvirt_address + sc->offset);
	  ldata = sc->data;
	  *lptr = ldata;
	}
	else if (sc->nbytes == 2)  //D16
	{
	  sptr = (u_short *)(sc->kvirt_address + sc->offset);
	  sdata = (u_short)sc->data;
	  *sptr = sdata;
	}
	else                       //D8
	{
	  bptr = (u_char *)(sc->kvirt_address + sc->offset);
	  bdata = (u_char)sc->data;
	  *bptr = bdata;
	}        
      }

      //MJ: Do we have to wait a bit for bus errors to arrive?  mjmj
      //MJ: What to do with addr, multi and am in case of BERR?
      ret = berr_check(&addr, &multi, &am);
      if (ret)
      {
	kerror(("vme_rcc_tsi(ioctl,VMESCSAFE): Bus error received. addr=0x%08x  multi=%d  am=0x%02x\n", addr, multi, am))
	init_cct_berr();
	kfree(sc);
        kdebug(("vme_rcc_tsi(ioctl,VMESCSAFE): kfree called for sc\n"))
        spin_unlock_irqrestore(&slock, irq_flags); 
	return(-VME_BUSERROR);
      }
      
      //Reenable the CCT BERR logic and clear the BERR flag in the CCT logic
      init_cct_berr();
      spin_unlock_irqrestore(&slock, irq_flags); 

      if (copy_to_user((void *)arg, sc, sizeof(VME_SingleCycle_t)))
      {
	kerror(("vme_rcc_tsi(ioctl,VMESCSAFE): error from copy_to_user\n"))
	kfree(sc);
        kdebug(("vme_rcc_tsi(ioctl,VMESCSAFE): kfree called for sc\n"))
	return(-VME_EFAULT);
      }
      kfree(sc);
      kdebug(("vme_rcc_tsi(ioctl,VMESCSAFE): kfree called for sc\n"))

      break;
    } 

    case VMEDMASTART:
    {
      VME_DMAstart_t *dma_params;
      u_int windex;
     
      dma_params = (VME_DMAstart_t *)kmalloc(sizeof(VME_DMAstart_t), GFP_KERNEL);     //MJ: can't we allocate this array statically?
      kdebug(("vme_rcc_tsi(ioctl,VMEDMASTART): 0x%08lx bytes allocated [dma_params](sizeof(VME_DMAstart_t)))\n", sizeof(VME_DMAstart_t)))
      if (dma_params == NULL)
      {
	kerror(("vme_rcc_tsi(ioctl,VMEDMASTART): error from kmalloc\n"));
        return(-VME_KMALLOC);
      }
      
      if (copy_from_user(dma_params, (void *)arg, sizeof(VME_DMAstart_t)))  
      {
	kerror(("vme_rcc_tsi(ioctl,VMEDMASTART(%d)): error from copy_from_user\n", current->pid))
        kfree(dma_params);
        kdebug(("vme_rcc_tsi(ioctl,VMEDMASTART): kfree called for dma_params\n"))
	return(-VME_EFAULT);
      }

      kdebug(("vme_rcc_tsi(ioctl,VMEDMASTART(%d)): PCI address of chain = 0x%016lx\n", current->pid, dma_params->paddr))

      spin_lock_irqsave(&dmalock, dma_irq_flags); 

      if(dma_todo_num == VME_DMATODOSIZE || dmas_pending == VME_DMATODOSIZE)
      {
        kerror(("vme_rcc_tsi(ioctl,VMEDMASTART(%d)): no memory left in to-do list\n", current->pid))
        spin_unlock_irqrestore(&dmalock, dma_irq_flags); 
	return(-VME_NODOMEMEM);
      }
      dma_todo_num++;
      dmas_pending++;
      windex = dma_todo_write;
      dma_todo_write++;
      if (dma_todo_write == VME_DMATODOSIZE)
        dma_todo_write = 0;	
     
      dma_list_todo[windex].paddr = dma_params->paddr;
      dma_list_todo[windex].pid   = current->pid;
      kdebug(("vme_rcc_tsi(ioctl,VMEDMASTART(%d)): using entry %d in to-do list.\n", current->pid, windex))
      dma_todo_total++;
      spin_unlock_irqrestore(&dmalock, dma_irq_flags); 
      
      kfree(dma_params);
      kdebug(("vme_rcc_tsi(ioctl,VMEDMASTART): kfree called for dma_params\n"))

      start_next_dma(0);
      kdebug(("vme_rcc_tsi(ioctl,VMEDMASTART(%d)): done\n", current->pid)) 
      break;        
    }

    case VMEDMAPOLL:
    {      
      int serror, dummyerror;
      u_int jiffytowait;
      VME_DMAhandle_t *dma_poll_params;  
  
      dma_poll_params = (VME_DMAhandle_t *)kmalloc(sizeof(VME_DMAhandle_t), GFP_KERNEL);
      kdebug(("vme_rcc_tsi(ioctl,VMEDMAPOLL)): 0x%08lx bytes allocated [dma_poll_params](sizeof(VME_DMAhandle_t)))\n", sizeof(VME_DMAhandle_t)))
      if (dma_poll_params == NULL)
      {
 	kerror(("vme_rcc_tsi(ioctl,VMEDMAPOLL(%d)): error from kmalloc\n", current->pid));
	return(-VME_KMALLOC);
      }
      
      if (copy_from_user(dma_poll_params, (void *)arg, sizeof(VME_DMAhandle_t)))   
      {
	kerror(("vme_rcc_tsi(ioctl,VMEDMAPOLL(%d)): error from copy_from_user\n", current->pid))
	kfree(dma_poll_params);
        kdebug(("vme_rcc_tsi(ioctl,VMEDMAPOLL): kfree called for dma_poll_params\n"))
	return(-VME_EFAULT);
      }
        	
      //Special cases:
      //timeoutval = 0:  Just check if the DMA is done or not
      //timeoutval = -1: Wait forever
      kdebug(("vme_rcc_tsi(ioctl,VMEDMAPOLL(%d)): timeoutval = %d\n", current->pid, dma_poll_params->timeoutval)) 

      if (dma_poll_params->timeoutval == 0)
      {
        kdebug(("vme_rcc_tsi(ioctl,VMEDMAPOLL(%d)): Checking if DMA is ready\n", current->pid))      
        serror = dma_is_done(dma_poll_params);
	if (serror == 0)  //still BUSY
	{      
	  dma_poll_params->ctrl    = 0x87654321;  // This actually means "don't look at it"
          dma_poll_params->counter = 0x87654321;  // This actually means "don't look at it"
          dma_poll_params->dclau   = 0x87654321;  // This actually means "don't look at it"
          dma_poll_params->dclal   = 0x87654321;  // This actually means "don't look at it"

          dummyerror = copy_to_user((void *)arg, dma_poll_params, sizeof(VME_DMAhandle_t)); 
	  if (dummyerror) 
            kerror(("vme_rcc_tsi(ioctl,VMEDMAPOLL(%d)): error from copy_to_user\n", current->pid))
          
	  kerror(("vme_rcc_tsi(ioctl,VMEDMAPOLL(%d)): DMA busy\n", current->pid))
	  kfree(dma_poll_params);
          kdebug(("vme_rcc_tsi(ioctl,VMEDMAPOLL): kfree called for dma_poll_params\n"))
	  return(-VME_DMABUSY);   //exit case 4
	}
	else             //DMA done
	{
	  dummyerror = copy_to_user((void *)arg, dma_poll_params, sizeof(VME_DMAhandle_t));  
	  if (dummyerror) 
            kerror(("vme_rcc_tsi(ioctl,VMEDMAPOLL(%d)): error from copy_to_user\n", current->pid))

          kerror(("vme_rcc_tsi(ioctl,VMEDMAPOLL(%d)): DMA done\n", current->pid))
	  kfree(dma_poll_params);
          kdebug(("vme_rcc_tsi(ioctl,VMEDMAPOLL): kfree called for dma_poll_params\n"))
	  return(-VME_SUCCESS);  //exit case 3 
	}
      }
      else
      {      
        kdebug(("vme_rcc_tsi(ioctl,VMEDMAPOLL(%d)): Starting to wait\n", current->pid))      
	if (dma_poll_params->timeoutval == -1)
          serror = wait_event_interruptible(dma_wait, dma_is_done(dma_poll_params));
	else
	{
	  jiffytowait = dma_poll_params->timeoutval * HZ / 1000;  
	  kdebug(("vme_rcc_tsi(ioctl,VMEDMAPOLL(%d)): jiffytowait = %d with HZ = %d\n", current->pid, jiffytowait, HZ))      
          serror = wait_event_interruptible_timeout(dma_wait, dma_is_done(dma_poll_params), jiffytowait);
        }

	kdebug(("vme_rcc_tsi(ioctl,VMEDMAPOLL(%d)): Woken up from wait\n", current->pid))      
	if (serror == -ERESTARTSYS)
	{
          kerror(("vme_rcc_tsi(ioctl,VMEDMAPOLL(%d)): wait_event_interruptible(_timeout) was interrupted by a signal\n", current->pid))      
          return(-VME_ERESTARTSYS);  //exit case 6
	}

	kdebug(("vme_rcc_tsi(ioctl,VMEDMAPOLL(%d)): wait_event_interruptible(_timeout) returns %d\n", current->pid, serror))      

	if (serror == 0 && dma_poll_params->timeoutval != -1)  //Time-out
	{      
          //MJ: if we have a timeout we should also clear the entry of the respective request in the DMA to-do-list

          kerror(("vme_rcc_tsi(ioctl,VMEDMAPOLL(%d)): DMA time out\n", current->pid))      
	  dma_poll_params->ctrl    = 0x87654321;  // This actually means "don't look at it"
          dma_poll_params->counter = 0x87654321;  // This actually means "don't look at it"
          dma_poll_params->dclau   = 0x87654321;  // This actually means "don't look at it"
	  dma_poll_params->dclal   = 0x87654321;  // This actually means "don't look at it"

	  //Clear the DMA controller   
          tsi->dctl0 = bswap(0x08000000);
	  
	  //Now that the DMA controller is cleared we will not get a EOT interrupt any more. Therefore we
	  //have to declare the DMA controller to be free.
	  //MJ: as dma_is_free must be set to "0" right now (a DMA is active) we can touch it without requesting the spinlock and don't have to be affraid of a race condition
	  dma_is_free = 1;

          dummyerror = copy_to_user((void *)arg, dma_poll_params, sizeof(VME_DMAhandle_t));  
	  if (dummyerror) 
            kerror(("vme_rcc_tsi(ioctl,VMEDMAPOLL(%d)): error from copy_to_user\n", current->pid))

          kerror(("vme_rcc_tsi(ioctl,VMEDMAPOLL(%d)): done with timeout\n", current->pid))
	  kfree(dma_poll_params);
          kdebug(("vme_rcc_tsi(ioctl,VMEDMAPOLL): kfree called for dma_poll_params\n"))
	  return(-VME_TIMEOUT);   //exit case 1
	}
      }
      
      kdebug(("vme_rcc_tsi(ioctl,VMEDMAPOLL(%d)): Woken up from wait 2\n", current->pid))
      
      //exit case 2 and 5      
      if (copy_to_user((void *)arg, dma_poll_params, sizeof(VME_DMAhandle_t)))
      {
	kerror(("vme_rcc_tsi(ioctl,VMEDMAPOLL(%d)): error from copy_to_user\n", current->pid))
	kfree(dma_poll_params);
        kdebug(("vme_rcc_tsi(ioctl,VMEDMAPOLL): kfree called for dma_poll_params\n"))
	return(-VME_EFAULT);
      }
	
      kfree(dma_poll_params);
      kdebug(("vme_rcc_tsi(ioctl,VMEDMAPOLL): kfree called for dma_poll_params\n"))
      kdebug(("vme_rcc_tsi(ioctl,VMEDMAPOLL(%d)): done\n", current->pid))

      break;
    }

    case VMEDMADUMP: 
    {  
      char *buf;
      u_int data, value, value2;
      int len;
      
      buf = (char *)kmalloc(TEXT_SIZE2, GFP_KERNEL);
      kdebug(("vme_rcc_tsi(ioctl,VMESLAVEMAPDUMP): %d bytes allocated [buf](TEXT_SIZE2))\n", TEXT_SIZE2))
      if (buf == NULL)
      {
        kerror(("vme_rcc_tsi(ioctl,VMESLAVEMAPDUMP): error from kmalloc\n"))
        return(-VME_KMALLOC);
      }
      len = 0; 
      
      len += sprintf(buf + len, "NOTE: Only DMA controller #0 is currently supported\n");
  
      data = bswap(tsi->dctl0);
      len += sprintf(buf + len, "dctl0 (0x%08x):\n", data);
      len += sprintf(buf + len, "DMA mode:                              %s\n", (data & 0x800000)?"Direct":"Linked list"); 
      len += sprintf(buf + len, "Deliver buffered data on VMEbus error: %s\n", (data & 0x20000)?"Yes":"No");
      len += sprintf(buf + len, "Deliver buffered data on PCI error:    %s\n", (data & 0x10000)?"Yes":"No");

      value = (data >> 12) & 0x7;
      if (value == 0) len += sprintf(buf + len, "VMEbus (read) block size:              32 bytes\n");
      if (value == 1) len += sprintf(buf + len, "VMEbus (read) block size:              64 bytes\n");
      if (value == 2) len += sprintf(buf + len, "VMEbus (read) block size:              128 bytes\n");
      if (value == 3) len += sprintf(buf + len, "VMEbus (read) block size:              256 bytes\n");
      if (value == 4) len += sprintf(buf + len, "VMEbus (read) block size:              512 bytes\n");
      if (value == 5) len += sprintf(buf + len, "VMEbus (read) block size:              1024 bytes\n");
      if (value == 6) len += sprintf(buf + len, "VMEbus (read) block size:              2048 bytes\n");
      if (value == 7) len += sprintf(buf + len, "VMEbus (read) block size:              4096 bytes\n");

      value = (data >> 8) & 0x7;
      if (value == 0) len += sprintf(buf + len, "VMEbus back off time:                  0 us\n");
      if (value == 1) len += sprintf(buf + len, "VMEbus back off time:                  1 us\n");
      if (value == 2) len += sprintf(buf + len, "VMEbus back off time:                  2 us\n");
      if (value == 3) len += sprintf(buf + len, "VMEbus back off time:                  4 us\n");
      if (value == 4) len += sprintf(buf + len, "VMEbus back off time:                  8 us\n");
      if (value == 5) len += sprintf(buf + len, "VMEbus back off time:                  16 us\n");
      if (value == 6) len += sprintf(buf + len, "VMEbus back off time:                  32 us\n");
      if (value == 7) len += sprintf(buf + len, "VMEbus back off time:                  64 us\n");

      value = (data >> 4) & 0x7;
      if (value == 0) len += sprintf(buf + len, "PCI (read) block size:                 32 bytes\n");
      if (value == 1) len += sprintf(buf + len, "PCI (read) block size:                 64 bytes\n");
      if (value == 2) len += sprintf(buf + len, "PCI (read) block size:                 128 bytes\n");
      if (value == 3) len += sprintf(buf + len, "PCI (read) block size:                 256 bytes\n");
      if (value == 4) len += sprintf(buf + len, "PCI (read) block size:                 512 bytes\n");
      if (value == 5) len += sprintf(buf + len, "PCI (read) block size:                 1024 bytes\n");
      if (value == 6) len += sprintf(buf + len, "PCI (read) block size:                 2048 bytes\n");
      if (value == 7) len += sprintf(buf + len, "PCI (read) block size:                 4096 bytes\n");

      value = data & 0x7;
      if (value == 0) len += sprintf(buf + len, "PCI back off time:                     0 us\n");
      if (value == 1) len += sprintf(buf + len, "PCI back off time:                     1 us\n");
      if (value == 2) len += sprintf(buf + len, "PCI back off time:                     2 us\n");
      if (value == 3) len += sprintf(buf + len, "PCI back off time:                     4 us\n");
      if (value == 4) len += sprintf(buf + len, "PCI back off time:                     8 us\n");
      if (value == 5) len += sprintf(buf + len, "PCI back off time:                     16 us\n");
      if (value == 6) len += sprintf(buf + len, "PCI back off time:                     32 us\n");
      if (value == 7) len += sprintf(buf + len, "PCI back off time:                     64 us\n");

      data = bswap(tsi->dsta0);
      len += sprintf(buf + len, "\ndsta0 (0x%08x):\n", data);
      len += sprintf(buf + len, "PCI/X or VME error:        %s\n", (data & 0x10000000)?"Yes":"No");
      len += sprintf(buf + len, "DMA aborted:               %s\n", (data & 0x8000000)?"Yes":"No");
      len += sprintf(buf + len, "DMA paused:                %s\n", (data & 0x4000000)?"Yes":"No");
      len += sprintf(buf + len, "DMA done wihtout error:    %s\n", (data & 0x2000000)?"Yes":"No");
      len += sprintf(buf + len, "DMA controller still busy: %s\n", (data & 0x1000000)?"Yes":"No");
      if (data & 0x10000000)
      {
	len += sprintf(buf + len, "Source of error:           %s\n", (data & 0x100000)?"PCI":"VME");
	value = (data >> 20) & 0x1;
	value2 = (data >> 16) & 0x3;
	if (value == 0 && value2 == 0) len += sprintf(buf + len, "Type details:              Bus error (SCT, BLT, MBLT, 2eVME even data, 2eSST)\n");
	if (value == 0 && value2 == 1) len += sprintf(buf + len, "Type details:              Bus error (2eVME odd data)\n");
	if (value == 0 && value2 == 2) len += sprintf(buf + len, "Type details:              Slave termination (2eVME even data, 2eSST read)\n");
	if (value == 0 && value2 == 3) len += sprintf(buf + len, "Type details:              Slave termination (2eVME odd data, 2eSST read last word invalid)\n");
	if (value == 1 && value2 == 0) len += sprintf(buf + len, "Type details:              PXI/X error\n");
	if (value == 1 && value2 == 1) len += sprintf(buf + len, "Type details:              undefined\n");
	if (value == 1 && value2 == 2) len += sprintf(buf + len, "Type details:              undefined\n");
	if (value == 1 && value2 == 3) len += sprintf(buf + len, "Type details:              undefined\n");
      }

      len += sprintf(buf + len, "\nInitial source address:   0x%08x%08x\n", bswap(tsi->dsau0), bswap(tsi->dsal0));  
      len += sprintf(buf + len, "(Current) source address: 0x%08x%08x\n", bswap(tsi->dcsau0), bswap(tsi->dcsal0));  
      data = bswap(tsi->dsat0);
      len += sprintf(buf + len, "\ndsat0 (0x%08x):\n", data);

      value = (data >> 28) & 0x3;
      if (value == 0) len += sprintf(buf + len, "Data source bus:     PCI\n");
      if (value == 1) len += sprintf(buf + len, "Data source bus:     VMEbus\n");
      if (value > 1)  len += sprintf(buf + len, "Data source bus:     Data pattern\n");

      value = (data >> 11) & 0x3;
      if (value == 0) len += sprintf(buf + len, "2eSST speed:         160 MB/s\n");
      if (value == 1) len += sprintf(buf + len, "2eSST speed:         267 MB/s\n");
      if (value == 2) len += sprintf(buf + len, "2eSST speed:         320 MB/s\n");
      if (value == 3) len += sprintf(buf + len, "2eSST speed:         undefined\n");

      value = (data >> 8) & 0x7;
      if (value == 0) len += sprintf(buf + len, "Transfer mode:       Single cycle\n");
      if (value == 1) len += sprintf(buf + len, "Transfer mode:       BLT\n");
      if (value == 2) len += sprintf(buf + len, "Transfer mode:       MBLT\n");
      if (value == 3) len += sprintf(buf + len, "Transfer mode:       2eVME\n");
      if (value == 4) len += sprintf(buf + len, "Transfer mode:       2eSST\n");
      if (value == 5) len += sprintf(buf + len, "Transfer mode:       2eSST broadcast\n");
      if (value > 5)  len += sprintf(buf + len, "Transfer mode:       undefined\n");

      value = (data >> 6) & 0x3;
      if (value == 0) len += sprintf(buf + len, "VMEbus data width:   16 bit\n");
      if (value == 1) len += sprintf(buf + len, "VMEbus data width:   32 bit\n");
      if (value > 1)  len += sprintf(buf + len, "VMEbus data width:   undefined\n");

      value = (data >> 4) & 0x3;
      if (value == 0) len += sprintf(buf + len, "VMEbus AM-code type: User / Data\n");
      if (value == 1) len += sprintf(buf + len, "VMEbus AM-code type: User / Program/n");
      if (value == 2) len += sprintf(buf + len, "VMEbus AM-code type: Supervisor / Data\n");
      if (value == 3) len += sprintf(buf + len, "VMEbus AM-code type: Supervisor / Program\n");

      value = data & 0xf;
	   if (value == 0)  len += sprintf(buf + len, "VMEbus base AM-code: A16\n");
      else if (value == 1)  len += sprintf(buf + len, "VMEbus base AM-code: A24\n");
      else if (value == 2)  len += sprintf(buf + len, "VMEbus base AM-code: A32\n");
      else if (value == 3)  len += sprintf(buf + len, "VMEbus base AM-code: A64\n");
      else if (value == 4)  len += sprintf(buf + len, "VMEbus base AM-code: CR-CSR\n");
      else if (value == 8)  len += sprintf(buf + len, "VMEbus base AM-code: User1\n");
      else if (value == 9)  len += sprintf(buf + len, "VMEbus base AM-code: User2\n");
      else if (value == 10) len += sprintf(buf + len, "VMEbus base AM-code: User3\n");
      else if (value == 11) len += sprintf(buf + len, "VMEbus base AM-code: User4\n");
      else                  len += sprintf(buf + len, "VMEbus base AM-code: undefined\n");

      len += sprintf(buf + len, "\nInitial destination address:   0x%08x%08x\n", bswap(tsi->ddau0), bswap(tsi->ddal0));  
      len += sprintf(buf + len, "(Current) destination address: 0x%08x%08x\n", bswap(tsi->dcdau0), bswap(tsi->dcdal0));  
      data = bswap(tsi->ddat0);
      len += sprintf(buf + len, "\nddat0 (0x%08x):\n", data);
      len += sprintf(buf + len, "Data source bus:     %s\n", (data & 0x10000000)?"VMEbus":"PCI");

      value = (data >> 11) & 0x3;
      if (value == 0) len += sprintf(buf + len, "2eSST speed:         160 MB/s\n");
      if (value == 1) len += sprintf(buf + len, "2eSST speed:         267 MB/s\n");
      if (value == 2) len += sprintf(buf + len, "2eSST speed:         320 MB/s\n");
      if (value == 3) len += sprintf(buf + len, "2eSST speed:         undefined\n");

      value = (data >> 8) & 0x7;
      if (value == 0) len += sprintf(buf + len, "Transfer mode:       Single cycle\n");
      if (value == 1) len += sprintf(buf + len, "Transfer mode:       BLT\n");
      if (value == 2) len += sprintf(buf + len, "Transfer mode:       MBLT\n");
      if (value == 3) len += sprintf(buf + len, "Transfer mode:       2eVME\n");
      if (value == 4) len += sprintf(buf + len, "Transfer mode:       2eSST\n");
      if (value > 4)  len += sprintf(buf + len, "Transfer mode:       undefined\n");

      value = (data >> 6) & 0x3;
      if (value == 0) len += sprintf(buf + len, "VMEbus data width:   16 bit\n");
      if (value == 1) len += sprintf(buf + len, "VMEbus data width:   32 bit\n");
      if (value > 1)  len += sprintf(buf + len, "VMEbus data width:   undefined\n");

      value = (data >> 4) & 0x3;
      if (value == 0) len += sprintf(buf + len, "VMEbus AM-code type: User / Data\n");
      if (value == 1) len += sprintf(buf + len, "VMEbus AM-code type: User / Program/n");
      if (value == 2) len += sprintf(buf + len, "VMEbus AM-code type: Supervisor / Data\n");
      if (value == 3) len += sprintf(buf + len, "VMEbus AM-code type: Supervisor / Program\n");

      value = data & 0xf;
	   if (value == 0)  len += sprintf(buf + len, "VMEbus base AM-code: A16\n");
      else if (value == 1)  len += sprintf(buf + len, "VMEbus base AM-code: A24\n");
      else if (value == 2)  len += sprintf(buf + len, "VMEbus base AM-code: A32\n");
      else if (value == 3)  len += sprintf(buf + len, "VMEbus base AM-code: A64\n");
      else if (value == 4)  len += sprintf(buf + len, "VMEbus base AM-code: CR-CSR\n");
      else if (value == 8)  len += sprintf(buf + len, "VMEbus base AM-code: User1\n");
      else if (value == 9)  len += sprintf(buf + len, "VMEbus base AM-code: User2\n");
      else if (value == 10) len += sprintf(buf + len, "VMEbus base AM-code: User3\n");
      else if (value == 11) len += sprintf(buf + len, "VMEbus base AM-code: User4\n");
      else                  len += sprintf(buf + len, "VMEbus base AM-code: undefined\n");

      len += sprintf(buf + len, "\nLast element in list: %s\n", (bswap(tsi->dnlal0) & 0x1)?"Yes":"No");

      len += sprintf(buf + len, "(Current) linked list PCI address: 0x%08x%08x\n", bswap(tsi->dclau0), bswap(tsi->dclal0));  
      len += sprintf(buf + len, "DMA count:                         0x%08x\n", bswap(tsi->dcnt0));  
      len += sprintf(buf + len, "2eSST broadcast select:            0x%08x\n", bswap(tsi->ddbs0) & 0x1fffff);

      if (copy_to_user((void *)arg, buf, TEXT_SIZE2 * sizeof(char)))
      {
	kerror(("vme_rcc_tsi(ioctl,VMEDMADUMP): error from copy_to_user\n"))
	return(-VME_EFAULT);
      }
      kfree(buf);
      kdebug(("vme_rcc_tsi(ioctl,VMEDMADUMP): kfree called for buf\n"))
      break;
    }
    
    case VMELINK: 
    {
      u_int i;
      int vct;
      VME_IntHandle_t *group_ptr;
      VME_IntHandle_t *VME_int_handle;

      VME_int_handle = (VME_IntHandle_t *)kmalloc(sizeof(VME_IntHandle_t), GFP_KERNEL);
      kdebug(("vme_rcc_tsi(ioctl,VMELINK): 0x%08lx bytes allocated [VME_int_handle](sizeof(VME_IntHandle_t)))\n", sizeof(VME_IntHandle_t)))
      if (VME_int_handle == NULL)
      {
	kerror(("vme_rcc_tsi(ioctl,VMELINK): error from kmalloc\n"));
        return(-VME_KMALLOC);
      }

      if (copy_from_user(VME_int_handle, (void *)arg, sizeof(VME_IntHandle_t)))
      {
	kerror(("vme_rcc_tsi(ioctl,VMELINK): error from copy_from_user\n"))
	kfree(VME_int_handle);
        kdebug(("vme_rcc_tsi(ioctl,VMELINK): kfree called for VME_int_handle\n"))
	return(-VME_EFAULT);
      }
      kdebug(("vme_rcc_tsi(ioctl,VMELINK): # vectors = %d\n", VME_int_handle->nvectors))
      for (i = 0; i < VME_int_handle->nvectors; i++)
        kdebug(("vme_rcc_tsi(ioctl,VMELINK): vector # %d = 0x%8x\n", i, VME_int_handle->vector[i]))

      if (irq_level_table[VME_int_handle->level - 1] == 0)        // is this level enabled?
      {
        kdebug(("vme_rcc_tsi(ioctl,VMELINK): level %d is disabled\n", VME_int_handle->level))
	kfree(VME_int_handle);
        kdebug(("vme_rcc_tsi(ioctl,VMELINK): kfree called for VME_int_handle\n"))
        return(-VME_INTDISABLED);
      }
      
      kdebug(("vme_rcc_tsi(ioctl,VMELINK): level = %d, type = %d\n", VME_int_handle->level, VME_int_handle->type))

      if (VME_int_handle->type != irq_level_table[VME_int_handle->level - 1])        // check type
      {
        kdebug(("vme_rcc_tsi(ioctl,VMELINK): level %d, user mode = %d, tsiconfig mode = %d\n",
		   VME_int_handle->level, VME_int_handle->type, irq_level_table[VME_int_handle->level - 1]))
	kfree(VME_int_handle);
        kdebug(("vme_rcc_tsi(ioctl,VMELINK): kfree called for VME_int_handle\n"))
        return(-VME_INTCONF);
      }

      down(&link_table.link_dsc_sem);

      for (i = 0; i < VME_int_handle->nvectors; i++)
      {
        vct = VME_int_handle->vector[i]; 
        if (link_table.link_dsc[vct].pid) 
        {
          kdebug(("vme_rcc_tsi(ioctl,VMELINK): vector busy, pid = %d\n", link_table.link_dsc[vct].pid))
          up(&link_table.link_dsc_sem);
 	  kfree(VME_int_handle);
          kdebug(("vme_rcc_tsi(ioctl,VMELINK): kfree called for VME_int_handle\n"))
          return(-VME_INTUSED);
        }
      }

      // allocate aux. structure for multi vector handles and fill it
      group_ptr = (VME_IntHandle_t*) NULL;
      if (VME_int_handle->nvectors > 1)
      {
        group_ptr = kmalloc(sizeof(VME_IntHandle_t), GFP_KERNEL);  //MJ: Where is the matching kfree()? In VMEUNLINK?
        kdebug(("vme_rcc_tsi(ioctl,VMELINK2): 0x%08lx bytes allocated [group_ptr](sizeof(VME_IntHandle_t)))\n", sizeof(VME_IntHandle_t)))
        if (!group_ptr)
        {  
          up(&link_table.link_dsc_sem);
 	  kfree(VME_int_handle);
          kdebug(("vme_rcc_tsi(ioctl,VMELINK): kfree called for VME_int_handle\n"))
          return(-VME_ENOMEM);
        }
        group_ptr->nvectors = VME_int_handle->nvectors;
        for (i = 0; i < VME_int_handle->nvectors; i++)
          group_ptr->vector[i] = VME_int_handle->vector[i];
      }

      for (i = 0; i < VME_int_handle->nvectors; i++)
      {
        vct = VME_int_handle->vector[i]; 

        link_table.link_dsc[vct].pid = current->pid;		       // current process
        link_table.link_dsc[vct].vct = vct;	        	       // vector
        link_table.link_dsc[vct].lvl = VME_int_handle->level;          // level
        link_table.link_dsc[vct].type = VME_int_handle->type;          // type
        link_table.link_dsc[vct].pending = 0;	        	       // no interrupt pending
        link_table.link_dsc[vct].total_count = 0;       	       // total # interrupts
        link_table.link_dsc[vct].group_ptr = group_ptr; 	       // the group vectors
        sema_init(&link_table.link_dsc[vct].sem, 0); 		       // initialise semaphore count
        link_table.link_dsc[vct].sig = 0;	        	       // no signal
      
        pdata->link_dsc_flag[vct] = 1;			               // remember which file it belongs to
      }

      up(&link_table.link_dsc_sem);
      kfree(VME_int_handle);
      kdebug(("vme_rcc_tsi(ioctl,VMELINK): kfree called for VME_int_handle\n"))
      break;
    }

    case VMEINTENABLE: 
    {
      u_int inteo;
      u_long flags;
      VME_IntEnable_t *VME_IntEnable2;

      VME_IntEnable2 = (VME_IntEnable_t *)kmalloc(sizeof(VME_IntEnable_t), GFP_KERNEL);
      kdebug(("vme_rcc_tsi(ioctl,VMEINTENABLE): 0x%08lx bytes allocated [VME_IntEnable2](sizeof(VME_IntEnable_t)))\n", sizeof(VME_IntEnable_t)))
      if (VME_IntEnable2 == NULL)
      {
	kerror(("vme_rcc_tsi(ioctl,VMEINTENABLE): error from kmalloc\n"));
        return(-VME_KMALLOC);
      }

      if (copy_from_user(VME_IntEnable2, (void *)arg, sizeof(VME_IntEnable_t)))
      {
	kerror(("vme_rcc_tsi(ioctl,VMEINTENABLE): error from copy_from_user\n"))
	kfree(VME_IntEnable2);
        kdebug(("vme_rcc_tsi(ioctl,VMEINTENABLE): kfree called for VME_IntEnable2\n"))
	return(-VME_EFAULT);
      }
      kdebug(("vme_rcc_tsi(ioctl,VMEINTENABLE): level = %d\n", VME_IntEnable2->level))

      // ONLY allowed if RORA level
      if (irq_level_table[VME_IntEnable2->level-1] != VME_INT_RORA)
      {
	kfree(VME_IntEnable2);
        kdebug(("vme_rcc_tsi(ioctl,VMEINTENABLE): kfree called for VME_IntEnable2\n"))
        return(-VME_LVLISNOTRORA);
      }
      
      local_irq_save(flags);                            //MJ: for 2.6 see p274 & p275

      inteo = bswap(tsi->inteo);                        // read enabled irq bits
      inteo |= (1 << VME_IntEnable2->level);	        // add selected level
      kdebug(("vme_rcc_tsi(ioctl,VMEINTENABLE): inteo = 0x%08x\n", inteo))
      tsi->inteo = bswap(inteo);                               // write it back

      local_irq_restore(flags);
      kfree(VME_IntEnable2);
      kdebug(("vme_rcc_tsi(ioctl,VMEINTENABLE): kfree called for VME_IntEnable2\n"))
      break;
    }

    case VMEINTDISABLE: 
    {
      u_int inteo;
      u_long flags;
      VME_IntEnable_t *VME_IntEnable3;

      VME_IntEnable3 = (VME_IntEnable_t *)kmalloc(sizeof(VME_IntEnable_t), GFP_KERNEL);
      kdebug(("vme_rcc_tsi(ioctl,VMEINTDISABLE2): 0x%08lx bytes allocated [VME_IntEnable3](sizeof(VME_IntEnable_t)))\n", sizeof(VME_IntEnable_t)))
      if (VME_IntEnable3 == NULL)
      {
	kerror(("vme_rcc_tsi(ioctl,VMEINTDISABLE): error from kmalloc\n"));
        return(-VME_KMALLOC);
      }

      if (copy_from_user(VME_IntEnable3, (void *)arg, sizeof(VME_IntEnable_t)))
      {
	kerror(("vme_rcc_tsi(ioctl,VMEINTDISABLE): error from copy_from_user\n"))
	kfree(VME_IntEnable3);
        kdebug(("vme_rcc_tsi(ioctl,VMEINTDISABLE): kfree called for VME_IntEnable3\n"))
	return(-VME_EFAULT);
      }
      kdebug(("vme_rcc_tsi(ioctl,VMEINTDISABLE): level = %d\n", VME_IntEnable3->level))

      local_irq_save(flags);                            //MJ: for 2.6 see p274 & p275

      inteo = bswap(tsi->inteo);    	                // read enabled irq bits
      inteo &= ~(1 << VME_IntEnable3->level);	        // remove selected level
      kdebug(("vme_rcc_tsi(ioctl,VMEINTDISABLE): inteo = 0x%08x\n", inteo))
      tsi->inteo = bswap(inteo);                        // write it back

      local_irq_restore(flags);
      kfree(VME_IntEnable3);
      kdebug(("vme_rcc_tsi(ioctl,VMEINTDISABLE): kfree called for VME_IntEnable3\n"))
      break;
    }

    case VMEWAIT:
    {
      int ret, first_vct, cur_vct = 0, use_timer = 0, result = 0;
      u_int i;
      u_long flags;
      struct timer_list int_timer;
      VME_IntHandle_t *group_ptr;
      VME_WaitInt_t *VME_WaitInt2;

      VME_WaitInt2 = (VME_WaitInt_t *)kmalloc(sizeof(VME_WaitInt_t), GFP_KERNEL);
      kdebug(("vme_rcc_tsi(ioctl,VMEWAIT): 0x%08lx bytes allocated [VME_WaitInt2](sizeof(VME_WaitInt_t)))\n", sizeof(VME_WaitInt_t)))
      if (VME_WaitInt2 == NULL)
      {
	kerror(("vme_rcc_tsi(ioctl,VMEWAIT): error from kmalloc\n"));
        return(-VME_KMALLOC);
      }

      if (copy_from_user(VME_WaitInt2, (void *)arg, sizeof(VME_WaitInt_t))) 
      {
	kerror(("vme_rcc_tsi(ioctl,VMEWAIT): error from copy_from_user\n"))
	kfree(VME_WaitInt2);
        kdebug(("vme_rcc_tsi(ioctl,VMEINTDISABLE): kfree called for VME_WaitInt2\n"))
	return(-VME_EFAULT);
      }
      kdebug(("vme_rcc_tsi(ioctl,VMEWAIT): # vectors = %d, timeout = %d\n", VME_WaitInt2->int_handle.nvectors, VME_WaitInt2->timeout))
      for (i = 0; i < VME_WaitInt2->int_handle.nvectors; i++)
        kdebug(("vme_rcc_tsi(ioctl,VMEWAIT): vector # %d = 0x%8x\n", i, VME_WaitInt2->int_handle.vector[i]))

      first_vct = VME_WaitInt2->int_handle.vector[0];

      if (VME_WaitInt2->timeout > 0) 
      {
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,0,0)
        init_timer(&int_timer);
        int_timer.function = vme_intTimeout;
        int_timer.data = (u_long)VME_WaitInt2;  
        int_timer.expires = jiffies + VME_WaitInt2->timeout * HZ / 1000;  //convert from ms to kernel-jiffis (pretty pointless for the current 1 ms jiffies)
#else
        timer_setup(&int_timer, vme_intTimeout, 0);
#endif	                                                                  //Note: At the kernel level a jiffi is 1 ms (10 ms at the user level) 
        add_timer(&int_timer);
        use_timer = 1;
      }

      if (VME_WaitInt2->timeout) 		
      {
        kdebug(("vme_rcc_tsi(ioctl,VMEWAIT): Before Down, semaphore = %d\n", link_table.link_dsc[first_vct].sem.count))

        // IR handled before we wait. Not quite RACE safe but only for statistics  .. 
        if (link_table.link_dsc[first_vct].sem.count > 0) 
        {
          if (link_table.link_dsc[first_vct].sem.count == 1)
            sem_positive[0]++;
          else
            sem_positive[1]++;
        }

        ret = down_interruptible(&link_table.link_dsc[first_vct].sem);	// wait ..

        if (signal_pending(current))
        {						// interrupted system call
          kdebug(("vme_rcc_tsi(ioctl,VMEWAIT): Interrupted by signal\n"))
          result = -VME_INTBYSIGNAL;		        // or ERESTARTSYS ?
        }

        else if ((use_timer == 1) && (VME_WaitInt2->timeout == 0))
          result = -VME_TIMEOUT;
      }

      VME_WaitInt2->vector   = 0;                   // return 0 if NO pending
      VME_WaitInt2->level    = 0;
      VME_WaitInt2->type     = 0;
      VME_WaitInt2->multiple = 0;

      if (result == 0)		                                // for all values of timeout & no early returns: get IR info
      {
        if (link_table.link_dsc[first_vct].group_ptr == NULL)	// single vector
        {
          if (link_table.link_dsc[first_vct].pending > 0)
          {
            VME_WaitInt2->level    = link_table.link_dsc[first_vct].lvl;
            VME_WaitInt2->type     = link_table.link_dsc[first_vct].type;
            VME_WaitInt2->vector   = first_vct;
            VME_WaitInt2->multiple = link_table.link_dsc[first_vct].pending;    // possible RACE with ISR
            link_table.link_dsc[first_vct].pending--;                           // done
          }
        }
        else  // scan vectors in group and return FIRST one pending. The scan starts from the first: an issue ?
        {
          local_irq_save(flags);  // may not really be needed //MJ: for 2.6 see p274 & p275

          group_ptr = link_table.link_dsc[first_vct].group_ptr;
          kdebug(("vme_rcc_tsi(ioctl,VMEWAIT): group pointer = 0x%p\n",group_ptr))
          for (i = 0; i < group_ptr->nvectors; i++)
          {
            cur_vct = group_ptr->vector[i];
            kdebug(("vme_rcc_tsi(ioctl,VMEWAIT): current vector = %d\n",cur_vct))
            if (link_table.link_dsc[cur_vct].pending > 0)
            {
              VME_WaitInt2->level    = link_table.link_dsc[cur_vct].lvl;
              VME_WaitInt2->type     = link_table.link_dsc[cur_vct].type;
              VME_WaitInt2->vector   = cur_vct;
              VME_WaitInt2->multiple = link_table.link_dsc[cur_vct].pending;
              link_table.link_dsc[cur_vct].pending--;		                   // done
              break;
            }
          }
          local_irq_restore(flags);
        }
      }

      if (use_timer == 1)
        del_timer(&int_timer);	// OK also if timer ran out

      if (copy_to_user((void *)arg, VME_WaitInt2, sizeof(VME_WaitInt_t)))
      {
	kerror(("vme_rcc_tsi(ioctl,VMEWAIT): error from copy_to_user\n"))
	kfree(VME_WaitInt2);
        kdebug(("vme_rcc_tsi(ioctl,VMEWAIT): kfree called for VME_WaitInt2\n"))
	return(-VME_EFAULT);
      }

      kfree(VME_WaitInt2);
      kdebug(("vme_rcc_tsi(ioctl,VMEWAIT): kfree called for VME_WaitInt2\n"))
      return (result);
      break;	// dummy ..
    }

    case VMEREGISTERSIGNAL:
    {
      int vct;
      u_int i;
      VME_RegSig_t *VME_RegSig;
      
      VME_RegSig = (VME_RegSig_t *)kmalloc(sizeof(VME_RegSig_t), GFP_KERNEL);
      kdebug(("vme_rcc_tsi(ioctl,VMEREGISTERSIGNAL): 0x%08lx bytes allocated [VME_RegSig](sizeof(VME_RegSig_t)))\n", sizeof(VME_RegSig_t)))
      if (VME_RegSig == NULL)
      {
	kerror(("vme_rcc_tsi(ioctl,VMEREGISTERSIGNAL): error from kmalloc\n"));
        return(-VME_KMALLOC);
      }

      if (copy_from_user(VME_RegSig, (void *)arg, sizeof(VME_RegSig_t)))   
      {
	kerror(("vme_rcc_tsi(ioctl,VMEREGISTERSIGNAL): error from copy_from_user\n"))
	kfree(VME_RegSig);
        kdebug(("vme_rcc_tsi(ioctl,VMEREGISTERSIGNAL): kfree called for VME_RegSig\n"))
	return(-VME_EFAULT);
      }

      pdata->VME_int_handle2 = VME_RegSig->int_handle;

      kdebug(("vme_rcc_tsi(ioctl,VMEREGISTERSIGNAL): # vectors = %d, signal # = %d\n", pdata->VME_int_handle2.nvectors, VME_RegSig->signum))
      for (i = 0; i < pdata->VME_int_handle2.nvectors; i++)
        kdebug(("vme_rcc_tsi(ioctl,VMEREGISTERSIGNAL): vector # %d = 0x%8x\n", i, pdata->VME_int_handle2.vector[i]));

      down(&link_table.link_dsc_sem);

      for (i = 0; i < pdata->VME_int_handle2.nvectors; i++)
      {
        vct = pdata->VME_int_handle2.vector[i]; 
        link_table.link_dsc[vct].sig = VME_RegSig->signum;
      }

      up(&link_table.link_dsc_sem);
      kfree(VME_RegSig);
      kdebug(("vme_rcc_tsi(ioctl,VMEREGISTERSIGNAL): kfree called for VME_RegSig\n"))
      break;
    }

    case VMEINTERRUPTINFOGET:
    {
      int first_vct, cur_vct = 0;
      u_int i;
      u_long flags;
      VME_IntHandle_t *group_ptr;
      VME_WaitInt_t *VME_WaitInt3;

      VME_WaitInt3 = (VME_WaitInt_t *)kmalloc(sizeof(VME_WaitInt_t), GFP_KERNEL);
      kdebug(("vme_rcc_tsi(ioctl,VMEINTERRUPTINFOGET): 0x%08lx bytes allocated [VME_WaitInt3](sizeof(VME_WaitInt_t)))\n", sizeof(VME_WaitInt_t)))
      if (VME_WaitInt3 == NULL)
      {
	kerror(("vme_rcc_tsi(ioctl,VMEINTERRUPTINFOGET): error from kmalloc\n"));
        return(-VME_KMALLOC);
      }

      if (copy_from_user(VME_WaitInt3, (void *)arg, sizeof(VME_WaitInt_t)))
      {
	kdebug(("vme_rcc_tsi(ioctl,VMEINTERRUPTINFOGET): error from copy_from_user\n"))
	kfree(VME_WaitInt3);
        kdebug(("vme_rcc_tsi(ioctl,VMEINTERRUPTINFOGET): kfree called for VME_WaitInt3\n"))
	return(-VME_EFAULT);
      }
      kdebug(("vme_rcc_tsi(ioctl,VMEINTERRUPTINFOGET): # vectors = %d \n", VME_WaitInt3->int_handle.nvectors))
      for (i = 0; i < VME_WaitInt3->int_handle.nvectors; i++)
        kdebug(("vme_rcc_tsi(ioctl,VMEINTERRUPTINFOGET): vector # %d = 0x%8x\n", i, VME_WaitInt3->int_handle.vector[i]))

      first_vct = VME_WaitInt3->int_handle.vector[0];

      VME_WaitInt3->vector   = 0;      // return 0 if NO pending
      VME_WaitInt3->level    = 0;
      VME_WaitInt3->type     = 0;
      VME_WaitInt3->multiple = 0;

      if (link_table.link_dsc[first_vct].group_ptr == NULL)
      {
        if (link_table.link_dsc[first_vct].pending > 0)
        {
          kdebug(("vme_rcc_tsi(ioctl,VMEINTERRUPTINFOGET): Case 1\n"))
          VME_WaitInt3->level    = link_table.link_dsc[first_vct].lvl;
          VME_WaitInt3->type     = link_table.link_dsc[first_vct].type;
          VME_WaitInt3->vector   = first_vct;
          VME_WaitInt3->multiple = link_table.link_dsc[first_vct].pending;	
          link_table.link_dsc[first_vct].pending--;                          
        }
      }
      else		         // scan vectors in group and return FIRST one pending 
      {
        kdebug(("vme_rcc_tsi(ioctl,VMEINTERRUPTINFOGET): Case 2\n"))
        local_irq_save(flags);   // may not really be needed //MJ: for 2.6 see p274 & p275

        group_ptr = link_table.link_dsc[first_vct].group_ptr;
        kdebug(("vme_rcc_tsi(ioctl,VMEINTERRUPTINFOGET): group pointer = 0x%p\n", group_ptr))
        for (i = 0; i < group_ptr->nvectors; i++)
        {
          cur_vct = group_ptr->vector[i];
          kdebug(("vme_rcc_tsi(ioctl,VMEINTERRUPTINFOGET): current vector = %d\n", cur_vct))
          if (link_table.link_dsc[cur_vct].pending > 0)
          {
            VME_WaitInt3->level    = link_table.link_dsc[cur_vct].lvl;
            VME_WaitInt3->type     = link_table.link_dsc[cur_vct].type;
            VME_WaitInt3->vector   = cur_vct;
            VME_WaitInt3->multiple = link_table.link_dsc[cur_vct].pending;
            link_table.link_dsc[cur_vct].pending--;                           
            kdebug(("vme_rcc_tsi(ioctl,VMEINTERRUPTINFOGET): pending is = %d\n", link_table.link_dsc[cur_vct].pending))
            break;
          }
        }
        local_irq_restore(flags);
      }

      if (copy_to_user((void *)arg, VME_WaitInt3, sizeof(VME_WaitInt_t)))
      {
	kerror(("vme_rcc_tsi(ioctl,VMEINTERRUPTINFOGET): error from copy_to_user\n"))
	kfree(VME_WaitInt3);
        kdebug(("vme_rcc_tsi(ioctl,VMEINTERRUPTINFOGET): kfree called for VME_WaitInt3);\n"))
	return(-VME_EFAULT);
      }
      kfree(VME_WaitInt3);
      kdebug(("vme_rcc_tsi(ioctl,VMEINTERRUPTINFOGET): kfree called for VME_WaitInt3);\n"))
      break;
    }

    case VMEUNLINK: 
    {
      u_int i;
      int vct, first_vector;
      VME_IntHandle_t *VME_int_handle3;

      VME_int_handle3 = (VME_IntHandle_t *)kmalloc(sizeof(VME_IntHandle_t), GFP_KERNEL);
      kdebug(("vme_rcc_tsi(ioctl,VMEUNLINK): 0x%08lx bytes allocated [VME_int_handle3](sizeof(VME_IntHandle_t)))\n", sizeof(VME_IntHandle_t)))
      if (VME_int_handle3 == NULL)
      {
	kerror(("vme_rcc_tsi(ioctl,VMEUNLINK): error from kmalloc\n"));
        return(-VME_KMALLOC);
      }

      if (copy_from_user(VME_int_handle3, (void *)arg, sizeof(VME_IntHandle_t)))
      {
	kerror(("vme_rcc_tsi(ioctl,VMEUNLINK): error from copy_from_user\n"))
	kfree(VME_int_handle3);
        kdebug(("vme_rcc_tsi(ioctl,VMEUNLINK): kfree called for VME_int_handle3\n"))
	return(-VME_EFAULT);
      }
      kdebug(("vme_rcc_tsi(ioctl,VMEUNLINK): # vectors = %d\n", VME_int_handle3->nvectors))
      for (i = 0; i < VME_int_handle3->nvectors; i++)
        kdebug(("vme_rcc_tsi(ioctl,VMEUNLINK): vector # %d = 0x%x\n", i, VME_int_handle3->vector[i]));

      down(&link_table.link_dsc_sem);

      if (VME_int_handle3->nvectors > 1)
      {
        first_vector = VME_int_handle3->vector[0];
        kfree(link_table.link_dsc[first_vector].group_ptr);  //MJ: this seems to be the kfree for the kmalloc in VMELINK
        kdebug(("vme_rcc_tsi(ioctl,VMEUNLINK): kfree called for link_table.link_dsc[first_vector].group_ptr\n"))
        kdebug(("vme_rcc_tsi(ioctl,VMEUNLINK): first vector = %d\n", first_vector))
        kdebug(("vme_rcc_tsi(ioctl,VMEUNLINK): group_ptr = 0x%p\n", link_table.link_dsc[first_vector].group_ptr))
      }

      for (i = 0; i < VME_int_handle3->nvectors; i++)
      {
        vct = VME_int_handle3->vector[i]; 

        link_table.link_dsc[vct].pid         = 0;
        link_table.link_dsc[vct].vct         = 0;
        link_table.link_dsc[vct].lvl         = 0;
        link_table.link_dsc[vct].pending     = 0;
        link_table.link_dsc[vct].total_count = 0;
        link_table.link_dsc[vct].group_ptr   = 0;
        link_table.link_dsc[vct].sig         = 0;
        sema_init(&link_table.link_dsc[vct].sem, 0);

        pdata->link_dsc_flag[vct] = 0;
      }
      up(&link_table.link_dsc_sem);
      kfree(VME_int_handle3);
      kdebug(("vme_rcc_tsi(ioctl,VMEUNLINK): kfree called for VME_int_handle3\n"))
      break;
    }
 
    // called from tsiconfig when Tsi148 registers are updated
    case VMEUPDATE:   
    {      
      int ret, i;
      VME_Update_t *update_data;
    
      update_data = (VME_Update_t *)kmalloc(sizeof(VME_Update_t), GFP_KERNEL);
      kdebug(("vme_rcc_tsi(ioctl,VMEUPDATE): 0x%08lx bytes allocated [update_data](sizeof(VME_Update_t)))\n", sizeof(VME_Update_t)))
      if (update_data == NULL)
      {
	kerror(("vme_rcc_tsi(ioctl,VMEUPDATE): error from kmalloc\n"));
        return(-VME_KMALLOC);
      }

      kdebug(("vme_rcc_tsi(ioctl,VMEUPDATE): Function called\n")) 
        
      ret = copy_from_user(update_data, (void *)arg, sizeof(VME_Update_t));
      if (ret)
      {
	kerror(("vme_rcc_tsi(ioctl,VMEUPDATE): error %d from copy_from_user\n",ret))
	kfree(update_data);
        kdebug(("vme_rcc_tsi(ioctl,VMEUPDATE): kfree called for update_data\n"))
	return(-VME_EFAULT);
      }
      
      ret = fill_mstmap(bswap(tsi->otsau0), bswap(tsi->otsal0), bswap(tsi->oteau0), bswap(tsi->oteal0), bswap(tsi->otofu0), bswap(tsi->otofl0), bswap(tsi->otat0), &stmap.master[0]); if (ret) return(-VME_EIO);
      ret = fill_mstmap(bswap(tsi->otsau1), bswap(tsi->otsal1), bswap(tsi->oteau1), bswap(tsi->oteal1), bswap(tsi->otofu1), bswap(tsi->otofl1), bswap(tsi->otat1), &stmap.master[1]); if (ret) return(-VME_EIO);
      ret = fill_mstmap(bswap(tsi->otsau2), bswap(tsi->otsal2), bswap(tsi->oteau2), bswap(tsi->oteal2), bswap(tsi->otofu2), bswap(tsi->otofl2), bswap(tsi->otat2), &stmap.master[2]); if (ret) return(-VME_EIO);
      ret = fill_mstmap(bswap(tsi->otsau3), bswap(tsi->otsal3), bswap(tsi->oteau3), bswap(tsi->oteal3), bswap(tsi->otofu3), bswap(tsi->otofl3), bswap(tsi->otat3), &stmap.master[3]); if (ret) return(-VME_EIO);
      ret = fill_mstmap(bswap(tsi->otsau4), bswap(tsi->otsal4), bswap(tsi->oteau4), bswap(tsi->oteal4), bswap(tsi->otofu4), bswap(tsi->otofl4), bswap(tsi->otat4), &stmap.master[4]); if (ret) return(-VME_EIO);
      ret = fill_mstmap(bswap(tsi->otsau5), bswap(tsi->otsal5), bswap(tsi->oteau5), bswap(tsi->oteal5), bswap(tsi->otofu5), bswap(tsi->otofl5), bswap(tsi->otat5), &stmap.master[5]); if (ret) return(-VME_EIO);
      ret = fill_mstmap(bswap(tsi->otsau6), bswap(tsi->otsal6), bswap(tsi->oteau6), bswap(tsi->oteal6), bswap(tsi->otofu6), bswap(tsi->otofl6), bswap(tsi->otat6), &stmap.master[6]); if (ret) return(-VME_EIO);
      ret = fill_mstmap(bswap(tsi->otsau7), bswap(tsi->otsal7), bswap(tsi->oteau7), bswap(tsi->oteal7), bswap(tsi->otofu7), bswap(tsi->otofl7), bswap(tsi->otat7), &stmap.master[7]); if (ret) return(-VME_EIO);

      ret = fill_slvmap(bswap(tsi->itsau0), bswap(tsi->itsal0), bswap(tsi->iteau0), bswap(tsi->iteal0), bswap(tsi->itofu0), bswap(tsi->itofl0), bswap(tsi->itat0), &stmap.slave[0]); if (ret) return(-VME_EIO);
      ret = fill_slvmap(bswap(tsi->itsau1), bswap(tsi->itsal1), bswap(tsi->iteau1), bswap(tsi->iteal1), bswap(tsi->itofu1), bswap(tsi->itofl1), bswap(tsi->itat1), &stmap.slave[1]); if (ret) return(-VME_EIO);
      ret = fill_slvmap(bswap(tsi->itsau2), bswap(tsi->itsal2), bswap(tsi->iteau2), bswap(tsi->iteal2), bswap(tsi->itofu2), bswap(tsi->itofl2), bswap(tsi->itat2), &stmap.slave[2]); if (ret) return(-VME_EIO);
      ret = fill_slvmap(bswap(tsi->itsau3), bswap(tsi->itsal3), bswap(tsi->iteau3), bswap(tsi->iteal3), bswap(tsi->itofu3), bswap(tsi->itofl3), bswap(tsi->itat3), &stmap.slave[3]); if (ret) return(-VME_EIO);
      ret = fill_slvmap(bswap(tsi->itsau4), bswap(tsi->itsal4), bswap(tsi->iteau4), bswap(tsi->iteal4), bswap(tsi->itofu4), bswap(tsi->itofl4), bswap(tsi->itat4), &stmap.slave[4]); if (ret) return(-VME_EIO);
      ret = fill_slvmap(bswap(tsi->itsau5), bswap(tsi->itsal5), bswap(tsi->iteau5), bswap(tsi->iteal5), bswap(tsi->itofu5), bswap(tsi->itofl5), bswap(tsi->itat5), &stmap.slave[5]); if (ret) return(-VME_EIO);
      ret = fill_slvmap(bswap(tsi->itsau6), bswap(tsi->itsal6), bswap(tsi->iteau6), bswap(tsi->iteal6), bswap(tsi->itofu6), bswap(tsi->itofl6), bswap(tsi->itat6), &stmap.slave[6]); if (ret) return(-VME_EIO);
      ret = fill_slvmap(bswap(tsi->itsau7), bswap(tsi->itsal7), bswap(tsi->iteau7), bswap(tsi->iteal7), bswap(tsi->itofu7), bswap(tsi->itofl7), bswap(tsi->itat7), &stmap.slave[7]); if (ret) return(-VME_EIO);

      // update level table
      if (update_data->irq_mode[0])
      {
        kdebug(("vme_rcc_tsi(ioctl,VMEUPDATE): IRQ mode parameters are valid\n"))
        for (i = 0; i < 7; i++)	
        {
          irq_level_table[i] = update_data->irq_mode[i + 1];
          kdebug(("vme_rcc_tsi(ioctl,VMEUPDATE): IRQ mode for level %d = %d\n", i, update_data->irq_mode[i + 1]))
        }
        irq_sysfail =  update_data->irq_mode[8];
        kdebug(("vme_rcc_tsi(ioctl,VMEUPDATE): IRQ mode for SYSFAIL = %d\n", update_data->irq_mode[8]))
      }
      else
        kdebug(("vme_rcc_tsi(ioctl,VMEUPDATE): IRQ mode parameters are not valid\n"))
      vmetab_ok = 1;
      kfree(update_data);
      kdebug(("vme_rcc_tsi(ioctl,VMEUPDATE): kfree called for update_data\n"))
      break;
    } 
    
    case VMESYSRST:
    {
      kdebug(("vme_rcc_tsi(ioctl,VMESYSRST): Setting SYSRESET\n"))
      tsi->vctrl |= bswap(0x00020000);   
      break;
    }  
    
    case VMESBCTYPE:
    {
      u_int sbctype = VME_SBC_TSI148; 
      kdebug(("vme_rcc_tsi(ioctl,VMESBCTYPE): This is a TSI148 based SBC\n"))
      if (copy_to_user((void *)arg, &sbctype, sizeof(u_int)))
      {
	kerror(("vme_rcc_tsi(ioctl,VMESBCTYPE): error from copy_to_user\n"))
	return(-VME_EFAULT);
      }      
      break;
    }  
    
    case VMETEST:
    {
      int ret, count;

      ret = copy_from_user(&count, (void *)arg, sizeof(int));
      if (ret)
      {
	kerror(("vme_rcc_tsi(ioctl,VMETEST): error %d from copy_from_user\n", ret))
	return(-VME_EFAULT);
      }

      count++;

      if (copy_to_user((void *)arg, &count, sizeof(int)))
      {
	kerror(("vme_rcc_tsi(ioctl,VMETEST): error from copy_to_user\n"))
	return(-VME_EFAULT);
      }

      kdebug(("vme_rcc_tsi(ioctl,VMETEST): VME_ENOSYS=0x%08x\n", VME_ENOSYS))
      return(-VME_ENOSYS);
      break;
    }
    default:
    {
      kerror(("vme_rcc_tsi(ioctl,default): cmd = %d, VME_RCC_CONSISTENCY_CHECK = 0x%08x\n", cmd, VME_RCC_CONSISTENCY_CHECK))
      kerror(("vme_rcc_tsi(ioctl,default): You should not be here\n"))
      return(-EINVAL);
    }
  }
  return(0);
}


/*********************************************************/
static void vme_rcc_tsi_vmaOpen(struct vm_area_struct *vma)
/*********************************************************/
{ 
  kdebug(("vme_rcc_tsi_vmaOpen: Called\n"));
}


/**********************************************************/
static void vme_rcc_tsi_vmaClose(struct vm_area_struct *vma)
/**********************************************************/
{ 
  kdebug(("vme_rcc_tsi_vmaClose: mmap released\n"));
}


/************************************************************************/
static int vme_rcc_tsi_mmap(struct file *file, struct vm_area_struct *vma)
/************************************************************************/
{
  u_long size, offset;  

  kdebug(("vme_rcc_tsi_mmap: vma->vm_end       = 0x%016lx\n", (u_long)vma->vm_end));
  kdebug(("vme_rcc_tsi_mmap: vma->vm_start     = 0x%016lx\n", (u_long)vma->vm_start));
  kdebug(("vme_rcc_tsi_mmap: vma->vm_pgoff     = 0x%016lx\n", (u_long)vma->vm_pgoff));
  kdebug(("vme_rcc_tsi_mmap: vma->vm_flags     = 0x%08x\n", (u_int)vma->vm_flags));
  kdebug(("vme_rcc_tsi_mmap: PAGE_SHIFT        = 0x%016lx\n", (u_long)PAGE_SHIFT));
  kdebug(("vme_rcc_tsi_mmap: PAGE_SIZE         = 0x%016lx\n", (u_long)PAGE_SIZE));

  size = vma->vm_end - vma->vm_start;
  offset = vma->vm_pgoff << PAGE_SHIFT;

  kdebug(("vme_rcc_tsi_mmap: size                  = 0x%016lx\n", size));
  kdebug(("vme_rcc_tsi_mmap: physical base address = 0x%016lx\n", offset));

  if (remap_pfn_range(vma, vma->vm_start, vma->vm_pgoff, size, vma->vm_page_prot))
  {
    kerror(("vme_rcc_tsi_mmap: function remap_pfn_range failed \n"));
    return(-VME_REMAP);
  }
  kdebug(("io_rcc_mvme_rcc_tsi_mmapmap: remap_pfn_range OK, vma->vm_start(2) = 0x%016lx\n", (u_long)vma->vm_start));

  vma->vm_ops = &vme_rcc_tsi_vm_ops;

  return(0);
}


/*************************************************************************************************************/
static ssize_t vme_rcc_tsi_proc_write(struct file *file, const char *buffer, size_t count, loff_t *startOffset)
/*************************************************************************************************************/
{
  int len;
  char value[100];

  kdebug(("vme_rcc_tsi(write_procmem): vme_rcc_tsi_write_procmem called\n"))

  if(count > 99)
    len = 99;
  else
    len = count;

  if (copy_from_user(value, buffer, len))
  {
    kdebug(("vme_rcc_tsi(write_procmem): error from copy_from_user\n"))
    return(-VME_EFAULT);
  }

  kdebug(("vme_rcc_tsi(write_procmem): len = %d\n", len))
  value[len - 1] = '\0';
  kdebug(("vme_rcc_tsi(write_procmem): text passed = %s\n", value))

  if (!strcmp(value, "debug"))
  {
    debug = 1;
    kdebug(("vme_rcc_tsi(write_procmem): debugging enabled\n"))
  }

  if (!strcmp(value, "nodebug"))
  {
    kdebug(("vme_rcc_tsi(write_procmem): debugging disabled\n"))
    debug = 0;
  }

  if (!strcmp(value, "elog"))
  {
    kdebug(("vme_rcc_tsi(write_procmem): error logging enabled\n"))
    errorlog = 1;
  }

  if (!strcmp(value, "noelog"))
  {
    kdebug(("vme_rcc_tsi(write_procmem): error logging disabled\n"))
    errorlog = 0;
  }
  return len;
}


/********************************************************/
int vme_rcc_tsi_proc_show(struct seq_file *sfile, void *p)
/********************************************************/
{
  int cnt, loop; 
  u_int ints, inten, inteo, intmask;
  
  kdebug(("vme_rcc_tsi(vme_rcc_tsi_proc_show): Creating text....\n"));

  //The COMCOMCOM below is defined in vme_rcc_common.h
  //If you execute "strings vme_rcc_tsi-3.10.0-957.21.3.el7.x86_64.ko | grep ccc" you should see "cccc1111" this has to match the result of "strings libvme_rcc.so | grep -e lll -e ccc"
  seq_printf(sfile, "VMEbus driver (ioctr %s, CS9 ready) for TDAQ release %s (based on tag %s, vme_rcc_tsi.c revision %s)\n", COMCOMCOM, RELEASE_NAME, CVSTAG, VME_RCC_TSI_TAG);

  if (board_type == VP_717) seq_printf(sfile, "Board type: VP_717\n");
  if (board_type == VP_917) seq_printf(sfile, "Board type: VP_917\n");

  seq_printf(sfile, "\nThe Tsi148 chip %s been initialized with tsiconfig\n", vmetab_ok ? "has" : "has NOT");
  seq_printf(sfile, "\n VMEbus interrupts\n\n");
  seq_printf(sfile, "  level|    state| enabled| masked| pending| # interrupts|\n");
  seq_printf(sfile, "  =====|=========|========|=======|========|=============|\n");

  ints  = bswap(tsi->ints);
  inten = bswap(tsi->inten);
  inteo = bswap(tsi->inteo);

  for (cnt = 0; cnt < 7; cnt++)
  {
    intmask = 1 << (cnt + 1);			// IRQ vector

    if (irq_level_table[cnt] == VME_LEVELISDISABLED)
      seq_printf(sfile, "      %d| disabled|     %s|    -- |     -- |          -- |\n", cnt + 1, (ints & intmask) ? "yes":" no"); //mjmj: test

    else if (irq_level_table[cnt] == VME_INT_ROAK)
      seq_printf(sfile, "      %d|     ROAK|  %6s| %6s| %7s|     %8d|\n", cnt + 1, (inten & intmask) ? "yes":" no", (inteo & intmask) ? " no":"yes", (ints & intmask) ? "yes":" no", Interrupt_counters.virq[cnt]);

    else if (irq_level_table[cnt] == VME_INT_RORA)
      seq_printf(sfile, "      %d|     RORA|  %6s| %6s| %7s|     %8d|\n", cnt + 1, (inten & intmask) ? "yes":" no", (inteo & intmask) ? " no":"yes", (ints & intmask) ? "yes":" no", Interrupt_counters.virq[cnt]);
  }

  if (irq_sysfail == VME_LEVELISDISABLED)
    seq_printf(sfile, "SYSFAIL| disabled|     %s|    -- |     -- |          -- |\n", (ints & intmask) ? "yes":" no"); //mjmj: test
  else 
    seq_printf(sfile, "SYSFAIL|     RORA|  %6s| %6s| %7s|     %8d|\n", (inten & 0x200) ? "yes":" no", (inteo & 0x200) ? "yes":" no", (ints & 0x200) ? "yes":" no", Interrupt_counters.sysfail);

  seq_printf(sfile, "\n# of Bus Error related interrupts: %d\n", berr_int_count);
  seq_printf(sfile, "# of DMA related interrupts:       %d\n", Interrupt_counters.dma);

  seq_printf(sfile, "\n Link Descriptors: \n");
  for (cnt = 0; cnt< VME_VCTSIZE; cnt++) 
  {
    if (link_table.link_dsc[cnt].pid)
    {
      seq_printf(sfile, "\n Array index     = %02d\n", cnt);
      seq_printf(sfile, " Pid             = %02d\n", link_table.link_dsc[cnt].pid);
      seq_printf(sfile, " Vector          = %02d\n", link_table.link_dsc[cnt].vct);
      seq_printf(sfile, " Level           = %02d\n", link_table.link_dsc[cnt].lvl);
      seq_printf(sfile, " Type            = %02d\n", link_table.link_dsc[cnt].type);
      seq_printf(sfile, " Pending         = %02d\n", link_table.link_dsc[cnt].pending);
      seq_printf(sfile, " # interrupts    = %02d\n", link_table.link_dsc[cnt].total_count);
      seq_printf(sfile, " group pointer   = 0x%p\n", link_table.link_dsc[cnt].group_ptr);
      seq_printf(sfile, " Semaphore count = 0x%x\n", link_table.link_dsc[cnt].sem.count);
      seq_printf(sfile, " Signal #        = %d\n", link_table.link_dsc[cnt].sig);
    }
  }

  seq_printf(sfile, " interrupt semaphore statistics\n");
  seq_printf(sfile, " semaphore count = 1: %d times\n", sem_positive[0]);
  seq_printf(sfile, " semaphore count > 1: %d times\n", sem_positive[1]);

  seq_printf(sfile, "\n BERR Links: \n");
  for (cnt = 0; cnt< VME_MAX_BERR_PROCS; cnt++) 
  {
    if (berr_proc_table.berr_link_dsc[cnt].pid)
    {
      seq_printf(sfile, "\n Array index     = %02d\n", cnt);
      seq_printf(sfile, " Pid             = %02d\n", berr_proc_table.berr_link_dsc[cnt].pid);
      seq_printf(sfile, " Signal #        = %d\n", berr_proc_table.berr_link_dsc[cnt].sig);
    }
  }

  seq_printf(sfile, "\n SYSFAIL Link: \n");
  if (sysfail_link.pid)
  {
    seq_printf(sfile, " Pid             = %02d\n", sysfail_link.pid);
    seq_printf(sfile, " Signal #        = %d\n", sysfail_link.sig);
  }

  seq_printf(sfile, "\n BERR Descriptor: \n");
  seq_printf(sfile, " VMEbus address   = 0x%08x\n", berr_dsc.vmeadd);
  seq_printf(sfile, " address modifier = 0x%08x\n", berr_dsc.am);
  seq_printf(sfile, " IACK             = %d\n", berr_dsc.iack);
  seq_printf(sfile, " LWORD*           = %d\n", berr_dsc.lword);
  seq_printf(sfile, " DS0*             = %d\n", berr_dsc.ds0);
  seq_printf(sfile, " DS1*             = %d\n", berr_dsc.ds1);
  seq_printf(sfile, " WRITE*           = %d\n", berr_dsc.wr);
  seq_printf(sfile, " multiple bit     = 0x%08x\n", berr_dsc.multiple);
  seq_printf(sfile, " bus error flag   = 0x%08x\n", berr_dsc.flag);

  seq_printf(sfile, "\n Block transfer status\n");
  seq_printf(sfile, "dma_todo_total    = %d\n", dma_todo_total);
  seq_printf(sfile, "dma_done_total    = %d\n", dma_done_total);
  seq_printf(sfile, "dmas_pending      = %d\n", dmas_pending);

  seq_printf(sfile, "        PCI address |      pid\n");
  for (loop = 0; loop < VME_DMATODOSIZE; loop++)
  {
    seq_printf(sfile, " 0x%016lx |", dma_list_todo[loop].paddr);
    seq_printf(sfile, " %8d \n", dma_list_todo[loop].pid);
  }

  seq_printf(sfile, "        PCI address |   DMA ctrl | DMA counter |      pid |in_use\n");
  for (loop = 0; loop < VME_DMATODOSIZE; loop++)
  {
    seq_printf(sfile, " 0x%016lx |", dma_list_done[loop].paddr);
    seq_printf(sfile, " 0x%08x |", dma_list_done[loop].ctrl);
    seq_printf(sfile, "  0x%08x |", dma_list_done[loop].counter);
    seq_printf(sfile, " %8d |", dma_list_done[loop].pid);
    seq_printf(sfile, "     %d \n", dma_list_done[loop].in_use);
  }

  seq_printf(sfile, "\n");
  seq_printf(sfile, "The command 'echo <action> > /proc/vme_rcc_tsi', executed as root,\n");
  seq_printf(sfile, "allows you to interact with the driver. Possible actions are:\n");
  seq_printf(sfile, "debug   -> Enable debugging\n");
  seq_printf(sfile, "nodebug -> Disable debugging\n");
  seq_printf(sfile, "elog    -> Log errors to /var/log/messages\n");
  seq_printf(sfile, "noelog  -> Do not log errors to /var/log/messages\n");
  
  return(0);
}


/************************/
/* PCI driver functions */
/************************/

/*******************************************************************************/
static int vme_rcc_tsi_Probe(struct pci_dev *dev, const struct pci_device_id *id)  
/*******************************************************************************/
{
  u_char pci_revision;
  u_int ret, size, pci_ioaddr = 0;
  u_long flags;
  
  kdebug(("vme_rcc_tsi(vme_rcc_tsi_Probe): called for device: 0x%04x / 0x%04x\n", dev->vendor, dev->device));
  kdebug(("vme_rcc_tsi(vme_rcc_tsi_Probe): Bus: 0x%04x / Devfn 0x%04x\n", dev->bus->number, dev->devfn));

  if (maxcard == 0)
  {
    kdebug(("vme_rcc_tsi(vme_rcc_tsi_Probe): First (and hopefully only) Tsi148 found\n"));
    maxcard = 1;
  }
  else
  {
    kdebug(("vme_rcc_tsi(vme_rcc_tsi_Probe): Another Tsi148 found (Multiple Tsi148 are not supported)\n"));
    return(-VME_EIO);
  }
  
  // get revision directly from the Tundra
  pci_read_config_byte(dev, PCI_REVISION_ID, &pci_revision);
  kdebug(("vme_rcc_tsi(vme_rcc_tsi_Probe): revision = %x\n", pci_revision))
  if (pci_revision != 1)
  {
    kerror(("vme_rcc_tsi(vme_rcc_tsi_Probe): Illegal Tsi148 Revision %d\n", pci_revision))
    return(-VME_ILLREV);
  }
  
  flags = pci_resource_flags(dev, BAR0);  //MJ: See p317
  if ((flags & IORESOURCE_MEM) != 0) 
  {
    pci_ioaddr = pci_resource_start(dev, BAR0) & PCI_BASE_ADDRESS_MEM_MASK;        //MJ: See p317
    size = pci_resource_end(dev, BAR0) - pci_resource_start(dev, BAR0);            //MJ: See p317
    kdebug(("vme_rcc_tsi(vme_rcc_tsi_Probe): Mapping %d bytes at PCI address 0x%08x\n", size, pci_ioaddr));
    tsi = (tsi148_regs_t *)ioremap(pci_ioaddr, size);
    if (tsi == NULL)
    {
      kerror(("vme_rcc_tsi(vme_rcc_tsi_Probe): error from ioremap\n"));
      return(-VME_EFAULT);
    }
    kdebug(("vme_rcc_tsi(vme_rcc_tsi_Probe): tsi is at 0x%016lx\n", (u_long)tsi));
  }
  else 
  {
    kerror(("vme_rcc_tsi(vme_rcc_tsi_Probe): Bar 0 is not a memory resource"));
    return(-EIO);
  }

  //Enable the device (See p314) and get the interrupt line
  ret = pci_enable_device(dev);
  vme_irq_line = dev->irq;
  kdebug(("vme_rcc_tsi(vme_rcc_tsi_Probe): interrupt line = %d \n", vme_irq_line))

  if (request_irq(vme_irq_line, tsi_irq_handler, IRQF_SHARED, "vme_rcc_tsi", tsi_irq_handler))	
  {
    kerror(("vme_rcc_tsi(vme_rcc_tsi_Probe): request_irq failed on IRQ = %d\n", vme_irq_line))
    return(-VME_REQIRQ);
  }
  kdebug(("vme_rcc_tsi(vme_rcc_tsi_Probe): request_irq OK\n"))

  //Clear and enable DMA0 interrupt
  tsi->intc  |= bswap(BM_LINT_DMA);
  tsi->inteo |= bswap(BM_LINT_DMA);
  tsi->inten |= bswap(BM_LINT_DMA);

  // Read the static master and slave mappings 
  ret = fill_mstmap(bswap(tsi->otsau0), bswap(tsi->otsal0), bswap(tsi->oteau0), bswap(tsi->oteal0), bswap(tsi->otofu0), bswap(tsi->otofl0), bswap(tsi->otat0), &stmap.master[0]); if (ret) return(-VME_EIO);
  ret = fill_mstmap(bswap(tsi->otsau1), bswap(tsi->otsal1), bswap(tsi->oteau1), bswap(tsi->oteal1), bswap(tsi->otofu1), bswap(tsi->otofl1), bswap(tsi->otat1), &stmap.master[1]); if (ret) return(-VME_EIO);
  ret = fill_mstmap(bswap(tsi->otsau2), bswap(tsi->otsal2), bswap(tsi->oteau2), bswap(tsi->oteal2), bswap(tsi->otofu2), bswap(tsi->otofl2), bswap(tsi->otat2), &stmap.master[2]); if (ret) return(-VME_EIO);
  ret = fill_mstmap(bswap(tsi->otsau3), bswap(tsi->otsal3), bswap(tsi->oteau3), bswap(tsi->oteal3), bswap(tsi->otofu3), bswap(tsi->otofl3), bswap(tsi->otat3), &stmap.master[3]); if (ret) return(-VME_EIO);
  ret = fill_mstmap(bswap(tsi->otsau4), bswap(tsi->otsal4), bswap(tsi->oteau4), bswap(tsi->oteal4), bswap(tsi->otofu4), bswap(tsi->otofl4), bswap(tsi->otat4), &stmap.master[4]); if (ret) return(-VME_EIO);
  ret = fill_mstmap(bswap(tsi->otsau5), bswap(tsi->otsal5), bswap(tsi->oteau5), bswap(tsi->oteal5), bswap(tsi->otofu5), bswap(tsi->otofl5), bswap(tsi->otat5), &stmap.master[5]); if (ret) return(-VME_EIO);
  ret = fill_mstmap(bswap(tsi->otsau6), bswap(tsi->otsal6), bswap(tsi->oteau6), bswap(tsi->oteal6), bswap(tsi->otofu6), bswap(tsi->otofl6), bswap(tsi->otat6), &stmap.master[6]); if (ret) return(-VME_EIO);
  ret = fill_mstmap(bswap(tsi->otsau7), bswap(tsi->otsal7), bswap(tsi->oteau7), bswap(tsi->oteal7), bswap(tsi->otofu7), bswap(tsi->otofl7), bswap(tsi->otat7), &stmap.master[7]); if (ret) return(-VME_EIO);

  ret = fill_slvmap(bswap(tsi->itsau0), bswap(tsi->itsal0), bswap(tsi->iteau0), bswap(tsi->iteal0), bswap(tsi->itofu0), bswap(tsi->itofl0), bswap(tsi->itat0), &stmap.slave[0]); if (ret) return(-VME_EIO);
  ret = fill_slvmap(bswap(tsi->itsau1), bswap(tsi->itsal1), bswap(tsi->iteau1), bswap(tsi->iteal1), bswap(tsi->itofu1), bswap(tsi->itofl1), bswap(tsi->itat1), &stmap.slave[1]); if (ret) return(-VME_EIO);
  ret = fill_slvmap(bswap(tsi->itsau2), bswap(tsi->itsal2), bswap(tsi->iteau2), bswap(tsi->iteal2), bswap(tsi->itofu2), bswap(tsi->itofl2), bswap(tsi->itat2), &stmap.slave[2]); if (ret) return(-VME_EIO);
  ret = fill_slvmap(bswap(tsi->itsau3), bswap(tsi->itsal3), bswap(tsi->iteau3), bswap(tsi->iteal3), bswap(tsi->itofu3), bswap(tsi->itofl3), bswap(tsi->itat3), &stmap.slave[3]); if (ret) return(-VME_EIO);
  ret = fill_slvmap(bswap(tsi->itsau4), bswap(tsi->itsal4), bswap(tsi->iteau4), bswap(tsi->iteal4), bswap(tsi->itofu4), bswap(tsi->itofl4), bswap(tsi->itat4), &stmap.slave[4]); if (ret) return(-VME_EIO);
  ret = fill_slvmap(bswap(tsi->itsau5), bswap(tsi->itsal5), bswap(tsi->iteau5), bswap(tsi->iteal5), bswap(tsi->itofu5), bswap(tsi->itofl5), bswap(tsi->itat5), &stmap.slave[5]); if (ret) return(-VME_EIO);
  ret = fill_slvmap(bswap(tsi->itsau6), bswap(tsi->itsal6), bswap(tsi->iteau6), bswap(tsi->iteal6), bswap(tsi->itofu6), bswap(tsi->itofl6), bswap(tsi->itat6), &stmap.slave[6]); if (ret) return(-VME_EIO);
  ret = fill_slvmap(bswap(tsi->itsau7), bswap(tsi->itsal7), bswap(tsi->iteau7), bswap(tsi->iteal7), bswap(tsi->itofu7), bswap(tsi->itofl7), bswap(tsi->itat7), &stmap.slave[7]); if (ret) return(-VME_EIO);

  kdebug(("vme_rcc_tsi(vme_rcc_tsi_Probe): function OK\n"))
  return(0);
}


/*************************************************/
static void vme_rcc_tsi_Remove(struct pci_dev *dev) 
/*************************************************/
{
  u_int i, reg;

  for (i = 0; i < 7; i++)				// restore initial enable bits from vmetab 
  {
    kdebug(("vme_rcc_tsi(vme_rcc_tsi_Remove): irq_level_table[%d] =0x%x\n", i, irq_level_table[i]))
    if (irq_level_table[i] != VME_LEVELISDISABLED)
    {
      reg = bswap(tsi->inteo);                         // read enabled irq bits
      kdebug(("vme_rcc_tsi(vme_rcc_tsi_Remove): reg =0x%x\n", reg))
      reg |= (1 << (i + 1)); 			// select level
      kdebug(("vme_rcc_tsi(vme_rcc_tsi_Remove): reg =0x%x\n", reg))
      tsi->inteo = bswap(reg);
    }
  }

  kdebug(("vme_rcc_tsi(vme_rcc_tsi_Remove): irqsysfail = 0x%x\n", irq_sysfail))
  if (irq_sysfail != VME_LEVELISDISABLED)
  {
    reg = bswap(tsi->inteo);	// read enabled irq bits
    kdebug(("vme_rcc_tsi(vme_rcc_tsi_Remove): reg =0x%x\n", reg))
    reg |= 0x200; 			// SYSFAIL
    kdebug(("vme_rcc_tsi(vme_rcc_tsi_Remove): reg =0x%x\n", reg))
    tsi->inteo = bswap(reg);
  }
  
  free_irq(vme_irq_line, tsi_irq_handler);	
  kdebug(("vme_rcc_tsi(vme_rcc_tsi_Remove): interrupt line %d released\n", vme_irq_line))
 
  iounmap((void *)tsi);
  kerror(("vme_rcc_tsi(vme_rcc_tsi_Remove): Tsi148 removed\n"));
  maxcard = 0;
}



/*********************/
/* Service functions */
/*********************/


// Interrupt timeout function. Called when an interrupt fails to occur following a call to VME_InterruptWait()
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,0,0)
/************************************/
static void vme_intTimeout(u_long arg)
/************************************/
{
  VME_WaitInt_t *iPtr;
  int first_vct;

  iPtr = (VME_WaitInt_t *) arg;
  iPtr->timeout = 0;                            // zero to indicate timeout has occurred
  first_vct = iPtr->int_handle.vector[0];
  kdebug(("vme_rcc(vme_intTimeout): first vector = 0x%x\n", first_vct))
  up(&link_table.link_dsc[first_vct].sem);      // wake up WAIT
}
#else
/***************************************/
void vme_intTimeout(struct timer_list *t)
/***************************************/
{
  int first_vct = 0;
  up(&link_table.link_dsc[first_vct].sem);	// wake up WAIT
}
#endif

// Interrupt timeout function. Called when an interrupt fails to occur following a call to VME_SysfailInterruptWait()
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,0,0)
/*******************************************/
static void vme_intSysfailTimeout(u_long arg)
/*******************************************/
{
  VME_WaitInt_t *iPtr;

  iPtr = (VME_WaitInt_t *)arg;
  kdebug(("vme_rcc_tsi(vme_intSysfailTimeout): timeout = %d\n", iPtr->timeout))
  iPtr->timeout = 0;                            // zero to indicate timeout has occurred
  up(&sysfail_link.sem);			// wake up WAIT
}
#else
  /**********************************************/
  void vme_intSysfailTimeout(struct timer_list *t)
  /**********************************************/
  {
    kdebug(("vme_rcc(vme_intSysfailTimeout): timeout\n"))
    up(&sysfail_link.sem);                      // wake up WAIT
  }
#endif


/*****************************/
static void init_cct_berr(void)
/*****************************/
{
  tsi->veat = bswap(0x20000000);                 // clear any old errors
  tsi->inten = bswap(bswap(tsi->inten) | BM_LINT_VERR); // Enable the interrupt on VMEbus BERR
  tsi->inteo = bswap(bswap(tsi->inteo) | BM_LINT_VERR); // Unmask the interrupt on VMEbus BERR
  kdebug(("vme_rcc_tsi(init_cct_berr): tsi->inten = 0x%08x\n", bswap(tsi->inten)))
  kdebug(("vme_rcc_tsi(init_cct_berr): tsi->inteo = 0x%08x\n", bswap(tsi->inteo)))
  berr_interlock = 0;        
}


/*****************************/
static void mask_cct_berr(void)
/*****************************/
{
  berr_interlock = 1;                                    // If the interrupt is disabled (we do this for safe cycles with synchronous BERR checking) we can ignore BERRs in the ISR as a possible source of interrupts
  tsi->inten = bswap(bswap(tsi->inten) & ~BM_LINT_VERR); // Disable the interrupt on VMEbus BERR
  tsi->inteo = bswap(bswap(tsi->inteo) & ~BM_LINT_VERR); // Mask the interrupt on VMEbus BERR

  kdebug(("vme_rcc_tsi(mask_cct_berr): tsi->inten = 0x%08x\n", bswap(tsi->inten)))
  kdebug(("vme_rcc_tsi(mask_cct_berr): tsi->inteo = 0x%08x\n", bswap(tsi->inteo)))
}


//This is the DMA completion error. DMA provoked errors included
/*******************************/
static void vme_dma_handler(void)
/*******************************/
{       
  u_int uivalue;
  u_int loop, windex = 0, loop_dgcs;
  
  dma_done_total++;

  uivalue = bswap(tsi->dsta0);
  if (!(uivalue & 0x1e000000))
  {
    kdebug(("vme_rcc_tsi(vme_dma_handler)(%d): ERROR: uivalue IS NULL. DGCS = 0x%08x\n", current->pid, uivalue))
    
    for(loop_dgcs = 0; loop_dgcs < 1000; loop_dgcs++)
    {
      uivalue = bswap(tsi->dsta0);      
      if(uivalue & 0x1e000000)
      {
        kdebug(("vme_rcc_tsi(vme_dma_handler)(%d): No ERROR: uivalue NOW IS NOT NULL. DGCS = 0x%08x, loop=%d\n", current->pid, uivalue, loop_dgcs))
        break;    
      }
    }
  
    uivalue = bswap(tsi->dsta0);      
    if (!(uivalue & 0x1e000000))
    {
      kerror(("vme_rcc_tsi(vme_dma_handler)(%d): ERROR: uivalue IS STILL NULL. DGCS = 0x%08x\n", current->pid, uivalue))    
      debug = 1;     
      kdebug(("vme_rcc_tsi(vme_dma_handler)(%d): auto-enabling debugging\n", current->pid))
    }
  }
  
  //A DMA has completed. Store the result in the dma_list_done
  spin_lock_irqsave(&dmalock, dma_irq_flags); 
  for(loop = 0; loop < VME_DMATODOSIZE; loop++)
  {
    if (dma_list_done[loop].in_use == 0)
    {
      windex = loop;
      dma_list_done[loop].in_use = 1;
      kdebug(("vme_rcc_tsi(vme_dma_handler)(%d): Empty element found at index %d\n", current->pid, windex))   
      break; 
    }
  }
  
  if(windex == VME_DMATODOSIZE)
  {
    kerror(("vme_rcc_tsi(vme_dma_handler)(%d): no element free in the array. This must never happen\n", current->pid))    
    spin_unlock_irqrestore(&dmalock, dma_irq_flags); 
    return;
  }
  
  dma_list_done[windex].ctrl    = uivalue;
  dma_list_done[windex].counter = bswap(tsi->dcnt0);
  dma_list_done[windex].paddr   = dma_current_pciaddr;
  dma_list_done[windex].pid     = dma_current_pid;
  dma_list_done[windex].dclau   = bswap(tsi->dclau0);
  dma_list_done[windex].dclal   = bswap(tsi->dclal0);

  tsi->intc  |= bswap(BM_LINT_DMA);   //Clear the DMA interrupt

  dma_is_free = 1;

  spin_unlock_irqrestore(&dmalock, dma_irq_flags); 

  wake_up_interruptible(&dma_wait);
  start_next_dma(1);  //Check if another DMA is pending

  kdebug(("vme_rcc_tsi(vme_dma_handler)(%d): done\n", current->pid))
}


/* function for hanling VMEbus error related interrupts
*  If there is a BERR interrupt: 
*  If the BERR descriptor is empty, fill it and set the count = 1, else just count++*/
/**************************/
static int cct_berrInt(void)
/**************************/
{
  int result = 0;
  u_int data;

  data = bswap(tsi->veat);	        // error with single cycle ?
  kdebug(("vme_rcc_tsi(cct_berrInt): veat = 0x%x\n", data))
  if (data & 0x80000000)		// Error captured?
  {
    if (berr_dsc.flag == 0)		// copy info from Tsi148 into BERR descriptor
    {
      if (board_type == VP_717 || board_type == VP_917)
        read_berr_capture();
      kdebug(("vme_rcc_tsi(cct_berrInt): VAERR = 0x%x\n", berr_dsc.vmeadd))
      kerror(("vme_rcc_tsi(cct_berrInt): VMEbus error: address = 0x%8x, AM code = 0x%8x\n", berr_dsc.vmeadd, berr_dsc.am))   // log the error
      send_berr_signals();              // send signals, if anybody registered
      berr_dsc.flag = 1;	        // reset by BusErrorInfoGet or RegisterSignal
    }
    else
      kdebug(("vme_rcc_tsi(cct_berrInt): berr_dsc.flag is set\n"))
  }
  result = 1;		                // BERR
  berr_int_count++;

/*
======================================================
In the universe version this function checks DMA errors. The code that detects DMA BERR is:
  if ((uni->dgcs & 0x700))                      // was there a DMA BERR
If the TSI has the same concept we will need this code (or a variant thereof)
----------------------------
    
//Store the result in the dma_list_done
    spin_lock_irqsave(&dmalock, dma_irq_flags); 
    for(loop = 0; loop < VME_DMATODOSIZE; loop++)
    {
      if (dma_list_done[loop].in_use == 0)
      {
	windex = loop;
	dma_list_done[loop].in_use = 1;
	kdebug(("vme_rcc_tsi(cct_berrInt)(%d): Empty element found at index %d\n", current->pid, windex))   
	break; 
      }
    }

    if(windex == VME_DMATODOSIZE)
    {
      kerror(("vme_rcc_tsi(cct_berrInt)(%d): no element free in the array. This must never happen\n", current->pid))    
      spin_unlock_irqrestore(&dmalock, dma_irq_flags); 
      return(1);
    }

    dma_list_done[windex].ctrl    = uni->dgcs & 0xf00;
    dma_list_done[windex].counter = uni->dtbc;
    dma_list_done[windex].paddr   = dma_current_pciaddr;
    dma_list_done[windex].pid     = dma_current_pid;
    
    uni->dctl=0;
    uni->dtbc=0;
    uni->dla =0;
    uni->dva =0;
    uni->dcpp=0;
    uni->dgcs=0x6f00;
    
    dma_is_free = 1;
    spin_unlock_irqrestore(&dmalock, dma_irq_flags); 
  
    if (berr_dsc.flag == 0)       // copy info from Universe into BERR descriptor
    { 
      if (board_type == VP_100 || board_type == VP_110 || board_type == VP_315 || board_type == VP_317 || board_type == VP_327 || board_type == VP_417)
        read_berr_capture();
      else
      {
        berr_dsc.vmeadd = 0xffffffff;
        berr_dsc.am = 0xffffffff;
        berr_dsc.iack = 0xffffffff;
        berr_dsc.multiple = 0xffffffff;
        berr_dsc.lword = 0xffffffff;
        berr_dsc.ds0 = 0xffffffff;
        berr_dsc.ds1 = 0xffffffff;
        berr_dsc.wr = 0xffffffff;
      }
      // log the error
      kerror(("vme_rcc_tsi(cct_berrInt): bus error on VMEbus block transfer, current pid = %d\n", current->pid))
      berr_dsc.flag = 1;		                // reset by BusErrorInfoGet or RegisterSignal
    }
    
    wake_up_interruptible(&dma_wait);
    start_next_dma(1);  //Check if another DMA is pending
    
    kdebug(("vme_rcc_tsi(cct_berrInt): done\n"))
 ----------------------------
======================================================
*/


  return(result);
}


/***********************************/
static void sysfail_irq_handler(void)
/***********************************/
{
  u_int inteo;
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,0,0)
  struct siginfo info;
#else
  struct kernel_siginfo info;
#endif
  struct task_struct *task;

  // The SYSFAIL interupt is active as long as the SYSFAIL signal is asserted on the bus
  // In order to prevent a deadlock we disable the interrupt here and provide an ioctl() to re-enable it
  inteo = bswap(tsi->inteo);    // read enabled irq bits
  kdebug(("vme_rcc_tsi(sysfail_irq_handler): inteo = 0x%x\n", inteo))
  inteo &= 0xfffffdff;				
  kdebug(("vme_rcc_tsi(sysfail_irq_handler): inteo after masking = 0x%x\n", inteo))
  tsi->inteo = bswap(inteo);	//disable SYSFAIL

  if (sysfail_link.pid == 0)	// not linked: "spurious" interrupt
  {
    kerror(("vme_rcc_tsi(sysfail_irq_handler): SYSFAIL interrupt not linked\n"))
    kerror(("vme_rcc_tsi(sysfail_irq_handler): SYSFAIL interrupt disabled\n"))
  }
  else
  {
    if (sysfail_link.sig > 0)
    {
      kdebug(("vme_rcc_tsi(sysfail_irq_handler): pid = %d, signal = %d\n", sysfail_link.pid, sysfail_link.sig))
      
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,0,0)
        memset(&info, 0, sizeof(struct siginfo));   // Generate the struct for signals
#else
        memset(&info, 0, sizeof(struct kernel_siginfo));   // Generate the struct for signals
#endif
      
      info.si_signo = sysfail_link.sig;           // Config the signals
      info.si_code = SI_QUEUE;                    // Config SI_QUERE std or SI_KERNEL for RTC
      task = pid_task(find_vpid(sysfail_link.pid), PIDTYPE_PID); 
      if(task == NULL)
        kerror(("vme_rcc_tsi(sysfail_irq_handler): Error finding the pid task\n"));
      send_sig_info(sysfail_link.sig, &info, task);
    }
    else
      up(&sysfail_link.sem);
  } 
}


/************************************/
static void vme_irq_handler(int level)
/************************************/
{
  u_char vector; 
  u_long offset;
  int first_vector;
  u_int inteo;
  VME_IntHandle_t* group_ptr;
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,0,0)
  struct siginfo info;
#else
  struct kernel_siginfo info;
#endif
  struct task_struct *task;

  Interrupt_counters.virq[level - 1]++;
  
  offset = (u_long)&tsi->viack1 + 4 * (level - 1);
  kdebug(("vme_rcc_tsi(vme_irq_handler): level is %d\n", level))
  vector = readb((char*)offset);	                //mjmj: This will only work for 8bit IACK!!	
  if ((bswap(tsi->veat) & 0x80010000) == 0x80010000)
  {
    kerror(("vme_rcc_tsi(vme_irq_handler): bus error during IACK cycle on level %d\n", level))
  }
  else
  {
    kdebug(("vme_rcc_tsi(vme_irq_handler): vector = 0x%08x\n", vector))

    if (link_table.link_dsc[vector].pid == 0)	        // not linked: "spurious" interrupt
    {
      //mjmj Remove once IRQs are debugged
      kerror(("vme_rcc_tsi(vme_irq_handler): interrupt with vector %d not linked\n", vector))
    }
    else		// handle it
    {
      if (irq_level_table[level-1] == VME_INT_RORA)
      {
        inteo = bswap(tsi->inteo);	                // read enabled irq bits
        kdebug(("vme_rcc_tsi(vme_irq_handler): level = %d, inteo = 0x%x\n", level, inteo))
        inteo &= ~(1 << level);				// BM_LINT_VIRQxxx
        kdebug(("vme_rcc_tsi(vme_irq_handler): inteo after masking = 0x%x\n", inteo))
        tsi->inteo = bswap(inteo);	 	        //disable irq
      }

      link_table.link_dsc[vector].lvl = level;	        // store in link descriptor
      link_table.link_dsc[vector].pending++;
      link_table.link_dsc[vector].total_count++;

      // signal FIRST(common) semaphore in a group
      // for signals: use signal in the link descriptor for "vector"
      group_ptr = link_table.link_dsc[vector].group_ptr;
      if (group_ptr == NULL)
        first_vector = vector;
      else 
        first_vector = group_ptr->vector[0];
      kdebug(("vme_rcc_tsi(vme_irq_handler): first vector = 0x%x\n",first_vector))

      if (link_table.link_dsc[vector].sig > 0)
      {
        kdebug(("vme_rcc_tsi(vme_irq_handler): pid = %d, signal = %d\n", link_table.link_dsc[vector].pid, link_table.link_dsc[vector].sig))

#if LINUX_VERSION_CODE < KERNEL_VERSION(4,14,0)
          memset(&info, 0, sizeof(struct siginfo));                // Generate the struct for signals
#else
          memset(&info, 0, sizeof(struct kernel_siginfo));         // Generate the struct for signals
#endif
	
	info.si_signo = link_table.link_dsc[vector].sig;           // Config the signals
	info.si_code = SI_QUEUE;                                   // Config SI_QUERE std or SI_KERNEL for RTC
        task = pid_task(find_vpid(link_table.link_dsc[vector].pid), PIDTYPE_PID); 
	if(task == NULL)
          kerror(("vme_rcc_tsi(vme_irq_handler): Error finding the pid task\n"));
	send_sig_info(link_table.link_dsc[vector].sig, &info, task);
      }
      else
      {
        up(&link_table.link_dsc[first_vector].sem);
      }
    } 
  }
}


// This function handles Tsi148 Interrupts
/*******************************************************/
static irqreturn_t tsi_irq_handler(int irq, void *dev_id)
/*******************************************************/
{
  u_int ints, intstat, inteo, inten;
  //__asm__ volatile ("rdtsc" :"=a" (yy3), "=d" (yy4));

  ints = bswap(tsi->ints);	        // read the interrupt bits
  inteo = bswap(tsi->inteo);	        // read enabled irq bits
  inten = bswap(tsi->inten);
  intstat = ints & inten;		// and look only at enabled irqs
  kdebug(("vme_rcc_tsi(tsi_irq_handler): ints    = 0x%x\n", ints))
  kdebug(("vme_rcc_tsi(tsi_irq_handler): inteo   = 0x%x\n", inteo))
  kdebug(("vme_rcc_tsi(tsi_irq_handler): inten   = 0x%x\n", inten))
  kdebug(("vme_rcc_tsi(tsi_irq_handler): intstat = 0x%x\n", intstat))

/*  M
  if (!berr_interlock && cct_berrInt())	// first check and handle VMEbus errors
  {
    kdebug(("vme_rcc_tsi(tsi_irq_handler): exiting\n"))
    return(IRQ_HANDLED);                // MJ: to be checked
  }
*/  
  //handle the interrupts sequentially
  
  if ((intstat & BM_LINT_VERR) && !berr_interlock)  cct_berrInt();
  if (intstat & BM_LINT_VIRQ7)                      vme_irq_handler(7);
  if (intstat & BM_LINT_VIRQ6)                      vme_irq_handler(6);
  if (intstat & BM_LINT_VIRQ5)                      vme_irq_handler(5);
  if (intstat & BM_LINT_VIRQ4)                      vme_irq_handler(4);
  if (intstat & BM_LINT_VIRQ3)                      vme_irq_handler(3);
  if (intstat & BM_LINT_VIRQ2)                      vme_irq_handler(2);
  if (intstat & BM_LINT_VIRQ1)                      vme_irq_handler(1);
  if (intstat & BM_LINT_DMA)                        { Interrupt_counters.dma++;     vme_dma_handler();     }
  if (intstat & BM_LINT_SYSFAIL)                    { Interrupt_counters.sysfail++; sysfail_irq_handler(); }
  
  // These errors will just be counted but do not trigger any action
  if (intstat & BM_LINT_ACFAIL)  Interrupt_counters.acfail++;
  if (intstat & BM_LINT_VERR)    Interrupt_counters.verr++;
  if (intstat & BM_LINT_LERR)    Interrupt_counters.lerr++;
      
  // clear only the enabled bits. In particular do not to clear the DMA IRQ again
  intstat &= ~(BM_LINT_DMA);
  kdebug(("vme_rcc_tsi(tsi_irq_handler): new intstat = 0x%x\n", intstat))
  
  tsi->intc = bswap(intstat);  // clear only the enabled bits
  kdebug(("vme_rcc_tsi(tsi_irq_handler): Interrupts cleared\n"))
  return(IRQ_HANDLED);         // MJ: for 2.6 see p 273   //MJ: we also have to treat IRQ_NONE
}


/**************************************************************************************************************************************/
static int fill_mstmap(u_int otsau, u_int otsal, u_int oteau, u_int oteal, u_int otofu, u_int otofl, u_int otat, mstslvmap_t *mstslvmap)
/**************************************************************************************************************************************/
{
  unsigned long long out_start, out_end, out_offset;

  kdebug(("vme_rcc_tsi(fill_mstmap): otsau = 0x%08x\n", otsau));
  kdebug(("vme_rcc_tsi(fill_mstmap): otsal = 0x%08x\n", otsal));
  kdebug(("vme_rcc_tsi(fill_mstmap): oteau = 0x%08x\n", oteau));
  kdebug(("vme_rcc_tsi(fill_mstmap): oteal = 0x%08x\n", oteal));
  kdebug(("vme_rcc_tsi(fill_mstmap): otofu = 0x%08x\n", otofu));
  kdebug(("vme_rcc_tsi(fill_mstmap): otofl = 0x%08x\n", otofl));
  kdebug(("vme_rcc_tsi(fill_mstmap): otat  = 0x%08x\n", otat));

  out_start  = ((unsigned long long)otsau << 32) + otsal; 
  out_end    = ((unsigned long long)oteau << 32) + oteal;
  out_offset = ((unsigned long long)otofu << 32) + otofl;
 
  kdebug(("vme_rcc_tsi(fill_mstmap): out_start  = 0x%016llx\n", out_start));
  kdebug(("vme_rcc_tsi(fill_mstmap): out_end    = 0x%016llx\n", out_end));
  kdebug(("vme_rcc_tsi(fill_mstmap): out_offset = 0x%016llx\n", out_offset));

  mstslvmap->vbase   = out_start + out_offset;
  mstslvmap->vtop    = out_end + out_offset + 0xffff;  //We have to add 64 KB because the map decoder has a 64 KB granularity
  mstslvmap->pbase   = out_start;
  mstslvmap->ptop    = out_end + 0xffff;  //We have to add 64 KB because the map decoder has a 64 KB granularity
  mstslvmap->am      = otat & 0xf;   //0 = A16, 1 = A24, 2 = A32, etc.
  mstslvmap->options = otat & 0x00000030;
  mstslvmap->enab    = (otat >> 31) & 0x1;
  kdebug(("vme_rcc_tsi(fill_mstmap): mstslvmap->vbase   = 0x%016llx @  0x%016lx\n", mstslvmap->vbase, (u_long) &mstslvmap->vbase))
  kdebug(("vme_rcc_tsi(fill_mstmap): mstslvmap->vtop    = 0x%016llx @  0x%016lx\n", mstslvmap->vtop, (u_long) &mstslvmap->vtop))
  kdebug(("vme_rcc_tsi(fill_mstmap): mstslvmap->am      = %d @  0x%016lx\n", mstslvmap->am, (u_long) &mstslvmap->am))
  kdebug(("vme_rcc_tsi(fill_mstmap): mstslvmap->options = %d @  0x%016lx\n", mstslvmap->options, (u_long) &mstslvmap->options))
  kdebug(("vme_rcc_tsi(fill_mstmap): mstslvmap->enab    = %d @  0x%016lx\n", mstslvmap->enab, (u_long) &mstslvmap->enab))
  return(0);
}


/**************************************************************************************************************************************/
static int fill_slvmap(u_int itsau, u_int itsal, u_int iteau, u_int iteal, u_int itofu, u_int itofl, u_int itat, mstslvmap_t *mstslvmap)
/**************************************************************************************************************************************/
{
  unsigned long long in_start, in_end, in_offset;

  kdebug(("vme_rcc_tsi(fill_slvmap): itsau = 0x%08x\n", itsau));
  kdebug(("vme_rcc_tsi(fill_slvmap): itsal = 0x%08x\n", itsal));
  kdebug(("vme_rcc_tsi(fill_slvmap): iteau = 0x%08x\n", iteau));
  kdebug(("vme_rcc_tsi(fill_slvmap): iteal = 0x%08x\n", iteal));
  kdebug(("vme_rcc_tsi(fill_slvmap): itofu = 0x%08x\n", itofu));
  kdebug(("vme_rcc_tsi(fill_slvmap): itofl = 0x%08x\n", itofl));
  kdebug(("vme_rcc_tsi(fill_slvmap): itat  = 0x%08x\n", itat));

  in_start  = ((unsigned long long)itsau << 32) + itsal; 
  in_end    = ((unsigned long long)iteau << 32) + iteal;
  in_offset = ((unsigned long long)itofu << 32) + itofl;
 
  kdebug(("vme_rcc_tsi(fill_slvmap): in_start  = 0x%016llx\n", in_start));
  kdebug(("vme_rcc_tsi(fill_slvmap): in_end    = 0x%016llx\n", in_end));
  kdebug(("vme_rcc_tsi(fill_slvmap): in_offset = 0x%016llx\n", in_offset));
  
  mstslvmap->vbase = in_start;
  kdebug(("vme_rcc_tsi(fill_slvmap): mstslvmap->vbase   = 0x%016llx @  0x%016lx\n", mstslvmap->vbase, (u_long) &mstslvmap->vbase))
  mstslvmap->vtop  = in_end;
  mstslvmap->pbase = in_start + in_offset;
  mstslvmap->ptop  = in_end + in_offset;
  mstslvmap->am    = (itat >> 4) & 0x7;   //0 = A16, 1 = A24, 2 = A32
  mstslvmap->enab  = (itat >> 31) & 0x1;
  return(0);
}


/*********************************/
static void send_berr_signals(void)
/*********************************/
{
  u_int i;
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,14,0)
  struct siginfo info;
#else
  struct kernel_siginfo info;
#endif
  struct task_struct *task;

  for (i = 0; i < VME_MAX_BERR_PROCS; i++)
  {
    if (berr_proc_table.berr_link_dsc[i].pid)
    {
      kdebug(("vme_rcc_tsi(send_berr_signals): index = %d, pid (current)= %d, signal = %d\n", i, berr_proc_table.berr_link_dsc[i].pid, berr_proc_table.berr_link_dsc[i].sig))
      
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,14,0)
        memset(&info, 0, sizeof(struct siginfo));                       // Generate the struct for signals
#else
        memset(&info, 0, sizeof(struct kernel_siginfo));                // Generate the struct for signals
#endif

      info.si_signo = berr_proc_table.berr_link_dsc[i].sig;           // Config the signals
      info.si_code = SI_QUEUE;                                        // Config SI_QUERE std or SI_KERNEL for RTC
      task = pid_task(find_vpid(berr_proc_table.berr_link_dsc[i].pid), PIDTYPE_PID); // Find the task from pid
      if(task == NULL)
        kerror(("vme_rcc_tsi(send_berr_signals): Error finding the pid task\n"));
      send_sig_info(berr_proc_table.berr_link_dsc[i].sig, &info, task);     
    }
  }
}

/*********************************************************/
static int berr_check(u_int *addr, u_int *multi, u_int *am)
/*********************************************************/
{ 
  //This function is to look for single cycle related bus errors
  //It does not handle BERRs of DMAs or IACK cycles
  u_int data;

  kdebug(("vme_rcc_tsi(berr_check): called\n"))

  data = bswap(tsi->veat);
  if ((data & 0x80080000) != 0x80080000)
  {
    kdebug(("vme_rcc_tsi(berr_check): No BERR detected\n"))
    return(0);               
  }
  
  if (bswap(tsi->dsta0) & 0x10000000)
  {
    kdebug(("vme_rcc_tsi(berr_check): BERR of DMA detected\n"))
    return(0);               
  }
  
  if (data & 0x00010000)
  {
    kdebug(("vme_rcc_tsi(berr_check): BERR of IACK detected\n"))
    return(0);               
  }
  
  *am    = (data >> 8) & 0x3f;
  *multi = (data >> 30) & 0x1;
  *addr  = bswap(tsi->veal);
  tsi->veat = bswap(0x20000000);
  kdebug(("vme_rcc_tsi(berr_check): BERR on single cycle detected\n"))
  return(VME_BUSERROR);
}


/*********************************/
static void read_berr_capture(void)
/*********************************/
{  
  u_int reg;
  
  reg = bswap(tsi->veat);
  if (!(reg & 0x80000000))
  {
    berr_dsc.vmeadd   = 0xffffffff;
    berr_dsc.am       = 0xffffffff;
    berr_dsc.iack     = 0xffffffff;
    berr_dsc.multiple = 0xffffffff;
    berr_dsc.lword    = 0xffffffff;
    berr_dsc.ds0      = 0xffffffff;
    berr_dsc.ds1      = 0xffffffff;
    berr_dsc.wr       = 0xffffffff;
    kerror(("vme_rcc_tsi(read_berr_capture): No bus error logged\n"))
    return;
  }
  
  berr_dsc.vmeadd   = bswap(tsi->veal);
  berr_dsc.am       = (reg >> 8) & 0x3f;
  berr_dsc.multiple = (reg >> 30) & 0x1;
  berr_dsc.lword    = 1 - ((reg >> 18) & 0x1);  //We have to invert the bit to get the right value for an active low signal
  berr_dsc.ds0      = 1 - ((reg >> 14) & 0x1);  //We have to invert the bit to get the right value for an active low signal
  berr_dsc.ds1      = 1 - ((reg >> 15) & 0x1);  //We have to invert the bit to get the right value for an active low signal
  berr_dsc.wr       = 1 - ((reg >> 17) & 0x1);  //We have to invert the bit to get the right value for an active low signal
  berr_dsc.iack     = 1 - ((reg >> 16) & 0x1);  //We have to invert the bit to get the right value for an active low signal

  kdebug(("vme_rcc_tsi(read_berr_capture): berr_dsc.vmeadd = 0x%08x\n", berr_dsc.vmeadd))
  kdebug(("vme_rcc_tsi(read_berr_capture): berr_dsc.am     = 0x%02x\n", berr_dsc.am))
  kdebug(("vme_rcc_tsi(read_berr_capture): berr_dsc.iack   = 0x%02x\n", berr_dsc.iack))
  kdebug(("vme_rcc_tsi(read_berr_capture): berr_dsc.lword  = %d\n", berr_dsc.lword))
  kdebug(("vme_rcc_tsi(read_berr_capture): berr_dsc.ds0    = %d\n", berr_dsc.ds0))
  kdebug(("vme_rcc_tsi(read_berr_capture): berr_dsc.ds1    = %d\n", berr_dsc.ds1))
  kdebug(("vme_rcc_tsi(read_berr_capture): berr_dsc.write  = %d\n", berr_dsc.wr))
  
  tsi->veat = bswap(0x20000000); //Clear error
}


/****************************/
static u_int bswap(u_int word)
/****************************/
{
  return ((word & 0xFF) << 24) + ((word & 0xFF00) << 8) + ((word & 0xFF0000) >> 8) + ((word & 0xFF000000) >> 24);
}




/**************************/
void start_next_dma(int src)
/**************************/
{
  static u_int rindex;
  volatile u_int regdata;
  u_int add_low, add_high;

  kdebug(("vme_rcc_tsi(start_next_dma)(%d, %d): called \n", src, current->pid))
  
  spin_lock_irqsave(&dmalock, dma_irq_flags); 
  //Is the DMA conroller free?
  if (!dma_is_free)
  {
    kdebug(("vme_rcc_tsi(start_next_dma)(%d, %d): DMA controller is not free\n", src, current->pid))
    spin_unlock_irqrestore(&dmalock, dma_irq_flags); 
    return;
  }
  
  regdata = bswap(tsi->dsta0);
  if (regdata & 0x01000000)  //Check the "BSY" bit
    kerror(("vme_rcc_tsi(start_next_dma)(%d, %d): DMA controller is busy. regdata = 0x%08x\n", src, current->pid, regdata))
    
  //Is a request DMA pending?
  if (dma_todo_num == 0)
  {
    kdebug(("vme_rcc_tsi(start_next_dma)(%d, %d): No transfers pending\n", src, current->pid))
    spin_unlock_irqrestore(&dmalock, dma_irq_flags); 
    return;
  }
  dma_todo_num--;
  rindex = dma_todo_read;
  dma_todo_read++;
  if (dma_todo_read == VME_DMATODOSIZE)
    dma_todo_read = 0;
  
  kdebug(("vme_rcc_tsi(start_next_dma)(%d, %d): Starting transfer with rindex = %d and paddr = 0x%016lx and pid = %d\n", src, current->pid, rindex, dma_list_todo[rindex].paddr, dma_list_todo[rindex].pid))
  
  dma_current_pid     = dma_list_todo[rindex].pid;
  dma_current_pciaddr = dma_list_todo[rindex].paddr;

  add_high = dma_list_todo[rindex].paddr >> 32;
  add_low  = dma_list_todo[rindex].paddr & 0xffffffff;
  kdebug(("vme_rcc_tsi(start_next_dma): add_low  = 0x%08x\n", add_low))
  kdebug(("vme_rcc_tsi(start_next_dma): add_high = 0x%08x\n", add_high))

  tsi->inteo |= bswap(BM_LINT_DMA);  //enable EOT interrupt
  tsi->dnlau0 = bswap(add_high);
  tsi->dnlal0 = bswap(add_low);
  tsi->dctl0  = bswap(0x02006070);  //Go!  mjmj: Check if it is OK to use the max. block size  //MJ: CCT still investigating why VBKS=7 does not work well
  kdebug(("vme_rcc_tsi(start_next_dma): Transfer started with dctl0 = 0x%08x\n", bswap(tsi->dctl0)))
    
  dma_is_free = 0;
  
  spin_unlock_irqrestore(&dmalock, dma_irq_flags); 
}   


/***********************************************/
int dma_is_done(VME_DMAhandle_t *dma_poll_params)
/***********************************************/
{
  int loop, dma_ok = 0;
  
  kdebug(("vme_rcc_tsi(dma_is_done): called\n"))
  
  spin_lock_irqsave(&dmalock, dma_irq_flags); 

  for (loop = 0; loop < VME_DMATODOSIZE; loop++)
  {    
    kdebug(("vme_rcc_tsi(dma_is_done): paddr of loop(%d) is 0x%016lx. Looking for 0x%016lx\n", loop, dma_list_done[loop].paddr, dma_poll_params->paddr));
    if (dma_list_done[loop].paddr == dma_poll_params->paddr)  //Found it
    {
      kdebug(("vme_rcc_tsi(dma_is_done): bingo\n"));
      dma_poll_params->ctrl       = dma_list_done[loop].ctrl;
      dma_poll_params->counter    = dma_list_done[loop].counter;
      dma_poll_params->dclau      = dma_list_done[loop].dclau;
      dma_poll_params->dclal      = dma_list_done[loop].dclal;
      
      dma_list_done[loop].paddr   = 0;
      dma_list_done[loop].ctrl    = 0;
      dma_list_done[loop].counter = 0;
      dma_list_done[loop].in_use  = 0;
      dma_list_done[loop].pid     = 0;
      dma_list_done[loop].dclal   = 0;
      dma_list_done[loop].dclal   = 0;
      dmas_pending--;
      dma_ok = 1;
      break;
    }
  }

  spin_unlock_irqrestore(&dmalock, dma_irq_flags); 
   
  kdebug(("vme_rcc_tsi(dma_is_done)): done\n"))
  return(dma_ok);  
}


   





