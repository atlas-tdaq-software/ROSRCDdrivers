// $Id$
/************************************************************************/
/*									*/
/* File: vme_rcc_driver.c						*/
/*									*/
/* RCC VMEbus driver							*/
/*									*/
/* 25. Oct. 01  MAJO  created						*/
/* 01. Nov. 01  JOP  prepare for interrupts				*/
/* 26. Nov. 01  JOP  bus errors						*/
/* 15. Jan. 02  JOP  clean link & berr structures in release		*/
/* 26. Feb. 02  JOP  update to Linux 2.4				*/
/* 17. Apr. 02  JOP  RORA interrupts					*/
/* 27. May. 02  JOP  modify API for IRs					*/
/*									*/
/************ C 2021 - The software with that certain something *********/


//MJ - FIXME - We have to decide what to do with the pid

// https://lwn.net/Articles/167897/  is out fo date. There is no "data" in the hrtimer structure. See: ./src/kernels/4.18.0-147.8.1.el8_1.x86_64/include/linux/hrtimer.h




//Problems to be solved:
// 1) In case of a time-out on DMA the POLL ioctl is not notified and takes wrong conclusions (semaphore gets lost)
// 2) In case of a BERR on DMA one has to wait until the time-out expires
//    --> Fix: Enable all DMA termination interrupts. That way a BERR will trigger an interrupt and unblock the transaction
// 3) There are two semaphores for DMA. This opens the door for a race condition





/************************************************************************/
/*NOTES:								*/
/*- This driver should work on kernels from 2.6.9 onwards		*/
/*- This driver requires a UniverseII. Universe I is not supported.	*/
/*- The proper way of accessing the Universe would be through 		*/
/*  writel/readl functions. For a first implementation I have chosen to */
/*  use pointer based I/O as this simplifies the re-use of old code	*/ 
/************************************************************************/

#include <linux/init.h>           //MJ: for 2.6, p30
#include <linux/module.h>
#include <linux/delay.h>
#include <linux/moduleparam.h>    //MJ: for 2.6, p30
#include <linux/kernel.h>
#include <linux/stat.h>           //MJ: for 2.6, e.g. for module_param
#include <linux/pci.h>
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/mm.h>
#include <linux/slab.h>
#include <linux/cdev.h>           //e.g. for cdev_alloc
#include <linux/interrupt.h>      //e.g. for request_irq
#include <linux/version.h>        
#include <linux/sched.h>          //MJ: for current->pid (first needed with SLC6)
#include <linux/spinlock.h>       //For the spin-lock 
#include <linux/wait.h>           //For the wait queue 
#include <linux/seq_file.h>       //CS9

#if LINUX_VERSION_CODE < KERNEL_VERSION(4,0,0)
  #include <asm/uaccess.h>
#else
  #include <linux/uaccess.h>        //For CC8
  #include <linux/hrtimer.h>        //For CC8  
  #include <linux/sched/signal.h>
#endif

#include <asm/io.h>
#include "vme_rcc/universe.h"
#include "vme_rcc/cct_berr.h"
#include "vme_rcc/vme_rcc_driver.h"
#include "ROSRCDdrivers/tdaq_drivers.h"

#ifndef PCI_VENDOR_ID_TUNDRA
  #define  PCI_VENDOR_ID_TUNDRA 0x10e3
#endif

#ifndef PCI_DEVICE_ID_TUNDRA_CA91C042
  #define  PCI_DEVICE_ID_TUNDRA_CA91C042 0x0000
#endif

//MJ hack hack hack
#define VP_FXX 15



/*********/
/*Globals*/
/*********/
static int berr_interlock = 0, vmetab_ok = 0, debug = 0, errorlog = 1;
static int sem_positive[2] = {0,0};
static char *proc_read_text;
static u_char vme_irq_line;
static u_int maxcard = 0;
static u_int crcsr_mapped = 0;
static VME_MasterMapInt_t mm_crcsr;
static u_int board_type;
static u_int irq_level_table[7];
static u_int irq_sysfail;
static u_int berr_int_count = 0;
static u_int txfifo_wait_1;		        // # waits on TXFIFO empty
static u_int txfifo_wait_2;
static all_mstslvmap_t stmap;
static master_map_t master_map_table;
static link_dsc_table_t link_table;		// link descriptors
static berr_proc_table_t berr_proc_table;	// processes with BERR handling
static sysfail_link_dsc_t sysfail_link;	        // processes with SYSFAIL handling
static berr_dsc_t berr_dsc;			// last BERR
#if defined (VMEDEBUG) || defined (INTDEBUG)
  static u_int *marker;
#endif
static InterruptCounters_t Interrupt_counters = {0,0,0,0,0,0,0, {0,0,0,0,0,0,0}, 0};
static dev_t major_minor;
static universe_regs_t *uni;

#if LINUX_VERSION_CODE < KERNEL_VERSION(4,0,0)
  static struct timer_list dma_timer;
#endif  

static struct cdev *vme_rcc_cdev;
static struct pci_dev *universedev;


static u_int dma_done_total = 0, dma_todo_total = 0, dma_todo_write, dma_todo_read, dma_todo_num, dmas_pending = 0, dma_is_free = 1;
static u_int dma_current_pciaddr, dma_current_pid;
static u_long dma_irq_flags;
ktime_t mjkt;

//Note: The DMA requests can arrive at any time and from any number of processes. The driver will queue the requests and start them in the order in which they were entered into the queue. 
//      Therefore I am using a FIFO based bechanism
static VME_DMAhandle_todo_t dma_list_todo[VME_DMATODOSIZE];

//Note: Information about completed DMAs will be entered into a random-access array. Using a FIFO is not possible because the user applications may not poll for the end of their DMAs in parallel
//      The absence of polling activity from one process should not compromize the polling of other processes. If an entry in the array is free or busy will be controlled bya flag in the array elements
static VME_DMAhandle_done_t dma_list_done[VME_DMATODOSIZE];
DECLARE_WAIT_QUEUE_HEAD(dma_wait);

static DEFINE_SPINLOCK(dmalock);
static DEFINE_SPINLOCK(slock);

#if LINUX_VERSION_CODE > KERNEL_VERSION(4,0,0)
static struct hires_timer_data
{
  struct hrtimer timer;
  unsigned int data;
} hires_tinfo;
#endif  


/************/
/*Prototypes*/
/************/
/******************************/
/*Standard function prototypes*/
/******************************/
static int vme_rcc_open(struct inode *ino, struct file *filep);
static int vme_rcc_release(struct inode *ino, struct file *filep);
static long vme_rcc_ioctl(struct file *file, u_int cmd, u_long arg);
static int vme_rcc_mmap(struct file *file, struct vm_area_struct *vma);
static void vme_rcc_vmaClose(struct vm_area_struct *vma);
static void vme_rcc_vmaOpen(struct vm_area_struct *vma);
static ssize_t vme_rcc_proc_write(struct file *file, const char *buffer, size_t count, loff_t *startOffset);
static int vme_rcc_Probe(struct pci_dev *dev, const struct pci_device_id *id);
static int vme_rcc_init(void);
static void vme_rcc_Remove(struct pci_dev *dev); 
static void vme_rcc_cleanup(void);
int vme_rcc_proc_open(struct inode *inode, struct file *file);
int vme_rcc_proc_show(struct seq_file *sfile, void *p);

/*****************************/
/*Special function prototypes*/
/*****************************/
static int berr_check(u_int *addr, u_int *multi, u_int *am);
static int cct_berrInt(void);
static void fill_mstmap(u_int d1, u_int d2, u_int d3, u_int d4, mstslvmap_t *mstslvmap);
static void fill_slvmap(u_int d1, u_int d2, u_int d3, u_int d4, mstslvmap_t *mstslvmap);
static void vme_dma_handler(void);

#if LINUX_VERSION_CODE < KERNEL_VERSION(4,0,0)
  static void vme_intTimeout(u_long arg);
  static void vme_intSysfailTimeout(u_long arg);
#else
  void vme_intSysfailTimeout(struct timer_list *t);
  void vme_intTimeout(struct timer_list *t);
#endif

static void vme_irq_handler(int level);
static void init_cct_berr(void);
static void mask_cct_berr(void);
static void send_berr_signals(void);
static void read_berr_capture(void);
static irqreturn_t universe_irq_handler (int irq, void *dev_id);
int dma_is_done(VME_DMAhandle_t *dma_poll_params);
void start_next_dma(int src);


/***************************************************************/
/* Use /sbin/modinfo <module name> to extract this information */
/***************************************************************/
MODULE_DESCRIPTION("VMEbus driver for the Tundra Universe on SBCs from CCT");
MODULE_AUTHOR("Markus Joos & Jorgen Petersen, CERN/PH");
MODULE_VERSION("3.0");
MODULE_LICENSE("GPL");
module_param (debug, int, S_IRUGO | S_IWUSR);  //MJ: for 2.6 p37
MODULE_PARM_DESC(debug, "1 = enable debugging   0 = disable debugging");
module_param (errorlog, int, S_IRUGO | S_IWUSR);  //MJ: for 2.6 p37
MODULE_PARM_DESC(errorlog, "1 = enable error logging   0 = disable error logging");


//MJ: Not actually required. Just for kdebug
struct vm_operations_struct vme_rcc_vm_ops =
{       
  .close = vme_rcc_vmaClose,      
  .open  = vme_rcc_vmaOpen,      //MJ: Note the comma at the end of the list!
};

static struct file_operations fops = 
{
  .owner          = THIS_MODULE,
  .unlocked_ioctl = vme_rcc_ioctl,
  .open           = vme_rcc_open,    
  .mmap           = vme_rcc_mmap,
  .release        = vme_rcc_release,
};



//CS9
//Inspiration taken from: https://stackoverflow.com/questions/64931555/how-to-fix-error-passing-argument-4-of-proc-create-from-incompatible-pointer
//===========================================================
#if LINUX_VERSION_CODE >= KERNEL_VERSION(5,6,0)
static const struct proc_ops vme_rcc_proc_file_ops = 
{
  .proc_open    = vme_rcc_proc_open,
  .proc_write   = vme_rcc_proc_write,
  .proc_read    = seq_read,    
  .proc_lseek   = seq_lseek,	
  .proc_release = single_release
};
#else
static struct file_operations vme_rcc_proc_file_ops = 
{
  .owner   = THIS_MODULE,
  .open    = vme_rcc_proc_open,
  .write   = vme_rcc_proc_write,
  .read    = seq_read,
  .llseek  = seq_lseek,
  .release = single_release
};
#endif
//==============================================================


// PCI driver structures. See p311
static struct pci_device_id vme_rcc_Ids[] = 
{
  {PCI_DEVICE(PCI_VENDOR_ID_TUNDRA, PCI_DEVICE_ID_TUNDRA_CA91C042)},      
  {0,},
};

//PCI operations
static struct pci_driver __refdata vme_rcc_PciDriver = 
{
  .name     = "vme_rcc",
  .id_table = vme_rcc_Ids,
  .probe    = vme_rcc_Probe,
  .remove   = vme_rcc_Remove,
};


/*****************************/
/* Standard driver functions */
/*****************************/


/***********************************************************/
int vme_rcc_proc_open(struct inode *inode, struct file *file) 
/***********************************************************/
{
  return single_open(file, vme_rcc_proc_show, NULL);
}


#if LINUX_VERSION_CODE > KERNEL_VERSION(4,0,0)
//Taken from https://books.google.fr/books?id=wTmbDwAAQBAJ&pg=PA155&lpg=PA155&dq=from_timer+timer_setup&source=bl&ots=mkRsGFK3yH&sig=ACfU3U2fJokR-AtMySorxHMA7bMP9gCOiw&hl=de&sa=X&ved=2ahUKEwjS7fbrvJXqAhWmC2MBHaAXCks4ChDoATAFegQIChAB#v=onepage&q=hires_timer_data&f=false
/**********************************************************/
static enum hrtimer_restart hrt_handler(struct hrtimer *ptr)
/**********************************************************/
{
  struct hires_timer_data *info;
 
  kdebug(("vme_rcc(hrt_handler): timeout\n"))
  info = container_of(ptr, struct hires_timer_data, timer);
  kdebug(("vme_rcc(hrt_handler): data = %d\n", info->data));       
  return(HRTIMER_NORESTART);
}
#endif

/***************************/
static int vme_rcc_init(void)
/***************************/
{
  int found, result, loop;
  u_int bidloop, i;
  u_char reg;
  static struct proc_dir_entry *vme_rcc_file;
  u_long bidaddr;
  char *id_ptr;

  kerror(("vme_rcc(vme_rcc_init): Error debug is active\n"))

  if(alloc_chrdev_region (&major_minor, 0, 1, "vme_rcc")) //MJ: for 2.6 p45
  {
    kdebug(("vme_rcc(vme_rcc_init): failed to obtain device numbers\n"));
    result = -EIO;
    goto fail1;
  }
  
  proc_read_text = (char *)kmalloc(MAX_PROC_TEXT_SIZE, GFP_KERNEL);
  if (proc_read_text == NULL)
  {
    kdebug(("vme_rcc(vme_rcc_init): error from kmalloc\n"));
    result = -ENOMEM;
    goto fail2;
  }

  vme_rcc_file = proc_create("vme_rcc", 0644, NULL, &vme_rcc_proc_file_ops);
  if (vme_rcc_file == NULL)
  {
    kdebug(("vme_rcc(vme_rcc_init): error from call to create_proc_entry\n"));
    result = -EFAULT;
    goto fail3;
  }

  kdebug(("vme_rcc(vme_rcc_init): registering PCIDriver\n"));
  result = pci_register_driver(&vme_rcc_PciDriver);  //MJ: See P312
  if (result < 0)
  {
    kerror(("vme_rcc(vme_rcc_init): ERROR! no UNIVERSE chip found!\n"));
    result = -EIO;
    goto fail4;
  }
  else 
  {
    kerror(("vme_rcc(vme_rcc_init): UNIVERSE chip found!\n"));
  }
    
  // Read the board identification
  // The old Universe SBCs (VP110, VP315 and VP417) us a ID mechanism that is different from that of the Exx and Bxx series
  // CCT sugested to first check if the board is a Exx or Bxx. If this ID fails it must be on old card
  board_type = VP_UNKNOWN;
  
  //Identify a Exx, Bxx or Fxx
  kdebug(("vme_rcc(vme_rcc_init): Exx/Bxx/Fxx identification!\n"));
  bidaddr = (u_long)ioremap(0xe0000, 0x20000);
  id_ptr = (u_char *)bidaddr;
  found = 0;
  for (bidloop = 0; (bidloop + 7) < 0x20000; bidloop++)
  {
    if ((id_ptr[0] == '$') && (id_ptr[1] == 'S') && (id_ptr[2] == 'H') && (id_ptr[3] == 'A') && (id_ptr[4] == 'A') && (id_ptr[5] == 'N') && (id_ptr[6] == 'T') && (id_ptr[7] == 'I'))
    {
      kdebug(("vme_rcc(vme_rcc_init): Board ID table found!\n"));
      kdebug(("vme_rcc(vme_rcc_init): Board Id Structure Version:%d\n", id_ptr[8]));   //Exx = 1,    Bxx/Fxx=2
      kdebug(("vme_rcc(vme_rcc_init): Board Type:%d\n", id_ptr[9]));                   //Exx = 2,    Bxx/Fxx=2
      kdebug(("vme_rcc(vme_rcc_init): Board Id:%d,%d\n", id_ptr[10], id_ptr[11]));     //Exx = 0x2B, Bxx=0x29, Fxx=0x2e
      found = 1;
      if (id_ptr[8] == 1 && id_ptr[9] == 2 && id_ptr[10] == 0x2b && id_ptr[11] == 0) 
        board_type = VP_EXX;   
      if (id_ptr[8] == 2 && id_ptr[9] == 2 && id_ptr[10] == 0x29 && id_ptr[11] == 0) 
        board_type = VP_BXX;   
      if (id_ptr[8] == 2 && id_ptr[9] == 2 && id_ptr[10] == 0x2e && id_ptr[11] == 0) 
      {
        board_type = VP_FXX;      
	kdebug(("vme_rcc(vme_rcc_init): F64 found\n"));     
      }
    }
    if (found)
      break;
    id_ptr++;
  }

  //Identify a VP110, VP315 or VP417
  if (board_type == VP_UNKNOWN)
  {
    kdebug(("vme_rcc(vme_rcc_init): VP110/315/417 identification!\n"));
    outb(BID1, CMOSA);
    reg = inb(CMOSD);
    kdebug(("vme_rcc(vme_rcc_init): BID1 = 0x%02x\n", reg))

    if (!(reg & 0x80))
    {
      kerror(("vme_rcc(vme_rcc_init): Unable to determine board type\n"))
      result = -VME_UNKNOWN_BOARD;
      goto fail5;
    }

    outb(BID2, CMOSA);
    reg = inb(CMOSD);
    kdebug(("vme_rcc(vme_rcc_init): BID2 = 0x%02x\n", reg))

    reg &= 0x1f;  // Mask board ID bits
	 if (reg == 2)  {board_type = VP_PSE; kdebug(("vme_rcc(vme_rcc_init): Board type = VP-PSE\n"));}
    else if (reg == 3)  {board_type = VP_PSE; kdebug(("vme_rcc(vme_rcc_init): Board type = VP-PSE\n"));}
    else if (reg == 4)  {board_type = VP_PSE; kdebug(("vme_rcc(vme_rcc_init): Board type = VP-PSE\n"));}
    else if (reg == 5)  {board_type = VP_PMC; kdebug(("vme_rcc(vme_rcc_init): Board type = VP-PMC\n"));}
    else if (reg == 6)  {board_type = VP_CP1; kdebug(("vme_rcc(vme_rcc_init): Board type = VP-CP1\n"));}
    else if (reg == 7)  {board_type = VP_100; kdebug(("vme_rcc(vme_rcc_init): Board type = VP-100\n"));}
    else if (reg == 8)  {board_type = VP_110; kdebug(("vme_rcc(vme_rcc_init): Board type = VP-110\n"));}
    else if (reg == 11) {board_type = VP_315; kdebug(("vme_rcc(vme_rcc_init): Board type = VP-315\n"));}
    else if (reg == 13) {board_type = VP_317; kdebug(("vme_rcc(vme_rcc_init): Board type = VP-317\n"));}
    else if (reg == 14) {board_type = VP_325; kdebug(("vme_rcc(vme_rcc_init): Board type = VP-325\n"));}
    else if (reg == 15) {board_type = VP_327; kdebug(("vme_rcc(vme_rcc_init): Board type = VP-327\n"));}
    else if (reg == 23) {board_type = VP_417; kdebug(("vme_rcc(vme_rcc_init): Board type = VP-417\n"));}
  }

  if (!board_type)
  {
    kerror(("vme_rcc(vme_rcc_init): Unable to determine board type(2). Board seems to be of type %d\n", reg))
    result = -VME_UNKNOWN_BOARD;
    goto fail5;
  } 
      
  if (board_type == VP_100 || board_type == VP_110 || board_type == VP_315 || board_type == VP_317 || board_type == VP_325 || board_type == VP_327 || board_type == VP_417)
    outb(0x1, BERR_VP100);                          // Enable VMEbus BERR capturing

  init_cct_berr();	                            // Enable CCT bus errors
  sema_init(&link_table.link_dsc_sem, 1);           // Initialize the Link Descriptor protection semaphore

#if LINUX_VERSION_CODE < KERNEL_VERSION(4,0,0)
  init_timer(&dma_timer);                           // Initialise the watchdog timer for DMA transactions
#endif  
  
  for (loop = 0; loop < VME_DMATODOSIZE; loop++)    // Initialize the list of DMA transactions
  {
    dma_list_todo[loop].paddr = 0;
    dma_list_done[loop].paddr = 0;
    dma_list_done[loop].in_use = 0;
  }  
  dma_todo_write = 0; 
  dma_todo_read  = 0;
  dma_todo_num   = 0;
  
  for (loop = 0; loop < VME_MAX_MASTERMAP; loop++)  //Initialize the list of active master mappings
  {
    master_map_table.map[loop].pid = 0;
    master_map_table.map[loop].vaddr = 0;
  }
  sema_init(&master_map_table.sem, 1);
  
  for (i = 0; i < VME_VCTSIZE; i++)                 // Reset IRQ link and BERR tables
  {
    link_table.link_dsc[i].pid = 0;
    link_table.link_dsc[i].vct = 0;
    link_table.link_dsc[i].lvl = 0;
    link_table.link_dsc[i].pending = 0;
    link_table.link_dsc[i].total_count = 0;
    link_table.link_dsc[i].group_ptr = 0;
    sema_init(&link_table.link_dsc[i].sem, 0);
    link_table.link_dsc[i].sig = 0;
  }

  for (i = 0; i < VME_MAX_BERR_PROCS; i++)
  {
    berr_proc_table.berr_link_dsc[i].pid = 0;
    berr_proc_table.berr_link_dsc[i].sig = 0;
    sema_init(&berr_proc_table.proc_sem, 1);        //MJ: Is there a need for initializing the semaphore in the loop?
  }

  berr_dsc.vmeadd = 0;
  berr_dsc.am = 0;
  berr_dsc.iack = 0;
  berr_dsc.lword = 0;
  berr_dsc.ds0 = 0;
  berr_dsc.ds1 = 0;
  berr_dsc.wr = 0;
  berr_dsc.multiple = 0;
  berr_dsc.flag = 0;

  sysfail_link.pid = 0;
  sysfail_link.sig = 0;
  sema_init(&sysfail_link.proc_sem, 1);

  vme_rcc_cdev = (struct cdev *)cdev_alloc();      //MJ: for 2.6 p55
  vme_rcc_cdev->ops = &fops;
  result = cdev_add(vme_rcc_cdev, major_minor, 1);  //MJ: for 2.6 p56
  if (result)
  {
    kdebug(("vme_rcc(vme_rcc_init): error from call to cdev_add.\n"));
    goto fail5;
  }
  
#if LINUX_VERSION_CODE > KERNEL_VERSION(4,0,0)
  //CC8 hrtimer
  mjkt = ktime_set(10, 0);    // 10s,  0ns  
  hrtimer_init(&hires_tinfo.timer, CLOCK_MONOTONIC, HRTIMER_MODE_REL | HRTIMER_MODE_SOFT);
  hires_tinfo.timer.function = hrt_handler;
  hires_tinfo.data           = 42;
  hrtimer_start(&hires_tinfo.timer, mjkt, HRTIMER_MODE_REL);
  kdebug(("vme_rcc(vme_rcc_init): HR_TIMER started\n"));
#endif
  
  kdebug(("vme_rcc(vme_rcc_init): driver loaded; major device number = %d\n", MAJOR(major_minor)));
  return(0);

  fail5:
    pci_unregister_driver(&vme_rcc_PciDriver);

  fail4:
    remove_proc_entry("vme_rcc", NULL);

  fail3:
    kfree(proc_read_text);

  fail2:
    unregister_chrdev_region(major_minor, 1); //MJ: for 2.6 p45

  fail1:
    return(result);
}


/********************************/
static void vme_rcc_cleanup(void)
/********************************/
{
  pci_unregister_driver(&vme_rcc_PciDriver);
  
#if LINUX_VERSION_CODE > KERNEL_VERSION(4,0,0)
  //CC8 hrtimer
  hrtimer_cancel(&hires_tinfo.timer);
  kdebug(("vme_rcc(vme_rcc_cleanup): HR_TIMER removed\n"))
#endif 
  
  cdev_del(vme_rcc_cdev);                     //MJ: for 2.6 p56
  remove_proc_entry("vme_rcc", NULL);
  kfree(proc_read_text);
  unregister_chrdev_region(major_minor, 1);  //MJ: for 2.6 p45
  kdebug(("vme_rcc(vme_rcc_cleanup): driver removed\n"))
}


module_init(vme_rcc_init);    //MJ: for 2.6 p16
module_exit(vme_rcc_cleanup); //MJ: for 2.6 p16


/************************************************************/
static int vme_rcc_open(struct inode *ino, struct file *filep)
/************************************************************/
{
  int vct, berr, loop;
  private_data_t *pdata;

  pdata = (private_data_t *)kmalloc(sizeof(private_data_t), GFP_KERNEL);
  if (pdata == NULL)
  {
    kerror(("vme_rcc(open): error from kmalloc\n"))
    return(-VME_KMALLOC);
  }

  kdebug(("vme_rcc(open): pointer to link flags in file descriptor = %p\n", &pdata->link_dsc_flag[0]))
  for (vct = 0; vct < VME_VCTSIZE; vct++)
    pdata->link_dsc_flag[vct] = 0;

  kdebug(("vme_rcc(open): pointer to berr flags in file descriptor = %p\n", &pdata->berr_dsc_flag[0]))
  for (berr = 0; berr < VME_MAX_BERR_PROCS; berr++)
    pdata->berr_dsc_flag[berr] = 0;

  pdata->sysfail_dsc_flag = 0;
  
  for (loop = 0; loop < VME_DMADONESIZE; loop++)
    pdata->dma[loop] = 0;
  
  for (loop = 0; loop < VME_MAX_MASTERMAP; loop++)
    pdata->mastermap[loop] = 0;
  
  filep->private_data = pdata;
  return(0);
}


/***************************************************************/
static int vme_rcc_release(struct inode *ino, struct file *filep)
/***************************************************************/
{
  int vct, no_freed, isave, free_flag, loop;
  u_int i;
  VME_IntHandle_t *group_ptr;
  VME_IntHandle_t **group_ptr_save;
  private_data_t *pdata;
  
  group_ptr_save = (VME_IntHandle_t **)kmalloc(sizeof(VME_IntHandle_t *) * VME_VCTSIZE, GFP_KERNEL);
  if (group_ptr_save == NULL)
  {
    kerror(("vme_rcc(vme_rcc_release): error from kmalloc\n"))
    return(-VME_KMALLOC);
  }
  
  pdata = (private_data_t *)filep->private_data;

  down(&link_table.link_dsc_sem);

  no_freed = 0;
  group_ptr_save[0] = (VME_IntHandle_t*)0xffffffff;

  kdebug(("vme_rcc(release): pid = %d\n", current->pid))

  for (vct = 0; vct < VME_VCTSIZE; vct++)
  {
    if (pdata->link_dsc_flag[vct] == 1)
    {
      if (link_table.link_dsc[vct].group_ptr)	// grouped vector
      {
        group_ptr = link_table.link_dsc[vct].group_ptr;
        kdebug(("vme_rcc(release): vector = %d, group_ptr = 0x%p\n", vct, group_ptr))
        free_flag = 1;
        for (isave = 0; isave <= no_freed; isave++)
        {
          if (group_ptr == group_ptr_save[isave])
          {
            free_flag = 0;	// already freed
            break;
          }
        }

        kdebug(("vme_rcc(release): free_flag = %d\n", free_flag))
        if (free_flag)
        {
          kdebug(("vme_rcc(release): group_ptr = 0x%p is now kfree'd\n", group_ptr))
          kfree(group_ptr);                            //MJ: where is the corresponding kmalloc? In VMELINK?
          group_ptr_save[no_freed] = group_ptr;        
          no_freed++;
          kdebug(("vme_rcc(release): no_freed = %d \n", no_freed))
        }
      }

      link_table.link_dsc[vct].pid = 0;
      link_table.link_dsc[vct].vct = 0;
      link_table.link_dsc[vct].lvl = 0;
      link_table.link_dsc[vct].pending = 0;
      link_table.link_dsc[vct].total_count = 0;
      link_table.link_dsc[vct].group_ptr = 0;
      sema_init(&link_table.link_dsc[vct].sem, 0);
      link_table.link_dsc[vct].sig = 0;

      pdata->link_dsc_flag[vct] = 0;
    }
  }
  up(&link_table.link_dsc_sem);
  kfree(group_ptr_save);

  // and now the BERR & SYSFAIL links
  down(&berr_proc_table.proc_sem);
  for (i = 0; i < VME_MAX_BERR_PROCS; i++)
  {
    if (pdata->berr_dsc_flag[i] == 1)
    {
      kdebug(("vme_rcc(release): clearing BERR entry %d \n", i))
      berr_proc_table.berr_link_dsc[i].pid = 0;
      berr_proc_table.berr_link_dsc[i].sig = 0;
      pdata->link_dsc_flag[i] = 0;                 //MJ: is this OK or a bug? Should it be "pdata->berr_dsc_flag[i] = 0;"??
    }
  }
  up(&berr_proc_table.proc_sem);
  
  down(&sysfail_link.proc_sem);
  if (pdata->sysfail_dsc_flag)
  {
    kdebug(("vme_rcc(release): clearing SYSFAIL entry that has been left open by process %d\n", sysfail_link.pid))
    sysfail_link.pid = 0;
    sysfail_link.sig = 0;
    pdata->sysfail_dsc_flag = 0;
  }
  up(&sysfail_link.proc_sem);  

  // Release orphaned master mappings
  down(&master_map_table.sem);
  for (loop = 0; loop < VME_MAX_MASTERMAP; loop++)
  {
    if (pdata->mastermap[loop] == 1)
    {
      kdebug(("vme_rcc(release): Unmapping orphaned virtual address 0x%016lx for process %d\n", master_map_table.map[loop].vaddr, master_map_table.map[loop].pid))
      iounmap((void *)master_map_table.map[loop].vaddr);
      master_map_table.map[loop].vaddr = 0;
      master_map_table.map[loop].pid = 0;
      pdata->mastermap[loop] = 0;
    }
    if (pdata->mastermap[loop] == 2)
    {
      kdebug(("vme_rcc(release): Not unmapping orphaned but protected virtual address 0x%016lx (used for CR/CSR access)\n", master_map_table.map[loop].vaddr))
    }
  }
  up(&master_map_table.sem);

  // and finally the DMA lists
  for (loop = 0; loop < VME_DMATODOSIZE; loop++)
  {
    if (dma_list_todo[loop].pid == current->pid)
    {
      kdebug(("vme_rcc(release)(%d): entry %d in dma_list_todo list released. paddr=0x%016lx\n", current->pid, loop, dma_list_todo[loop].paddr))
      dma_list_todo[loop].paddr = 0;
      dma_list_todo[loop].pid = 0;
    }
    if (dma_list_done[loop].pid == current->pid)
    {
      kdebug(("vme_rcc(release)(%d): entry %d in dma_list_done list released. paddr=0x%016lx, in_use=%d \n", current->pid, loop, dma_list_done[loop].paddr, dma_list_done[loop].in_use))
      dma_list_done[loop].paddr = 0;
      dma_list_done[loop].pid = 0;
      dma_list_done[loop].in_use = 0;

      dmas_pending--;
    }
  }
    

  kfree(pdata);
  kdebug(("vme_rcc(release): kfreed file private data @ %p\n", pdata))
  return(0);
}


/******************************************************************/
static long vme_rcc_ioctl(struct file *filep, u_int cmd, u_long arg)
/******************************************************************/
{
  private_data_t *pdata;
  u_int ret;
  u_long irq_flags;

  kdebug(("vme_rcc(ioctl): filep at %p\n", filep))
  kdebug(("vme_rcc(ioctl): cmd = %d\n", cmd))
  pdata = (private_data_t *)filep->private_data;

  switch (cmd)
  {    
    case VMEMASTERMAP:   
    {
      int ok2, ok = 0, loop, loop2;
      void *vaddr;

      VME_MasterMapInt_t *mm;
      
      mm = (VME_MasterMapInt_t *)kmalloc(sizeof(VME_MasterMapInt_t), GFP_KERNEL);
      if (mm == NULL)
      {
	kdebug(("vme_rcc(ioctl,VMEMASTERMAP): error from kmalloc\n"));
        return(-VME_KMALLOC);
      }
      
      if (copy_from_user(mm, (void *)arg, sizeof(VME_MasterMapInt_t)))
      {
	kerror(("vme_rcc(ioctl,VMEMASTERMAP): error from copy_from_user\n"))
	kfree(mm);
	return(-VME_EFAULT);
      }
      kdebug(("vme_rcc(ioctl,VMEMASTERMAP): mm.in.vmebus_address   = 0x%08x\n", mm->in.vmebus_address))
      kdebug(("vme_rcc(ioctl,VMEMASTERMAP): mm.in.window_size      = 0x%08x\n", mm->in.window_size))
      kdebug(("vme_rcc(ioctl,VMEMASTERMAP): mm.in.address_modifier = 0x%08x\n", mm->in.address_modifier))
      kdebug(("vme_rcc(ioctl,VMEMASTERMAP): mm.in.options          = 0x%08x\n", mm->in.options))

      for (loop = 0; loop < 8; loop++)
      {
	if (mm->in.vmebus_address >= stmap.master[loop].vbase && 
           (mm->in.vmebus_address + mm->in.window_size) <= stmap.master[loop].vtop && 
	   stmap.master[loop].enab == 1 && 
	   stmap.master[loop].am == mm->in.address_modifier &&
           stmap.master[loop].options == mm->in.options)
        {
          kdebug(("vme_rcc(ioctl,VMEMASTERMAP): Decoder %d matches parameters\n", loop))
          mm->pci_address = stmap.master[loop].pbase + (mm->in.vmebus_address-stmap.master[loop].vbase); 
          kdebug(("vme_rcc(ioctl,VMEMASTERMAP): PCI address = 0x%016llx\n", mm->pci_address))

          //find a free slot in the master map table
          down(&master_map_table.sem);
          ok2 = 0;
          for (loop2 = 0; loop2 < VME_MAX_MASTERMAP; loop2++)
          {
            if (master_map_table.map[loop2].pid == 0)
            {
              kdebug(("vme_rcc(ioctl,VMEMASTERMAP): Will use entry %d in master_map_table\n", loop2))
              ok2 = 1;
              break;
            }
          }
          if (!ok2)
          {
            kdebug(("vme_rcc(ioctl,VMEMASTERMAP): master_map_table is full\n"))
            up(&master_map_table.sem);
	    kfree(mm);
            return(-VME_NOSTATMAP2);
          }
	  vaddr = ioremap(mm->pci_address, mm->in.window_size);
          if (!vaddr)
          {
            kerror(("vme_rcc(ioctl,VMEMASTERMAP): error from ioremap\n"))
            up(&master_map_table.sem);
	    kfree(mm);
            return(-VME_EFAULT);
          }
          master_map_table.map[loop2].vaddr = (u_long) vaddr;
          master_map_table.map[loop2].pid = current->pid;
          pdata->mastermap[loop2] = 1;
          up(&master_map_table.sem);

          kdebug(("vme_rcc(ioctl,VMEMASTERMAP): kernel virtual address = %p   pid = %d\n", vaddr, current->pid))
		
          mm->kvirt_address = (u_long) vaddr;
	  ok = 1;
	  break;
        }
        else
        {
          kdebug(("vme_rcc(ioctl,VMEMASTERMAP): stmap.master[%d].vbase   = 0x%08x\n", loop, (u_int)stmap.master[loop].vbase))
          kdebug(("vme_rcc(ioctl,VMEMASTERMAP): stmap.master[%d].vtop    = 0x%08x\n", loop, (u_int)stmap.master[loop].vtop))
          kdebug(("vme_rcc(ioctl,VMEMASTERMAP): stmap.master[%d].enab    = %d\n", loop, stmap.master[loop].enab))
          kdebug(("vme_rcc(ioctl,VMEMASTERMAP): stmap.master[%d].am      = %d\n", loop, stmap.master[loop].am))
          kdebug(("vme_rcc(ioctl,VMEMASTERMAP): stmap.master[%d].options = 0x%08x\n", loop, stmap.master[loop].options))
        }
      }
      if (!ok)
      {
	kfree(mm);
	return(-VME_NOSTATMAP);
      }
      if (copy_to_user((void *)arg, mm, sizeof(VME_MasterMapInt_t)))
      {
	kerror(("vme_rcc(ioctl,VMEMASTERMAP): error from copy_to_user\n"))
	kfree(mm);
	return(-VME_EFAULT);
      }
      
      kfree(mm);
      break;
    }
    
    case VMEMASTERMAPCRCSR:   //Get a permanent mapping for generic CR/CSR access
    {
      int ok2, ok = 0, loop, loop2;
      u_long vaddr;
      
      if (!crcsr_mapped)
      {
	mm_crcsr.in.vmebus_address = 0x0;
	mm_crcsr.in.window_size = 0x1000000;
	mm_crcsr.in.address_modifier = 5;
	mm_crcsr.in.options = 0;
	mm_crcsr.used = 2;
        kdebug(("vme_rcc(ioctl,VMEMASTERMAPCRCSR): used set to 2\n"))

	for (loop = 0; loop < 8; loop++)
	{
	  if (mm_crcsr.in.vmebus_address >= stmap.master[loop].vbase && 
             (mm_crcsr.in.vmebus_address + mm_crcsr.in.window_size) <= stmap.master[loop].vtop && 
	     stmap.master[loop].enab == 1 && 
	     stmap.master[loop].am == mm_crcsr.in.address_modifier &&
             stmap.master[loop].options == mm_crcsr.in.options)
          {
            kdebug(("vme_rcc(ioctl,VMEMASTERMAPCRCSR): Decoder %d matches parameters\n", loop))
            mm_crcsr.pci_address = stmap.master[loop].pbase + (mm_crcsr.in.vmebus_address - stmap.master[loop].vbase); 
            kdebug(("vme_rcc(ioctl,VMEMASTERMAPCRCSR): PCI address = 0x%016llx\n", mm_crcsr.pci_address))

            //find a free slot in the master map table
            down(&master_map_table.sem);
            ok2 = 0;
            for (loop2 = 0; loop2 < VME_MAX_MASTERMAP; loop2++)
            {
              if (master_map_table.map[loop2].pid == 0)
              {
        	kdebug(("vme_rcc(ioctl,VMEMASTERMAPCRCSR): Will use entry %d in master_map_table\n", loop2))
        	ok2 = 1;
        	break;
              }
            }
            if (!ok2)
            {
              kdebug(("vme_rcc(ioctl,VMEMASTERMAPCRCSR): master_map_table is full\n"))
              up(&master_map_table.sem);
              return(-VME_NOSTATMAP2);
            }

	    vaddr = (u_long)ioremap(mm_crcsr.pci_address, mm_crcsr.in.window_size);
            if (!vaddr)
            {
              kerror(("vme_rcc(ioctl,VMEMASTERMAPCRCSR): error from ioremap\n"))
              up(&master_map_table.sem);
              return(-VME_EFAULT);
            }
            master_map_table.map[loop2].vaddr = vaddr;
            master_map_table.map[loop2].pid = current->pid;
            pdata->mastermap[loop2] = 2;
            up(&master_map_table.sem);

            kdebug(("vme_rcc(ioctl,VMEMASTERMAPCRCSR): kernel virtual address = 0x%016lx   pid = %d\n", vaddr, current->pid))
            mm_crcsr.kvirt_address = vaddr;
	    ok = 1;
	    break;
          }
          else
          {
            kdebug(("vme_rcc(ioctl,VMEMASTERMAPCRCSR): stmap.master[%d].vbase   = 0x%08x\n", loop, (u_int)stmap.master[loop].vbase))
            kdebug(("vme_rcc(ioctl,VMEMASTERMAPCRCSR): stmap.master[%d].vtop    = 0x%08x\n", loop, (u_int)stmap.master[loop].vtop))
            kdebug(("vme_rcc(ioctl,VMEMASTERMAPCRCSR): stmap.master[%d].enab    = %d\n", loop, stmap.master[loop].enab))
            kdebug(("vme_rcc(ioctl,VMEMASTERMAPCRCSR): stmap.master[%d].am      = %d\n", loop, stmap.master[loop].am))
            kdebug(("vme_rcc(ioctl,VMEMASTERMAPCRCSR): stmap.master[%d].options = 0x%08x\n", loop, stmap.master[loop].options))
          }
	}
	if (!ok)
	  return(-VME_NOSTATMAP);
	  
	crcsr_mapped = 1;
      }          
      
      kdebug(("vme_rcc(ioctl,VMEMASTERMAPCRCSR): PCI address            = 0x%016llx\n", mm_crcsr.pci_address))
      kdebug(("vme_rcc(ioctl,VMEMASTERMAPCRCSR): kernel virtual address = 0x%016lx\n", mm_crcsr.kvirt_address))
      	
      if (copy_to_user((void *)arg, &mm_crcsr, sizeof(VME_MasterMapInt_t)))
      {
	kerror(("vme_rcc(ioctl,VMEMASTERMAPCRCSR): error from copy_to_user\n"))
	return(-VME_EFAULT);
      }
      break;
    }
    
    case VMEMASTERUNMAP:
    {    
      u_int ok, loop;
      void *mm;
      if (copy_from_user(&mm, (void *)arg, sizeof(void *)))
      {
	kerror(("vme_rcc(ioctl,VMEMASTERUNMAP): error from copy_from_user\n"))
	return(-VME_EFAULT);
      }

      down(&master_map_table.sem);
      ok = 0;
      for (loop = 0; loop < VME_MAX_MASTERMAP; loop++)
      {
	kdebug(("vme_rcc(ioctl,VMEMASTERUNMAP): master_map_table.map[%d].vaddr = 0x%016lx  <-> mm = %p\n", loop, master_map_table.map[loop].vaddr, mm));
	kdebug(("vme_rcc(ioctl,VMEMASTERUNMAP): master_map_table.map[%d].pid = %d <-> current->pid = %d\n", loop, master_map_table.map[loop].pid, current->pid));
      
        if (master_map_table.map[loop].vaddr == (u_long) mm) 
        {
          kdebug(("vme_rcc(ioctl,VMEMASTERUNMAP): Entry %d in master_map_table matches\n", loop))
          ok = 1;
          kdebug(("vme_rcc(ioctl,VMEMASTERUNMAP): unmapping address %p\n", mm))
	  iounmap(mm);

          master_map_table.map[loop].vaddr = 0;
          master_map_table.map[loop].pid = 0;
          pdata->mastermap[loop] = 0;
          break;
        }
      }
      up(&master_map_table.sem);
  
      if (!ok)
      {
        kdebug(("vme_rcc(ioctl,VMEMASTERUNMAP): No entry in master_map_table matched for address %p\n", mm))
        return(-VME_IOUNMAP);
      }
      break;
    }

    case VMEMASTERMAPDUMP:
    {
      char *buf;
      int len, loop;
      
      buf = (char *)kmalloc(TEXT_SIZE1, GFP_KERNEL);
      if (buf == NULL)
      {
        kerror(("vme_rcc(ioctl,VMEMASTERMAPDUMP): error from kmalloc\n"))
        return(-VME_KMALLOC);
      }

      len = 0;
      len += sprintf(buf + len, "Master mapping:\n");
      len += sprintf(buf + len, "LSI  VME address range  PCI address range   EN   WP   AM\n");
      for (loop = 0; loop < 8; loop++)
	len += sprintf(buf + len, "%d    %08x-%08x  %08x-%08x    %d    %d    %d\n"
        	      ,loop
	 	      ,(u_int)stmap.master[loop].vbase
		      ,(u_int)stmap.master[loop].vtop
		      ,(u_int)stmap.master[loop].pbase
		      ,(u_int)stmap.master[loop].ptop
		      ,stmap.master[loop].enab
		      ,stmap.master[loop].wp
		      ,stmap.master[loop].am);
      if (copy_to_user((void *)arg, buf, TEXT_SIZE1 * sizeof(char)))
      {
	kerror(("vme_rcc(ioctl,VMEMASTERMAPDUMP): error from copy_to_user\n"))
	return(-VME_EFAULT);
      }
            
      kfree(buf);
      break;
    }
     
    case VMESYSFAILLINK:
    {
      down(&sysfail_link.proc_sem);      //MJ: Rubini recommands down_interruptible()

      if (sysfail_link.pid)              // Already linked by any other process?
      {
        up(&sysfail_link.proc_sem);
        return(-VME_SYSFAILTBLFULL);
      }

      sysfail_link.pid = current->pid;
      pdata->sysfail_dsc_flag = 1;
      sema_init(&sysfail_link.sem, 0);   // initialise semaphore count
      up(&sysfail_link.proc_sem);
      break;
    }

    case VMESYSFAILUNLINK:
    {
      down(&sysfail_link.proc_sem);

      if (pdata->sysfail_dsc_flag == 0)  //Check if the current process (group) has linked it
      {
        up(&sysfail_link.proc_sem);
        return(-VME_SYSFAILTBLNOTLINKED);
      }

      sysfail_link.pid = 0;
      sysfail_link.sig = 0;
      pdata->sysfail_dsc_flag = 0;
      up(&sysfail_link.proc_sem);
      break;
    }

    case VMESYSFAILREGISTERSIGNAL:
    {
      VME_RegSig_t *VME_SysfailRegSig;
      
      VME_SysfailRegSig = (VME_RegSig_t *)kmalloc(sizeof(VME_RegSig_t), GFP_KERNEL);
      if (VME_SysfailRegSig == NULL)
      {
	kdebug(("vme_rcc(ioctl,VMESYSFAILREGISTERSIGNAL): error from kmalloc\n"));
        return(-VME_KMALLOC);
      }

      if (copy_from_user(VME_SysfailRegSig, (void *)arg, sizeof(VME_RegSig_t)))
      {
        kerror(("vme_rcc(ioctl,VMESYSFAILREGISTERSIGNAL): error from copy_from_user\n"))
	kfree(VME_SysfailRegSig);
        return(-VME_EFAULT);
      }
      kdebug(("vme_rcc(ioctl,VMESYSFAILREGISTERSIGNAL): signal # = %d\n", VME_SysfailRegSig->signum))
      down(&sysfail_link.proc_sem);

      if (sysfail_link.pid != current->pid)
      {
        up(&sysfail_link.proc_sem);
	kfree(VME_SysfailRegSig);
        return(-VME_SYSFAILNOTLINKED);
      }
      if (VME_SysfailRegSig->signum)        // register
        sysfail_link.sig = VME_SysfailRegSig->signum;
      else                                  // unregister
        sysfail_link.sig = 0;
      kfree(VME_SysfailRegSig);
      up(&sysfail_link.proc_sem);
      break;
    }
    
    case VMESYSFAILWAIT:   
    {
      struct timer_list int_timer;
      int use_timer = 0, result = 0;
      VME_WaitInt_t *VME_WaitInt;
      
      VME_WaitInt = (VME_WaitInt_t *)kmalloc(sizeof(VME_WaitInt_t), GFP_KERNEL);
      if (VME_WaitInt == NULL)
      {
	kdebug(("vme_rcc(ioctl,VMESYSFAILWAIT): error from kmalloc\n"));
        return(-VME_KMALLOC);
      }

      if (copy_from_user(VME_WaitInt, (void *)arg, sizeof(VME_WaitInt_t)))
      {
        kerror(("vme_rcc(ioctl,VMESYSFAILWAIT): error from copy_from_user\n"))
 	kfree(VME_WaitInt);
	return(-VME_EFAULT);
      }
      kdebug(("vme_rcc(ioctl,VMESYSFAILWAIT): timeout = %d\n", VME_WaitInt->timeout))

      if (VME_WaitInt->timeout > 0) 
      {
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,0,0) 
        init_timer(&int_timer);
        int_timer.function = vme_intSysfailTimeout;
        int_timer.data = (u_long)VME_WaitInt; 
        int_timer.expires = jiffies + VME_WaitInt->timeout * HZ / 1000;  //convert from ms to kernel-jiffis (pretty pointless for the current 1 ms jiffies)
        add_timer(&int_timer);
        use_timer = 1;
#else
        timer_setup(&int_timer, vme_intSysfailTimeout, 0);
	mod_timer(&int_timer, jiffies + VME_WaitInt->timeout * HZ / 1000); //convert from ms to kernel-jiffis (pretty pointless for the current 1 ms jiffies)
        //MJ: it is not yet clear how the VME_WaitInt structure can be communicated to vme_intSysfailTimeout
#endif
      }

      if (VME_WaitInt->timeout) 		
      {
        kdebug(("vme_rcc(ioctl,VMESYSFAILWAIT): Before Down, semaphore = %d\n", sysfail_link.sem.count))

        ret = down_interruptible(&sysfail_link.sem);	// wait ..
        if (signal_pending(current))
        {					// interrupted system call
          kdebug(("vme_rcc(ioctl,VMESYSFAILWAIT): Interrupted by signal\n"))
          result = -VME_INTBYSIGNAL;		// or ERESTARTSYS ?
        }
        else if ((use_timer == 1) && (VME_WaitInt->timeout == 0))
          result = -VME_TIMEOUT;
      }
      
      if (use_timer == 1)
        del_timer(&int_timer);	// OK also if timer ran out

      if (copy_to_user((void *)arg, VME_WaitInt, sizeof(VME_WaitInt_t)))  //MJ: What are we copying back to the user?
      {
	kerror(("vme_rcc(ioctl,VMESYSFAILWAIT): error from copy_to_user\n"))
 	kfree(VME_WaitInt);
        return(-VME_EFAULT);
      }

      kfree(VME_WaitInt);
      return (result);
      break;	// dummy ..
    }

    // SYSFAIL is latched in lint_stat (I believe). Therefore, even if SYSFAIL is removed from VMEbus
    // the interrupt status bit has to be cleared in LINT_STAT
    // this will only work if SYSFAIL is not on the bus
    case VMESYSFAILREENABLE:
    {
      u_int lint_stat, lint_en;

      lint_stat = readl((char*)uni + LINT_STAT);	// read the interrupt bits
      lint_stat |= 0x4000;
      kdebug(("vme_rcc(ioctl,VMESYSFAILREENABLE): lint_stat = %x\n", lint_stat))
      writel(lint_stat, (char*)uni + LINT_STAT);	// only success if SYSFAIL is not asserted on VMEbus
      lint_en = readl((char*)uni + LINT_EN);	        // read enabled irq bits
      kdebug(("vme_rcc(ioctl,VMESYSFAILREENABLE): lint_en = 0x%x\n", lint_en))
      lint_en |= 0x4000;				
      kdebug(("vme_rcc(ioctl,VMESYSFAILREENABLE): lint_en after masking = 0x%x\n", lint_en))
      writel(lint_en, (char*)uni + LINT_EN);	        // reenable SYSFAIL
      break;
    }

    // reset the SYSFAIL interrupt bit. If it comes back on, the SYSFAIL is on VMEbus
    case VMESYSFAILPOLL:
    {
      int flag;
      u_int lint_stat;

      lint_stat = readl((char*)uni + LINT_STAT);	// read the interrupt bits
      lint_stat |= 0x4000;
      kdebug(("vme_rcc(ioctl,VMESYSFAILPOLL): lint_stat = %x\n", lint_stat))
      writel(lint_stat, (char*)uni + LINT_STAT);	// only success if SYSFAIL is not asserted on VMEbus
      lint_stat = readl((char*)uni + LINT_STAT);	// read the interrupt bits
      kdebug(("vme_rcc(ioctl,VMESYSFAILPOLL): lint_stat = %x\n", lint_stat))
      if (lint_stat & 0x4000)	// the SYSFAIL bit
        flag = 1;
      else
        flag = 0;

      if (copy_to_user((void *)arg, &flag, sizeof(int)))
      {
	kerror(("vme_rcc(ioctl,VMESYSFAILPOLL): error from copy_to_user\n"))
        return(-VME_EFAULT);
      }
      break;
    }
    
    case VMESYSFAILSET:
    {   
      u_int vcsr_set;

      vcsr_set = readl((char*)uni + VCSR_SET);
      vcsr_set |= 0x40000000;				// bit SYSFAIL
      kdebug(("vme_rcc(ioctl,VMESYSFAILSET): vcsr_set = %x\n", vcsr_set))
      writel(vcsr_set, (char*)uni + VCSR_SET);
      break;
    }
  
    case VMESYSFAILRESET:
    {   
      u_int vcsr_clr;

      vcsr_clr = readl((char*)uni + VCSR_CLR);
      vcsr_clr |= 0x40000000;				// bit SYSFAIL
      kdebug(("vme_rcc(ioctl,VMESYSFAILRESET): vcsr_clr = %x\n", vcsr_clr))
      writel(vcsr_clr, (char*)uni + VCSR_CLR);
      break;
    }

    case VMEBERRREGISTERSIGNAL:   
    {
      u_int i;
      int index;
      VME_RegSig_t *VME_BerrRegSig;

      VME_BerrRegSig = (VME_RegSig_t *)kmalloc(sizeof(VME_RegSig_t), GFP_KERNEL);
      if (VME_BerrRegSig == NULL)
      {
	kdebug(("vme_rcc(ioctl,VMEBERRREGISTERSIGNAL): error from kmalloc\n"));
        return(-VME_KMALLOC);
      }

      if (copy_from_user(VME_BerrRegSig, (void *)arg, sizeof(VME_RegSig_t)))
      {
	kerror(("vme_rcc(ioctl,VMEBERRREGISTERSIGNAL): error from copy_from_user\n"))
	kfree(VME_BerrRegSig);
	return(-VME_EFAULT);
      }

      kdebug(("vme_rcc(ioctl,VMEBERRREGISTERSIGNAL): signal # = %d\n", VME_BerrRegSig->signum))
      down(&berr_proc_table.proc_sem);

      if (VME_BerrRegSig->signum)
      {
        // find a free slot
        index = -1;
        for (i = 0; i < VME_MAX_BERR_PROCS; i++)
        {
          if (berr_proc_table.berr_link_dsc[i].pid == 0)
          {
            index = i;
            break;
          }
        }
        if (index == -1)
        {
          up(&berr_proc_table.proc_sem);
	  kfree(VME_BerrRegSig);
          return(-VME_BERRTBLFULL);
        }

        berr_proc_table.berr_link_dsc[index].pid = current->pid;
        berr_proc_table.berr_link_dsc[index].sig = VME_BerrRegSig->signum;
        berr_dsc.flag = 0; 	                // enable BERR latching via signal
				                // race: could occur while a(nother) bus error is latched and not yet serviced ..
        pdata->berr_dsc_flag[index] = 1;	// remember which file berr belongs to
      }
      else		                        // unregister
      {
        index = -1;
        for (i = 0; i < VME_MAX_BERR_PROCS; i++)
        {
          if (pdata->berr_dsc_flag[i] == 1)  //MJ: Are we sure there is only one "1" in the berr_dsc_flag array?
          {
            index = i;
            break;
          }
        }
        if (index == -1)
        {
          up(&berr_proc_table.proc_sem);
	  kfree(VME_BerrRegSig);
          return(-VME_BERRNOTFOUND);
        }

        berr_proc_table.berr_link_dsc[index].pid = 0;
        berr_proc_table.berr_link_dsc[index].sig = 0;
        pdata->berr_dsc_flag[index] = 0;
      }

      up(&berr_proc_table.proc_sem);
      kfree(VME_BerrRegSig);
      break;
    }

    case VMEBERRINFO:
    {
      u_long flags;

      if (berr_dsc.flag == 0)		// slight risk of race with ISR ..
        return(-VME_NOBUSERROR);

      local_irq_save(flags);            //MJ: for 2.6 see p274 & p275

      pdata->VME_BerrInfo.vmebus_address = berr_dsc.vmeadd;
      pdata->VME_BerrInfo.address_modifier = berr_dsc.am;
      pdata->VME_BerrInfo.multiple = berr_dsc.multiple;
      pdata->VME_BerrInfo.lword = berr_dsc.lword;
      pdata->VME_BerrInfo.iack = berr_dsc.iack;
      pdata->VME_BerrInfo.ds0 = berr_dsc.ds0;
      pdata->VME_BerrInfo.ds1 = berr_dsc.ds1;
      pdata->VME_BerrInfo.wr = berr_dsc.wr;

      berr_dsc.vmeadd = 0;
      berr_dsc.am = 0;
      berr_dsc.multiple = 0;
      berr_dsc.iack = 0;
      berr_dsc.flag = 0;
      berr_dsc.lword = 0;
      berr_dsc.ds0 = 0;
      berr_dsc.ds1 = 0;
      berr_dsc.wr = 0;

      local_irq_restore(flags);

      if (copy_to_user((void *)arg, &pdata->VME_BerrInfo, sizeof(VME_BusErrorInfo_t)))
      {
	kerror(("vme_rcc(ioctl,VMEBERRINFO): error from copy_to_user\n"))
	return(-VME_EFAULT);
      }
      break;
    }
		
    case VMESLAVEMAP:   
    {
      int loop, ok = 0;
      VME_SlaveMapInt_t *sm;

      sm = (VME_SlaveMapInt_t *)kmalloc(sizeof(VME_SlaveMapInt_t), GFP_KERNEL);
      if (sm == NULL)
      {
	kdebug(("vme_rcc(ioctl,VMESLAVEMAP): error from kmalloc\n"));
        return(-VME_KMALLOC);
      }

      if (copy_from_user(sm, (void *)arg, sizeof(VME_SlaveMapInt_t)))
      {
	kerror(("vme_rcc(ioctl,VMESLAVEMAP): error from copy_from_user\n"))
	kfree(sm);
	return(-VME_EFAULT);
      }
      kdebug(("vme_rcc(ioctl,VMESLAVEMAP): mm.in.system_iobus_address = 0x%016llx\n", sm->in.system_iobus_address))
      kdebug(("vme_rcc(ioctl,VMESLAVEMAP): mm.in.window_size          = 0x%08x\n", sm->in.window_size))
      kdebug(("vme_rcc(ioctl,VMESLAVEMAP): mm.in.address_width        = 0x%08x\n", sm->in.address_width))
      kdebug(("vme_rcc(ioctl,VMESLAVEMAP): mm.in.options              = 0x%08x\n", sm->in.options))

      for(loop = 0; loop < 8; loop++)
      {
	kdebug(("vme_rcc(ioctl,VMESLAVEMAP): stmap.slave[%d].pbase = 0x%08x\n", loop, (u_int)stmap.slave[loop].pbase))
	kdebug(("vme_rcc(ioctl,VMESLAVEMAP): stmap.slave[%d].ptop  = 0x%08x\n", loop, (u_int)stmap.slave[loop].ptop))
	kdebug(("vme_rcc(ioctl,VMESLAVEMAP): stmap.slave[%d].enab  = 0x%08x\n", loop, stmap.slave[loop].enab))
	kdebug(("vme_rcc(ioctl,VMESLAVEMAP): stmap.slave[%d].space = 0x%08x\n", loop, stmap.slave[loop].space))
	if (sm->in.system_iobus_address >= stmap.slave[loop].pbase && 
           (sm->in.system_iobus_address + sm->in.window_size) <= stmap.slave[loop].ptop && 
	   stmap.slave[loop].enab == 1 && 
	   stmap.slave[loop].am == sm->in.address_width)
        {
	  kdebug(("vme_rcc(ioctl,VMESLAVEMAP): Decoder %d matches parameters\n", loop))
	  sm->vme_address = stmap.slave[loop].vbase + (sm->in.system_iobus_address - stmap.slave[loop].pbase);
          kdebug(("vme_rcc(ioctl,VMESLAVEMAP): VME address = 0x%08x\n", sm->vme_address))
	  ok = 1;
	  break;
        }
      }
      if (!ok)
      {
	kerror(("vme_rcc(ioctl,VMESLAVEMAP): No matching mapping found\n"))
	kfree(sm);
	return(-VME_NOSTATMAP);   
      }
      if (copy_to_user((void *)arg, sm, sizeof(VME_SlaveMapInt_t)))
      {
	kerror(("vme_rcc(ioctl,VMESLAVEMAP): error from copy_to_user\n"))
	kfree(sm);
	return(-VME_EFAULT);
      }
      kfree(sm);
      break;
    } 

    case VMESLAVEMAPDUMP: 
    {  
      char *buf;
      int len, loop;

      buf = (char *)kmalloc(TEXT_SIZE1, GFP_KERNEL);
      if (buf == NULL)
      {
        kerror(("vme_rcc(ioctl,VMESLAVEMAPDUMP): error from kmalloc\n"))
        return(-VME_KMALLOC);
      }
      len = 0;
      len += sprintf(buf + len, "Slave mapping:\n");
      len += sprintf(buf + len, "VSI  VME address range  PCI address range   EN   WP   RP   AM PCI Space\n");
      for (loop = 0 ;loop < 8; loop++)
	len += sprintf(buf + len, "%d    %08x-%08x  %08x-%08x    %d    %d    %d    %d         %d\n"
          	      ,loop
		      ,(u_int)stmap.slave[loop].vbase
	  	      ,(u_int)stmap.slave[loop].vtop
		      ,(u_int)stmap.slave[loop].pbase
		      ,(u_int)stmap.slave[loop].ptop
		      ,stmap.slave[loop].enab
		      ,stmap.slave[loop].wp
		      ,stmap.slave[loop].rp
		      ,stmap.slave[loop].am
		      ,stmap.slave[loop].space);
      if (copy_to_user((void *)arg, buf, TEXT_SIZE1*sizeof(char)))
      {
	kerror(("vme_rcc(ioctl,VMESLAVEMAPDUMP): error from copy_to_user\n"))
	return(-VME_EFAULT);
      }
      kfree(buf);
      break;
    }

    case VMESCSAFE:
    {    
      u_int addr, multi, am, *lptr, ldata;
      u_short *sptr, sdata;
      u_char *bptr, bdata;
      int ret;
      VME_SingleCycle_t *sc;

      sc = (VME_SingleCycle_t *)kmalloc(sizeof(VME_SingleCycle_t), GFP_KERNEL);
      if (sc == NULL)
      {
	kerror(("vme_rcc(ioctl,VMESCSAFE): error from kmalloc\n"));
        return(-VME_KMALLOC);
      }

      if (copy_from_user(sc, (void *)arg, sizeof(VME_SingleCycle_t)))
      {
	kerror(("vme_rcc(ioctl,VMESCSAFE): error from copy_from_user\n"))
	kfree(sc);
	return(-VME_EFAULT);
      }
      
      //  Provoke a kernel panic at 0x1472
      //  *(int *)0 = 0;
            
      kdebug(("vme_rcc(ioctl,VMESCSAFE): sc.kvirt_address = 0x%016lx\n", sc->kvirt_address))
      kdebug(("vme_rcc(ioctl,VMESCSAFE): sc.offset        = 0x%08x\n", sc->offset))
      kdebug(("vme_rcc(ioctl,VMESCSAFE): sc.nbytes        = %d\n", sc->nbytes))
      kdebug(("vme_rcc(ioctl,VMESCSAFE): sc.rw            = %d\n", sc->rw))
      kdebug(("vme_rcc(ioctl,VMESCSAFE): pid              = %d\n", current->pid))

      //Wait until the Universe has emptied the write posting buffer
      while (!(uni->misc_stat & 0x40000))
      {
        txfifo_wait_1++;
        kdebug(("vme_rcc(ioctl,VMESCSAFE): TXFIFO still not empty\n"))
      }

      spin_lock_irqsave(&slock, irq_flags); 
        
      //Disable the CCT BERR logic for the duration of the safe single cycle
      mask_cct_berr();
        
      if (sc->rw == 1)             //read
      {
	if (sc->nbytes == 4)       //D32
	{
	  lptr = (u_int *)(sc->kvirt_address + sc->offset);
	  ldata = *lptr;
	  sc->data = (u_int)ldata;
          kdebug(("vme_rcc(ioctl,VMESCSAFE): D32 read. Data = 0x%08x = 0x%08x\n", ldata, sc->data))
	}
	else if (sc->nbytes == 2)  //D16
        {
	  sptr = (u_short *)(sc->kvirt_address + sc->offset);
	  sdata = *sptr;
	  sc->data = (u_int)sdata;
          kdebug(("vme_rcc(ioctl,VMESCSAFE): D16 read. Data = 0x%08x\n", sdata))
	}
	else                      //D8
	{
	  bptr = (u_char *)(sc->kvirt_address + sc->offset);
	  bdata = *bptr;
	  sc->data = (u_int)bdata;
          kdebug(("vme_rcc(ioctl,VMESCSAFE): D8 read. Data = 0x%08x\n", bdata))
	}
      }
      
      //  Provoke a kernel panic at 0x158c
      //  *(int *)0 = 0;

      else                         //write
      {
	if (sc->nbytes == 4)       //D32
	{
	  lptr = (u_int *)(sc->kvirt_address + sc->offset);
	  ldata = sc->data;
	  *lptr = ldata;
	}
	else if (sc->nbytes == 2)  //D16
	{
	  sptr = (u_short *)(sc->kvirt_address + sc->offset);
	  sdata = (u_short)sc->data;
	  *sptr = sdata;
	}
	else                       //D8
	{
	  bptr = (u_char *)(sc->kvirt_address + sc->offset);
	  bdata = (u_char)sc->data;
	  *bptr = bdata;
	}
        
        //Wait again until the Universe has emptied the write posting buffer
        while (!(uni->misc_stat & 0x40000))
        {
          txfifo_wait_2++;
          kdebug(("vme_rcc(ioctl,VMESCSAFE): TXFIFO still not empty (2)\n"))
        }
      }

      //MJ: Do we have to wait a bit for bus errors to arrive?
      //MJ: What to do with addr, multi and am in case of BERR?
      ret = berr_check(&addr, &multi, &am);
      if (ret)
      {
	kerror(("vme_rcc(ioctl,VMESCSAFE): Bus error received. addr=0x%08x  multi=%d  am=0x%02x\n", addr, multi, am))
	init_cct_berr();
        spin_unlock_irqrestore(&slock, irq_flags); 
	kfree(sc);
	return(-VME_BUSERROR);
      }
      
      //Reenable the CCT BERR logic and clear the BERR flag in the CCT logic
      init_cct_berr();
      spin_unlock_irqrestore(&slock, irq_flags); 

      if (copy_to_user((void *)arg, sc, sizeof(VME_SingleCycle_t)))
      {
	kerror(("vme_rcc(ioctl,VMESCSAFE): error from copy_to_user\n"))
	kfree(sc);
	return(-VME_EFAULT);
      }
      kfree(sc);
      break;
    } 
    
    case VMEDMASTART:
    {
      VME_DMAstart_t *dma_params;
      u_int windex;
      
      dma_params = (VME_DMAstart_t *)kmalloc(sizeof(VME_DMAstart_t), GFP_KERNEL);     //MJ: can't we allocate this array statically?
      if (dma_params == NULL)
      {
	kdebug(("vme_rcc(ioctl,VMEDMASTART(%d)): error from kmalloc\n", current->pid));
        return(-VME_KMALLOC);
      }

      if (copy_from_user(dma_params, (void *)arg, sizeof(VME_DMAstart_t)))  
      {
	kerror(("vme_rcc(ioctl,VMEDMASTART(%d)): error from copy_from_user\n", current->pid))
        kfree(dma_params);
	return(-VME_EFAULT);
      }

      kdebug(("vme_rcc(ioctl,VMEDMASTART(%d)): PCI address of chain = 0x%016lx\n", current->pid, dma_params->paddr))
      
      spin_lock_irqsave(&dmalock, dma_irq_flags); 

      if(dma_todo_num == VME_DMATODOSIZE || dmas_pending == VME_DMATODOSIZE)
      {
        kerror(("vme_rcc(ioctl,VMEDMASTART(%d)): no memory left in to-do list\n", current->pid))
        spin_unlock_irqrestore(&dmalock, dma_irq_flags); 
	return(-VME_NODOMEMEM);
      }
      dma_todo_num++;
      dmas_pending++;
      windex = dma_todo_write;
      dma_todo_write++;
      if (dma_todo_write == VME_DMATODOSIZE)
        dma_todo_write = 0;	
     
      dma_list_todo[windex].paddr = dma_params->paddr;
      dma_list_todo[windex].pid   = current->pid;
      kdebug(("vme_rcc(ioctl,VMEDMASTART(%d)): using entry %d in to-do list.\n", current->pid, windex))
      dma_todo_total++;
      spin_unlock_irqrestore(&dmalock, dma_irq_flags); 

      kfree(dma_params);
      
      start_next_dma(0);
      kdebug(("vme_rcc(ioctl,VMEDMASTART(%d)): done\n", current->pid))      
      break;
    }
    
    case VMEDMAPOLL:
    {
      int serror, dummyerror;
      u_int jiffytowait;
      VME_DMAhandle_t *dma_poll_params; 
      
      dma_poll_params = (VME_DMAhandle_t *)kmalloc(sizeof(VME_DMAhandle_t), GFP_KERNEL);
      if (dma_poll_params == NULL)
      {
	kdebug(("vme_rcc(ioctl,VMEDMAPOLL(%d)): error from kmalloc\n", current->pid));
        return(-VME_KMALLOC);
      }
      
      if (copy_from_user(dma_poll_params, (void *)arg, sizeof(VME_DMAhandle_t)))   
      {
	kerror(("vme_rcc(ioctl,VMEDMAPOLL(%d)): error from copy_from_user\n", current->pid))
	kfree(dma_poll_params);
	return(-VME_EFAULT);
      }

      //Special cases:
      //timeoutval = 0:  Just check if the DMA is done or not
      //timeoutval = -1: Wait forever
      kdebug(("vme_rcc(ioctl,VMEDMAPOLL(%d)): timeoutval = %d\n", current->pid, dma_poll_params->timeoutval)) 

      if (dma_poll_params->timeoutval == 0)
      {
        kdebug(("vme_rcc(ioctl,VMEDMAPOLL(%d)): Checking if DMA is ready\n", current->pid))      
        serror = dma_is_done(dma_poll_params);
	if (serror == 0)  //still BUSY
	{      
	  dma_poll_params->ctrl    = 0x87654321;  // This actually means "don't look at it"
          dma_poll_params->counter = 0x87654321;  // This actually means "don't look at it"
          dummyerror = copy_to_user((void *)arg, dma_poll_params, sizeof(VME_DMAhandle_t)); 
	  if (dummyerror) 
            kerror(("vme_rcc(ioctl,VMEDMAPOLL(%d)): error from copy_to_user\n", current->pid))
          
	  kerror(("vme_rcc(ioctl,VMEDMAPOLL(%d)): DMA busy\n", current->pid))
	  kfree(dma_poll_params);
	  return(-VME_DMABUSY);   //exit case 4
	}
	else             //DMA done
	{
	  dummyerror = copy_to_user((void *)arg, dma_poll_params, sizeof(VME_DMAhandle_t));  
	  if (dummyerror) 
            kerror(("vme_rcc(ioctl,VMEDMAPOLL(%d)): error from copy_to_user\n", current->pid))

          kerror(("vme_rcc(ioctl,VMEDMAPOLL(%d)): DMA done\n", current->pid))
	  kfree(dma_poll_params);
	  return(-VME_SUCCESS);  //exit case 3 
	}
      }
      else
      {      
        kdebug(("vme_rcc(ioctl,VMEDMAPOLL(%d)): Starting to wait\n", current->pid))      
	if (dma_poll_params->timeoutval == -1)
          serror = wait_event_interruptible(dma_wait, dma_is_done(dma_poll_params));
	else
	{
	  jiffytowait = dma_poll_params->timeoutval * HZ / 1000;  
	  kdebug(("vme_rcc(ioctl,VMEDMAPOLL(%d)): jiffytowait = %d with HZ = %d\n", current->pid, jiffytowait, HZ))      
          serror = wait_event_interruptible_timeout(dma_wait, dma_is_done(dma_poll_params), jiffytowait);
        }

	kdebug(("vme_rcc(ioctl,VMEDMAPOLL(%d)): Woken up from wait\n", current->pid))      
	if (serror == -ERESTARTSYS)
	{
          kerror(("vme_rcc(ioctl,VMEDMAPOLL(%d)): wait_event_interruptible(_timeout) was interrupted by a signal\n", current->pid))      
          return(-VME_ERESTARTSYS);  //exit case 6
	}

	kdebug(("vme_rcc(ioctl,VMEDMAPOLL(%d)): wait_event_interruptible(_timeout) returns %d\n", current->pid, serror))      

	if (serror == 0 && dma_poll_params->timeoutval != -1)  //Time-out
	{      
          //MJ: if we have a timeout we should clear the entry of the respective request in the DMA to-do-list

          kerror(("vme_rcc(ioctl,VMEDMAPOLL(%d)): DMA time out\n", current->pid))      
	  dma_poll_params->ctrl    = 0x87654321;  // This actually means "don't look at it"
          dma_poll_params->counter = 0x87654321;  // This actually means "don't look at it"

	  //Clear the DMA controller
	  uni->dctl = 0;
          uni->dtbc = 0;
	  uni->dla  = 0;
	  uni->dva  = 0;
	  uni->dcpp = 0;
	  uni->dgcs = 0x6f00;
	  
	  //Now that the DMA controller is cleared we will not get a EOT interrupt any more. Therefore we
	  //have to declare the DMA controller to be free.
	  //MJ: as dma_is_free must be set to "0" right now (a DMA is active) we can touch it without requesting the spinlock and don't have to be affraid of a race condition
	  dma_is_free = 1;

          dummyerror = copy_to_user((void *)arg, dma_poll_params, sizeof(VME_DMAhandle_t));  
	  if (dummyerror) 
            kerror(("vme_rcc(ioctl,VMEDMAPOLL(%d)): error from copy_to_user\n", current->pid))

          kerror(("vme_rcc(ioctl,VMEDMAPOLL(%d)): done with timeout\n", current->pid))
	  kfree(dma_poll_params);
	  return(-VME_TIMEOUT);   //exit case 1
	}
      }
      
      kdebug(("vme_rcc(ioctl,VMEDMAPOLL(%d)): Woken up from wait\n", current->pid))
      
      //exit case 2 and 5      
      if (copy_to_user((void *)arg, dma_poll_params, sizeof(VME_DMAhandle_t)))
      {
	kerror(("vme_rcc(ioctl,VMEDMAPOLL(%d)): error from copy_to_user\n", current->pid))
	kfree(dma_poll_params);
	return(-VME_EFAULT);
      }
	
      kfree(dma_poll_params);
      kdebug(("vme_rcc(ioctl,VMEDMAPOLL(%d)): done\n", current->pid))
      break;
    }         
      

    case VMEDMADUMP: 
    {  
      char *buf;
      int len;
      u_int data, value;
      
      buf = (char *)kmalloc(TEXT_SIZE2, GFP_KERNEL);
      if (buf == NULL)
      {
        kerror(("vme_rcc(ioctl,VMESLAVEMAPDUMP): error from kmalloc\n"))
        return(-VME_KMALLOC);
      }
      len = 0;
      len += sprintf(buf + len, "PCI_CSR register               = 0x%08x\n", uni->pci_csr);
      len += sprintf(buf + len, "DMA registers:\n");
      len += sprintf(buf + len, "PCI address              (DLA) = 0x%08x\n", uni->dla);
      len += sprintf(buf + len, "VME address              (DVA) = 0x%08x\n", uni->dva);
      len += sprintf(buf + len, "Byte count              (DTBC) = 0x%08x\n", uni->dtbc);
      len += sprintf(buf + len, "Chain pointer           (DCPP) = 0x%08x\n", uni->dcpp);
      data = uni->dctl;
      len += sprintf(buf + len, "Transfer control reg.   (DCTL) = 0x%08x\n", data);
      len += sprintf(buf + len, "64 bit PCI is                    %s\n", data & 0x80 ? "Enabled" : "Disabled");
      len += sprintf(buf + len, "VMEbus cycle type:               %s\n", data & 0x100 ? "Block cycles" : "Single cycles");
      len += sprintf(buf + len, "AM code tpye:                    %s\n", data & 0x1000 ? "Supervisor" : "User");
      len += sprintf(buf + len, "AM code tpye:                    %s\n", data & 0x4000 ? "Programm" : "Data");
      len += sprintf(buf + len, "VMEbus address space:            ");
      value = (data >> 16) & 0x7;
      if (value == 0) len += sprintf(buf + len, "A16\n");
      if (value == 1) len += sprintf(buf + len, "A24\n");
      if (value == 2) len += sprintf(buf + len, "A32\n");
      if (value == 3) len += sprintf(buf + len, "Reserved\n");
      if (value == 4) len += sprintf(buf + len, "Reserved\n");
      if (value == 5) len += sprintf(buf + len, "Reserved\n");
      if (value == 6) len += sprintf(buf + len, "User 1\n");
      if (value == 7) len += sprintf(buf + len, "User 2\n");
      len += sprintf(buf + len, "VMEbus max data width:           ");
      value = (data >> 22) & 0x3;
      if (value == 0) len += sprintf(buf + len, "D8\n");
      if (value == 1) len += sprintf(buf + len, "D16\n");
      if (value == 2) len += sprintf(buf + len, "D32\n");
      if (value == 3) len += sprintf(buf + len, "D64\n");
      len += sprintf(buf + len, "Transfer direction:              VMEbus %s\n", data & 0x80000000 ? "Write":  "Read");
      data = uni->dgcs;
      len += sprintf(buf + len, "General control reg.    (DGCS) = 0x%08x\n", data);
      len += sprintf(buf + len, "Interrupt on protocol error:     %s\n", data & 0x1 ? "Enabled" : "Disabled");
      len += sprintf(buf + len, "Interrupt on VME error:          %s\n", data & 0x2 ? "Enabled" : "Disabled");
      len += sprintf(buf + len, "Interrupt on PCI error:          %s\n", data & 0x4 ? "Enabled" : "Disabled");
      len += sprintf(buf + len, "Interrupt when done:             %s\n", data & 0x8 ? "Enabled" : "Disabled");
      len += sprintf(buf + len, "Interrupt when halted:           %s\n", data & 0x20 ? "Enabled" : "Disabled");
      len += sprintf(buf + len, "Interrupt when stopped:          %s\n", data & 0x40 ? "Enabled" : "Disabled");
      len += sprintf(buf + len, "Protocol error:                  %s\n", data & 0x100 ? "Yes" : "No");
      len += sprintf(buf + len, "VMEbus error:                    %s\n", data & 0x200 ? "Yes" : "No");
      len += sprintf(buf + len, "PCI Bus error:                   %s\n", data & 0x400 ? "Yes" : "No");
      len += sprintf(buf + len, "Transfer complete:               %s\n", data & 0x800 ? "Yes" : "No");
      len += sprintf(buf + len, "DMA halted:                      %s\n",data&0x2000?"Yes":"No");
      len += sprintf(buf + len, "DMA stopped:                     %s\n",data&0x4000?"Yes":"No");
      len += sprintf(buf + len, "DMA active:                      %s\n",data&0x8000?"Yes":"No");
      value = (data >> 16) & 0xf;
      len += sprintf(buf + len, "Minimum inter tenure gap:        ");
      if (value == 0) len += sprintf(buf + len, "0 us\n");
      else if (value == 1) len += sprintf(buf + len, "16 us\n");
      else if (value == 2) len += sprintf(buf + len, "32 us\n");
      else if (value == 3) len += sprintf(buf + len, "64 us\n");
      else if (value == 4) len += sprintf(buf + len, "128 us\n");
      else if (value == 5) len += sprintf(buf + len, "256 us\n");
      else if (value == 6) len += sprintf(buf + len, "512 us\n");
      else if (value == 7) len += sprintf(buf + len, "1024 us\n");
      else len += sprintf(buf + len, "Out of range\n");
      len += sprintf(buf + len, "Bytes transfered per bus tenure: ");
      value = (data >> 20) & 0xf;
      if (value == 0) len += sprintf(buf + len, "Until done\n");
      else if (value == 1) len += sprintf(buf + len, "256 bytes\n");
      else if (value == 2) len += sprintf(buf + len, "512 bytes\n");
      else if (value == 3) len += sprintf(buf + len, "1 Kbye\n");
      else if (value == 4) len += sprintf(buf + len, "2 Kbye\n");
      else if (value == 5) len += sprintf(buf + len, "4 Kbye\n");
      else if (value == 6) len += sprintf(buf + len, "8 Kbye\n");
      else if (value == 7) len += sprintf(buf + len, "16 Kbye\n");
      else len += sprintf(buf + len, "Out of range\n");
      len += sprintf(buf + len, "DMA chaining:                    %s\n",data & 0x8000000 ? "Yes" : "No");
      len += sprintf(buf + len, "DMA halt request:                %s\n",data & 0x20000000 ? "Halt DMA after current package" : "No");
      len += sprintf(buf + len, "DMA stop request:                %s\n",data & 0x40000000 ? "process buffered data and stop" : "No");
      len += sprintf(buf + len, "DMA go:                          %s\n",data & 0x80000000 ? "Yes" : "No");

      if (copy_to_user((void *)arg, buf, TEXT_SIZE2*sizeof(char)))
      {
	kerror(("vme_rcc(ioctl,VMEDMADUMP): error from copy_to_user\n"))
	return(-VME_EFAULT);
      }
      kfree(buf);
      break;
    }
    
    case VMELINK: 
    {
      u_int i;
      int vct;
      VME_IntHandle_t *group_ptr;
      VME_IntHandle_t *VME_int_handle;

      VME_int_handle = (VME_IntHandle_t *)kmalloc(sizeof(VME_IntHandle_t), GFP_KERNEL);
      if (VME_int_handle == NULL)
      {
	kdebug(("vme_rcc(ioctl,VMELINK): error from kmalloc\n"));
        return(-VME_KMALLOC);
      }

      if (copy_from_user(VME_int_handle, (void *)arg, sizeof(VME_IntHandle_t)))
      {
	kerror(("vme_rcc(ioctl,VMELINK): error from copy_from_user\n"))
	kfree(VME_int_handle);
	return(-VME_EFAULT);
      }
      kdebug(("vme_rcc(ioctl,VMELINK): # vectors = %d\n", VME_int_handle->nvectors))
      for (i = 0; i < VME_int_handle->nvectors; i++)
        kdebug(("vme_rcc(ioctl,VMELINK): vector # %d = 0x%8x\n", i, VME_int_handle->vector[i]))

      if (irq_level_table[VME_int_handle->level - 1] == 0)        // is this level enabled?
      {
        kdebug(("vme_rcc(ioctl,VMELINK): level %d is disabled\n", VME_int_handle->level))
	kfree(VME_int_handle);
        return(-VME_INTDISABLED);
      }
      
      kdebug(("vme_rcc(ioctl,VMELINK): level = %d, type = %d\n", VME_int_handle->level, VME_int_handle->type))

      if (VME_int_handle->type != irq_level_table[VME_int_handle->level - 1])        // check type
      {
        kdebug(("vme_rcc(ioctl,VMELINK): level %d, user mode = %d, VMEconfig mode = %d\n",
		   VME_int_handle->level, VME_int_handle->type,  irq_level_table[VME_int_handle->level - 1]))
	kfree(VME_int_handle);
        return(-VME_INTCONF);
      }

      down(&link_table.link_dsc_sem);

      for (i = 0; i < VME_int_handle->nvectors; i++)
      {
        vct = VME_int_handle->vector[i]; 
        if (link_table.link_dsc[vct].pid) 
        {
          kdebug(("vme_rcc(ioctl,VMELINK): vector busy, pid = %d\n", link_table.link_dsc[vct].pid))
          up(&link_table.link_dsc_sem);
 	  kfree(VME_int_handle);
          return(-VME_INTUSED);
        }
      }

      // allocate aux. structure for multi vector handles and fill it
      group_ptr = (VME_IntHandle_t*) NULL;
      if (VME_int_handle->nvectors > 1)
      {
        group_ptr = kmalloc(sizeof(VME_IntHandle_t), GFP_KERNEL);  //MJ: Where is the matching kfree()? In VMEUNLINK?
        if (!group_ptr)
        {  
          up(&link_table.link_dsc_sem);
 	  kfree(VME_int_handle);
          return(-VME_ENOMEM);
        }
        group_ptr->nvectors = VME_int_handle->nvectors;
        for (i = 0; i < VME_int_handle->nvectors; i++)
          group_ptr->vector[i] = VME_int_handle->vector[i];
      }

      for (i = 0; i < VME_int_handle->nvectors; i++)
      {
        vct = VME_int_handle->vector[i]; 

        link_table.link_dsc[vct].pid = current->pid;		       // current process
        link_table.link_dsc[vct].vct = vct;	        	       // vector
        link_table.link_dsc[vct].lvl = VME_int_handle->level;          // level
        link_table.link_dsc[vct].type = VME_int_handle->type;          // type
        link_table.link_dsc[vct].pending = 0;	        	       // no interrupt pending
        link_table.link_dsc[vct].total_count = 0;       	       // total # interrupts
        link_table.link_dsc[vct].group_ptr = group_ptr; 	       // the group vectors
        sema_init(&link_table.link_dsc[vct].sem, 0); 		       // initialise semaphore count
        link_table.link_dsc[vct].sig = 0;	        	       // no signal
      
        pdata->link_dsc_flag[vct] = 1;			               // remember which file it belongs to
      }

      up(&link_table.link_dsc_sem);
      kfree(VME_int_handle);
      break;
    }

    case VMEINTENABLE: 
    {
      u_int lint_en;
      u_long flags;
      VME_IntEnable_t *VME_IntEnable2;

      VME_IntEnable2 = (VME_IntEnable_t *)kmalloc(sizeof(VME_IntEnable_t), GFP_KERNEL);
      if (VME_IntEnable2 == NULL)
      {
	kdebug(("vme_rcc(ioctl,VMEINTENABLE): error from kmalloc\n"));
        return(-VME_KMALLOC);
      }

      if (copy_from_user(VME_IntEnable2, (void *)arg, sizeof(VME_IntEnable_t)))
      {
	kerror(("vme_rcc(ioctl,VMEINTENABLE): error from copy_from_user\n"))
	kfree(VME_IntEnable2);
	return(-VME_EFAULT);
      }
      kdebug(("vme_rcc(ioctl,VMEINTENABLE): level = %d\n", VME_IntEnable2->level))

      // ONLY allowed if RORA level
      if (irq_level_table[VME_IntEnable2->level-1] != VME_INT_RORA)
      {
	kfree(VME_IntEnable2);
        return(-VME_LVLISNOTRORA);
      }
      
      local_irq_save(flags);                            //MJ: for 2.6 see p274 & p275

      lint_en = readl((char*)uni + LINT_EN);    	// read enabled irq bits
      lint_en |= (1 << VME_IntEnable2->level);	        // BM_LINT_VIRQxxx
      kdebug(("vme_rcc(ioctl,VMEINTENABLE): lint_en =0x %x\n", lint_en))
      writel(lint_en, (char*)uni + LINT_EN);	        // write it back

      local_irq_restore(flags);
      kfree(VME_IntEnable2);
      break;
    }
/*
    case VMEINTDISABLE: 
    {
      u_int lint_en;
      u_long flags;
      VME_IntEnable_t *VME_IntEnable3;

      VME_IntEnable3 = (VME_IntEnable_t *)kmalloc(sizeof(VME_IntEnable_t), GFP_KERNEL);
      if (VME_IntEnable3 == NULL)
      {
	kdebug(("vme_rcc(ioctl,VMEINTDISABLE): error from kmalloc\n"));
        return(-VME_KMALLOC);
      }

      if (copy_from_user(VME_IntEnable3, (void *)arg, sizeof(VME_IntEnable_t)))
      {
	kerror(("vme_rcc(ioctl,VMEINTDISABLE): error from copy_from_user\n"))
	kfree(VME_IntEnable3);
	return(-VME_EFAULT);
      }
      kdebug(("vme_rcc(ioctl,VMEINTDISABLE): level = %d\n", VME_IntEnable3->level))

      local_irq_save(flags);                            //MJ: for 2.6 see p274 & p275

      lint_en = readl((char*)uni + LINT_EN);	        // read enabled irq bits
      lint_en &= ~(1 << VME_IntEnable3->level);	        // BM_LINT_VIRQxxx
      kdebug(("vme_rcc(ioctl,VMEINTDISABLE): lint_en =0x %x\n", lint_en))
      writel(lint_en, (char*)uni + LINT_EN);	        // write it back

      local_irq_restore(flags);
      kfree(VME_IntEnable3);
      break;
    }
*/
    case VMEWAIT:
    {
      int first_vct, cur_vct = 0, use_timer = 0, result = 0;
      u_int i;
      u_long flags;
      struct timer_list int_timer;
      VME_IntHandle_t *group_ptr;
      VME_WaitInt_t *VME_WaitInt2;

      VME_WaitInt2 = (VME_WaitInt_t *)kmalloc(sizeof(VME_WaitInt_t), GFP_KERNEL);
      if (VME_WaitInt2 == NULL)
      {
	kdebug(("vme_rcc(ioctl,VMEWAIT): error from kmalloc\n"));
        return(-VME_KMALLOC);
      }

      if (copy_from_user(VME_WaitInt2, (void *)arg, sizeof(VME_WaitInt_t))) 
      {
	kerror(("vme_rcc(ioctl,VMEWAIT): error from copy_from_user\n"))
	kfree(VME_WaitInt2);
	return(-VME_EFAULT);
      }
      kdebug(("vme_rcc(ioctl,VMEWAIT): # vectors = %d, timeout = %d\n", VME_WaitInt2->int_handle.nvectors, VME_WaitInt2->timeout))
      for (i = 0; i < VME_WaitInt2->int_handle.nvectors; i++)
        kdebug(("vme_rcc(ioctl,VMEWAIT): vector # %d = 0x%8x\n", i, VME_WaitInt2->int_handle.vector[i]))

      first_vct = VME_WaitInt2->int_handle.vector[0];

      if (VME_WaitInt2->timeout > 0) 
      {
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,0,0) 
        init_timer(&int_timer);
        int_timer.function = vme_intTimeout;
        int_timer.data = (u_long)VME_WaitInt2;  
        int_timer.expires = jiffies + VME_WaitInt2->timeout * HZ / 1000;  //convert from ms to kernel-jiffis (pretty pointless for the current 1 ms jiffies)
#else
        timer_setup(&int_timer, vme_intTimeout, 0);
#endif                                                     //Note: At the kernel level a jiffi is 1 ms (10 ms at the user level) 
        kdebug(("vme_rcc(ioctl,VMEWAIT): timeout will fire at %lu jiffies\n", jiffies + VME_WaitInt2->timeout * HZ / 1000))
        add_timer(&int_timer);
        use_timer = 1;
      }

      if (VME_WaitInt2->timeout) 		
      {
        kdebug(("vme_rcc(ioctl,VMEWAIT): Before Down, semaphore = %d\n", link_table.link_dsc[first_vct].sem.count))
        // IR handled before we wait. Not quite RACE safe but only for statistics  .. 
        if (link_table.link_dsc[first_vct].sem.count > 0) 
        {
          if (link_table.link_dsc[first_vct].sem.count == 1)
            sem_positive[0]++;
          else
            sem_positive[1]++;
        }

        ret = down_interruptible(&link_table.link_dsc[first_vct].sem);	// wait ..  //MJ: why are we not interested in the value of "ret"?

        if (signal_pending(current))
        {						// interrupted system call
          kdebug(("vme_rcc(ioctl,VMEWAIT): Interrupted by signal\n"))
          result = -VME_INTBYSIGNAL;		        // or ERESTARTSYS ?
        }

        else if ((use_timer == 1) && (VME_WaitInt2->timeout == 0))
        {						// interrupted system call
          kdebug(("vme_rcc(ioctl,VMEWAIT): Time out period expired. jiffies = %lu\n", jiffies))
          result = -VME_TIMEOUT;
        }
      }

      VME_WaitInt2->vector = 0;                   // return 0 if NO pending
      VME_WaitInt2->level = 0;
      VME_WaitInt2->type = 0;
      VME_WaitInt2->multiple = 0;

      if (result == 0)		                                // for all values of timeout & no early returns: get IR info
      {
        if (link_table.link_dsc[first_vct].group_ptr == NULL)	// single vector
        {
          if (link_table.link_dsc[first_vct].pending > 0)
          {
            VME_WaitInt2->level = link_table.link_dsc[first_vct].lvl;
            VME_WaitInt2->type = link_table.link_dsc[first_vct].type;
            VME_WaitInt2->vector = first_vct;
            VME_WaitInt2->multiple = link_table.link_dsc[first_vct].pending;    // possible RACE with ISR
            link_table.link_dsc[first_vct].pending--;                           // done
          }
        }
        else  // scan vectors in group and return FIRST one pending. The scan starts from the first: an issue ?
        {
          local_irq_save(flags);  // may not really be needed //MJ: for 2.6 see p274 & p275

          group_ptr = link_table.link_dsc[first_vct].group_ptr;
          kdebug(("vme_rcc(ioctl,VMEWAIT): group pointer = 0x%p\n",group_ptr))
          for (i = 0; i < group_ptr->nvectors; i++)
          {
            cur_vct = group_ptr->vector[i];
            kdebug(("vme_rcc(ioctl,VMEWAIT): current vector = %d\n",cur_vct))
            if (link_table.link_dsc[cur_vct].pending > 0)
            {
              VME_WaitInt2->level = link_table.link_dsc[cur_vct].lvl;
              VME_WaitInt2->type = link_table.link_dsc[cur_vct].type;
              VME_WaitInt2->vector = cur_vct;
              VME_WaitInt2->multiple = link_table.link_dsc[cur_vct].pending;
              link_table.link_dsc[cur_vct].pending--;		                   // done
              break;
            }
          }
          local_irq_restore(flags);
        }
      }

      if (use_timer == 1)
        del_timer(&int_timer);	// OK also if timer ran out

      if (copy_to_user((void *)arg, VME_WaitInt2, sizeof(VME_WaitInt_t)))
      {
	kerror(("vme_rcc(ioctl,VMEWAIT): error from copy_to_user\n"))
	kfree(VME_WaitInt2);
	return(-VME_EFAULT);
      }

      kfree(VME_WaitInt2);
      return (result);
      break;	// dummy ..
    }

    case VMEREGISTERSIGNAL:
    {
      int vct;
      u_int i;
      VME_RegSig_t *VME_RegSig;
      
      VME_RegSig = (VME_RegSig_t *)kmalloc(sizeof(VME_RegSig_t), GFP_KERNEL);
      if (VME_RegSig == NULL)
      {
	kdebug(("vme_rcc(ioctl,VMEREGISTERSIGNAL): error from kmalloc\n"));
        return(-VME_KMALLOC);
      }

      if (copy_from_user(VME_RegSig, (void *)arg, sizeof(VME_RegSig_t)))   
      {
	kerror(("vme_rcc(ioctl,VMEREGISTERSIGNAL): error from copy_from_user\n"))
	kfree(VME_RegSig);
	return(-VME_EFAULT);
      }

      pdata->VME_int_handle2 = VME_RegSig->int_handle;

      kdebug(("vme_rcc(ioctl,VMEREGISTERSIGNAL): # vectors = %d, signal # = %d\n", pdata->VME_int_handle2.nvectors, VME_RegSig->signum))
      for (i = 0; i < pdata->VME_int_handle2.nvectors; i++)
        kdebug(("vme_rcc(ioctl,VMEREGISTERSIGNAL): vector # %d = 0x%8x\n", i, pdata->VME_int_handle2.vector[i]));

      down(&link_table.link_dsc_sem);

      for (i = 0; i < pdata->VME_int_handle2.nvectors; i++)
      {
        vct = pdata->VME_int_handle2.vector[i]; 
        link_table.link_dsc[vct].sig = VME_RegSig->signum;
      }

      up(&link_table.link_dsc_sem);
      kfree(VME_RegSig);
      break;
    }

    case VMEINTERRUPTINFOGET:
    {
      int first_vct, cur_vct = 0;
      u_int i;
      u_long flags;
      VME_IntHandle_t *group_ptr;
      VME_WaitInt_t *VME_WaitInt3;

      VME_WaitInt3 = (VME_WaitInt_t *)kmalloc(sizeof(VME_WaitInt_t), GFP_KERNEL);
      if (VME_WaitInt3 == NULL)
      {
	kdebug(("vme_rcc(ioctl,VMEINTERRUPTINFOGET): error from kmalloc\n"));
        return(-VME_KMALLOC);
      }

      if (copy_from_user(VME_WaitInt3, (void *)arg, sizeof(VME_WaitInt_t)))
      {
	kdebug(("vme_rcc(ioctl,VMEINTERRUPTINFOGET): error from copy_from_user\n"))
	kfree(VME_WaitInt3);
	return(-VME_EFAULT);
      }
      kdebug(("vme_rcc(ioctl,VMEINTERRUPTINFOGET): # vectors = %d \n", VME_WaitInt3->int_handle.nvectors))
      for (i = 0; i < VME_WaitInt3->int_handle.nvectors; i++)
        kdebug(("vme_rcc(ioctl,VMEINTERRUPTINFOGET): vector # %d = 0x%8x\n", i, VME_WaitInt3->int_handle.vector[i]))

      first_vct = VME_WaitInt3->int_handle.vector[0];

      VME_WaitInt3->vector   = 0;      // return 0 if NO pending
      VME_WaitInt3->level    = 0;
      VME_WaitInt3->type     = 0;
      VME_WaitInt3->multiple = 0;

      if (link_table.link_dsc[first_vct].group_ptr == NULL)
      {
        if (link_table.link_dsc[first_vct].pending > 0)
        {
          kdebug(("vme_rcc(ioctl,VMEINTERRUPTINFOGET): Case 1\n"))
          VME_WaitInt3->level    = link_table.link_dsc[first_vct].lvl;
          VME_WaitInt3->type     = link_table.link_dsc[first_vct].type;
          VME_WaitInt3->vector   = first_vct;
          VME_WaitInt3->multiple = link_table.link_dsc[first_vct].pending;	
          link_table.link_dsc[first_vct].pending--;                          
        }
      }
      else		// scan vectors in group and return FIRST one pending 
      {
        kdebug(("vme_rcc(ioctl,VMEINTERRUPTINFOGET): Case 2\n"))
        local_irq_save(flags);  // may not really be needed //MJ: for 2.6 see p274 & p275

        group_ptr = link_table.link_dsc[first_vct].group_ptr;
        kdebug(("vme_rcc(ioctl,VMEINTERRUPTINFOGET): group pointer = 0x%p\n", group_ptr))
        for (i = 0; i < group_ptr->nvectors; i++)
        {
          cur_vct = group_ptr->vector[i];
          kdebug(("vme_rcc(ioctl,VMEINTERRUPTINFOGET): current vector = %d\n", cur_vct))
          if (link_table.link_dsc[cur_vct].pending > 0)
          {
            VME_WaitInt3->level    = link_table.link_dsc[cur_vct].lvl;
            VME_WaitInt3->type     = link_table.link_dsc[cur_vct].type;
            VME_WaitInt3->vector   = cur_vct;
            VME_WaitInt3->multiple = link_table.link_dsc[cur_vct].pending;
            link_table.link_dsc[cur_vct].pending--;                           
            kdebug(("vme_rcc(ioctl,VMEINTERRUPTINFOGET): pending is = %d\n", link_table.link_dsc[cur_vct].pending))
            break;
          }
        }
        local_irq_restore(flags);
      }

      if (copy_to_user((void *)arg, VME_WaitInt3, sizeof(VME_WaitInt_t)))
      {
	kerror(("vme_rcc(ioctl,VMEINTERRUPTINFOGET): error from copy_to_user\n"))
	kfree(VME_WaitInt3);
	return(-VME_EFAULT);
      }
      kfree(VME_WaitInt3);
      break;
    }

    case VMEUNLINK: 
    {
      u_int i;
      int vct, first_vector;
      VME_IntHandle_t *VME_int_handle3;

      VME_int_handle3 = (VME_IntHandle_t *)kmalloc(sizeof(VME_IntHandle_t), GFP_KERNEL);
      if (VME_int_handle3 == NULL)
      {
	kdebug(("vme_rcc(ioctl,VMEUNLINK): error from kmalloc\n"));
        return(-VME_KMALLOC);
      }

      if (copy_from_user(VME_int_handle3, (void *)arg, sizeof(VME_IntHandle_t)))
      {
	kerror(("vme_rcc(ioctl,VMEUNLINK): error from copy_from_user\n"))
	kfree(VME_int_handle3);
	return(-VME_EFAULT);
      }
      kdebug(("vme_rcc(ioctl,VMEUNLINK): # vectors = %d\n", VME_int_handle3->nvectors))
      for (i = 0; i < VME_int_handle3->nvectors; i++)
        kdebug(("vme_rcc(ioctl,VMEUNLINK): vector # %d = 0x%x\n", i, VME_int_handle3->vector[i]));

      down(&link_table.link_dsc_sem);

      if (VME_int_handle3->nvectors > 1)
      {
        first_vector = VME_int_handle3->vector[0];
        kfree(link_table.link_dsc[first_vector].group_ptr);  //MJr: this seems to be the kfree for the kmalloc in VMELINK
        kdebug(("vme_rcc(ioctl,VMEUNLINK): first vector = %d\n", first_vector))
        kdebug(("vme_rcc(ioctl,VMEUNLINK): group_ptr = 0x%p\n", link_table.link_dsc[first_vector].group_ptr))
      }

      for (i = 0; i < VME_int_handle3->nvectors; i++)
      {
        vct = VME_int_handle3->vector[i]; 

        link_table.link_dsc[vct].pid = 0;
        link_table.link_dsc[vct].vct = 0;
        link_table.link_dsc[vct].lvl = 0;
        link_table.link_dsc[vct].pending = 0;
        link_table.link_dsc[vct].total_count = 0;
        link_table.link_dsc[vct].group_ptr = 0;
        sema_init(&link_table.link_dsc[vct].sem, 0);
        link_table.link_dsc[vct].sig = 0;

        pdata->link_dsc_flag[vct] = 0;
      }
      up(&link_table.link_dsc_sem);
      kfree(VME_int_handle3);
      break;
    }
 
    // called from vmeconfig when Universe registers are updated
    case VMEUPDATE:   
    {      
      int ret, i;
      VME_Update_t *update_data;
    
      update_data = (VME_Update_t *)kmalloc(sizeof(VME_Update_t), GFP_KERNEL);
      if (update_data == NULL)
      {
	kdebug(("vme_rcc(ioctl,VMEUPDATE): error from kmalloc\n"));
        return(-VME_KMALLOC);
      }

      kdebug(("vme_rcc(ioctl,VMEUPDATE): Function called\n")) 
        
      ret = copy_from_user(update_data, (void *)arg, sizeof(VME_Update_t));
      if (ret)
      {
	kerror(("vme_rcc(ioctl,VMEUPDATE): error %d from copy_from_user\n",ret))
	kfree(update_data);
	return(-VME_EFAULT);
      }
      
      fill_mstmap(uni->lsi0_ctl, uni->lsi0_bs, uni->lsi0_bd, uni->lsi0_to, &stmap.master[0]);
      fill_mstmap(uni->lsi1_ctl, uni->lsi1_bs, uni->lsi1_bd, uni->lsi1_to, &stmap.master[1]);
      fill_mstmap(uni->lsi2_ctl, uni->lsi2_bs, uni->lsi2_bd, uni->lsi2_to, &stmap.master[2]);
      fill_mstmap(uni->lsi3_ctl, uni->lsi3_bs, uni->lsi3_bd, uni->lsi3_to, &stmap.master[3]);
      fill_mstmap(uni->lsi4_ctl, uni->lsi4_bs, uni->lsi4_bd, uni->lsi4_to, &stmap.master[4]);
      fill_mstmap(uni->lsi5_ctl, uni->lsi5_bs, uni->lsi5_bd, uni->lsi5_to, &stmap.master[5]);
      fill_mstmap(uni->lsi6_ctl, uni->lsi6_bs, uni->lsi6_bd, uni->lsi6_to, &stmap.master[6]);
      fill_mstmap(uni->lsi7_ctl, uni->lsi7_bs, uni->lsi7_bd, uni->lsi7_to, &stmap.master[7]);

      fill_slvmap(uni->vsi0_ctl, uni->vsi0_bs, uni->vsi0_bd, uni->vsi0_to, &stmap.slave[0]);
      fill_slvmap(uni->vsi1_ctl, uni->vsi1_bs, uni->vsi1_bd, uni->vsi1_to, &stmap.slave[1]);
      fill_slvmap(uni->vsi2_ctl, uni->vsi2_bs, uni->vsi2_bd, uni->vsi2_to, &stmap.slave[2]);
      fill_slvmap(uni->vsi3_ctl, uni->vsi3_bs, uni->vsi3_bd, uni->vsi3_to, &stmap.slave[3]);
      fill_slvmap(uni->vsi4_ctl, uni->vsi4_bs, uni->vsi4_bd, uni->vsi4_to, &stmap.slave[4]);
      fill_slvmap(uni->vsi5_ctl, uni->vsi5_bs, uni->vsi5_bd, uni->vsi5_to, &stmap.slave[5]);
      fill_slvmap(uni->vsi6_ctl, uni->vsi6_bs, uni->vsi6_bd, uni->vsi6_to, &stmap.slave[6]);
      fill_slvmap(uni->vsi7_ctl, uni->vsi7_bs, uni->vsi7_bd, uni->vsi7_to, &stmap.slave[7]); 
 
      // update level table
      if (update_data->irq_mode[0])
      {
        kdebug(("vme_rcc(ioctl,VMEUPDATE): IRQ mode parameters are valid\n"))
        for (i = 0; i < 7; i++)	
        {
          irq_level_table[i] = update_data->irq_mode[i + 1];
          kdebug(("vme_rcc(ioctl,VMEUPDATE): IRQ mode for level %d = %d\n", i, update_data->irq_mode[i + 1]))
        }
        irq_sysfail =  update_data->irq_mode[8];
        kdebug(("vme_rcc(ioctl,VMEUPDATE): IRQ mode for SYSFAIL = %d\n", update_data->irq_mode[8]))
      }
      else
        kdebug(("vme_rcc(ioctl,VMEUPDATE): IRQ mode parameters are not valid\n"))
      vmetab_ok = 1;
      kfree(update_data);
      break;
    } 
    
    case VMESYSRST:
    {
      int regval;
      
      kdebug(("vme_rcc(ioctl,VMESYSRST): Setting SYSRESET\n"))
      regval = uni->misc_ctl;
      uni->misc_ctl = (regval | 0x00400000);
      //according to the VMEbus standard sysreset has to be active for at least 200 ms
      msleep(200);      
      kdebug(("vme_rcc(ioctl,VMESYSRST): Clearing SYSRESET\n"))    
      uni->misc_ctl = regval;
      break;
    }  
    
    case VMESBCTYPE:
    {
      u_int sbctype = VME_SBC_UNIVERSE; 
      kdebug(("vme_rcc(ioctl,VMESBCTYPE): This is a UNIVERSE based SBC\n"))
      if (copy_to_user((void *)arg, &sbctype, sizeof(u_int)))
      {
	kerror(("vme_rcc(ioctl,VMESBCTYPE): error from copy_to_user\n"))
	return(-VME_EFAULT);
      }      
      break;
    }  
   
    case VMETEST:
    {
      int ret, count;

      ret = copy_from_user(&count, (void *)arg, sizeof(int));
      if (ret)
      {
	kerror(("vme_rcc(ioctl,VMETEST): error %d from copy_from_user\n",ret))
	return(-VME_EFAULT);
      }

      count++;

      if (copy_to_user((void *)arg, &count, sizeof(int)))
      {
	kerror(("vme_rcc(ioctl,VMETEST): error from copy_to_user\n"))
	return(-VME_EFAULT);
      }

      kdebug(("vme_rcc(ioctl,VMETEST): VME_ENOSYS=0x%08x\n",VME_ENOSYS))
      return(-VME_ENOSYS);
      break;
    }
    
    default:
    {
      kerror(("vme_rcc(ioctl,default): cmd = %d, VME_RCC_CONSISTENCY_CHECK = 0x%08x\n", cmd, VME_RCC_CONSISTENCY_CHECK))
      kerror(("vme_rcc(ioctl,default): You should not be here\n"))
      return(-EINVAL);
    }
  }
  return(0);
}


/*****************************************************/
static void vme_rcc_vmaOpen(struct vm_area_struct *vma)
/*****************************************************/
{ 
  kdebug(("vme_rcc_vmaOpen: Called\n"));
}


/******************************************************/
static void vme_rcc_vmaClose(struct vm_area_struct *vma)
/******************************************************/
{ 
  kdebug(("vme_rcc_vmaClose: mmap released\n"));
}


/********************************************************************/
static int vme_rcc_mmap(struct file *file, struct vm_area_struct *vma)
/********************************************************************/
{
  u_long size, offset;  

  kdebug(("vme_rcc_mmap: vma->vm_end       = 0x%016lx\n", (u_long)vma->vm_end));
  kdebug(("vme_rcc_mmap: vma->vm_start     = 0x%016lx\n", (u_long)vma->vm_start));
  kdebug(("vme_rcc_mmap: vma->vm_pgoff     = 0x%016lx\n", (u_long)vma->vm_pgoff));
  kdebug(("vme_rcc_mmap: vma->vm_flags     = 0x%08x\n", (u_int)vma->vm_flags));
  kdebug(("vme_rcc_mmap: PAGE_SHIFT        = 0x%016lx\n", (u_long)PAGE_SHIFT));
  kdebug(("vme_rcc_mmap: PAGE_SIZE         = 0x%016lx\n", (u_long)PAGE_SIZE));

  size = vma->vm_end - vma->vm_start;
  offset = vma->vm_pgoff << PAGE_SHIFT;

  kdebug(("vme_rcc_mmap: size                  = 0x%016lx\n", size));
  kdebug(("vme_rcc_mmap: physical base address = 0x%016lx\n", offset));

  if (remap_pfn_range(vma, vma->vm_start, vma->vm_pgoff, size, vma->vm_page_prot))
  {
    kerror(("vme_rcc_mmap: function remap_pfn_range failed \n"));
    return(-VME_REMAP);
  }
  kdebug(("io_rcc_mvme_rcc_mmapmap: remap_pfn_range OK, vma->vm_start(2) = 0x%016lx\n", (u_long)vma->vm_start));

  vma->vm_ops = &vme_rcc_vm_ops;

/* This is the original code (OK on 32bit Linux bay maybe too complicated)
  u_int size, offset;  

  vma->vm_flags |= VM_RESERVED;
  vma->vm_flags |= VM_LOCKED;

  kdebug(("vme_rcc(mmap): vma->vm_end    = 0x%08x\n", (u_int)vma->vm_end))
  kdebug(("vme_rcc(mmap): vma->vm_start  = 0x%08x\n", (u_int)vma->vm_start))
  kdebug(("vme_rcc(mmap): vma->vm_offset = 0x%08x\n", (u_int)vma->vm_pgoff << PAGE_SHIFT))
  kdebug(("vme_rcc(mmap): vma->vm_flags  = 0x%08x\n", (u_int)vma->vm_flags))

  size = vma->vm_end - vma->vm_start;
  offset = vma->vm_pgoff << PAGE_SHIFT;

  if (remap_pfn_range(vma, vma->vm_start, vma->vm_pgoff, size, vma->vm_page_prot))
  {
    kerror(("vme_rcc(mmap): function remap_page_range failed \n"))
    return(-VME_REMAP);
  }
  kdebug(("vme_rcc(mmap): vma->vm_start(2) = 0x%08x\n", (u_int)vma->vm_start))

  vma->vm_ops = &vme_rcc_vm_ops;*/
  
  return(0);
}


/*********************************************************************************************************/
static ssize_t vme_rcc_proc_write(struct file *file, const char *buffer, size_t count, loff_t *startOffset)
/*********************************************************************************************************/
{
  int len;
  char value[100];

  kdebug(("vme_rcc(write_procmem): vme_rcc_write_procmem called\n"))

  if(count > 99)
    len = 99;
  else
    len = count;

  if (copy_from_user(value, buffer, len))
  {
    kdebug(("vme_rcc(write_procmem): error from copy_from_user\n"))
    return(-VME_EFAULT);
  }

  kdebug(("vme_rcc(write_procmem): len = %d\n", len))
  value[len - 1] = '\0';
  kdebug(("vme_rcc(write_procmem): text passed = %s\n", value))

  if (!strcmp(value, "debug"))
  {
    debug = 1;
    kdebug(("vme_rcc(write_procmem): debugging enabled\n"))
  }

  if (!strcmp(value, "nodebug"))
  {
    kdebug(("vme_rcc(write_procmem): debugging disabled\n"))
    debug = 0;
  }

  if (!strcmp(value, "elog"))
  {
    kdebug(("vme_rcc(write_procmem): error logging enabled\n"))
    errorlog = 1;
  }

  if (!strcmp(value, "noelog"))
  {
    kdebug(("vme_rcc(write_procmem): error logging disabled\n"))
    errorlog = 0;
  }
  return len;
}


/****************************************************/
int vme_rcc_proc_show(struct seq_file *sfile, void *p)
/****************************************************/
{
  int cnt, loop;
  u_int lint_stat, lint_en, lint_mask;
  
  kdebug(("vme_rcc(vme_rcc_proc_show): Creating text....\n"));

  //The COMCOMCOM below is defined in vme_rcc_common.h
  //If you execute "strings vme_rcc-3.10.0-957.21.3.el7.x86_64.ko | grep ccc" you should see "cccc1111" this has to match the result of "strings libvme_rcc.so | grep -e lll -e ccc"
  seq_printf(sfile, "VMEbus driver (ioctr %s, ALMA9 ready) for TDAQ release %s (based on tag %s, vme_rcc.c revision %s)\n", COMCOMCOM, RELEASE_NAME, CVSTAG, VME_RCC_TAG);

  if (board_type == VP_PSE) seq_printf(sfile, "Board type: VP_PSE\n");
  if (board_type == VP_PMC) seq_printf(sfile, "Board type: VP_PMC\n");
  if (board_type == VP_100) seq_printf(sfile, "Board type: VP_100\n");
  if (board_type == VP_CP1) seq_printf(sfile, "Board type: VP_CP1\n");
  if (board_type == VP_110) seq_printf(sfile, "Board type: VP_110\n");
  if (board_type == VP_315) seq_printf(sfile, "Board type: VP_315\n");
  if (board_type == VP_317) seq_printf(sfile, "Board type: VP_317\n");
  if (board_type == VP_325) seq_printf(sfile, "Board type: VP_325\n");
  if (board_type == VP_327) seq_printf(sfile, "Board type: VP_327\n");
  if (board_type == VP_417) seq_printf(sfile, "Board type: VP_417\n");    
  if (board_type == VP_EXX) seq_printf(sfile, "Board type: VP_Exx\n");
  if (board_type == VP_BXX) seq_printf(sfile, "Board type: VP_Bxx\n");
  if (board_type == VP_FXX) seq_printf(sfile, "Board type: VP_Fxx\n");

  seq_printf(sfile, "\nThe Universe chip %s been initialized with vmeconfig\n", vmetab_ok ? "has" : "has NOT");
  seq_printf(sfile, "\n SAFE function statistics\n");
  seq_printf(sfile, " TXFIFO not empty before safe cycle : %d times\n", txfifo_wait_1);
  seq_printf(sfile, " TXFIFO not empty after safe cycle  : %d times\n", txfifo_wait_2);
  seq_printf(sfile, "\n VMEbus interrupts\n");
  seq_printf(sfile, "\n  level  state     enabled  pending  # interrupts\n");

  lint_stat = readl((char*)uni + LINT_STAT);	// read the interrupt bits
  lint_en = readl((char*)uni + LINT_EN);	// read enabled irq bitsa

  for (cnt = 0; cnt < 7; cnt++)
  {
    lint_mask = 1<<(cnt+1);			// BM_Lxxx_VIRQxxx

    if (irq_level_table[cnt] == VME_LEVELISDISABLED)
      seq_printf(sfile, "      %d  %s  %s  %s\n", cnt+1,"disabled", "           ", (lint_stat & lint_mask) ? "yes":" no");
    else if (irq_level_table[cnt] == VME_INT_ROAK)
      seq_printf(sfile, "      %d  %s  %11s  %7s      %8d\n", cnt+1,"ROAK", (lint_en & lint_mask) ? "yes":" no", (lint_stat & lint_mask) ? "yes":" no", Interrupt_counters.virq[cnt]);
    else if (irq_level_table[cnt] == VME_INT_RORA)
      seq_printf(sfile, "      %d  %s  %11s  %7s      %8d\n", cnt+1,"RORA", (lint_en & lint_mask) ? "yes":" no", (lint_stat & lint_mask) ? "yes":" no", Interrupt_counters.virq[cnt]);
  }

  if (irq_sysfail == VME_LEVELISDISABLED)
    seq_printf(sfile, "      %d  %s  %s  %s\n", cnt+1, "disabled", "           ", (lint_stat & lint_mask) ? "yes":" no") ;

  else 
    seq_printf(sfile, "SYSFAIL  %s  %11s  %7s      %8d\n", "RORA", (lint_en & 0x4000) ? "yes":" no", (lint_stat & 0x4000) ? "yes":" no", Interrupt_counters.sysfail);

  seq_printf(sfile, "   # of Bus Error detected      %8d\n", berr_int_count);
  seq_printf(sfile, "   # of block transfers done    %8d\n", Interrupt_counters.dma);
  seq_printf(sfile, "\n Link Descriptors: \n");

  for (cnt = 0; cnt< VME_VCTSIZE; cnt++) 
  {
    if (link_table.link_dsc[cnt].pid)
    {
      seq_printf(sfile, "\n Array index     = %02d\n", cnt);
      seq_printf(sfile, " Pid             = %02d\n", link_table.link_dsc[cnt].pid);
      seq_printf(sfile, " Vector          = %02d\n", link_table.link_dsc[cnt].vct);
      seq_printf(sfile, " Level           = %02d\n", link_table.link_dsc[cnt].lvl);
      seq_printf(sfile, " Type            = %02d\n", link_table.link_dsc[cnt].type);
      seq_printf(sfile, " Pending         = %02d\n", link_table.link_dsc[cnt].pending);
      seq_printf(sfile, " # interrupts    = %02d\n", link_table.link_dsc[cnt].total_count);
      seq_printf(sfile, " group pointer   = 0x%p\n", link_table.link_dsc[cnt].group_ptr);
      seq_printf(sfile, " Semaphore count = 0x%x\n", link_table.link_dsc[cnt].sem.count);
      seq_printf(sfile, " Signal #        = %d\n", link_table.link_dsc[cnt].sig);
    }
  }

  seq_printf(sfile, " Interrupt semaphore statistics\n");
  seq_printf(sfile, " semaphore count = 1 : %d times\n", sem_positive[0]);
  seq_printf(sfile, " semaphore count > 1 : %d times\n", sem_positive[1]);

  seq_printf(sfile, "\n BERR Links: \n");
  for (cnt = 0; cnt< VME_MAX_BERR_PROCS; cnt++) 
  {
    if (berr_proc_table.berr_link_dsc[cnt].pid)
    {
      seq_printf(sfile, "\n Array index     = %02d\n", cnt);
      seq_printf(sfile, " Pid             = %02d\n", berr_proc_table.berr_link_dsc[cnt].pid);
      seq_printf(sfile, " Signal #        = %d\n", berr_proc_table.berr_link_dsc[cnt].sig);
    }
  }

  seq_printf(sfile, "\n SYSFAIL Link: \n");
  if (sysfail_link.pid)
  {
    seq_printf(sfile, " Pid             = %02d\n", sysfail_link.pid);
    seq_printf(sfile, " Signal #        = %d\n", sysfail_link.sig);
  }

  seq_printf(sfile, "\n BERR Descriptor: \n");
  seq_printf(sfile, " VMEbus address   = 0x%8x\n", berr_dsc.vmeadd);
  seq_printf(sfile, " address modifier = 0x%8x\n", berr_dsc.am);
  seq_printf(sfile, " IACK             = %d\n", berr_dsc.iack);
  seq_printf(sfile, " LWORD*           = %d\n", berr_dsc.lword);
  seq_printf(sfile, " DS0*             = %d\n", berr_dsc.ds0);
  seq_printf(sfile, " DS1*             = %d\n", berr_dsc.ds1);
  seq_printf(sfile, " WRITE*           = %d\n", berr_dsc.wr);
  seq_printf(sfile, " multiple bit     = 0x%8x\n", berr_dsc.multiple);
  seq_printf(sfile, " bus error flag   = 0x%8x\n", berr_dsc.flag);

  seq_printf(sfile, "\n Block transfer status\n");

  seq_printf(sfile, "dma_todo_total    = %d\n", dma_todo_total);
  seq_printf(sfile, "dma_done_total    = %d\n", dma_done_total);
  seq_printf(sfile, "dmas_pending      = %d\n", dmas_pending);

  seq_printf(sfile, "        PCI address |      pid\n");
  for (loop = 0; loop < VME_DMATODOSIZE; loop++)
  {
    seq_printf(sfile, " 0x%016lx |", dma_list_todo[loop].paddr);
    seq_printf(sfile, " %8d \n", dma_list_todo[loop].pid);
  }

  seq_printf(sfile, "        PCI address |   DMA ctrl | DMA counter |      pid |in_use\n");
  for (loop = 0; loop < VME_DMATODOSIZE; loop++)
  {
    seq_printf(sfile, " 0x%016lx |", dma_list_done[loop].paddr);
    seq_printf(sfile, " 0x%08x |", dma_list_done[loop].ctrl);
    seq_printf(sfile, "  0x%08x |", dma_list_done[loop].counter);
    seq_printf(sfile, " %8d |", dma_list_done[loop].pid);
    seq_printf(sfile, "     %d \n", dma_list_done[loop].in_use);
  }

  seq_printf(sfile, " \n");
  seq_printf(sfile, "The command 'echo <action> > /proc/vme_rcc', executed as root,\n");
  seq_printf(sfile, "allows you to interact with the driver. Possible actions are:\n");
  seq_printf(sfile, "debug   -> Enable debugging\n");
  seq_printf(sfile, "nodebug -> Disable debugging\n");
  seq_printf(sfile, "elog    -> Log errors to /var/log/messages\n");
  seq_printf(sfile, "noelog  -> Do not log errors to /var/log/messages\n");
  
  return(0);
}


/*****************************/
/* PCI driver functions      */
/*****************************/

/***************************************************************************/
static int vme_rcc_Probe(struct pci_dev *dev, const struct pci_device_id *id)   
/***************************************************************************/
{
  u_char pci_revision;
  u_int ret, size, pci_ioaddr = 0;
  u_long flags;
    
  kdebug(("vme_rcc(vme_rcc_Probe): called for device: 0x%04x / 0x%04x\n", dev->vendor, dev->device));
  kdebug(("vme_rcc(vme_rcc_Probe): Bus: 0x%04x / Devfn 0x%04x\n", dev->bus->number, dev->devfn));

  if (maxcard == 0)
  {
    kdebug(("vme_rcc(vme_rcc_Probe): First (and hopefully only) Universe found\n"));
    universedev = dev;
    maxcard = 1;
  }
  else
  {
    kdebug(("vme_rcc(vme_rcc_Probe): Another Universe found (Parallel Universes are not supported)\n"));
    return(-VME_EIO);
  }

  // get revision directly from the Tundra
  pci_read_config_byte(universedev, PCI_REVISION_ID, &pci_revision);
  kdebug(("vme_rcc(vme_rcc_Probe): revision = %x\n", pci_revision))
  if (pci_revision < 1)
  {
    kerror(("vme_rcc(vme_rcc_Probe): Illegal Universe Revision %d\n", pci_revision))
    return(-VME_ILLREV);
  }
  
  flags = pci_resource_flags(dev, BAR0);  //MJ: See p317
  if ((flags & IORESOURCE_MEM) != 0) 
  {
    pci_ioaddr = pci_resource_start(dev, BAR0) & PCI_BASE_ADDRESS_MEM_MASK;        //MJ: See p317
    size = pci_resource_end(dev, BAR0) - pci_resource_start(dev, BAR0);                             //MJ: See p317
    kdebug(("vme_rcc(vme_rcc_Probe): Mapping %d bytes at PCI address 0x%08x\n", size, pci_ioaddr));
    uni = (universe_regs_t *)ioremap(pci_ioaddr, size);
    if (uni == NULL)
    {
      kerror(("vme_rcc(vme_rcc_Probe): error from ioremap\n"));
      return(-VME_EFAULT);
    }
    kdebug(("vme_rcc(vme_rcc_Probe): uni is at %p\n", uni));
  }
  else 
  {
    kerror(("vme_rcc(vme_rcc_Probe): Bar 0 is not a memory resource"));
    return(-EIO);
  }

  //Enable the device (See p314) and get the interrupt line
  ret = pci_enable_device(dev);
  vme_irq_line = dev->irq;
  kdebug(("vme_rcc(vme_rcc_Probe): interrupt line = %d \n", vme_irq_line))

  if (request_irq(vme_irq_line, universe_irq_handler, IRQF_SHARED, "vme_rcc", universe_irq_handler))	
  {
    kerror(("vme_rcc(vme_rcc_Probe): request_irq failed on IRQ = %d\n", vme_irq_line))
    return(-VME_REQIRQ);
  }
  kdebug(("vme_rcc(vme_rcc_Probe): request_irq OK\n"))

  // Clear and enable the DMA interrupt
  uni->lint_stat |= 0x100;
  uni->lint_en |= 0x100;

  // Read the static master and slave mappings once
  fill_mstmap(uni->lsi0_ctl, uni->lsi0_bs, uni->lsi0_bd, uni->lsi0_to, &stmap.master[0]);
  fill_mstmap(uni->lsi1_ctl, uni->lsi1_bs, uni->lsi1_bd, uni->lsi1_to, &stmap.master[1]);
  fill_mstmap(uni->lsi2_ctl, uni->lsi2_bs, uni->lsi2_bd, uni->lsi2_to, &stmap.master[2]);
  fill_mstmap(uni->lsi3_ctl, uni->lsi3_bs, uni->lsi3_bd, uni->lsi3_to, &stmap.master[3]);
  fill_mstmap(uni->lsi4_ctl, uni->lsi4_bs, uni->lsi4_bd, uni->lsi4_to, &stmap.master[4]);
  fill_mstmap(uni->lsi5_ctl, uni->lsi5_bs, uni->lsi5_bd, uni->lsi5_to, &stmap.master[5]);
  fill_mstmap(uni->lsi6_ctl, uni->lsi6_bs, uni->lsi6_bd, uni->lsi6_to, &stmap.master[6]);
  fill_mstmap(uni->lsi7_ctl, uni->lsi7_bs, uni->lsi7_bd, uni->lsi7_to, &stmap.master[7]);

  fill_slvmap(uni->vsi0_ctl, uni->vsi0_bs, uni->vsi0_bd, uni->vsi0_to, &stmap.slave[0]);
  fill_slvmap(uni->vsi1_ctl, uni->vsi1_bs, uni->vsi1_bd, uni->vsi1_to, &stmap.slave[1]);
  fill_slvmap(uni->vsi2_ctl, uni->vsi2_bs, uni->vsi2_bd, uni->vsi2_to, &stmap.slave[2]);
  fill_slvmap(uni->vsi3_ctl, uni->vsi3_bs, uni->vsi3_bd, uni->vsi3_to, &stmap.slave[3]);
  fill_slvmap(uni->vsi4_ctl, uni->vsi4_bs, uni->vsi4_bd, uni->vsi4_to, &stmap.slave[4]);
  fill_slvmap(uni->vsi5_ctl, uni->vsi5_bs, uni->vsi5_bd, uni->vsi5_to, &stmap.slave[5]);
  fill_slvmap(uni->vsi6_ctl, uni->vsi6_bs, uni->vsi6_bd, uni->vsi6_to, &stmap.slave[6]);
  fill_slvmap(uni->vsi7_ctl, uni->vsi7_bs, uni->vsi7_bd, uni->vsi7_to, &stmap.slave[7]);       

#if defined (VMEDEBUG) || defined (INTDEBUG)
  marker = (u_int *)ioremap(0x88008000, 1024);
  //*marker=1;
#endif

  return(0);
}


/*********************************************/
static void vme_rcc_Remove(struct pci_dev *dev) 
/*********************************************/
{
  u_int i, reg;

  for (i = 0; i < 7; i++)				// restore initial enable bits from vmetab 
  {
    kdebug(("vme_rcc(vme_rcc_Remove): irq_level_table[%d] =0x%x\n", i, irq_level_table[i]))
    if (irq_level_table[i] != VME_LEVELISDISABLED)
    {
      reg = readl((char*)uni + LINT_EN);	// read enabled irq bits
      kdebug(("vme_rcc(vme_rcc_Remove): reg =0x%x\n", reg))
      reg |= (1<<(i+1)); 			// BM_LINT_VIRQxxx
      kdebug(("vme_rcc(vme_rcc_Remove): reg =0x%x\n", reg))
      writel(reg, (char*)uni + LINT_EN);
    }
  }

  kdebug(("vme_rcc(vme_rcc_Remove): irqsysfail = 0x%x\n", irq_sysfail))
  if (irq_sysfail != VME_LEVELISDISABLED)
  {
    reg = readl((char*)uni + LINT_EN);	// read enabled irq bits
    kdebug(("vme_rcc(vme_rcc_Remove): reg =0x%x\n", reg))
    reg |= 0x4000; 			// SYSFAIL
    kdebug(("vme_rcc(vme_rcc_Remove): reg =0x%x\n", reg))
    writel(reg, (char*)uni + LINT_EN);
  }
  
  free_irq(vme_irq_line, universe_irq_handler);	
  kdebug(("vme_rcc(vme_rcc_Remove): interrupt line %d released\n", vme_irq_line))
 
  iounmap((void *)uni);
  kerror(("vme_rcc(vme_rcc_Remove): Universe removed\n"));
  maxcard = 0;
}


/*********************/
/* Service functions */
/*********************/


// Interrupt timeout function. Called when an interrupt fails to occur following a call to VME_InterruptWait()
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,0,0)
/************************************/
static void vme_intTimeout(u_long arg)
/************************************/
{
  VME_WaitInt_t *iPtr;
  int first_vct;

  iPtr = (VME_WaitInt_t *) arg;
  iPtr->timeout = 0;                            // zero to indicate timeout has occurred
  first_vct = iPtr->int_handle.vector[0];
  kdebug(("vme_rcc(vme_intTimeout): first vector = 0x%x\n", first_vct))
  up(&link_table.link_dsc[first_vct].sem);	// wake up WAIT
}
#else
/***************************************/
void vme_intTimeout(struct timer_list *t)
/***************************************/
{
  //VME_WaitInt_t *iPtr;
  int first_vct = 0;


  //MJ: Not yet clear how to port this to CC8
  //iPtr = (VME_WaitInt_t *) arg;
  //iPtr->timeout = 0;                            // zero to indicate timeout has occurred
  //first_vct = iPtr->int_handle.vector[0];
  //kdebug(("vme_rcc(vme_intTimeout): first vector = 0x%x\n", first_vct))
  up(&link_table.link_dsc[first_vct].sem);	// wake up WAIT
}
#endif

// Interrupt timeout function. Called when an interrupt fails to occur following
// a call to VME_SysfailInterruptWait()
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,0,0)
  /*******************************************/
  static void vme_intSysfailTimeout(u_long arg)
  /*******************************************/
  {
    VME_WaitInt_t *iPtr;
  
    iPtr = (VME_WaitInt_t *) arg;
    kdebug(("vme_rcc(vme_intSysfailTimeout): timeout = %d\n", iPtr->timeout))
    iPtr->timeout = 0;                            // zero to indicate timeout has occurred
    up(&sysfail_link.sem);			  // wake up WAIT
  }
#else
  /**********************************************/
  void vme_intSysfailTimeout(struct timer_list *t)
  /**********************************************/
  {
      //VME_WaitInt_t *iPtr = from_timer(iPtr, t, int_timer);  //MJ CC8: Does this make sense???
      kdebug(("vme_rcc(vme_intSysfailTimeout): timeout\n"))
      up(&sysfail_link.sem);			  // wake up WAIT
  }
#endif


/*******************************/
static void vme_dma_handler(void)
/*******************************/
{  
  u_int uivalue;
  u_int loop, windex = 0, loop_dgcs;
  
  dma_done_total++;

  uivalue = uni->dgcs;
  if (!(uivalue & 0xf00))
  {
    kdebug(("vme_rcc(vme_dma_handler)(%d): ERROR: uivalue IS NULL. DGCS = 0x%08x\n", current->pid, uivalue))
    
    for(loop_dgcs = 0; loop_dgcs < 1000; loop_dgcs++)
    {
      uivalue = uni->dgcs;
      if(uivalue & 0xf00)
      {
        kdebug(("vme_rcc(vme_dma_handler)(%d): No ERROR: uivalue NOW IS NOT NULL. DGCS = 0x%08x, loop=%d\n", current->pid, uivalue, loop_dgcs))
        break;    
      }
    }
  
    uivalue = uni->dgcs;
    if (!(uivalue & 0xf00))
    {
      kerror(("vme_rcc(vme_dma_handler)(%d): ERROR: uivalue IS STILL NULL. DGCS = 0x%08x\n", current->pid, uivalue))    
      debug = 1;     
      kdebug(("vme_rcc(vme_dma_handler)(%d): auto-enabling debugging\n", current->pid))
    }
  }
  
  //A DMA has completed. Store the result in the dma_list_done
  spin_lock_irqsave(&dmalock, dma_irq_flags); 
  for(loop = 0; loop < VME_DMATODOSIZE; loop++)
  {
    if (dma_list_done[loop].in_use == 0)
    {
      windex = loop;
      dma_list_done[loop].in_use = 1;
      kdebug(("vme_rcc(vme_dma_handler)(%d): Empty element found at index %d\n", current->pid, windex))   
      break; 
    }
  }
  
  if(windex == VME_DMATODOSIZE)
  {
    kerror(("vme_rcc(vme_dma_handler)(%d): no element free in the array. This must never happen\n", current->pid))    
    spin_unlock_irqrestore(&dmalock, dma_irq_flags); 
    return;
  }
  
  dma_list_done[windex].ctrl    = uivalue & 0xf00;
  dma_list_done[windex].counter = uni->dtbc;
  dma_list_done[windex].paddr   = dma_current_pciaddr;
  dma_list_done[windex].pid     = dma_current_pid;
  uni->dgcs = 0x6f00;  //Clear the IRQ flags
  
  uni->lint_stat |= 0x100;      // clear the DMA interrupt
  dma_is_free = 1;

  spin_unlock_irqrestore(&dmalock, dma_irq_flags); 

  wake_up_interruptible(&dma_wait);
  start_next_dma(1);  //Check if another DMA is pending

  kdebug(("vme_rcc(vme_dma_handler)(%d): done\n", current->pid))
}


/*****************************/
static void init_cct_berr(void)
/*****************************/
{
  u_char reg;

  reg = inb(BERR_INT_PORT);
  kdebug(("vme_rcc(init_cct_berr): CCT BERR register = 0x%x\n", reg))

  reg &= ~BERR_INT_MASK;      //Clear the error flag 
  outb(reg, BERR_INT_PORT);
  reg |= BERR_INT_ENABLE;     // Enable the BERR interrupt
  outb(reg, BERR_INT_PORT);
  
  berr_interlock = 0;         // For cards with shared interrupts such as the VP325
  kdebug(("vme_rcc(init_cct_berr): CCT BERR register = 0x%x\n", reg))
}


/*****************************/
static void mask_cct_berr(void)
/*****************************/
{
  u_char reg;

  berr_interlock = 1;   // For cards with shared interrupts such as the VP325
  reg = inb(BERR_INT_PORT);
  reg &= ~BERR_INT_ENABLE;
  outb(reg, BERR_INT_PORT);

  kdebug(("vme_rcc(mask_cct_berr): CCT BERR register = 0x%x\n", reg))
}


/* VME bus error interrupt function for boards which have the hardware support.
*  e.g VP PMC/C1x and VP CPI/Pxx boards
*
*  If there is a BERR interrupt: 
*  If the BERR descriptor is empty, fill it and set the count = 1, else just count++
*  re-enable the latching of the BERR registers in the Universe for evry interrupt.*/
/**************************/
static int cct_berrInt(void)
/**************************/
{
  u_char reg;
  u_int data, loop, windex = 0;

  reg = inb(BERR_INT_PORT);
  kdebug(("vme_rcc(cct_berrInt): CCT BERR register = 0x%x\n", reg))

  if ((uni->dgcs & 0x700))                      // was there a DMA BERR
  {
    kerror(("vme_rcc(cct_berrInt): DMA BERR detected\n"))
    reg &= ~BERR_INT_MASK;		        // clear VME bus error
    outb(reg, BERR_INT_PORT);

    //error: reset the DMA controller
    kdebug(("vme_rcc(cct_berrInt): Dumping DMA registers before reset\n"))
    kdebug(("vme_rcc(cct_berrInt): uni->dctl = 0x%08x\n", uni->dctl))
    kdebug(("vme_rcc(cct_berrInt): uni->dtbc = 0x%08x\n", uni->dtbc))
    kdebug(("vme_rcc(cct_berrInt): uni->dla  = 0x%08x\n", uni->dla))
    kdebug(("vme_rcc(cct_berrInt): uni->dva  = 0x%08x\n", uni->dva))
    kdebug(("vme_rcc(cct_berrInt): uni->dcpp = 0x%08x\n", uni->dcpp))
    kdebug(("vme_rcc(cct_berrInt): uni->dgcs = 0x%08x\n", uni->dgcs))

    //Store the result in the dma_list_done
    spin_lock_irqsave(&dmalock, dma_irq_flags); 
    for(loop = 0; loop < VME_DMATODOSIZE; loop++)
    {
      if (dma_list_done[loop].in_use == 0)
      {
	windex = loop;
	dma_list_done[loop].in_use = 1;
	kdebug(("vme_rcc(cct_berrInt)(%d): Empty element found at index %d\n", current->pid, windex))   
	break; 
      }
    }

    if(windex == VME_DMATODOSIZE)
    {
      kerror(("vme_rcc(cct_berrInt)(%d): no element free in the array. This must never happen\n", current->pid))    
      spin_unlock_irqrestore(&dmalock, dma_irq_flags); 
      return(1);
    }

    dma_list_done[windex].ctrl    = uni->dgcs & 0xf00;
    dma_list_done[windex].counter = uni->dtbc;
    dma_list_done[windex].paddr   = dma_current_pciaddr;
    dma_list_done[windex].pid     = dma_current_pid;
    
    uni->dctl=0;
    uni->dtbc=0;
    uni->dla =0;
    uni->dva =0;
    uni->dcpp=0;
    uni->dgcs=0x6f00;
    
    dma_is_free = 1;
    spin_unlock_irqrestore(&dmalock, dma_irq_flags); 
 
    if (berr_dsc.flag == 0)       // copy info from Universe into BERR descriptor
    { 
      if (board_type == VP_100 || board_type == VP_110 || board_type == VP_315 || board_type == VP_317 || board_type == VP_327 || board_type == VP_417)
        read_berr_capture();
      else
      {
        berr_dsc.vmeadd = 0xffffffff;
        berr_dsc.am = 0xffffffff;
        berr_dsc.iack = 0xffffffff;
        berr_dsc.multiple = 0xffffffff;
        berr_dsc.lword = 0xffffffff;
        berr_dsc.ds0 = 0xffffffff;
        berr_dsc.ds1 = 0xffffffff;
        berr_dsc.wr = 0xffffffff;
      }
      // log the error
      kerror(("vme_rcc(cct_berrInt): bus error on VMEbus block transfer, current pid = %d\n", current->pid))
      berr_dsc.flag = 1;		                // reset by BusErrorInfoGet or RegisterSignal
    }
    
    wake_up_interruptible(&dma_wait);
    start_next_dma(1);  //Check if another DMA is pending
    
    kdebug(("vme_rcc(cct_berrInt): done\n"))
    return(1);
  }
  
  if ((reg & BERR_INT_MASK))		        // if VME bus error has occurred
  {
    data = readl((char*)uni + PCI_CSR);		// error with coupled cycle ?
    kdebug(("vme_rcc(cct_berrInt): PCI_CSR = 0x%x\n", data))
    if (data & 0x08000000)			// check S_TA bit
    {
      kerror(("vme_rcc(cct_berrInt): bus error on coupled VMEbus cycle, current pid = %d\n", current->pid))
      if (berr_dsc.flag == 0)		        // copy info from Universe into BERR descriptor
      {
        if (board_type == VP_100 || board_type == VP_110 || board_type == VP_315 || board_type == VP_317 || board_type == VP_325 || board_type == VP_327 || board_type == VP_417)
          read_berr_capture();
        else
        {
          berr_dsc.vmeadd = 0xffffffff;
          berr_dsc.am = 0xffffffff;
          berr_dsc.iack = 0xffffffff;
          berr_dsc.multiple = 0xffffffff;
          berr_dsc.lword = 0xffffffff;
          berr_dsc.ds0 = 0xffffffff;
          berr_dsc.ds1 = 0xffffffff;
          berr_dsc.wr = 0xffffffff;
        }
        // log the error
        // send signals, if anybody registered
        send_berr_signals();
        berr_dsc.flag = 1;		                // reset by BusErrorInfoGet or RegisterSignal
      }
      writel(data | 0x08000000, (char*)uni + PCI_CSR);	// unlock error in Universe
    }

    data = readl((char*)uni + V_AMERR);	           	// error with posted cycle ?
    kdebug(("vme_rcc(cct_berrInt): V_AMERR = 0x%x\n", data))
    if (data & 0x00800000)			        // V_STAT
    {
      kerror(("vme_rcc(cct_berrInt): bus error on posted VMEbus cycle, current pid = %d\n", current->pid))
      if (berr_dsc.flag == 0)		                // copy info from Universe into BERR descriptor
      {
        if (board_type == VP_100 || board_type == VP_110 || board_type == VP_315 || board_type == VP_317 || board_type == VP_325 || board_type == VP_327 || board_type == VP_417)
          read_berr_capture();
        else
        {
          berr_dsc.am = (data >> 26) & 0x3f;
          berr_dsc.multiple = (data >> 24) & 0x1;
          berr_dsc.vmeadd = readl((char*)uni + VAERR);
          berr_dsc.iack = (data >> 25) & 0x1;
          berr_dsc.lword = 0xffffffff;
          berr_dsc.ds0 = 0xffffffff;
          berr_dsc.ds1 = 0xffffffff;
          berr_dsc.wr = 0xffffffff;
        }
        kdebug(("vme_rcc(cct_berrInt): VAERR = 0x%x\n", berr_dsc.vmeadd))

        // log the error
        kerror(("vme_rcc(cct_berrInt): VMEbus error: address = 0x%8x, AM code = 0x%8x\n", berr_dsc.vmeadd, berr_dsc.am))
        // send signals, if anybody registered
        send_berr_signals();
        berr_dsc.flag = 1;	// reset by BusErrorInfoGet or RegisterSignal
      }
      writel(0x00800000, (char*)uni + V_AMERR);	// unlock error
    }
    
    reg &= ~BERR_INT_MASK;		        // clear VME bus error
    outb(reg, BERR_INT_PORT);
    berr_int_count++;
    return(1);
  }
  return(0);
}


/***********************************/
static void sysfail_irq_handler(void)
/***********************************/
{
  u_int lint_en;

#if LINUX_VERSION_CODE < KERNEL_VERSION(4,0,0)
  struct siginfo info;
#else
  struct kernel_siginfo info;
#endif

  struct task_struct *task;  
  
  // The SYSFAIL interupt is active as long as the SYSFAIL signal is asserted on the bus
  // In order to prevent a deadlock we disable the interrupt here and provide an ioctl() to re-enable it
  lint_en = readl((char*)uni + LINT_EN);		// read enabled irq bits
  kdebug(("vme_rcc(sysfail_irq_handler): lint_en = 0x%x\n", lint_en))
  lint_en &= 0xffffbfff;				
  kdebug(("vme_rcc(sysfail_irq_handler): lint_en after masking = 0x%x\n", lint_en))
  writel(lint_en, (char*)uni + LINT_EN);	 	//disable SYSFAIL

  if (sysfail_link.pid == 0)	// not linked: "spurious" interrupt
  {
    kerror(("vme_rcc(sysfail_irq_handler): SYSFAIL interrupt not linked\n"))
    kerror(("vme_rcc(sysfail_irq_handler): SYSFAIL interrupt disabled\n"))
  }
  else
  {
    if (sysfail_link.sig > 0)
    {
      kdebug(("vme_rcc(sysfail_irq_handler): pid = %d, signal = %d\n", sysfail_link.pid, sysfail_link.sig))
      
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,0,0)
  memset(&info, 0, sizeof(struct siginfo));          // Generate the struct for signals
#else
  memset(&info, 0, sizeof(struct kernel_siginfo));   // Generate the struct for signals
#endif   
      
      info.si_signo = sysfail_link.sig;           // Config the signals
      info.si_code = SI_QUEUE;                    // Config SI_QUERE std or SI_KERNEL for RTC
      task = pid_task(find_vpid(sysfail_link.pid), PIDTYPE_PID); 
      if(task == NULL)
        kerror(("vme_rcc(sysfail_irq_handler): Error finding the pid task\n"));
      send_sig_info(sysfail_link.sig, &info, task);
    }
    else
      up(&sysfail_link.sem);
  } 
}


/************************************/
static void vme_irq_handler(int level)
/************************************/
{
  int statid, vector, first_vector;
  u_int lint_en;
  VME_IntHandle_t* group_ptr;
  
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,14,0)
  struct siginfo info;
#else
  struct kernel_siginfo info;
#endif 
  
  struct task_struct *task;

#ifdef VMEDEBUG 
    *marker=((current->pid) << 8) | 0x40; 
#endif

  Interrupt_counters.virq[level - 1]++;
  statid = readl((char*)uni + STATID + 4 * level);

  if (statid & 0x00000100) // bus error during IACK cycle
    kerror(("vme_rcc(vme_irq_handler): bus error during IACK cycle on level %d\n", level))
  else
  {
    vector = statid & 0x000000FF;
    kdebug(("vme_rcc(vme_irq_handler): vector = 0x%x\n",vector))

    if (link_table.link_dsc[vector].pid == 0)	// not linked: "spurious" interrupt
      kerror(("vme_rcc(vme_irq_handler): interrupt with vector %d not linked\n", vector))
    else		// handle it
    {
      if (irq_level_table[level-1] == VME_INT_RORA)
      {
        lint_en = readl((char*)uni + LINT_EN);		// read enabled irq bits
        kdebug(("vme_rcc(vme_irq_handler): level = %d, lint_en = 0x%x\n", level, lint_en))
        lint_en &= ~(1<<level);				// BM_LINT_VIRQxxx
        kdebug(("vme_rcc(vme_irq_handler): lint_en after masking = 0x%x\n", lint_en))
        writel(lint_en, (char*)uni + LINT_EN);	 	//disable  SHOULD BE MARKED??
      }

      link_table.link_dsc[vector].lvl = level;	// store in link descriptor
      link_table.link_dsc[vector].pending++;
      link_table.link_dsc[vector].total_count++;

      // signal FIRST(common) semaphore in a group
      // for signals: use signal in the link descriptor for "vector"
      group_ptr = link_table.link_dsc[vector].group_ptr;
      if (group_ptr == NULL)
        first_vector = vector;
      else 
        first_vector = group_ptr->vector[0];
      kdebug(("vme_rcc(vme_irq_handler): first vector = 0x%x\n",first_vector))
      
      if (link_table.link_dsc[vector].sig > 0)
      {
        kdebug(("vme_rcc(vme_irq_handler): pid = %d, signal = %d\n", link_table.link_dsc[vector].pid, link_table.link_dsc[vector].sig))

#if LINUX_VERSION_CODE < KERNEL_VERSION(4,0,0)
  memset(&info, 0, sizeof(struct siginfo));                  // Generate the struct for signals
#else
  memset(&info, 0, sizeof(struct kernel_siginfo));                  // Generate the struct for signals
#endif	
	
	info.si_signo = link_table.link_dsc[vector].sig;           // Config the signals
	info.si_code = SI_QUEUE;                                   // Config SI_QUERE std or SI_KERNEL for RTC
        task = pid_task(find_vpid(link_table.link_dsc[vector].pid), PIDTYPE_PID); 
	if(task == NULL)
          kerror(("vme_rcc(vme_irq_handler): Error finding the pid task\n"));
	send_sig_info(link_table.link_dsc[vector].sig, &info, task);

      }
      else
      {
        up(&link_table.link_dsc[first_vector].sem);
        #ifdef INTDEBUG
          *marker=((current->pid) << 8) | 0x2;
          *marker=first_vector;
        #endif
      }
    } 
  }
  #ifdef VMEDEBUG 
    *marker=((current->pid) << 8) | 0x4f; 
  #endif
}


// This function handles Universe Interrupts & CCT direct BUS error interrupts.
// The latter are handled first and if found, the function returns.
/*************************************************************/
static irqreturn_t universe_irq_handler (int irq, void *dev_id)
/*************************************************************/
{
  u_int lint_stat, lint_estat, lint_en;
  int result;

  lint_stat = readl((char*)uni + LINT_STAT);	// read the interrupt bits
  lint_en = readl((char*)uni + LINT_EN);	// read enabled irq bits
  lint_estat = lint_stat & lint_en;		// and look only at enabled irqs
  kdebug(("vme_rcc(universe_irq_handler): lint_estat = 0x%x, berr_interlock = %d\n", lint_estat, berr_interlock))

  if (!berr_interlock)	        // first check and handle CCT VMEbus errors
  {
    kdebug(("vme_rcc(universe_irq_handler): handling CCT VMEbus\n"))
    result = cct_berrInt();
    kdebug(("vme_rcc(universe_irq_handler): CCT VMEbus error handled. result = %d\n", result))
    if (result)
    {
      kdebug(("vme_rcc(universe_irq_handler): CCT VMEbus error handled and cleared.\n"))
      writel(BM_LINT_DMA, (char*)uni + LINT_STAT);  // clear only the DMA interrupt
      return(IRQ_HANDLED);                          // MJ: to be checked
    }
  }

  kdebug(("vme_rcc(universe_irq_handler): looking for next interrupt\n"))
  
  //handle all the interrupts sequentially
  if (lint_estat & BM_LINT_VIRQ7) vme_irq_handler(7);
  if (lint_estat & BM_LINT_VIRQ6) vme_irq_handler(6);
  if (lint_estat & BM_LINT_VIRQ5) vme_irq_handler(5);
  if (lint_estat & BM_LINT_VIRQ4) vme_irq_handler(4);
  if (lint_estat & BM_LINT_VIRQ3) vme_irq_handler(3);
  if (lint_estat & BM_LINT_VIRQ2) vme_irq_handler(2);
  if (lint_estat & BM_LINT_VIRQ1) vme_irq_handler(1);
  
  if (lint_estat & BM_LINT_SYSFAIL)
  {
    Interrupt_counters.sysfail++;    
    sysfail_irq_handler();
  }
  
  // ERROR CHECKING
  if (lint_estat & BM_LINT_VOWN)    Interrupt_counters.vown++;
  if (lint_estat & BM_LINT_ACFAIL)  Interrupt_counters.acfail++;
  if (lint_estat & BM_LINT_SW_INT)  Interrupt_counters.sw_int++;
  if (lint_estat & BM_LINT_SW_IACK) Interrupt_counters.sw_iack++;
  if (lint_estat & BM_LINT_VERR)    Interrupt_counters.verr++;
  if (lint_estat & BM_LINT_LERR)    Interrupt_counters.lerr++;
  if (lint_estat & BM_LINT_DMA)
  {
    Interrupt_counters.dma++;
    kdebug(("vme_rcc(universe_irq_handler): handling DMA EOT interrupt\n"))
    vme_dma_handler();
  }
  
  // clear only the enabled bits  //MJ: make sure not to clear the DMA IRQ again
  lint_estat &= ~0x100;
  kdebug(("vme_rcc(universe_irq_handler): new lint_estat = 0x%x\n", lint_estat))

  writel(lint_estat, (char*)uni + LINT_STAT);     

  return(IRQ_HANDLED);   //MJ: for 2.6 see p 273   // MJ: we also have to treat IRQ_NONE
}


/*************************************************************************************/
static void fill_mstmap(u_int d1, u_int d2, u_int d3, u_int d4, mstslvmap_t *mstslvmap)
/*************************************************************************************/
{
  if (d3 == 0) 
    d3 = 0xffffffff;
  d2 = d2 & 0xfffff000;
  d4 = d4 & 0xfffff000;
  mstslvmap->vbase   = d2 + d4;
  mstslvmap->vtop    = d3 + d4;
  mstslvmap->pbase   = d2;
  mstslvmap->ptop    = d3;
  mstslvmap->am      = (d1 >> 16) & 0x7;   //0 = A16, 1 = A24, 2 = A32
  mstslvmap->options = d1 & 0x0000f000;
  mstslvmap->enab    = (d1 >> 31) & 0x1;
  mstslvmap->wp      = (d1 >> 30) & 0x1;
  mstslvmap->rp      = 0;
  kdebug(("vme_rcc(fill_mstmap): mstslvmap->vbase   = 0x%08x @  %p\n", (u_int)mstslvmap->vbase,  &mstslvmap->vbase))
  kdebug(("vme_rcc(fill_mstmap): mstslvmap->vtop    = 0x%08x @  %p\n", (u_int)mstslvmap->vtop,  &mstslvmap->vtop))
  kdebug(("vme_rcc(fill_mstmap): mstslvmap->am      = %d @  %p\n", mstslvmap->am,  &mstslvmap->am))
  kdebug(("vme_rcc(fill_mstmap): mstslvmap->options = %d @  %p\n", mstslvmap->options,  &mstslvmap->options))
  kdebug(("vme_rcc(fill_mstmap): mstslvmap->enab    = %d @  %p\n", mstslvmap->enab,  &mstslvmap->enab))
}


/*************************************************************************************/
static void fill_slvmap(u_int d1, u_int d2, u_int d3, u_int d4, mstslvmap_t *mstslvmap)
/*************************************************************************************/
{
  d2 = d2 & 0xfffff000;
  d4 = d4 & 0xfffff000;
  mstslvmap->vbase = d2;
  mstslvmap->vtop  = d3;
  mstslvmap->pbase = d2 + d4;
  mstslvmap->ptop  = d3 + d4;
  mstslvmap->space = d1 & 0x3;
  mstslvmap->am    = (d1 >> 16) & 0x7;   //0 = A16, 1 = A24, 2 = A32
  mstslvmap->enab  = (d1 >> 31) & 0x1;
  mstslvmap->wp    = (d1 >> 30) & 0x1;
  mstslvmap->rp    = (d1 >> 29) & 0x1;
}


/*********************************/
static void send_berr_signals(void)
/*********************************/
{
  u_int i;

#if LINUX_VERSION_CODE < KERNEL_VERSION(4,14,0)
  struct siginfo info;
#else
  struct kernel_siginfo info;
#endif

  struct task_struct *task;

  for (i = 0; i < VME_MAX_BERR_PROCS; i++)
  {
    if (berr_proc_table.berr_link_dsc[i].pid)
    {
      kdebug(("vme_rcc(send_berr_signals): index = %d, pid (current)= %d, signal = %d\n", i, berr_proc_table.berr_link_dsc[i].pid, berr_proc_table.berr_link_dsc[i].sig))
      
#if LINUX_VERSION_CODE < KERNEL_VERSION(4,14,0)
  memset(&info, 0, sizeof(struct siginfo));                       // Generate the struct for signals
#else
  memset(&info, 0, sizeof(struct kernel_siginfo));                // Generate the struct for signals
#endif
      
      info.si_signo = berr_proc_table.berr_link_dsc[i].sig;           // Config the signals
      info.si_code = SI_QUEUE;                                        // Config SI_QUERE std or SI_KERNEL for RTC
      task = pid_task(find_vpid(berr_proc_table.berr_link_dsc[i].pid), PIDTYPE_PID); // Find the task from pid
      if(task == NULL)
        kerror(("vme_rcci(send_berr_signals): Error finding the pid task\n"));
      send_sig_info(berr_proc_table.berr_link_dsc[i].sig, &info, task);     
    }
  }
}


/*********************************************************/
static int berr_check(u_int *addr, u_int *multi, u_int *am)
/*********************************************************/
{ 
  //This function is to look for single cycle related bus errors
  //It does not handle BERRs of DMAs or IACK cycles
  u_int data;

  kdebug(("vme_rcc(berr_check): called\n"))

  //lets see if it there was an error with a coupled cycle
  data = uni->pci_csr;                 //read the target abort status bit
  if (data & 0x08000000)               //check S_TA bit
  { 
    uni->pci_csr = data | 0x08000000;  //clear the bit
    *addr  = 0xffffffff;
    *multi = 0xffffffff;
    *am    = 0xffffffff;
    kdebug(("vme_rcc(berr_check): BERR on coupled cycle detected\n"))
    return(VME_BUSERROR);              //bus error
  }

  //lets see if it there was a VMEbus error with a posted cycle
  data = uni->v_amerr;
  if (data & 0x00800000)
  {
    *am    = (data >> 26) & 0x3f;
    *multi = (data >> 24) & 0x1;
    *addr  = uni->vaerr;
    uni->v_amerr = 0x00800000;
    kdebug(("vme_rcc(berr_check): BERR on coupled posted detected\n"))
    return(VME_BUSERROR);
  }
  else
  {
    kdebug(("vme_rcc(berr_check): No BERR detected\n"))
    return(0);                //no error
  }
}


/*********************************/
static void read_berr_capture(void)
/*********************************/
{  
  u_char reg, berr_info[16];
  u_int data, loop;
  
  reg = inb(BERR_VP100);
  if (reg & 0x80)
  {
  
    berr_dsc.vmeadd   = 0xffffffff;
    berr_dsc.am       = 0xffffffff;
    berr_dsc.iack     = 0xffffffff;
    berr_dsc.multiple = 0xffffffff;
    berr_dsc.lword    = 0xffffffff;
    berr_dsc.ds0      = 0xffffffff;
    berr_dsc.ds1      = 0xffffffff;
    berr_dsc.wr       = 0xffffffff;
    kerror(("vme_rcc(read_berr_capture): The BERR capture logic was busy. Information could not be read\n"))
    return;
  }
  
  outb(0x2, BERR_VP100);   // Reset the read sequence
  for(loop = 0; loop < 16; loop++)
    berr_info[loop] = inb(BERR_VP100) & 0xf;   // Read the raw data
  
  // Assemble the values
  berr_dsc.vmeadd = (berr_info[0] << 28) +
                    (berr_info[1] << 24) +
                    (berr_info[2] << 20) +
                    (berr_info[3] << 16) +
                    (berr_info[4] << 12) +
                    (berr_info[5] << 8) +
                    (berr_info[6] << 4) +
                    (berr_info[7] & 0xe);
  berr_dsc.am       = ((berr_info[8] & 0x3) << 4) + berr_info[9];
  berr_dsc.multiple = 0xffffffff;
  berr_dsc.lword    = berr_info[7] & 0x1;
  berr_dsc.ds0      = (berr_info[8] & 0x4) >> 2;
  berr_dsc.ds1      = (berr_info[8] & 0x8) >> 3;
  berr_dsc.wr       = (berr_info[10] & 0x8) >> 3;
  
  data = readl((char*)uni + V_AMERR);		// error with IACK cycle ?
  kdebug(("vme_rcc(read_berr_capture): V_AMERR = 0x%x\n", data))
  if (data & 0x00800000)			// V_STAT
    berr_dsc.iack = (data >> 25) & 0x1;
  else
    berr_dsc.iack = 0xffffffff;

  kdebug(("vme_rcc(read_berr_capture): berr_dsc.vmeadd = 0x%08x\n", berr_dsc.vmeadd))
  kdebug(("vme_rcc(read_berr_capture): berr_dsc.am     = 0x%02x\n", berr_dsc.am))
  kdebug(("vme_rcc(read_berr_capture): berr_dsc.iack   = 0x%02x\n", berr_dsc.iack))
  kdebug(("vme_rcc(read_berr_capture): berr_dsc.lword  = %d\n", berr_dsc.lword))
  kdebug(("vme_rcc(read_berr_capture): berr_dsc.ds0    = %d\n", berr_dsc.ds0))
  kdebug(("vme_rcc(read_berr_capture): berr_dsc.ds1    = %d\n", berr_dsc.ds1))
  kdebug(("vme_rcc(read_berr_capture): berr_dsc.write  = %d\n", berr_dsc.wr))

  // Reset the capture buffer
  outb(0x4, BERR_VP100);

  //Reenable the capture
  outb(0x1, BERR_VP100);
}


/**************************/
void start_next_dma(int src)
/**************************/
{
  static u_int rindex;
  volatile u_int regdata;

  kdebug(("vme_rcc(start_next_dma)(%d, %d): called \n", src, current->pid))
  
  spin_lock_irqsave(&dmalock, dma_irq_flags); 
  //Is the DMA conroller free?
  if (!dma_is_free)
  {
    kdebug(("vme_rcc(start_next_dma)(%d, %d): DMA controller is not free\n", src, current->pid))
    spin_unlock_irqrestore(&dmalock, dma_irq_flags); 
    return;
  }

  regdata = uni->dgcs;
  if (regdata & 0x8000)  //Check the "ACTIVE" bit
    kerror(("vme_rcc(start_next_dma)(%d, %d): DMA controller is busy. regdata = 0x%08x\n", src, current->pid, regdata))
    
  //Is a request DMA pending?
  if (dma_todo_num == 0)
  {
    kdebug(("vme_rcc(start_next_dma)(%d, %d): No transfers pending\n", src, current->pid))
    spin_unlock_irqrestore(&dmalock, dma_irq_flags); 
    return;
  }
  dma_todo_num--;
  rindex = dma_todo_read;
  dma_todo_read++;
  if (dma_todo_read == VME_DMATODOSIZE)
    dma_todo_read = 0;
  
  kdebug(("vme_rcc(start_next_dma)(%d, %d): Starting transfer with rindex = %d and paddr = 0x%016lx and pid = %d\n", src, current->pid, rindex, dma_list_todo[rindex].paddr, dma_list_todo[rindex].pid))
  
  uni->dgcs |= 0x00006f00;  //clear all status and error flags
  uni->dcpp = dma_list_todo[rindex].paddr;  
  dma_current_pciaddr = dma_list_todo[rindex].paddr;
  dma_current_pid     = dma_list_todo[rindex].pid;
  uni->dgcs = 0x8800006f;   //start chaind DMA, enable all termination interrupts
  dma_is_free = 0;
  
  spin_unlock_irqrestore(&dmalock, dma_irq_flags); 
}   


/***********************************************/
int dma_is_done(VME_DMAhandle_t *dma_poll_params)
/***********************************************/
{
  int loop, dma_ok = 0;
  
  kdebug(("vme_rcc(dma_is_done): called\n"))
  
  spin_lock_irqsave(&dmalock, dma_irq_flags); 

  for (loop = 0; loop < VME_DMATODOSIZE; loop++)
  {    
    kdebug(("vme_rcc(dma_is_done): paddr of loop(%d) is 0x%016lx. Looking for 0x%016lx\n", loop, dma_list_done[loop].paddr, dma_poll_params->paddr));
    if (dma_list_done[loop].paddr == dma_poll_params->paddr)  //Found it
    {
      kdebug(("vme_rcc(dma_is_done): bingo\n"));
      dma_poll_params->ctrl       = dma_list_done[loop].ctrl;
      dma_poll_params->counter    = dma_list_done[loop].counter;
      dma_list_done[loop].paddr   = 0;
      dma_list_done[loop].ctrl    = 0;
      dma_list_done[loop].counter = 0;
      dma_list_done[loop].in_use  = 0;
      dma_list_done[loop].pid     = 0;
      dmas_pending--;
      dma_ok = 1;
      break;
    }
  }

  spin_unlock_irqrestore(&dmalock, dma_irq_flags); 
   
  kdebug(("vme_rcc(dma_is_done)): done\n"))
  return(dma_ok);  
}


   
