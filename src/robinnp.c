
#include <linux/version.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/pci.h>
#include <linux/mm.h>
#include <linux/proc_fs.h>
#include <asm/uaccess.h>
#include <asm/current.h>
#include <linux/io.h>
#include <linux/semaphore.h>
#include <asm/io.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/cdev.h>
#include <linux/pagemap.h>
#include <linux/page-flags.h>
#include <linux/sched.h>
#include <linux/interrupt.h>
#include <linux/timekeeping.h>
#include <linux/delay.h>
#include <linux/seq_file.h>

#include "ROSRobinNP/RobinNP_common.h"
#include "ROSRobinNP/RobinNPRegisters.h"
#include "ROSRobinNP/RobinNPRegistersCommon.h"

//To-do list:
//
//- Does it make sense to replace the printk statement by kerror?


/***********/
/*Constants*/
/***********/
#define DESIGN_ID_NP            0x01060002   //MJ: Hard-coding the current revision numbers for now. Eventually add a set of constants to ROSRobinNP/RobinNP_driver.h
#define PCI_VENDOR_ID_RobinNP   0x10dc
#define PCI_DEVICE_ID_RobinNP   0x0187
#define PCIEx_VENDOR_ID_ROBIN3  0x10dc
#define PCIEx_DEVICE_ID_ROBIN3  0x0188
#define FIRSTMINOR              0
#define MAX_OSC_FREQ 300
#define DEFAULT_OSC_FREQ 212.5
#define SCALE1 1000000
#define SCALE2 2500000

/********/
/*Macros*/
/********/
#ifdef DRIVER_DEBUG
  #define kdebug(x) {if (debug) printk x;}
#else
  #define kdebug(x)
#endif

#ifdef DRIVER_ERROR
  #define kerror(x) {if (errorlog) printk x;}
#else
  #define kerror(x)
#endif

#define MAXMSI 8

struct irqInfo_struct 
{
  int interrupt;
  int card;
  int subrob;
};


/*********/
/*Globals*/
/*********/
char *devName = "robinnp";  //the device name as it will appear in /proc/devices
static int debug = 0, errorlog = 1;
int cardsFound = 0, interruptCount = 0, dmapagesize = 4096, ndmapages = MAXDMABUF, oscfreq = 100;
int msiblock = 8, irqFlag[MAXCARDS][MAXMSI] = {{0}}, msixEnabled[MAXCARDS];
unsigned int myIrq[MAXCARDS];
unsigned long irqCount[MAXCARDS][MAXMSI];

card_params_t cards[MAXCARDS];
struct cdev *test_cdev;
dev_t first_dev;
BAR0CommonStruct *registerBase[MAXCARDS];

/* uint32_t* msixBar[MAXCARDS], msixPbaOffset[MAXCARDS]; */
uint32_t msixOK[MAXCARDS];

static struct irqInfo_struct irqInfo[MAXCARDS][MAXMSI];

typedef struct {
   u_int n1;
   u_int hs_div;
   uint64_t rfreq_int;
   uint64_t fdco_int;
   uint64_t fxtal_int;
} refclk_intcfg_t;
refclk_intcfg_t clockConfig[MAXCARDS];
struct file* owner[MAXCARDS];

static DECLARE_WAIT_QUEUE_HEAD(waitQueue);

#if RHEL_RELEASE_CODE >= RHEL_RELEASE_VERSION(9,5)
  static DEFINE_SEMAPHORE(procMutex, 1);
  static DEFINE_SEMAPHORE(ownerMutex, 1);
#else
  static DEFINE_SEMAPHORE(procMutex);
  static DEFINE_SEMAPHORE(ownerMutex);
#endif


/************/
/*Prototypes*/
/************/
static int RobinNP_init(void);
static int RobinNP_Probe(struct pci_dev*, const struct pci_device_id*);
static int fpgaLoaded(u_int card, u_int *ver, u_int *min, u_int *maj);
static void RobinNP_exit(void);
static void RobinNP_Remove(struct pci_dev*);
int RobinNP_mmap(struct file*, struct vm_area_struct*);
//int RobinNP_ioctl(struct inode*, struct file*, u_int, u_long);
static long RobinNP_ioctl(struct file *file, u_int cmd, u_long arg);
int RobinNP_open(struct inode*, struct file*);
int RobinNP_Release(struct inode*, struct file*);
int proc_robinnp_open(struct inode *inode, struct file *file);
ssize_t proc_robinnp_write (struct file *, const char __user *, size_t, loff_t *);
int print_qsfp_text(struct seq_file*);
int check_slot_device(u_int);
void RobinNP_vmclose(struct vm_area_struct*);
void initialiseOscillator(BAR0CommonStruct *registers);
int i2cReadWrite(u_int chain, u_int slaveAddress, u_int memAddress, BAR0CommonStruct *registers, unsigned int readwriteSelect, u_int data);
int writeI2CConfig(u_int chainSelect, u_int slaveAddress, u_int command, u_int mode, u_int byteEnable, BAR0CommonStruct *registers);
int waitI2CCompletion(BAR0CommonStruct *registers);
void waitForClockClearance(uint8_t flag, BAR0CommonStruct *registers);
int getClockConfig(refclk_intcfg_t* cfg, BAR0CommonStruct *registers, long fout);
void computeClockConfig(refclk_intcfg_t* cfg, int newFrequency);
void applyClockConfig(refclk_intcfg_t* cfg, BAR0CommonStruct *registers);
int i2cDualWrite(u_int chain, u_int slaveAddress, u_int memAddress0, u_int memAddress1, BAR0CommonStruct *registers, u_int data0, u_int data1);

/************/
/*Structures*/
/************/
static struct pci_device_id RobinNP_IDs[] = 
{
		{ PCI_DEVICE(PCI_VENDOR_ID_RobinNP, PCI_DEVICE_ID_RobinNP) },
		{ PCI_DEVICE(PCIEx_VENDOR_ID_ROBIN3, PCIEx_DEVICE_ID_ROBIN3) },
		{ 0, },
};

struct file_operations fops =                          // needed to activate the device: cdev_add call, old, good file operations
{
		.owner          = THIS_MODULE,
		.mmap           = RobinNP_mmap,
		.unlocked_ioctl = RobinNP_ioctl,
		.open           = RobinNP_open,
		.release        = RobinNP_Release,
};

static struct pci_driver RobinNP_PCI_driver =          // needed by pci_register_driver fcall
{
		.name     = "robinnp",
		.id_table = RobinNP_IDs,
		.probe    = RobinNP_Probe,
		.remove   = RobinNP_Remove,
};

static struct vm_operations_struct RobinNP_vm_ops =    // memory handler functions used by mmap 
{
		.close =  RobinNP_vmclose,             // mmap-close
};

#if LINUX_VERSION_CODE<KERNEL_VERSION(5,6,0)
static struct file_operations robinnp_proc_file_ops = {
	.owner   = THIS_MODULE,
	.open    = proc_robinnp_open,
        .write   = proc_robinnp_write,
  	.read    = seq_read,
	.llseek  = seq_lseek,
	.release = single_release
};
#else
static struct proc_ops robinnp_proc_file_ops = {
   .proc_open    = proc_robinnp_open,
   .proc_write   = proc_robinnp_write,
   .proc_read    = seq_read,
   .proc_lseek   = seq_lseek,
   .proc_release = single_release
};
#endif


module_init(RobinNP_init);
module_exit(RobinNP_exit);

MODULE_DESCRIPTION("RobinNP driver");
MODULE_AUTHOR("Barry Green (RHUL), Gordon Crone (UCL), Markus Joos (CERN)");
MODULE_LICENSE("Dual BSD/GPL");
MODULE_DEVICE_TABLE(pci, RobinNP_IDs);

MODULE_PARM_DESC(dmapagesize, " Size of page to receive event data through the primary DMA");
module_param(dmapagesize,int,S_IRUGO);

MODULE_PARM_DESC(ndmapages, " Number of pages per card for receiving primary DMA data");
module_param(ndmapages,int,S_IRUGO);

MODULE_PARM_DESC(msiblock, " size of MSI block to enable");
module_param(msiblock,int,S_IRUGO);

MODULE_PARM_DESC(oscfreq, " frequency of s-link oscillator");
module_param(oscfreq,int,S_IRUGO);

MODULE_PARM_DESC(debug, "1 = enable debugging   0 = disable debugging"); 
module_param (debug, int, S_IRUGO | S_IWUSR);        //MJ: for 2.6 p37

MODULE_PARM_DESC(errorlog, "1 = enable error logging   0 = disable error logging"); 
module_param (errorlog, int, S_IRUGO | S_IWUSR);     //MJ: for 2.6 p37


struct msix_entry msixTable[MAXCARDS][MAXMSI];


void print_memorySPD_text(struct seq_file* sfile) {
   int cardIndex[MAXCARDS];
   int index;
   int card;
   int bank;
   unsigned int byte;
   unsigned int slaveAddr;
   BAR0CommonStruct *registers;
   int wordRead;
   char buffer[20];
   unsigned int len;
   unsigned int serNum;
   unsigned short vendor;
   int mfrYear;
   int mfrWeek;
   unsigned int gigaBytes;
   unsigned int ranks;

   index = 0;
   for (card = 0; card < cardsFound; card++) {
      while (cards[index].pciDevice == NULL) {
         index++;
      }
      if (index<MAXCARDS) {
         cardIndex[card]=index;
         index++;
         kdebug(("robinnp(print_memorySPD_text): card %d has index %d\n",
                 card,index));
      }
      else {
         kerror(("robinnp(print_memorySPD_text): Card indexing error\n"));
         return;
      }
   }

   seq_printf(sfile,
              "\nMemory modules\n Card | Module | Part number        | Vendor | Serial # | Manufactured | Capacity |\n");
   for (card = 0; card < cardsFound; card++) {
      registers = registerBase[cardIndex[card]];
      for (bank=0;bank<2;bank++) {
         slaveAddr=0x50+bank;

         wordRead = i2cReadWrite(I2C_CHAIN_DDR, slaveAddr, 7,
                                registers, I2C_READ, 0);
         ranks=((wordRead>>3)&0x07)+1;
         wordRead = i2cReadWrite(I2C_CHAIN_DDR, slaveAddr, 4,
                                registers, I2C_READ, 0);
         gigaBytes=ranks*(1<<((wordRead&0x0f)-2))*(1<<(wordRead>>4));

         len=0;
         for(byte = 128; byte < 146; ++byte) {
            buffer[len++] = i2cReadWrite(I2C_CHAIN_DDR, slaveAddr, byte,
                                         registers, I2C_READ, 0);
         }
         buffer[len]=0;

         wordRead = i2cReadWrite(I2C_CHAIN_DDR, slaveAddr, 149,
                                         registers, I2C_READ, 0);
         vendor=wordRead<<8;
         wordRead = i2cReadWrite(I2C_CHAIN_DDR, slaveAddr, 148,
                                         registers, I2C_READ, 0);
         vendor|=wordRead&0xff;


         wordRead = i2cReadWrite(I2C_CHAIN_DDR, slaveAddr, 120,
                                registers, I2C_READ, 0);
         mfrYear=(wordRead>>4)*10+(wordRead&0x0f);

         wordRead = i2cReadWrite(I2C_CHAIN_DDR, slaveAddr, 121,
                                registers, I2C_READ, 0);
         mfrWeek=(wordRead>>4)*10+(wordRead&0x0f);

         serNum=0;
         for(byte = 122; byte < 126; ++byte) {
            wordRead = i2cReadWrite(I2C_CHAIN_DDR, slaveAddr, byte,
                                         registers, I2C_READ, 0);
            serNum=(serNum<<8)|(wordRead&0xff);
         }

         seq_printf(sfile,
                    "   %d  |    %d   | %s |  %04x  | %08x | week %2d 20%02d |   %2d GB  |\n",
                    card, bank, buffer, vendor, serNum, mfrWeek, mfrYear, gigaBytes);
      }
   }
}

int print_qsfp_text(struct seq_file* sfile) {
   int cardIndex[MAXCARDS];
   int index;
   int card;
   int channel;
   BAR0CommonStruct *registers;
   unsigned int byte;
   unsigned int qsfp;
   int wordRead;
   unsigned int faultMap;
   int volts, mVolts, power;
   char buffer[20];
   unsigned int len;
   index = 0;
   for (card = 0; card < cardsFound; card++) {
      while (cards[index].pciDevice == NULL) {
         index++;
      }
      if (index<MAXCARDS) {
         cardIndex[card]=index;
         index++;
         kdebug(("robinnp(print_qsfp_text): card %d has index %d\n",
                 card,index));
      }
      else {
         kerror(("robinnp(print_qsfp_text): Card indexing error\n"));
         return 0;
      }
   }

   seq_printf(sfile, "\n\nQSFP parameters\n"
              " Card | QSFP | Vendor          | Part number     | Rev. | Serial number   | Date code  | Temperature | Voltage |\n");
   for (card = 0; card < cardsFound; card++) {
      registers = registerBase[cardIndex[card]];
      for (qsfp=0; qsfp<3; qsfp++) {
         seq_printf(sfile, "   %d  |   %d  | ", card, qsfp);

         if ((registers->GlobalStatusRegister.QSFPPresent & (1<<qsfp))) {
            i2cReadWrite(qsfp, I2C_SLAVE_QSFP0, 128, registers, I2C_WRITE, 0);
            len=0;
            for(byte = 148; byte < 164; ++byte) {
               buffer[len++] = i2cReadWrite(qsfp, I2C_SLAVE_QSFP0, byte,
                                            registers, I2C_READ, 0);
            }
            buffer[len]=0;
            seq_printf(sfile, "%s| ", buffer);

            len=0;
            for(byte = 168; byte < 184; ++byte) {
               buffer[len++] = i2cReadWrite(qsfp, I2C_SLAVE_QSFP0, byte,
                                            registers, I2C_READ, 0);
            }
            buffer[len]=0;
            seq_printf(sfile, "%s| ", buffer);

            len=0;
            for(byte = 184; byte < 186; ++byte) {
               buffer[len++] = i2cReadWrite(qsfp, I2C_SLAVE_QSFP0, byte,
                                            registers, I2C_READ, 0);
            }
            buffer[len]=0;
            seq_printf(sfile, " %s  | ", buffer);

            len=0;
            for(byte = 196; byte < 212; ++byte) {
               buffer[len++] = i2cReadWrite(qsfp, I2C_SLAVE_QSFP0, byte,
                                            registers, I2C_READ, 0);
            }
            buffer[len]=0;
            seq_printf(sfile, "%s| ", buffer);

            len=0;
            for(byte = 216; byte < 218; ++byte) {
               buffer[len++] = i2cReadWrite(qsfp, I2C_SLAVE_QSFP0, byte,
                                            registers, I2C_READ, 0);
            }
            buffer[len]=0;
            seq_printf(sfile, "%s/", buffer);

            len=0;
            for(byte = 214; byte < 216; ++byte) {
               buffer[len++] = i2cReadWrite(qsfp, I2C_SLAVE_QSFP0, byte,
                                            registers, I2C_READ, 0);
            }
            buffer[len]=0;
            seq_printf(sfile, "%s/", buffer);

            len=0;
            for(byte = 212; byte < 214; ++byte) {
               buffer[len++] = i2cReadWrite(qsfp, I2C_SLAVE_QSFP0, byte,
                                            registers, I2C_READ, 0);
            }
            buffer[len]=0;
            seq_printf(sfile, "20%s", buffer);

            wordRead = i2cReadWrite(qsfp, I2C_SLAVE_QSFP0, 22, registers,
                                    I2C_READ, 0);
            seq_printf(sfile," |  %d deg C   |", wordRead);

            wordRead = i2cReadWrite(qsfp, I2C_SLAVE_QSFP0, 26, registers,
                                    I2C_READ, 0);
            volts=(wordRead<<8)/10000;
            mVolts=((wordRead<<8)/10000-volts)*10000;
            seq_printf(sfile, " %d.%dV |\n", volts, mVolts);
         }
         else {
            seq_printf(sfile, "\t***  Not Present  ***\n");
         }
      }
   }

   seq_printf(sfile, "\n Card | QSFP | channel |  Rx power  |  Tx bias  | TX Err. | Loss of Signal Tx/Rx |\n");
   for (card = 0; card < cardsFound; card++) {
      registers = registerBase[cardIndex[card]];
      for (qsfp=0; qsfp<3; qsfp++) {
         if ((registers->GlobalStatusRegister.QSFPPresent & (1<<qsfp))) {

            //            i2cReadWrite(qsfp, I2C_SLAVE_QSFP0, 128, registers, I2C_WRITE, 0);
            wordRead = i2cReadWrite(qsfp, I2C_SLAVE_QSFP0, 4,
                                    registers, I2C_READ, 0);
            faultMap = wordRead << 8;
            wordRead = i2cReadWrite(qsfp, I2C_SLAVE_QSFP0, 3,
                                    registers, I2C_READ, 0);
            faultMap |= wordRead & 0xff;
            faultMap = faultMap & 0x3FF;
            for (channel=0; channel<4; channel++) {
               seq_printf(sfile, "   %d  |   %d  |    %d    |",
                          card, qsfp,channel);

               wordRead=i2cReadWrite(qsfp, I2C_SLAVE_QSFP0, 34+2*channel,
                                     registers, I2C_READ, 0);
               power=wordRead<<8;
               wordRead=i2cReadWrite(qsfp, I2C_SLAVE_QSFP0, 35+2*channel,
                                     registers, I2C_READ, 0);
               power|=wordRead;
               seq_printf(sfile, "  %d.%04d mW |",
                          power/10000,power%10000);


               wordRead=i2cReadWrite(qsfp, I2C_SLAVE_QSFP0, 42+2*channel,
                                     registers, I2C_READ, 0);
               power=wordRead<<8;
               wordRead=i2cReadWrite(qsfp, I2C_SLAVE_QSFP0, 43+2*channel,
                                     registers, I2C_READ, 0);
               power|=wordRead;
               power*=2;
               seq_printf(sfile, "  %d.%03d mA |",
                          power/1000, power%1000);

               if(faultMap & (1 << (channel+8))){
                  buffer[0]=1;
               }
               else {
                  buffer[0]=0;
               }

               if(faultMap & (1 << (channel+4))){
                  buffer[1]=1;
               }
               else {
                  buffer[1]=0;
               }

               if(faultMap & (1 << channel)){
                  buffer[2]=1;
               }
               else {
                  buffer[2]=0;
               }
               seq_printf(sfile, "    %1d    |      %1d   /   %1d       |",
                          buffer[0], buffer[1], buffer[2]);
               seq_printf(sfile, "\n");
            }
         }
      }
   }

   return 0;
}


int proc_robinnp_show(struct seq_file* sfile, void* p){
   int interrupt, cardIndex[MAXCARDS], index;
   u_int fpga_ok[MAXCARDS], perr, ret, card, channel, ver, min, maj, /*v0,*/ v1, v2, v3, v4, v5, v6, v7;
   u_short sdata;
   BAR0ChannelStruct *chan_regs;
   BAR0CommonStruct *registers;
   BAR0SubRobStruct *subrobRegisters;
   u_long dna;
   int tempInDegrees;
   u_int fanSpeed;
   unsigned int firmwareVersion = 0;
   int subRob;
   union {
      uint64_t value;
      SubRobStatusComponentStruct registers;
   } subRobStatus;
   union {
      uint64_t value;
      SubRobControlComponentStruct registers;
   } subRobControl;

   union {
      uint64_t value;
      BufferManagerStatusRegisterComponentStruct registers;
   } bufferManagerStatus;
   int freqency;

   index = 0;
   for (card = 0; card < cardsFound; card++)  {
      while (cards[index].pciDevice == NULL) {
            index++;
      }
      if (index<MAXCARDS) {
         cardIndex[card]=index;
         index++;
         kdebug(("robinnp(fill_proc_read_text): card %d has index %d\n",
                 card,index));
      }
      else {
         kerror(("robinnp(fill_proc_read_text): Card indexing error\n"));
         return 0;
      }
   }

   kdebug(("robinnp(fill_proc_read_text): Creating text....\n"));

   if (down_interruptible(&procMutex)) {
      return 0;
   }

   seq_printf(sfile, "\n\n\nRobinNP driver (ALMA9 ready) for TDAQ release %s (based on SVN tag %s, robinnp.c revision %s)\n", RELEASE_NAME, CVSTAG, ROBINNP_TAG);
   /*seq_printf(sfile, "Build time: %s %s\n",__TIME__, __DATE__);*/
   /*  seq_printf(sfile, "\nSupported FPGA F/W: Version = %d, Major = %d, Minor = %d\n", (DESIGN_ID_NP >> 24) & 0xff, (DESIGN_ID_NP >> 16) & 0xff, DESIGN_ID_NP & 0x7ff);   */
   seq_printf(sfile, "\nDebug                       = %d\n", debug);
   seq_printf(sfile, "Number of Dma pages         = %d\n", ndmapages);
   seq_printf(sfile, "DMA page size               = %d\n", dmapagesize);
   seq_printf(sfile, "Number of cards detected    = %d\n", cardsFound);


   seq_printf(sfile, "\ncard |  In use  | S-link Osc. Freq. |\n");
   for (card = 0; card < cardsFound; card++) {
      index=cardIndex[card];
      registers = registerBase[index];
      freqency=getClockConfig(&clockConfig[index],registers,0);
      seq_printf(sfile, "   %d |     %s  |             %d   |\n",
                 card, owner[index]?"Yes":" No", freqency);
   }

  
   seq_printf(sfile, "\n\ncard |  Interrupts handled");
   for (card = 0; card < cardsFound; card++) {
      seq_printf(sfile, "\n   %d |", card);
      for (interrupt = 0; interrupt < msiblock; interrupt++) 
         seq_printf(sfile, "\t%8lu |", irqCount[cardIndex[card]][interrupt]);
   }
  
   seq_printf(sfile, "\n\ncard |  Interrupt flag");
   for (card = 0; card < cardsFound; card++) {
      seq_printf(sfile, "\n   %d |", card);
      for (interrupt = 0; interrupt < msiblock; interrupt++) 
         seq_printf(sfile, "  %12d |", irqFlag[cardIndex[card]][interrupt]);
   }

   seq_printf(sfile, "\n\n");

   perr = 0;
   seq_printf(sfile, "\nSome other registers\n");
   seq_printf(sfile,
              "card |  PCI_STATUS | FPGA version | FPGA major | FPGA minor | # Channels | # ch. per sub-rob | # sub-robs |       BAR0 |"/*       BAR1 |*/"\n");
   for (card = 0; card < cardsFound; card++) {
      seq_printf(sfile, "%4d |", card);
      ret = pci_read_config_word(cards[cardIndex[card]].pciDevice, PCI_STATUS, &sdata);
      if (ret) {
         kerror(("robin(fill_proc_read_text): ERROR from pci_read_config_word\n"));
         seq_printf(sfile, "UNREADABLE\n");
      }
      else {
         seq_printf(sfile, "      0x%04x |", sdata);
         if (sdata & 0x8000) {
            perr = 1;      
            seq_printf(sfile, " Error: Card %d has a parity error\n", card);
         }
      }  
    
      ret = fpgaLoaded(cardIndex[card], &ver, &min, &maj);
      if (ret == 0) {
         seq_printf(sfile, "           %2d |", ver);
         seq_printf(sfile, "         %2d |", maj);
         seq_printf(sfile, "         %2d |", min);
         fpga_ok[card] = 1;
      }
      else {
         seq_printf(sfile, "Error: The FPGA of this card is out of date. version = %d, major = %d, minor = %d\n", ver, maj, min);
         fpga_ok[card] = 0;
      }

      registers = registerBase[cardIndex[card]];
      seq_printf(sfile, "         %2d |", registers->DesignID.NumberOfChannels);
      seq_printf(sfile, "                %2d |", registers->DesignID.NumberOfChannelsPerSubRob);
      seq_printf(sfile, "         %2d |", registers->DesignID.NumberOfSubRobs);
      seq_printf(sfile, " 0x%08x |\n", cards[cardIndex[card]].registerBaseAddress);
      /*    seq_printf(sfile, " 0x%08x |\n", cards[cardIndex[card]].bufferBaseAddress);*/
   }

   if (perr) {
      seq_printf(sfile, "ATTENTION: THERE WAS A PARITY ERROR ON ONE ROBIN\n");
   }
  
  
   seq_printf(sfile, "\nS-Link status bits\n");
   seq_printf(sfile, "Card | ROL | Link err. | Link up | Xoff | Link active | TLK sync | Test mode | ROL Emulation |\n");
   for (card = 0; card < cardsFound; card++) {
      for (channel = 0; channel < cards[cardIndex[card]].nChannels; channel++) {
         if (fpga_ok[card] == 1) {
            chan_regs = (BAR0ChannelStruct *)((unsigned long)registerBase[cardIndex[card]] + 0x1000 + (channel * 0x100));

            //v0 = chan_regs->ROLHandlerRegister0.BOFPattern;                       		//BOF pattern   //MJ: We should add the mask bits from ROLHandlerRegister1
            v1 = chan_regs->ChannelStatusRegister.Component.ROL_LinkDataError;      		//Link error
            v2 = chan_regs->ChannelStatusRegister.Component.ROL_LinkUp;             		//Link up
            v3 = chan_regs->ChannelStatusRegister.Component.ROL_FlowControl;		  		   //Xoff
            v4 = chan_regs->ChannelStatusRegister.Component.ROL_Activity;           		//Link active
            v5 = chan_regs->ChannelStatusRegister.Component.RXSynchStatus;                  // TLK sync status
            v6 = chan_regs->ChannelStatusRegister.Component.ROL_Test;                       //Test mode
            v7 = chan_regs->ChannelStatusRegister.Component.ROLHandler_LDCEmulationEnabled; //ROL emulation
            firmwareVersion = (registerBase[cardIndex[card]]->DesignID.DesignVersion << 24) | (registerBase[cardIndex[card]]->DesignID.DesignMajorRevision << 16) | registerBase[cardIndex[card]]->DesignID.DesignMinorRevision;
            if(firmwareVersion < 0x01050007) {
               seq_printf(sfile, "   %d |  %2d |       %s |     %s |  %s |         %s |      %s |       %s |           %s |\n",
                          card, channel, "N/A",  v2?"Yes":" No", v3?"Yes":" No", v4?"Yes":" No", v5?"Yes":" No", v6?"Yes":" No", v7?"Yes":" No");
            }
            else {
               seq_printf(sfile, "   %d |  %2d |       %s |     %s |  %s |         %s |      %s |       %s |           %s |\n",
                          card, channel, v1?"Yes":" No",  v2?"Yes":" No", v3?"Yes":" No", v4?"Yes":" No", v5?"Yes":" No", v6?"Yes":" No", v7?"Yes":" No");
            }
         }
         else {
            seq_printf(sfile, "Error: The FPGA of this card is out of date\n");
         }
      }
   }

   seq_printf(sfile, "\n");
   print_memorySPD_text(sfile);
   seq_printf(sfile, "\n");
   for (card = 0; card < cardsFound; card++) {
      index=cardIndex[card];
      if (msixOK[index]==0) {
         seq_printf(sfile, " Error: Card %d MSIX table not initialised\n", card);
      }
      if (fpga_ok[card]) {
         for (subRob = 0; subRob < cards[index].nSubRobs; subRob++) {
            subrobRegisters=(BAR0SubRobStruct*)((unsigned long)registerBase[index] + 0x4000 + (subRob * 0x200));
            if (subrobRegisters->MemoryControllerStatus.MemoryPhysicalInitDone==0) {
               seq_printf(sfile, " Error: Card %d subrob %d memory not initialised\n", card, subRob);
            }
            if (subrobRegisters->MemoryControllerStatus.MemoryPLLLocked==0) {
               seq_printf(sfile, " Error: Card %d subrob %d memory PLL not locked\n", card, subRob);
            }
         }
      }
   }

   seq_printf(sfile, " \n");
   for (card = 0; card < cardsFound; card++) {
      registers = registerBase[cardIndex[card]];
      dna=registers->GlobalStatusRegister.DNA;
      seq_printf(sfile, "FPGA 'DNA' card %d = 0x%09lx\n",
                 card, dna);
   }

   seq_printf(sfile, " \n");
   for (card = 0; card < cardsFound; card++) {
      registers = registerBase[cardIndex[card]];
      tempInDegrees=(((registers->GlobalStatusRegister.Temperature * 503975)-279705600)/1024)/1000;
      seq_printf(sfile, "FPGA temperature card %d = %d degrees celsius\n", card, tempInDegrees);
   }

   seq_printf(sfile, " \n");
   for (card = 0; card < cardsFound; card++) {
      registers = registerBase[cardIndex[card]];
      fanSpeed=registers->GlobalStatusRegister.FanSpeed;
      if (fanSpeed==255) {
         fanSpeed=0;
      }
      if (fanSpeed>0) {
         fanSpeed=195300/fanSpeed;
      }
      seq_printf(sfile, "FPGA fan speed card %d = %d RPM\n", card, fanSpeed);
   }

   seq_printf(sfile, " \n");
   seq_printf(sfile, "FIFO Fill levels\n");
   seq_printf(sfile,
              " card | SubRob | desc queue | First FIFO | Second FIFO | Common UPF |Host UPF space\n");
   for (card = 0; card < cardsFound; card++) {
      index=cardIndex[card];
      for (subRob = 0; subRob < cards[index].nSubRobs; subRob++) {
         subrobRegisters=(BAR0SubRobStruct*)((unsigned long)registerBase[index] + 0x4000 + (subRob * 0x200));

         subRobStatus.value=*(uint64_t*)&(subrobRegisters->SubRobStatus);
         subRobControl.value=*(uint64_t*)&(subrobRegisters->SubRobControl);
         seq_printf(sfile,
                    "   %d  |    %d   |    %4d    |    %4d    |     %4d    |    %4d    |     %3d\n",
                    card, subRob,
                    subRobStatus.registers.MemoryReadQueueFIFOFill,
                    subRobStatus.registers.MemoryOutputFirstFIFOFill,
                    subRobStatus.registers.MemoryOutputSecondFIFOFill,
                    subRobStatus.registers.CommonUPFFill,
                    subRobControl.registers.CommonUPFDuplicateSpaceAvailable);
      }
   }

   seq_printf(sfile, "\n card | ROL | Used FIFO | Free FIFO |\n");
   for (card = 0; card < cardsFound; card++) {
      index=cardIndex[card];
      for (channel = 0; channel < cards[index].nChannels; channel++) {
         if (fpga_ok[card] == 1) {
            chan_regs = (BAR0ChannelStruct *)((unsigned long)registerBase[cardIndex[card]] + 0x1000 + (channel * 0x100));
            bufferManagerStatus.value=*(uint64_t*)&(chan_regs->BufferManagerStatusRegister);
            seq_printf(sfile,
                       "   %d  | %2d  |    %4d   |    %4d   | \n",
                       card, channel,
                       bufferManagerStatus.registers.UsedFIFOFill,
                       bufferManagerStatus.registers.FreeFIFOFill);
         }
      }
   }

   print_qsfp_text(sfile);

   seq_printf(sfile, " \n");
   seq_printf(sfile, "The command 'echo <action> > /proc/robinnp', executed as root,\n");
   seq_printf(sfile, "allows you to interact with the driver. Possible actions are:\n");
   seq_printf(sfile, "debug   -> enable debugging\n");
   seq_printf(sfile, "nodebug -> disable debugging\n");
   seq_printf(sfile, "elog    -> Log errors to /var/log/message\n");
   seq_printf(sfile, "noelog  -> Do not log errors to /var/log/message\n");

   up(&procMutex);

   return 0;

}

int proc_robinnp_open(struct inode *inode, struct file *file) {
   return single_open(file, 
                      proc_robinnp_show,
                      NULL);
}


ssize_t proc_robinnp_write(struct file *file, const char __user *data,
                           size_t count, loff_t *offset) {
   int len;
   char buffer[32];

   kdebug(("robinnp(proc_robinnp_write):  called\n"));
   if (count > 31) {
      len = 31;
   }
   else {
      len = count;
   }
   if (copy_from_user(buffer, data, len)) {
      kerror(("robinnp(proc_robinnp_write): error from copy_from_user\n"));
      return(-RD_CFU);
   }

   kdebug(("robinnp(proc_robinnp_write): len = %d\n", len));
   buffer[len - 1] = '\0';
   kdebug(("robinnp(proc_robinnp_write): text passed = %s\n", buffer));

   if (!strcmp(buffer, "debug")) {
      debug = 1;
      kdebug(("robinnp(proc_robinnp_write): debugging enabled\n"));
   }
   else if (!strcmp(buffer, "nodebug")) {
      kdebug(("robinnp(proc_robinnp_write): debugging disabled\n"));
      debug = 0;
   }
   else if (!strcmp(buffer, "elog")) {
      kdebug(("robinnp(proc_robinnp_write): Error logging enabled\n"))
         errorlog = 1;
   }
   else if (!strcmp(buffer, "noelog")) {
      kdebug(("robinnp(proc_robinnp_write): Error logging disabled\n"))
         errorlog = 0;
   }
   return len;
}




/*******************************************************************/
static int fpgaLoaded(u_int card, u_int *ver, u_int *min, u_int *maj)  
/*******************************************************************/
{
  //Assumptions:
  //If the FPGA is not loaded at all the host PC will not be able to see the RobinNP
  //I.e. it will not show up in "lspci". Therefore the driver will not detect the card and we will never get into this function
  //Therefore, if we are here, we can assume the FPGA is loaded and just have to check the version of the F/W
  
  BAR0CommonStruct *registers;
  int version, majorRevision, minorRevision, eversion, emajorRevision, eminorRevision;
  
  kdebug(("robinnp(fpgaLoaded): function called\n"))

  eversion       = (DESIGN_ID_NP >> 24) & 0xff;
  emajorRevision = (DESIGN_ID_NP >> 16) & 0xff;
  eminorRevision = DESIGN_ID_NP & 0x7ff;
    
  //Read the designID register
  registers     = registerBase[card];
  version       = registers->DesignID.DesignVersion;
  majorRevision = registers->DesignID.DesignMajorRevision;
  minorRevision = registers->DesignID.DesignMinorRevision;

  *ver = version;
  *min = minorRevision;
  *maj = majorRevision;

  kdebug(("robinnp(fpgaLoaded): Expected:\n"));
  kdebug(("robinnp(fpgaLoaded): eversion:       0x%x\n", eversion));
  kdebug(("robinnp(fpgaLoaded): emajorRevision: 0x%x\n", emajorRevision));
  kdebug(("robinnp(fpgaLoaded): eminorRevision: 0x%x\n", eminorRevision));
  kdebug(("robinnp(fpgaLoaded): Detected:\n"));
  kdebug(("robinnp(fpgaLoaded): version:        0x%x\n", version));
  kdebug(("robinnp(fpgaLoaded): majorRevision:  0x%x\n", majorRevision));
  kdebug(("robinnp(fpgaLoaded): minorRevision:  0x%x\n", minorRevision));

  if ((eversion != version) || (emajorRevision > majorRevision) || (majorRevision-emajorRevision>2))
  {
    kerror(("robinnp(fpgaLoaded): Incompatible FPGA version. Can't continue!\n"));
    return -2;
  } 
  else 
  {
/*     if (eminorRevision > minorRevision) */
/*     { */
/*       kerror(("robinnp(fpgaLoaded): Incompatible FPGA revision (too small). Can't continue!\n")); */
/*       return -2; */
/*     } */
/*     if (eminorRevision < minorRevision) */
/*       kdebug(("robinnp(fpgaLoaded): Actual FPGA revision too high. Not all FPGA features might be exploited\n")); */
     if (emajorRevision!=majorRevision || eminorRevision!=minorRevision)
        kdebug(("robinnp(fpgaLoaded): Actual FPGA revision does not match. Not all FPGA features might be exploited\n"));
  }
  return 0;
}


/*-------------------------------------------------------------------------------------*/
/*---Module Init ----------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/

static int RobinNP_init(void) {
  int stat, cardNumber, major, interrupt;

  for (cardNumber = 0; cardNumber < MAXCARDS; cardNumber++) {
    cards[cardNumber].pciDevice = NULL;
    cards[cardNumber].nDmaPages = 0;
    cards[cardNumber].oscfreq = 0;
    registerBase[cardNumber] = NULL;

    for (interrupt = 0; interrupt < MAXMSI; interrupt++) {
       irqCount[cardNumber][interrupt] = 0;
    }
    owner[cardNumber]=NULL;
  }

   if (msiblock>MAXMSI) {
      printk(KERN_ALERT
             "robinnp(RobinNP_init):msiblock > MAXMSI - setting to max (%d)\n", MAXMSI);
      msiblock=MAXMSI;
   }

   if(oscfreq>MAX_OSC_FREQ) {
	   printk(KERN_ALERT
	              "robinnp(RobinNP_init):oscfreq > MAX_OSC_FREQ - setting to max (%d)\n", MAX_OSC_FREQ);
	   oscfreq=MAX_OSC_FREQ;
   }

	kdebug(("robinnp(RobinNP_init): registering PCIDriver   max channels=%d\n", MAXCHAN));
	stat=pci_register_driver(&RobinNP_PCI_driver);
	if (stat < 0) {
		printk(KERN_ALERT "robinnp(RobinNP_init): Status %d from pci_register_driver\n", stat);
		return stat;
	}

	stat=alloc_chrdev_region(&first_dev, FIRSTMINOR, MAXCARDS, devName);
	if (stat != 0) {
		kerror(("RobinNP_init: registering RobinNP driver failed.\n"));
		pci_unregister_driver(&RobinNP_PCI_driver);
		return stat;
	}

   major = MAJOR(first_dev);
   kdebug((KERN_ALERT "robinnp(RobinNP_init): major number is %d\n", major));

   test_cdev = cdev_alloc();
   test_cdev->ops = &fops;
   test_cdev->owner = THIS_MODULE;
   stat=cdev_add(test_cdev, first_dev, 1);
   if (stat != 0) {
      printk(KERN_ALERT "robinnp(RobinNP_init): cdev_add failed, driver will not load\n");
      unregister_chrdev_region(first_dev, MAXCARDS);
      pci_unregister_driver(&RobinNP_PCI_driver);
      return stat;
   }

   proc_create(devName, 0, NULL, &robinnp_proc_file_ops);

	printk(KERN_INFO "robinnp(RobinNP_init): RobinNP driver loaded, found %d device(s)\n", cardsFound);
	return 0;
}
/*-------------------------------------------------------------------------------------*/




/*-------------------------------------------------------------------------------------*/
/*---- Module Exit --------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/
static void RobinNP_exit(void) {
	remove_proc_entry(devName, NULL /* parent dir */);

	kdebug(("robinnp(RobinNP_exit): unregister device\n"));
	unregister_chrdev_region(first_dev,MAXCARDS);

	kdebug(("robinnp(RobinNP_exit): unregister driver\n"));
	pci_unregister_driver(&RobinNP_PCI_driver);

	cdev_del(test_cdev);

	printk(KERN_INFO "robinnp(RobinNP_exit): driver removed\n");
}
/*-------------------------------------------------------------------------------------*/


static irqreturn_t irqHandler(int irq, void* dev)
{
  struct irqInfo_struct *info;

  info=(struct irqInfo_struct*) dev;
  irqCount[info->card][info->interrupt] += 1;

  irqFlag[info->card][info->interrupt] = 1;
  wake_up_interruptible(&waitQueue);

  return IRQ_HANDLED;
}


/*-------------------------------------------------------------------------------------*/
/*-- Probe ----------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/


/* PCI driver functions  */
static int /*__devinit*/ RobinNP_Probe(struct pci_dev *dev, const struct pci_device_id *id) {
  int cardNumber, ret, bufferNumber, channel, subRob;
  int interrupt, msixCapOffset, msixData, msixBarNumber, msixTableOffset, msixLength;
  int version, majorRevision, minorRevision;
  unsigned int* dmaPage;
  BAR0CommonStruct *registers;
  BAR0ChannelStruct *chan_regs;
  dma_addr_t dmaHandle;
  PrimaryDMADoneFIFOStruct* dmadone;
  UPFStruct* upfdup;
  unsigned long msixAddress, registerBaseAddress;
  uint32_t* msixBar;
  ktime_t timeStart, timeCurrent, timeDiff;
  unsigned int duplicatorSize;

   cardNumber = 0;
   // Find first available slot
   while (cards[cardNumber].pciDevice != NULL && cardNumber<MAXCARDS) {
      cardNumber++;
   }
   if (cardNumber<MAXCARDS) {
			kdebug(("robinnp(RobinNP_Probe): Initialising device nr %d (counting from 0)\n", cardsFound));
			ret = pci_enable_device(dev);
			cardsFound++;
			cards[cardNumber].pciDevice = dev;
   }
   else {
		printk(KERN_ALERT "robinnp(RobinNP_Probe): Too many cards present, only %d is allowed\n", MAXCARDS);
		return -1;
	}

	kdebug(("robinnp(RobinNP_Probe): Reading configuration space for card %d :\n", cardNumber));
	cards[cardNumber].registerBaseAddress = pci_resource_start(dev, 0);
   registerBaseAddress = pci_resource_start(dev, 0);
	cards[cardNumber].registerSize        = pci_resource_len(dev, 0);

	/* remap the whole bar0 as registers */
	kdebug(("robinnp(RobinNP_Probe): Remaping BAR0 as registers space, card %d, phys add/size: 0x%x 0x%x\n",
			cardNumber, cards[cardNumber].registerBaseAddress, cards[cardNumber].registerSize));
	registerBase[cardNumber] = ioremap(registerBaseAddress, cards[cardNumber].registerSize);
	registers = registerBase[cardNumber];


   /* Do general reset of the robinNP card */
   registers->GlobalControlRegister.GlobalReset = 1;
   registers->GlobalControlRegister.GlobalReset = 0;

   /* Reset the interrupt logic of the robinNP card */
   registers->GlobalControlRegister.InterrupterReset = 1;
   registers->GlobalControlRegister.InterrupterReset = 0;


  version       = registers->DesignID.DesignVersion;
  majorRevision = registers->DesignID.DesignMajorRevision;
  minorRevision = registers->DesignID.DesignMinorRevision;


	cards[cardNumber].nChannels=registers->DesignID.NumberOfChannels;
	kdebug(("robinnp(RobinNP_Probe): NumberOf Channels      = %x\n", registers->DesignID.NumberOfChannels));
	if (cards[cardNumber].nChannels > MAXCHAN) {
	  printk(KERN_ALERT "robinnp(RobinNP_Probe): Illegal number of channels in designId %d\n", cards[cardNumber].nChannels);
	  return -1;
	}

   cards[cardNumber].nSubRobs = registers->DesignID.NumberOfSubRobs;
   kdebug(("robinnp(RobinNP_Probe): %d subRobs\n", cards[cardNumber].nSubRobs));

   for (subRob = 0; subRob < cards[cardNumber].nSubRobs; subRob++) {
      cards[cardNumber].dmaDoneDuplicateBuffer[subRob] =
			dma_alloc_coherent(&dev->dev,
                            4096,     /* 4k page */
                            &dmaHandle,
                            GFP_KERNEL);
      if (cards[cardNumber].dmaDoneDuplicateBuffer[subRob] == NULL) {
         printk(KERN_ALERT "robinnp(RobinNP_Probe): Failed to allocate DMA memory for dmaDone duplicator\n");
         return -1;
      }
      
      cards[cardNumber].dmaDoneDuplicateHandle[subRob] = dmaHandle;
      kdebug(("robinnp(RobinNP_Probe): Allocated DMA done FIFO duplicator DMA buffer with handle 0x%llx\n",
              cards[cardNumber].dmaDoneDuplicateHandle[subRob]));


      /*TSTART Just for testing write some data to allocated page */
      dmadone = cards[cardNumber].dmaDoneDuplicateBuffer[subRob];
      dmadone->pciAddressUpper = 0xfeedface;
      dmadone->pciAddressLower = 0xdeadbeef;
      dmadone->localAddress    = 0x1234abcd;
      /*TEND*/

      if (version<2 && majorRevision<7 && minorRevision<5) {
         cards[cardNumber].upfDuplicateBuffer[subRob] =
            dma_alloc_coherent(&dev->dev,
                               4096,     /* 4k page for 2 duplicators!! */
                               &dmaHandle,
                               GFP_KERNEL);
         if (cards[cardNumber].upfDuplicateBuffer[subRob] == NULL) {
            printk(KERN_ALERT "robinnp(RobinNP_Probe): Failed to allocate DMA memory for UPF duplicator\n");
            return -1;
         }
      }
      else {
         duplicatorSize=1<<(registers->GlobalControlRegister.FDSourceWidthBits + registers->GlobalControlRegister.FDDestinationLengthElementsLarge);
         kdebug(("robinnp(RobinNP_Probe): Allocating Used Page FIFO duplicator DMA buffer FDSourceWidthBits=0x%x  FDSourceLengthElements=0x%x  FDDestinationLengthElementsLarge=0x%x  FDInternalBusWidthBitBytes=0x%x  duplicatorSize=0x%x (%d)\n",
                 registers->GlobalControlRegister.FDSourceWidthBits, registers->GlobalControlRegister.FDSourceLengthElements,
                 registers->GlobalControlRegister.FDDestinationLengthElementsLarge, registers->GlobalControlRegister.FDInternalBusWidthBitBytes,
                 duplicatorSize, duplicatorSize));

         if (duplicatorSize<8192) {
            duplicatorSize=8192;
         }
         cards[cardNumber].upfDuplicateBuffer[subRob] =
            dma_alloc_coherent(&dev->dev,
                               duplicatorSize,
                               &dmaHandle,
                               GFP_KERNEL);
         /* Check that we allocated memory at a sensible address */
         if (cards[cardNumber].upfDuplicateBuffer[subRob] == NULL) {
            printk(KERN_ALERT "robinnp(RobinNP_Probe): Failed to allocate DMA memory for UPF duplicator\n");
            return -1;
         }
         else if (dmaHandle & 0x1fff) {
            printk(KERN_ALERT "robinnp(RobinNP_Probe): Warning DMA memory for UPF duplicator not on 8k boundary\n");
         }
      }
      cards[cardNumber].upfDuplicateHandle[subRob] = dmaHandle;
      kdebug(("robinnp(RobinNP_Probe): Allocated Used Page FIFO duplicator DMA buffer with handle 0x%llx\n",
              cards[cardNumber].upfDuplicateHandle[subRob]));
		/*TSTART Just for testing write some data to allocated page */
		upfdup = cards[cardNumber].upfDuplicateBuffer[subRob];
		upfdup->upper = 0xabcd0000 + subRob;
		upfdup->lower = 0xf00d0000 + subRob;
		/*TEND*/
	}
	cards[cardNumber].writeIndexBuffer =
			dma_alloc_coherent(&dev->dev,
					4096,     /* 4k page!! */
					&dmaHandle,
					GFP_KERNEL);
					
	if (cards[cardNumber].writeIndexBuffer==NULL) {
		printk(KERN_ALERT "robinnp(RobinNP_Probe): Failed to allocate DMA memory\n");
		return -1;
	}
	
	cards[cardNumber].writeIndexHandle = dmaHandle;

	if (ndmapages > MAXDMABUF) {
      printk(KERN_ALERT "robinnp(RobinNP_Probe): Requested number of DMA pages (%d) too high, setting to %d",
             ndmapages, MAXDMABUF);
		ndmapages = MAXDMABUF;
	}
	
	cards[cardNumber].dmaPageSize = dmapagesize;
	
	for (bufferNumber = 0; bufferNumber < ndmapages; bufferNumber++) {
		cards[cardNumber].primaryDmaBuffer[bufferNumber]=
				dma_alloc_coherent(&dev->dev,
						dmapagesize,
						&dmaHandle,
						GFP_KERNEL);
		if (cards[cardNumber].primaryDmaBuffer[bufferNumber] == NULL) {
			printk(KERN_ALERT "robinnp(RobinNP_Probe): Failed to allocate DMA memory page %d for card %d\n",
                cards[cardNumber].nDmaPages,cardNumber);
			break;
		}
		cards[cardNumber].primaryDmaHandle[bufferNumber] = dmaHandle;
		cards[cardNumber].nDmaPages++;

		/*TSTART Just for testing write some data to allocated page */
		dmaPage = cards[cardNumber].primaryDmaBuffer[bufferNumber];
		dmaPage[0] = (0x1234<<16) + bufferNumber;
		/*TEND*/
	}

   for (interrupt = 0; interrupt < msiblock; interrupt++) {
      msixTable[cardNumber][interrupt].entry = interrupt;
   }
   msixEnabled[cardNumber] = pci_enable_msix_range(dev, msixTable[cardNumber], 1, msiblock);

   msixCapOffset = pci_find_capability(dev,PCI_CAP_ID_MSIX);
   pci_read_config_dword(dev, msixCapOffset + 4, &msixData);
   msixBarNumber = msixData & 0xf;
   msixTableOffset = msixData & 0xfffffff0;
   kdebug(("robinnp(RobinNP_Probe): MSIX Vector table BAR %d, offset %08x\n", msixBarNumber, msixTableOffset));
   pci_read_config_dword(dev, msixCapOffset + 8, &msixData);
   msixBarNumber = msixData & 0xf;
   msixAddress = pci_resource_start(dev, msixBarNumber) + msixTableOffset;
   msixLength = pci_resource_len(dev, msixBarNumber);
   msixBar = ioremap(msixAddress, msixLength);
   if (msixBar == NULL) {
      printk(KERN_ALERT "robinnp(RobinNP_Probe): Failed to map MSI-X BAR\n");
      msixOK[cardNumber]=0;
   }
   else {
      msixOK[cardNumber]=msixBar[0];
      iounmap(msixBar);
      msixBar=NULL;
   }
   if (msixOK[cardNumber]==0) {
      printk(KERN_ALERT
             "robinnp(RobinNP_Probe): Failed to enable MSI-X interrupt block for card %d, enable returned %d but table is empty\n",
             cardNumber, msixEnabled[cardNumber]);
   }

   if (msixEnabled[cardNumber] != msiblock) {
      printk(KERN_ALERT
             "robinnp(RobinNP_Probe): Failed to enable MSI-X interrupt block for card %d, enable returned %d\n",
             cardNumber, msixEnabled[cardNumber]);
   } else {
      for (interrupt = 0; interrupt < msiblock; interrupt++) {
         kdebug(("robinnp(RobinNP_Probe): Trying to register IRQ %d\n", msixTable[cardNumber][interrupt].vector));

         irqInfo[cardNumber][interrupt].interrupt = interrupt;
         irqInfo[cardNumber][interrupt].card      = cardNumber;
         irqInfo[cardNumber][interrupt].subrob    = 0;
         ret = request_irq(msixTable[cardNumber][interrupt].vector,
                         irqHandler,
                         0, devName,
                         &irqInfo[cardNumber][interrupt]);
         if (ret != 0) {
            printk(KERN_ALERT "robinnp(RobinNP_Probe): Failed to register interrupt handler for MSI %d\n", interrupt);
         }
      }
   }


	/* Lets try to extract the DesignID using the BAR0 structure */
	kdebug(("robinnp(RobinNP_Probe): DesignID address       = %p\n", &(registers->DesignID)));
	kdebug(("robinnp(RobinNP_Probe): Design Version         = %x\n",  registers->DesignID.DesignVersion));
	kdebug(("robinnp(RobinNP_Probe): Design Major Revision  = %x\n",  registers->DesignID.DesignMajorRevision));
	kdebug(("robinnp(RobinNP_Probe): Design Minor Revision  = %x\n",  registers->DesignID.DesignMinorRevision));
	kdebug(("robinnp(RobinNP_Probe): Author                 = %x\n",  registers->DesignID.Author));
	kdebug(("robinnp(RobinNP_Probe): NumberOf Channels      = %x\n",  registers->DesignID.NumberOfChannels));

	// Set up s-link ocillator frequency

	/*
	 * Protocol:
	 *
	 * 1) Initialise Oscillator to factory defaults (freq 212.5 Mhz)
	 *
	 * 2) Read clock configuration registers assuming 212.5 Mhz, calculate crystal frequency etc...
	 *
	 * 3) Compute new clock configuration register values based on new desired frequency (default 100 Mhz)
	 *
	 * 4) Write new configuration to I2C bus
	 *
	 * 5) Read back configuration registers for confirmation
	 *
	 */

	initialiseOscillator(registers);

	getClockConfig(&clockConfig[cardNumber],registers,DEFAULT_OSC_FREQ*SCALE1);

	computeClockConfig(&clockConfig[cardNumber],oscfreq);

	kdebug(("robinnp(RobinNP_Probe): Apply new config\n"));
	applyClockConfig(&clockConfig[cardNumber],registers);

	cards[cardNumber].oscfreq=getClockConfig(&clockConfig[cardNumber],registers,0);
        if(cards[cardNumber].oscfreq != oscfreq) {
           printk(KERN_ALERT "robinnp(RobinNP_Probe): Failed to set oscillator for card %d to %d, currently set to%d\n",
                  cardNumber,oscfreq,cards[cardNumber].oscfreq);
        }

	// Attempt to bring up S-Links

	for (channel = 0; channel < cards[cardNumber].nChannels; channel++) {
	      chan_regs = (BAR0ChannelStruct *)((unsigned long)registerBase[cardNumber]
	                                        + 0x1000 + (channel * 0x100)); // FIXME naughty hardwired numbers should be symbolic constants from hardware header

		kdebug(("robinnp(RobinNP_Probe): Attempt s-link reset for ROL %d\n\n", channel));
		chan_regs->ChannelControlRegister.Component.HOLA_Reset = 1;

		chan_regs->ChannelControlRegister.Component.GTX_Reset = 1;
		chan_regs->ChannelControlRegister.Component.GTX_Reset = 0;

                timeStart=ktime_get();
		while(chan_regs->ChannelStatusRegister.Component.GTX_ResetDone == 0){
                   timeCurrent=ktime_get();
                   timeDiff=ktime_sub(timeCurrent,timeStart);
                   if(ktime_to_ns(timeDiff) > 100000000){
                      kdebug(("robinnp(RobinNP_Probe): GTX Reset Failed for channel = %d\n" ,chan_regs->ChannelStatusRegister.Component.ChannelNumber));
                      break;
                   }
		}

		chan_regs->ChannelControlRegister.Component.HOLA_Reset = 0;

                timeStart=ktime_get();
		while(chan_regs->ChannelStatusRegister.Component.ROL_LinkUp == 0){
                   timeCurrent=ktime_get();
                   timeDiff=ktime_sub(timeCurrent,timeStart);
                   if(ktime_to_ns(timeDiff) > 100000000){
                      kdebug(("robinnp(RobinNP_Probe): Link for ChannelNumber = %d failed to start\n" ,chan_regs->ChannelStatusRegister.Component.ChannelNumber));
                      break;
                   }
		}

		chan_regs->ChannelControlRegister.Component.ClearRXLostSyncLatched = 1;
		chan_regs->ChannelControlRegister.Component.ClearRXLostSyncLatched = 0;


		kdebug(("robinnp(RobinNP_Probe): ChannelNumber = %d\n" ,chan_regs->ChannelStatusRegister.Component.ChannelNumber));
		kdebug(("robinnp(RobinNP_Probe): ROLHandler_intUXOF = %d\n" ,chan_regs->ChannelStatusRegister.Component.HOLA_UXOFF_n));
		kdebug(("robinnp(RobinNP_Probe): ROLHandler_LDCEmulationEnabled = %d\n" ,chan_regs->ChannelStatusRegister.Component.ROLHandler_LDCEmulationEnabled));
		kdebug(("robinnp(RobinNP_Probe): ROL_LinkDataError = %d\n" ,chan_regs->ChannelStatusRegister.Component.ROL_LinkDataError));
		kdebug(("robinnp(RobinNP_Probe): ROL_LinkUp = %d\n", chan_regs->ChannelStatusRegister.Component.ROL_LinkUp));
		kdebug(("robinnp(RobinNP_Probe): ROL_FlowControl = %d\n", chan_regs->ChannelStatusRegister.Component.ROL_FlowControl));
		kdebug(("robinnp(RobinNP_Probe): ROL_Activity = %d\n" ,chan_regs->ChannelStatusRegister.Component.ROL_Activity));
		kdebug(("robinnp(RobinNP_Probe): ROL_Test = %d\n" ,chan_regs->ChannelStatusRegister.Component.ROL_Test));
		kdebug(("robinnp(RobinNP_Probe): GTX_ResetDone = %d\n", chan_regs->ChannelStatusRegister.Component.GTX_ResetDone));
		kdebug(("robinnp(RobinNP_Probe): TX_PLLLK = %d\n", chan_regs->ChannelStatusRegister.Component.TX_PLLLK));
		kdebug(("robinnp(RobinNP_Probe): RX_PLLLK = %d\n", chan_regs->ChannelStatusRegister.Component.RX_PLLLK));
		kdebug(("robinnp(RobinNP_Probe): RXSynchStatus = %d\n", chan_regs->ChannelStatusRegister.Component.RXSynchStatus));
		kdebug(("robinnp(RobinNP_Probe): RXSynchStatusLatched = %d\n\n", chan_regs->ChannelStatusRegister.Component.RXSynchStatusLatched));

	  }

   /* if (debug) { */
   /*    kdebug(("robinnp(RobinNP_Probe): msix address %08x, length %4x\n", msixAddress, msixLength)); */
   /*    bufferNumber = msixTableOffset / sizeof(uint32_t); */
   /*    for (interrupt = 0; interrupt < 8 /\*msiblock*\/; interrupt++) { */
   /*       kdebug(("robinnp(RobinNP_Probe): MSI-X table[%d] %08x %08x  %08x  %08x\n", */
   /*               interrupt, msixBar[cardNumber][bufferNumber], msixBar[cardNumber][bufferNumber+1], msixBar[cardNumber][bufferNumber+2], msixBar[cardNumber][bufferNumber+3])); */
   /*       bufferNumber += 4; */
   /*    } */

   /*    if (msixPbaOffset[cardNumber] + 3 * sizeof(uint32_t) < msixLength) { */
   /*       kdebug(("robinnp(RobinNP_Probe): MSI-X PBA %08x %08x  %08x  %08x\n", */
   /*               msixBar[cardNumber][msixPbaOffset[cardNumber] / sizeof(uint32_t)], */
   /*               msixBar[cardNumber][msixPbaOffset[cardNumber] / sizeof(uint32_t) + 1], */
   /*               msixBar[cardNumber][msixPbaOffset[cardNumber] / sizeof(uint32_t) + 2], */
   /*               msixBar[cardNumber][msixPbaOffset[cardNumber] / sizeof(uint32_t) + 3])); */
   /*    } */
   /*    else { */
   /*       printk(KERN_ALERT "robinnp(RobinNP_Probe): PBA offset %x is outside of BAR%d, length=%x \n", msixPbaOffset[cardNumber], msixBarNumber, msixLength); */
   /*    } */
   /* } */
	return 0;
}
/*-------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------*/
/*------ Remove -----------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/

static void RobinNP_Remove(struct pci_dev *dev) 
{
   int cardNumber, subRob, bufferNumber, interrupt;

   printk(KERN_ALERT "robinnp(RobinNP_Remove):  called\n");

   for(cardNumber = 0; cardNumber < MAXCARDS; cardNumber++ ) {
      if (cards[cardNumber].pciDevice == dev) {
         printk(KERN_ALERT "robinnp(RobinNP_Remove): for card %d\n", cardNumber);
         cards[cardNumber].pciDevice = NULL;
         owner[cardNumber]=NULL;
         cardsFound--;

         if (msixEnabled[cardNumber] == msiblock) {
            for (interrupt = 0; interrupt < msiblock; interrupt++) {
               kdebug(("robinnp(RobinNP_Remove): unregestering interrupt %d, vector %d\n", interrupt, msixTable[cardNumber][interrupt].vector));
               free_irq(msixTable[cardNumber][interrupt].vector, &irqInfo[cardNumber][interrupt]);
            }
         }
         pci_disable_msix(dev);

         if (registerBase[cardNumber] != NULL) {
            iounmap(registerBase[cardNumber]);
            registerBase[cardNumber]=NULL;
         }

         for (subRob = 0; subRob < cards[cardNumber].nSubRobs; subRob++) {
            dma_free_coherent(&dev->dev,
                              4096,     /* 4k page!! */
                              cards[cardNumber].dmaDoneDuplicateBuffer[subRob],
                              cards[cardNumber].dmaDoneDuplicateHandle[subRob]);


            dma_free_coherent(&dev->dev,
                              4096,     /* 4k page!! */
                              cards[cardNumber].upfDuplicateBuffer[subRob],
                              cards[cardNumber].upfDuplicateHandle[subRob]);
         }
         dma_free_coherent(&dev->dev,
                           4096,
                           cards[cardNumber].writeIndexBuffer,
                           cards[cardNumber].writeIndexHandle);


         for (bufferNumber = 0; bufferNumber < cards[cardNumber].nDmaPages; bufferNumber++) {
            dma_free_coherent(&dev->dev,
                              cards[cardNumber].dmaPageSize,
                              cards[cardNumber].primaryDmaBuffer[bufferNumber],
                              cards[cardNumber].primaryDmaHandle[bufferNumber]);
         }
         cards[cardNumber].nDmaPages=0;
         break;
      }
   }
}
/*-------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------*/
/*------ mmap -------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/
int RobinNP_mmap(struct file *file, struct vm_area_struct *vma) {
	u32 moff,msize;
	/*card_params_t *pdata;*/

	kdebug(("robinnp(RobinNP_mmap):\n"));

	/* it should be "shared" memory */
	if ((vma->vm_flags & VM_WRITE) && !(vma->vm_flags & VM_SHARED)) {
		printk(KERN_ALERT "robinnp(RobinNP_mmap): writeable mappings must be shared, rejecting\n");
		return -1;
	}

	msize = vma->vm_end - vma->vm_start;
	moff = (vma->vm_pgoff << PAGE_SHIFT);
	if (moff & ~PAGE_MASK) {
		printk(KERN_ALERT "robinnp(RobinNP_mmap): offset not aligned: %u\n", moff);
		return(-1);
	}

#if RHEL_RELEASE_CODE >= RHEL_RELEASE_VERSION(9,5)
   vm_flags_set(vma, VM_DONTDUMP | VM_IO | VM_LOCKED);
#else
   vma->vm_flags |= VM_DONTDUMP;
   vma->vm_flags |= VM_IO;
   vma->vm_flags |= VM_LOCKED; // we do not want to have this area swapped out, lock it
#endif

	if (remap_pfn_range(vma, vma->vm_start, vma->vm_pgoff,msize,vma->vm_page_prot) != 0) {
		printk(KERN_ALERT "robinnp(RobinNP_mmap): remap page range failed\n");
		return -EAGAIN;
	}

	vma->vm_ops = &RobinNP_vm_ops;
	return 0;
}

void RobinNP_vmclose(struct vm_area_struct *vma) {
	kdebug(("robinnp(RobinNP_mmap): closing mmap memory\n"));
}
/*-------------------------------------------------------------------------------------*/


/*-------------------------------------------------------------------------------------*/
/*---- IOCTLs-------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/

//int RobinNP_ioctl(struct inode *inode, struct file *file, u_int cmd, u_long arg) 
static long RobinNP_ioctl(struct file *file, u_int cmd, u_long arg)
{
  card_params_t *cardParams;
  static struct vm_area_struct *vmas, uvmas;
  int stat, size, testdata;
  dmaDebug_t* dmaDebug;
  unsigned int interrupt, card;

  kdebug(("robinnp(RobinNP_ioctl): entered\n"));
  vmas = &uvmas;

  switch(cmd) 
  {
    case GETCARDS:
      kdebug(("robinnp(RobinNP_ioctl,GETCARDS): GETCARDS\n"));
      if (copy_to_user(((int*)arg), &cardsFound, sizeof(int)) !=0) 
      {
         printk(KERN_ALERT "robinnp(RobinNP_ioctl,GETCARDS): Copy cardsFound to user space failed!\n");
         return(-1);
      }
      break;

    case WAIT_IRQ:    //WAIT_DMA
      kdebug(("robinnp(RobinNP_ioctl,WAIT_IRQ): WAIT_IRQ\n"));
      cardParams = (card_params_t*)file->private_data;
      card = cardParams->slot;
      if (copy_from_user(&interrupt, (void *)arg, sizeof(unsigned int)) !=0) {
         kerror(("robinnp(RobinNP_ioctl,WAIT_IRQ): error from copy_from_user\n"));
         return(-RD_CFU);
      }
      if (interrupt >= msiblock) {
         kerror(("robinnp(RobinNP_ioctl,WAIT_IRQ): invalid interrupt specified %d\n", interrupt));
         return(-1);
      }
      wait_event_interruptible(waitQueue, irqFlag[card][interrupt] == 1);
      irqFlag[card][interrupt] = 0;
      kdebug(("robinnp(RobinNP_ioctl,WAIT_IRQ): finished waiting for IRQ %d\n", interrupt));
      break;

    case CANCEL_IRQ_WAIT:
       kdebug(("RobinNP ioctl CANCEL_IRQ_WAIT  looping over %d interrupts\n", msiblock));
      cardParams = (card_params_t*)file->private_data;
      card = cardParams->slot;
      for (interrupt = 0; interrupt < msiblock; interrupt++) {
         irqFlag[card][interrupt] = 1;
      }
      /* Wake up everybody who was waiting for an interrupt */
      wake_up_interruptible(&waitQueue);
      break;

    case INIT_IRQ:
       kdebug(("RobinNP ioctl INIT_IRQ  looping over %d interrupts\n", msiblock));
       cardParams = (card_params_t*)file->private_data;
       card = cardParams->slot;
       for (interrupt = 0; interrupt < msiblock; interrupt++) {
          irqFlag[card][interrupt] = 0;
       }
       break;

    case SETLINK:
      kdebug(("RobinNP ioctl SETLINK\n"));
      if (copy_from_user(file->private_data, (void*) arg, sizeof(card_params_t)) !=0)
      {
	printk(KERN_ALERT "robinnp(RobinNP_ioctl,SETLINK): Copy card_params_t from user space failed!\n");
	return(-1);
      }

      cardParams = (card_params_t*)file->private_data;
      card = cardParams->slot;
      if (check_slot_device(card) != 0) 
      {
         printk(KERN_ALERT "robinnp(RobinNP_ioctl,SETLINK): Wrong slot !\n");
         return -6;
      }


      if (down_interruptible(&ownerMutex)) {
         return -4;
      }
      if (owner[card]!=0 && owner[card]!=file)
      {
         printk(KERN_ALERT "robinnp(RobinNP_ioctl,SETLINK): slot locked by another process!\n");
         up(&ownerMutex);
         return -16;
      }

      owner[card]=file;
      up(&ownerMutex);

      // OK, we have a valid slot, copy configuration back to user 
      if (copy_to_user(((card_params_t*)arg), &cards[card], sizeof(card_params_t)) !=0) 
      {
	printk(KERN_ALERT "robinnp(RobinNP_ioctl,SETLINK): Copy card_params_t to user space failed!\n");
	return(-1);
      }
      kdebug(("robinnp(RobinNP_ioctl,SETLINK): end of ioctl SETLINK\n"));
      break;

    case READ_DMABUF:
      kdebug(("robinnp(RobinNP_ioctl,READ_DMABUF): READ_DMABUF\n"));
      dmaDebug = (dmaDebug_t*)arg;

      if (check_slot_device(dmaDebug->card) != 0) 
      {
	printk(KERN_ALERT "robinnp(RobinNP_ioctl,READ_DMABUF): Wrong slot !\n");
	return(-1);
      }
      size = dmaDebug->size;
      if (size > 4096) 
	size = 4096;

      stat = copy_to_user(dmaDebug->buffer, cards[dmaDebug->card].upfDuplicateBuffer[dmaDebug->channel], size);
      if (stat !=0) 
      {
	printk(KERN_ALERT "robinnp(RobinNP_ioctl,READ_DMABUF): Copy UPF duplicate to user space failed!\n");
	return(-1);
      }
      kdebug(("robinnp(RobinNP_ioctl,READ_DMABUF): end of ioctl READ_DMA_BUF\n"));
      break;

    case CSTEST:
      kdebug(("robinnp(RobinNP_ioctl,CSTEST): Start of function\n"));
      if (copy_from_user(&testdata, (void *)arg, sizeof(int)) != 0)
      {
	kerror(("robinnp(RobinNP_ioctl,CSTEST): error from copy_from_user\n"));
	return(-RD_CFU);
      } 

      testdata++;

      if (copy_to_user(((int*)arg), &testdata, sizeof(int)) != 0) 
      {
	printk(KERN_ALERT "robinnp(RobinNP_ioctl,CSTEST): error from copy_to_user!\n");
	return(-RD_CTU);
      }
      break;

    default:
      printk(KERN_ALERT "robinnp(RobinNP_ioctl,default): Unknown ioctl %x\n", cmd);
      return(-EINVAL);
  }

  return 0;
}
/*----- End IOCTL section  ------------------------------------------------------------*/


/*-------------------------------------------------------------------------------------*/
/*------- Open  -----------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/
int RobinNP_open(struct inode *ino, struct file *file) {
	card_params_t *pdata;

	kdebug(("robinnp(RobinNP_open): called\n"));
	pdata = (card_params_t *)kmalloc(sizeof(card_params_t), GFP_KERNEL);
	if (pdata == NULL)
	{
		kerror(("robinnp(RobinNP_open): error from kmalloc\n"))
    		return(-RD_KMALLOC);
	}

	pdata->slot = -1;
	file->private_data = (char *)pdata;
	return 0;
}
/*-------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------*/
/*------ Release ----------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/

int RobinNP_Release(struct inode *ino, struct file *file) {
	card_params_t *pdata;
   int card;
   int status;

	kdebug(("robinnp(RobinNP_release): called\n"));
   status=down_interruptible(&ownerMutex);
   for (card=0; card<MAXCARDS; card++) {
      if (owner[card]==file) {
         owner[card]=0;
      }
   }
   if (status==0) {
      up(&ownerMutex);
   }
	pdata = (card_params_t *)file->private_data;
	kdebug(("robinnp(RobinNP_release): Releasing orphaned resources for slot %d\n", pdata->slot))

	kfree(file->private_data);
	return 0;
}
/*-------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/

int check_slot_device(u_int slot) {
	/* printk(KERN_ALERT "slot selected: %d\n",slot); */

	if (slot >= MAXCARDS) {
		printk(KERN_ALERT "robinnp(check_slot_device): Invalid (%d) slot number\n", slot);
		return -1;
	}

	if (cards[slot].pciDevice == NULL) {
		printk(KERN_ALERT "robinnp(check_slot_device): No card at this (%d) slot!\n", slot);
		return -1;
	}

	return 0;
}
/*-------------------------------------------------------------------------------------*/



/*-------------------------------------------------------------------------------------*/
/*------ S-Link ----------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/
void initialiseOscillator(BAR0CommonStruct *registers){
	//Reset Oscillator to default frequency (212.5 Mhz) using M_RECALL command
	u_int val = i2cReadWrite(I2C_CHAIN_REFCLK, I2C_SLAVE_REFCLK, 135, registers, I2C_WRITE, M_RECALL);
	kdebug(("robinnp(initialiseOscillator): Initialise result: %x\n",val));
	waitForClockClearance(M_RECALL, registers);

}


int i2cReadWrite(u_int chain, u_int slaveAddress, u_int memAddress, BAR0CommonStruct *registers, unsigned int readwriteSelect, u_int data)
{
	unsigned int ret = 0;
	volatile u_int value = 0;

	// We have to read out the cfg register before.
	// This might be related to the reset of WE/RE in the firmware.
	//  volatile u_int cfg = comm->i2c_new_cfg;

	//std::cout << "Reg: " << (void*)registers << std::endl;

	registers->I2CRegister.status = ((data<<8) | (memAddress));

	/* if(readwriteSelect == I2C_WRITE){ */
	/* 	kdebug(("robinnp(i2cReadWrite): Write Status check %x %x\n",registers->I2CRegister.status,((data<<8) | (memAddress)))); */
	/* } */
	/* else{ */
	/* 	kdebug(("robinnp(i2cReadWrite): Read Status check %x %x\n", registers->I2CRegister.status,((data<<8) | (memAddress)))); */
	/* } */
	writeI2CConfig(chain,slaveAddress,readwriteSelect,0/** mode */, 1/** byte_enable */, registers);

	ret =  waitI2CCompletion(registers);
	/* kdebug(("robinnp(i2cReadWrite): Completion output: %x\n", ret)); */
	value = ((registers->I2CRegister.status)>>8) & 0xff;

	return value;
}

int writeI2CConfig(u_int chainSelect, u_int slaveAddress, u_int command, u_int mode, u_int byteEnable, BAR0CommonStruct *registers)
{
	u_int cfgcmd = 0;
	unsigned int firmwareVersion = 	(registers->DesignID.DesignVersion << 24) | (registers->DesignID.DesignMajorRevision << 16) | registers->DesignID.DesignMinorRevision;

	/** Chain mapping:
	 * 0: QSFP - use mode select to choose which transciever to access
	 * 4: DDR3
	 * 5: FMC
	 * 6: RefClkGen
	 *
	 * bytes_enable:
	 * 0x1: lower byte
	 * 0x2: upper byte
	 * 0x3: both bytes
	 *
	 * mode:
	 * 0: 100 kHz
	 * 1: 400 kHz
	 * */

	if ( (chainSelect > 7) || (byteEnable > 3) || (command!=I2C_READ && command!=I2C_WRITE) || (mode>1) )
	{
		printk(KERN_ALERT "robinnp(writeI2CConfig): ERROR, Invalid Configuration");
		return 1;
	}

	if(firmwareVersion > 0x1060002){

			unsigned int selector = 0;
			switch(chainSelect){
         case 0:
            selector = 8;
            break;
         case 1:
            selector = 9;
            break;
         case 2:
            selector = 10;
            break;
			case 4:
				selector = 2;
				break;
			case 5:
				selector = 3;
				break;
			case 6:
				selector = 1;
				break;
			default:
				selector = 0;
				break;
			}

			registers->GlobalControlRegister.I2CSelect = selector;
			chainSelect = 0;
		}

	cfgcmd = (1 << chainSelect << 24) | (slaveAddress << 8) | (mode<<5) | (byteEnable<<3) | command;
	//std::cout << "Writing config: " << std::hex << cfgcmd << std::dec << std::endl;
	registers->I2CRegister.config = cfgcmd;

	return 0;
}

int waitI2CCompletion(BAR0CommonStruct *registers)
{
	while( (registers->I2CRegister.config & 0x1)==0 )
	{
      /*		msleep(1);*/
      usleep_range(50,225);
	}

	if ( registers->I2CRegister.config & (0xc0) ) /** error flags: bits 6,7 */
	{
		printk(KERN_ALERT "robinnp(waitI2CCompletion): Wait ERROR");
		return 1;
	}

	return 0;
}

void waitForClockClearance(uint8_t flag, BAR0CommonStruct *registers)
{
	/** wait for flag to be cleared by device */
	uint8_t reg135 = flag;
	while ( reg135 & flag )
	{
		reg135 = i2cReadWrite(I2C_CHAIN_REFCLK, I2C_SLAVE_REFCLK, 135, registers, I2C_READ,0);
	}
}


int getClockConfig(refclk_intcfg_t* cfg, BAR0CommonStruct *registers, long fout){

   int i;
   u_int value;
   uint64_t rfreq_int;
   u_int hs_div, n1;
   /* double fdco, fxtal, rfreq_float; */
   uint64_t ifdco, ifxtal;
   int result;

   value = i2cReadWrite(I2C_CHAIN_REFCLK, I2C_SLAVE_REFCLK, 13, registers, I2C_READ,0);
   kdebug(("robinnp(getClockConfig): Read Value register shift 13: %x\n", value));
   hs_div = ((value>>5) & 0x07) + 4;
   n1 = ((u_int)(value&0x1f))<<2;

   value = i2cReadWrite(I2C_CHAIN_REFCLK, I2C_SLAVE_REFCLK,14,registers, I2C_READ,0);
   kdebug(("robinnp(getClockConfig): Read Value register shift 14: %x\n", value));

   n1 += (value>>6)&0x03;
   n1++;

   rfreq_int = ((uint64_t)(value & 0x3f))<<((uint64_t)32);

   /** addr 15...18: RFREQ[31:0] */
   for(i=0; i<4; i++) {
      value = i2cReadWrite(I2C_CHAIN_REFCLK, I2C_SLAVE_REFCLK,i+15,registers, I2C_READ,0);
      kdebug(("robinnp(getClockConfig): Read Value register shift %d: %x\n", i + 15 , value));

      rfreq_int |= ((uint64_t)(value)) << ((3-i)*8);
   }

   if ( fout!=0 ) {
      ifdco = fout * n1 * hs_div;
      ifxtal = (SCALE2*ifdco)/((SCALE2*rfreq_int)/(1<<28));
      cfg->fdco_int = ifdco;
      cfg->fxtal_int = ifxtal;
   }

   cfg->n1 = n1;
   cfg->hs_div = hs_div;
   cfg->rfreq_int = rfreq_int;

   result =cfg->fdco_int/(cfg->n1*cfg->hs_div)/SCALE1;

   return result;
}
void computeClockConfig(refclk_intcfg_t* cfg, int newFrequency){

   // find the lowest value of N1 with the highest value of HS_DIV
   // to get fDCO in the range of 4.85...5.67 GHz
   int vco_found = 0;
   long fDCO_new;
   int n1, hs_div;
   long lastfDCO = 5670;
   
   for (n1=1; n1<=128; n1++) {
      /** N1 can be 1 or any even number up to 128 */
      if (n1!=1 && (n1 & 0x1))
         continue;

      for (hs_div=11; hs_div>3; hs_div--) {
         /** valid values for HSDIV are 4, 5, 6, 7, 9 or 11 */
         if (hs_div==8 || hs_div==10)
            continue;

         fDCO_new =  newFrequency * hs_div * n1;
         if ( fDCO_new >= 4850 && fDCO_new <= 5670 ) {
            vco_found = 1;
            /** find lowest possible fDCO for this configuration */
            if (fDCO_new<lastfDCO) {
               lastfDCO = fDCO_new;
               cfg->hs_div = hs_div;
               cfg->n1 =  n1;
               cfg->fdco_int = fDCO_new*SCALE1;
               cfg->rfreq_int = ((1<<28)*cfg->fdco_int) / cfg->fxtal_int;
            }
         }

      }
   }
}

void applyClockConfig(refclk_intcfg_t* cfg, BAR0CommonStruct *registers){

   uint8_t i= 0 ;
   uint8_t val = i2cReadWrite(I2C_CHAIN_REFCLK, I2C_SLAVE_REFCLK, 137, registers, I2C_READ,0);
   uint8_t value = 0;
   u_int loopval = 0;
   uint8_t freeze_val = 0;
   u_int res = 0;

   //std::cout << "Freeze Pre: " << val << std::endl;
   val |= FREEZE_DCO;
   //std::cout << "Freeze Post: " << val << std::endl;

   //std::cout << "Write Done" << std::endl;
   val = i2cReadWrite(I2C_CHAIN_REFCLK, I2C_SLAVE_REFCLK, 137, registers, I2C_WRITE, val);
   //std::cout << "Freeze Result: " << val << std::endl;

   // write new oscillator values
   value = ((cfg->hs_div - 4)<<5) | ((cfg->n1 - 1)>>2);
   //std::cout << "New Oscillator Value Reg 13: " << value << std::endl;

   val = i2cReadWrite(I2C_CHAIN_REFCLK, I2C_SLAVE_REFCLK, 13, registers, I2C_WRITE, value);
   //std::cout << "New Oscillator Value Reg 13 Result: " << val << std::endl;

   value = (((cfg->n1 - 1) & 0x03)<<6) | (cfg->rfreq_int>>32);
   //std::cout << "New Oscillator Value Reg 14: " << value << std::endl;

   val = i2cReadWrite(I2C_CHAIN_REFCLK, I2C_SLAVE_REFCLK, 14, registers, I2C_WRITE, value);
   //std::cout << "New Oscillator Value Reg 14 Result: " << val << std::endl;

   // addr 15...18: RFREQ[31:0]
   for(i=0; i<=3; i++) {
      value = ((cfg->rfreq_int>>((3-i)*8)) & 0xff);
      //std::cout << "New Oscillator Value Reg " << 15 + i << ": " << value << std::endl;
      loopval = i2cReadWrite(I2C_CHAIN_REFCLK, I2C_SLAVE_REFCLK, 15 + i, registers, I2C_WRITE, value);
   }

   freeze_val = i2cReadWrite(I2C_CHAIN_REFCLK, I2C_SLAVE_REFCLK, 137, registers, I2C_READ,0);

   //std::cout << "Unfreeze Pre: " << freeze_val << std::endl;
   // clear FREEZE_DCO bit
   freeze_val &= 0xef;
   //std::cout << "Unfreeze Post: " << freeze_val << std::endl;

   res = i2cDualWrite(I2C_CHAIN_REFCLK, I2C_SLAVE_REFCLK, 137, 135, registers,freeze_val, M_NEWFREQ);
   kdebug(("robinnp(applyClockConfig): Unfreeze Result: %d\n", res));
   // wait for NewFreq to be deasserted
   waitForClockClearance(M_NEWFREQ, registers);
}

int i2cDualWrite(u_int chain, u_int slaveAddress, u_int memAddress0, u_int memAddress1, BAR0CommonStruct *registers, u_int data0, u_int data1)
{
	// We have to read out the cfg register before.
	// This might be related to the reset of WE/RE in the firmware.
	//  volatile u_int cfg = comm->i2c_new_cfg;

	registers->I2CRegister.status =  (data1<<24) | (memAddress1<<16) | (data0<<8) | (memAddress0);
	kdebug(("robinnp(i2cDualWrite): Dual Write Status check %x %x\n", registers->I2CRegister.status , ((data1<<24) | (memAddress1<<16) | (data0<<8) | (memAddress0))));

	writeI2CConfig(chain,slaveAddress,I2C_WRITE,0, /** mode */3, /** byte_enable */registers);

	return waitI2CCompletion(registers);
}

