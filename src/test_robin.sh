#!/bin/bash
## Author: M. Joos, CERN

ctotest=$1

if [ -z "$1" ]; then
    echo "Error: RobinNP number missing"
    echo "Usage: "
    echo "Test the first RobinNP: test_robin 0"
    echo "Test the second RobinNP: test_robin 1"
    exit 185
fi


#echo "Testing the integrity of RobinNP #$ctotest"

if [[ ctotest -ne 0 ]] && [[ ctotest -ne 1 ]] ; then
    echo "The RobinNP number is neither 0 nor 1"
    exit 185
fi

if ! [ -e /proc/robinnp ] ; then
    echo "The robin driver seem not to be running"
    exit 185
fi

nerrors=$(grep Error /proc/robinnp | wc -l)
if [ $nerrors -ne 0 ]; then
    echo "At least one RobinNP has an internal error"
    exit 183
fi

nstring=$(grep detected /proc/robinnp)
ncards=${nstring:30:1}
#echo "There are $ncards RobinNPs in this ROS"

if [[ ctotest -eq 1 ]]  && [[ ncards -eq 1 ]] ; then
    echo "Only one RobinNP has been detected and you have asked for testing the second card"
    exit 185
fi

nstring=$(grep "temperature card $ctotest" /proc/robinnp)
ctemp=${nstring:26:2}
#echo "The temperature of the FPGA on card $ctotest is $ctemp C"
if [ $ctemp -gt 80 ]; then
    echo "RobinNP $ctotest is too hot:  $ctemp C"
    exit 183
fi

nstring=$(grep "fan speed card $ctotest" /proc/robinnp)
fanspeed=${nstring:24:5}
if [ $fanspeed -lt 8000 ]; then
    echo "The fan speed of card $ctotest is $fanspeed RPM"
    echo "The fan speed on RobinNP $ctotest is too low"
    exit 183
fi

exit 0


