/****************************************************************/
/* file:   robindriver.c					*/
/* author: Markus Joos, CERN-PH/ESS				*/
/****** C 2005 - The software with that certain something *******/


//  Description:
//  Driver for the ATLAS ROBIN PCI card
//  NOTES:
//  - This driver has been developed with SLC3 

//  Strings like P311 refer to pages (e.g.311) in the third edition of the 
//  Linux Device Drivers book

#include <linux/init.h>           //MJ: for 2.6, p30
#include <linux/module.h>
#include <linux/moduleparam.h>    //MJ: for 2.6, p30
#include <linux/kernel.h>
#include <linux/stat.h>           //MJ: for 2.6, e.g. for module_param
#include <linux/pci.h>
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/mm.h>
#include <linux/cdev.h>           //e.g. for cdev_alloc
#include <linux/slab.h>           //e.g. for kmalloc
#include <linux/delay.h>          //e.g. for udelay
#include <linux/interrupt.h>      //e.g. for request_irq
#include <linux/version.h>        
#include <linux/sched.h>          //MJ: for current->pid (first needed with SLC6)
#include <linux/seq_file.h>       //CS9
#include <asm/atomic.h>
#include <asm/io.h>
#include <asm/uaccess.h>
//#include <asm/system.h>           //e.g. for local_irq_save  //MJ: not available in CC7
#include "ROSRobin/robindriver_common.h"
#include "ROSRobin/robindriver.h"
#include "ROSRCDdrivers/tdaq_drivers.h"

#ifndef EXPORT_SYMTAB   //MJ: For 2.6: symbol seems not to be required
  #define EXPORT_SYMTAB
#endif


/*********/
/*Globals*/
/*********/
static int debug = 0, errorlog = 1, allvariants = 0, msg_fifo_avail[MAXCARDS];
static int robin_major = 0; // use dynamic allocation
static u_int maxcard;
static T_card robincard[MAXCARDS];
static T_blocklet blocklets[MAXCARDS][BLOCK_TYPES][MAX_BLOCKLETS];
static char *proc_read_text; 
static struct semaphore robinirq_semaphore;
static T_irq_info irq_info;
static atomic_t isrs_active = ATOMIC_INIT(0);


/*****************/
/*Some prototypes*/
/*****************/
static ssize_t robin_proc_write(struct file *file, const char *buffer, size_t count, loff_t *startOffset);
static int robinProbe(struct pci_dev *dev, const struct pci_device_id *id);
static void robinRemove(struct pci_dev *dev);
int robin_proc_open(struct inode *inode, struct file *file);
int robin_proc_show(struct seq_file *sfile, void *p);
static irqreturn_t robin_irq_handler(int irq, void *dev_id);


module_init(robindriver_init);  //MJ: See P39
module_exit(robindriver_exit);

MODULE_DESCRIPTION("ATLAS ROBIN");
MODULE_AUTHOR("Markus Joos, CERN/PH");
MODULE_VERSION("3.0");
MODULE_LICENSE("Private: Contact markus.joos@cern.ch");
module_param (debug, int, S_IRUGO | S_IWUSR);        //MJ: for 2.6 p37
MODULE_PARM_DESC(debug, "1 = enable debugging   0 = disable debugging"); 
module_param (errorlog, int, S_IRUGO | S_IWUSR);     //MJ: for 2.6 p37
MODULE_PARM_DESC(errorlog, "1 = enable debugging   0 = disable debugging"); 
module_param (allvariants, int, S_IRUGO | S_IWUSR);  //MJ: for 2.6 p37
MODULE_PARM_DESC(allvariants, "1 = Support all types of Robins   0 = Support just production Robins"); 

static struct file_operations fops = 
{
  .owner          = THIS_MODULE,
  .mmap           = robin_mmap,
  .unlocked_ioctl = robin_ioctl,
  .open           = robin_open,    
  .release        = robin_release,
};



//CS9
//Inspiration taken from: https://stackoverflow.com/questions/64931555/how-to-fix-error-passing-argument-4-of-proc-create-from-incompatible-pointer
//===========================================================
#if LINUX_VERSION_CODE >= KERNEL_VERSION(5,6,0)
static const struct proc_ops robin_proc_file_ops = 
{
  .proc_open    = robin_proc_open,
  .proc_write   = robin_proc_write,
  .proc_read    = seq_read,    
  .proc_lseek   = seq_lseek,	
  .proc_release = single_release
};
#else
static struct file_operations robin_proc_file_ops = 
{
  .owner   = THIS_MODULE,
  .open    = robin_proc_open,
  .write   = robin_proc_write,
  .read    = seq_read,
  .llseek  = seq_lseek,
  .release = single_release
};
#endif
//==============================================================




// memory handler functions
static struct vm_operations_struct robin_vm_ops = 
{
  close:  robin_vclose,   // mmap-close
};


/* PCI driver structures */

/* ID tables */  //MJ: See P311
static struct pci_device_id allrobinIds[] = 
{
  { PCI_DEVICE(PCI_VENDOR_ID_ROBIN3,     PCI_DEVICE_ID_ROBIN3) },        //ROBIN2, correct VID
  { PCI_DEVICE(PCI_VENDOR_ID_ROBIN3,     PCI_DEVICE_ID_ROBIN3_bad) },    //ROBIN2, wrong VID
  { PCI_DEVICE(PCI_VENDOR_ID_ROBIN2,     PCI_DEVICE_ID_ROBIN2) },        //ROBIN prototype, correct VID
  { PCI_DEVICE(PCIExUC_VENDOR_ID_ROBIN3, PCIExUC_DEVICE_ID_ROBIN3) },    //RobinExpress
  { PCI_DEVICE(PCIEx_VENDOR_ID_ROBIN3,   PCIEx_DEVICE_ID_ROBIN3) },      //RobinExpress
  { 0, },
};

static struct pci_device_id robinIds[] = 
{
  { PCI_DEVICE(PCI_VENDOR_ID_ROBIN3, PCI_DEVICE_ID_ROBIN3) },       //ROBIN2, correct VID
  { PCI_DEVICE(PCIEx_VENDOR_ID_ROBIN3,PCIEx_DEVICE_ID_ROBIN3) },    //RobinExpress
  { PCI_DEVICE(PCIExUC_VENDOR_ID_ROBIN3,PCIExUC_DEVICE_ID_ROBIN3)}, //RobinExpress
  { 0, },
};

/* The driver struct */  //MJ: See P311
static struct pci_driver __refdata robinPciDriverAll = 
{
  .name     = "robindriver",
  .id_table = allrobinIds,
  .probe    = robinProbe,
  .remove   = robinRemove,
};

static struct pci_driver __refdata robinPciDriver = 
{
  .name     = "robindriver",
  .id_table = robinIds,
  .probe    = robinProbe,
  .remove   = robinRemove,
};


/*********************************************************/
int robin_proc_open(struct inode *inode, struct file *file) 
/*********************************************************/
{
  return single_open(file, robin_proc_show, NULL);
}


/*******************************/
static int robindriver_init(void)
/*******************************/
{
  int result, i;
  static struct proc_dir_entry *robin_file;

  kerror(("robin driver version 2.5\n")); 
  sema_init(&robinirq_semaphore, 0);    // Initialize the IRQ semaphore to 0 (i.e. no interrupt seen)
  kerror(("IRQ semaphore initialized\n")); 

  // Initialise the robin card map. This is used to assign the slot parameter to a ROBIN board when calling ioctl(link)
  for (i = 0; i < MAXCARDS; i++) 
  {
    kdebug(("robin(robindriver_init): Initializing data structure for Robin %d\n", i)); 
    robincard[i].pciDevice = NULL;
    robincard[i].pci_memaddr_plx = 0;
    robincard[i].pci_memaddr_bus = 0;
    robincard[i].pci_memsize_plx = 0; 
    robincard[i].pci_memsize_bus = 0;
    robincard[i].mem_base = 0;
    robincard[i].mem_size = 0;
    robincard[i].plx_regs = NULL;
    robincard[i].fpga_regs = NULL;
    robincard[i].nrols = 0;
    robincard[i].bar2 = 0;
    robincard[i].irqnumber = 0;
    robincard[i].card_type = ROBIN_UNKNOWN;
    robincard[i].fpgaConfigured = 0;  //Will be set in INIT_DMA

    irq_info.ncards      = 0;
    irq_info.ch0_id[i]   = 0;
    irq_info.ch0_type[i] = 0;
    irq_info.ch1_id[i]   = 0;
    irq_info.ch1_type[i] = 0;
    irq_info.ch2_id[i]   = 0;
    irq_info.ch2_type[i] = 0;
  }

  maxcard = 0;

  kdebug(("robin(robindriver_init): registering PCIDriver. (allvariants = %d)\n", allvariants));
 
  if (allvariants)
  {
    kdebug(("robin(robindriver_init): Supporting all ROBINs\n"));
    result = pci_register_driver(&robinPciDriverAll);  //MJ: See P312
  }
  else
  {
    kdebug(("robin(robindriver_init): registering Just supporting production ROBINs\n"));
    result = pci_register_driver(&robinPciDriver);     //MJ: See P312
  }
  
  kdebug(("robin(robindriver_init): result of pci_register_driver is %d\n", result));
  if (result < 0)
  {
    kerror(("robin(robindriver_init): Warning! no PCI devices found!\n"));
    result = -RD_EIO;
    goto fail1;
  }
  else 
  {
    kerror(("robin(robindriver_init): Found some devices! (result = %d)\n", result));
  }

  result = register_chrdev(robin_major, "robin", &fops); //MJ: This is old fashioned. Update for 2.6. See P57
  if (result < 1)
  {
    kerror(("robin(robindriver_init): registering robin driver failed.\n"));
    result = -RD_EIO2;
    goto fail2;
  }
  robin_major = result;

  proc_read_text = (char *)kmalloc(PROC_MAX_CHARS, GFP_KERNEL);
  if (proc_read_text == NULL)
  {
    kerror(("robin(robindriver_init): error from kmalloc\n"));
    result = -RD_KMALLOC;
    goto fail3;
  }

  robin_file = proc_create("robin", 0644, NULL, &robin_proc_file_ops);
  if (robin_file == NULL)
  {
    kerror(("robin(robindriver_init): error from call to create_proc_entry\n"));
    result = -RD_PROC;
    goto fail4;
  }

  kdebug(("robin(robindriver_init): driver loaded; major device number = %d\n", robin_major));
  return(0);

  fail4:
    kfree(proc_read_text);
    
  fail3:
    unregister_chrdev(robin_major, "robin"); 

  fail2:
    if (allvariants)
      pci_unregister_driver(&robinPciDriverAll);
    else    
      pci_unregister_driver(&robinPciDriver);
    
  fail1:
    return(result);
}


/********************************/
static void robindriver_exit(void)
/********************************/
{
  unregister_chrdev(robin_major, "robin");
  
  kfree(proc_read_text);
  remove_proc_entry("robin", NULL);
  if (allvariants)
    pci_unregister_driver(&robinPciDriverAll);
  else
    pci_unregister_driver(&robinPciDriver);
  kdebug(("robin(cleanup_module): driver removed\n"));
}


/************************/
/* PCI driver functions */
/************************/

/************************************************************************/
static int robinProbe(struct pci_dev *dev, const struct pci_device_id *id) 
/************************************************************************/
{
  int i;
  u_long flags;
  u_int ret, regdata, baddr, size, card, bar2Available;

  // Check which type of ROBIN is installed
  kdebug(("robin(robinProbe): called for device: 0x%04x / 0x%04x\n", dev->vendor, dev->device));
  kdebug(("robin(robinProbe): Bus: 0x%04x / Devfn 0x%04x\n", dev->bus->number, dev->devfn));

  //MJ: Should we check if the FPGA is loaded and exit if not?
  
  /* Find a new "slot" for the card */
  for (i = 0, card = -1; (i < MAXCARDS) && (card == -1); i++) 
  {
    if (robincard[i].pciDevice == NULL) 
    {
      card = i;
      robincard[card].pciDevice = dev;
    }
  }

  if (card == -1) 
  {
    kerror(("robin(robinProbe): Could not find space in the robincard array for this card."));
    return(-RD_TOOMANYCARDS);
  }  

  flags = pci_resource_flags(dev, PLX_BAR);  //MJ: See P317
  if ((flags & IORESOURCE_MEM) != 0) 
  {
    baddr = pci_resource_start(dev, PLX_BAR) & PCI_BASE_ADDRESS_MEM_MASK;  //MJ: See P317
    size = pci_resource_end(dev, PLX_BAR) - pci_resource_start(dev, PLX_BAR);    //MJ: See P317
    kdebug(("robin(robinProbe): PLX: Mapping %d bytes at PCI address 0x%08x\n", size, baddr));
    robincard[card].plx_regs = (u_int *)ioremap(baddr, size);
    if (robincard[card].plx_regs == NULL)
    {
      kerror(("robin(robinProbe): error from ioremap for robincard[%d].plx_regs\n", card));
      robincard[card].pciDevice = NULL;
      return(-RD_REMAP);
    }
    kdebug(("robin(robinProbe): robincard[%d].plx_regs is at 0x%08lx\n", card, (u_long)robincard[card].plx_regs));

    robincard[card].pci_memaddr_plx = baddr;
    robincard[card].pci_memsize_plx = size;
  }
  else 
  {
    kerror(("robin(robinProbe): Bar 0 is not a memory resource"));
    return(-RD_BAR);
  }

  flags = pci_resource_flags(dev, FPGA_BAR);
  bar2Available = 1;
  if ((flags & IORESOURCE_MEM) != 0) 
  {
    baddr = pci_resource_start(dev, FPGA_BAR) & PCI_BASE_ADDRESS_MEM_MASK;
    size = pci_resource_end(dev, FPGA_BAR) - pci_resource_start(dev, FPGA_BAR);
    kdebug(("robin(robinProbe): FPGA: Mapping %d bytes at PCI address 0x%08x\n", size, baddr));
    robincard[card].fpga_regs = (u_int *)ioremap(baddr, size);
    robincard[card].bar2 = baddr;
    if (robincard[card].fpga_regs == NULL)
    {
      kerror(("robin(robinProbe): error from ioremap for robincard[%d].fpga_regs\n", card));
      return(-RD_REMAP);
    }
    kdebug(("robin(robinProbe): robincard[%d].fpga_regs is at 0x%08lx\n", card, (u_long)robincard[card].fpga_regs));
    
    robincard[card].pci_memaddr_bus = baddr;
    robincard[card].pci_memsize_bus = size;
  }
  else 
  {
    kerror(("robin(robinProbe): Warning! Bar 2 is not available"));
    bar2Available = 0;
  }

  //MJ: bar2Available is there for the rare case that a Robin is used un-initialized for production tests
  if ((dev->device == 0x188) || (dev->device == PCIExUC_VENDOR_ID_ROBIN3))
  {
    kdebug(("robin(robinProbe): PCI Express ROBIN detected\n"));
    robincard[card].card_type = ROBIN_EXPRESS;
    robincard[card].nrols = 3;
  }
  else if ((dev->device == 0x144) || (dev->device == 0x324))
  {
    kdebug(("robin(robinProbe): Classic ROBIN detected\n"));
    robincard[card].card_type = ROBIN_CLASSIC;
    robincard[card].nrols = 3;
  }  
  else if ((dev->device == 0x9656) && (bar2Available = 1)) 
  {
    kdebug(("robin(robinProbe): ROBIN prototype detected\n"));
    robincard[card].card_type = ROBIN_CLASSIC;
    robincard[card].nrols = 2;
  }
  else  
  {
    kerror(("robin(robinProbe): Unknown ROBIN detected\n"));
  }
  
  //Enable the device (See p314) and get the interrupt line

  ret = pci_enable_device(dev);
  kdebug(("robin(robinProbe): Robin %d: interrupt line = %d \n", maxcard, dev->irq))

  robincard[card].irqnumber = dev->irq;

  if (request_irq(dev->irq, robin_irq_handler, IRQF_SHARED, "robin", dev))	
  {
    kerror(("robin(robinProbe): Robin %d: request_irq failed on IRQ = %d\n", maxcard, dev->irq))
    return(-RD_REQIRQ);
  }
  kerror(("robin(robinProbe): request_irq OK for line %d and dev at 0x%08lx\n", dev->irq, (u_long)dev))

  //Enable the doorbell interrupts  
  robincard[card].plx_regs[PLX_L2PDBELL] = 0;
  regdata = robincard[card].plx_regs[PLX_INTCSR];
  regdata |= 0x300;
  robincard[card].plx_regs[PLX_INTCSR] = regdata;

  maxcard += 1;

  return(0);
}


/******************************************/
static void robinRemove(struct pci_dev *dev) 
/******************************************/
{
  int i;
  u_int regdata, card;

  kdebug(("robin(robinRemove): Function called\n"));
  /* Find the "slot" of the card */
  for (i = 0, card = -1; (i < MAXCARDS) && (card == -1); i++) 
  {
    if (robincard[i].pciDevice == dev) 
      card = i;
  }
  
  if (card == -1) 
  {
    kerror(("robin(robinRemove): Could not find device to remove.\n"));
    return;
  }

  //Disable the doorbell interrupts 
  free_irq(dev->irq, dev);	
  kerror(("robin(robinRemove): free_irq called for IRQ line %d and dev at 0x%08lx\n", dev->irq, (u_long)dev));

  regdata = robincard[card].plx_regs[PLX_INTCSR];
  regdata &= 0xfffffcff;
  robincard[card].plx_regs[PLX_INTCSR] = regdata;

  iounmap((void *)robincard[card].plx_regs);
  iounmap((void *)robincard[card].fpga_regs);

  robincard[card].pciDevice = NULL;
  robincard[card].pci_memaddr_plx = 0;
  robincard[card].pci_memaddr_bus = 0;
  robincard[card].pci_memsize_plx = 0; 
  robincard[card].pci_memsize_bus = 0;
  robincard[card].mem_base = 0;
  robincard[card].mem_size = 0;
  robincard[card].plx_regs = NULL;
  robincard[card].fpga_regs = NULL;
  robincard[card].nrols = 0;
  robincard[card].bar2 = 0;
  robincard[i].fpgaConfigured = 0;

  maxcard -= 1;
}



/*****************************/
/* Standard driver functions */
/*****************************/


/*********************************************************/
static int robin_open(struct inode *ino, struct file *file)
/*********************************************************/
{
  u_int loop;
  T_private_data *pdata;

  kdebug(("robin(robin_open): called from process %d\n", current->pid))
  pdata = (T_private_data *)kmalloc(sizeof(T_private_data), GFP_KERNEL);
  if (pdata == NULL)
  {
    kerror(("robin(robin_open): error from kmalloc\n"))
    return(-RD_KMALLOC);
  }

  //I guess we have to protect the code from being executed by parallel threads on a SMP machine. Therefore:
  sema_init(&pdata->thread_sema, 1);

  for(loop = 0; loop < MAXCARDS; loop++)
  {
    pdata->dpm_handle[loop] = -1;
    pdata->mem_handle[loop] = -1;
    pdata->fifo_handle[loop] = 0;
  }

  pdata->slot = -1;
   
  file->private_data = (char *)pdata;
  return(0);
}


/************************************************************/
static int robin_release(struct inode *ino, struct file *file)
/************************************************************/
{
  T_private_data *pdata;
  u_int ret;
  
  kdebug(("robin(robin_release): called from process %d\n", current->pid))

  pdata = (T_private_data *)file->private_data;
  kdebug(("robin(robin_release): Releasing orphaned resources for slot %d\n", pdata->slot))

  if (pdata->slot != -1) 
  {
    if (pdata->fifo_handle[pdata->slot] != 0)
    {  
      kdebug(("robin(robin_release): Releasing %d FIFO slots for card %d\n", pdata->fifo_handle[pdata->slot], pdata->slot))
      msg_fifo_avail[pdata->slot] += pdata->fifo_handle[pdata->slot];
    }
    
    if (pdata->dpm_handle[pdata->slot] != -1)
    {  
      kdebug(("robin(robin_release): Freeing handle %d of the DPM\n", pdata->dpm_handle[pdata->slot]))
      ret = retmem(pdata->slot, 0, pdata->dpm_handle[pdata->slot]);
      if (ret)
	kerror(("robin(robin_release): retmem for DPM returned %d\n", ret));
    }
    
    if (pdata->mem_handle[pdata->slot] != -1)
    { 
      kdebug(("robin(robin_release): Freeing handle %d of the PC MEM\n", pdata->mem_handle[pdata->slot]))
      ret = retmem(pdata->slot, 1, pdata->mem_handle[pdata->slot]);
      if (ret)
	kerror(("robin(robin_release): retmem for PC MEM returned %d\n", ret));
    }
  }

  kfree(file->private_data);
  kdebug(("robin(robin_release): kfreed file private data @ %lx\n", (u_long)file->private_data))
  file->private_data = NULL;	// Required?
  return(0);
}


/***************************************************************/
static long robin_ioctl(struct file *file, u_int cmd, u_long arg)
/***************************************************************/
{
  T_private_data *pdata;
  int slot;
  u_int ret;
  
  pdata = (T_private_data *)file->private_data;
  kdebug(("robin(ioctl): file at 0x%08lx, file private data at 0x%08lx\n", (u_long)file, (u_long)pdata))

  switch (cmd)
  { 
    case LINK:
    {
      kdebug(("robin(ioctl,LINK): called from process %d\n", current->pid))
      
      //As pdata->link_params has to be shared by all threads that are derived from the same
      //open(/dev/robin) call we have to avoid that two threads use it at the same time. Therefore:
      down(&pdata->thread_sema);
      
      if (copy_from_user(&pdata->link_params, (void *)arg, sizeof(T_link_params)) !=0)
      {
	kerror(("robin(ioctl,LINK): error from copy_from_user\n"));
        up(&pdata->thread_sema);
	return(-RD_CFU);
      }      

      if (robincard[pdata->link_params.slot].pciDevice == NULL) 
      {
        up(&pdata->thread_sema);
	return(-RD_NOCARD);
      }
      
      kdebug(("robin(ioctl,LINK): params.slot      = %d\n", pdata->link_params.slot))
      pdata->link_params.plx_base  = robincard[pdata->link_params.slot].pci_memaddr_plx;  
      pdata->link_params.plx_size  = robincard[pdata->link_params.slot].pci_memsize_plx;
      pdata->link_params.fpga_base = robincard[pdata->link_params.slot].pci_memaddr_bus;
      pdata->link_params.fpga_size = robincard[pdata->link_params.slot].pci_memsize_bus;
      pdata->link_params.nrols = robincard[pdata->link_params.slot].nrols;
      kdebug(("robin(ioctl,LINK): params.nrols     = 0x%08x\n", robincard[pdata->link_params.slot].nrols))
      kdebug(("robin(ioctl,LINK): params.plx_base  = 0x%08x\n", pdata->link_params.plx_base))
      kdebug(("robin(ioctl,LINK): params.plx_size  = 0x%08x\n", pdata->link_params.plx_size))
      kdebug(("robin(ioctl,LINK): params.fpga_base = 0x%08x\n", pdata->link_params.fpga_base))
      kdebug(("robin(ioctl,LINK): params.fpga_size = 0x%08x\n", pdata->link_params.fpga_size))

      if (copy_to_user((void *)arg, &pdata->link_params, sizeof(T_link_params)) != 0)
      {
	kerror(("robin(ioctl,LINK): error from copy_to_user\n"));
        up(&pdata->thread_sema);
	return(-RD_CTU);
      }

      up(&pdata->thread_sema);
      kdebug(("robin(ioctl,LINK): OK\n"))
      break;
    }

    case INIT_DMA:
    {
      kdebug(("robin(ioctl,INIT_DMA): called from process %d\n", current->pid))
      //This ioctl will only be called one from "robinconfigure". Therefore SMP and race conditions are not an issue            

      if (copy_from_user(&pdata->init_dma_params, (void *)arg, sizeof(T_init_dma_params)) !=0)
      {
	kerror(("robin(ioctl,INIT_DMA): error from copy_from_user\n"));
	return(-RD_CFU);
      } 

      //MJ: This is a bit of a trick. Before calling INIT_DMA "robinconfigure" checks via Jtag if the FPGA is loaded.
      //Therefore proc_read and the TTY driver will only work once "robinconfigure" has been run
      robincard[pdata->init_dma_params.slot].fpgaConfigured = 1;

      kdebug(("robin(ioctl,INIT_DMA): link_params.slot      = %d\n", pdata->init_dma_params.slot))
      kdebug(("robin(ioctl,INIT_DMA): link_params.mem_base  = 0x%08x\n", pdata->init_dma_params.mem_base))
      kdebug(("robin(ioctl,INIT_DMA): link_params.mem_size  = 0x%08x\n", pdata->init_dma_params.mem_size))
      robincard[pdata->init_dma_params.slot].mem_base = pdata->init_dma_params.mem_base;
      robincard[pdata->init_dma_params.slot].mem_size = pdata->init_dma_params.mem_size;

      //Initialize the memory manager
      initmem(pdata->init_dma_params.slot, pdata->init_dma_params.mem_size, pdata->init_dma_params.mem_base);

      break;
    }

    case MSG_REG:
    {
      kdebug(("robin(ioctl,MSG_REG): called from process %d\n", current->pid));
      
      //As pdata->msg_req_params has to be shared by all threads that are derived from the same
      //open(/dev/robin) call we have to avoid that two threads use it at the same time. Therefore:
      down(&pdata->thread_sema);

      if (copy_from_user(&pdata->msg_req_params, (void *)arg, sizeof(T_msg_req_params)) !=0)
      {
	kerror(("robin(ioctl,LINK): error from copy_from_user\n"));
        up(&pdata->thread_sema);
	return(-RD_CFU);
      }    
      
      kdebug(("robin(ioctl,MSG_REG): msg_req_params.slot     = %d\n", pdata->msg_req_params.slot))
      kdebug(("robin(ioctl,MSG_REG): msg_req_params.n_fifo   = %d\n", pdata->msg_req_params.n_fifo))
      kdebug(("robin(ioctl,MSG_REG): msg_req_params.dpm_size = %d\n", pdata->msg_req_params.dpm_size))
      kdebug(("robin(ioctl,MSG_REG): msg_req_params.mem_size = %d\n", pdata->msg_req_params.mem_size))
      
      //MJ: If pdata->slot already has a value assigned we should return an error
      pdata->slot = pdata->msg_req_params.slot;
      
      if ((pdata->dpm_handle[pdata->msg_req_params.slot] != -1) || (pdata->mem_handle[pdata->msg_req_params.slot] != -1))
      {
        kerror(("robin(ioctl,MSG_REG): This process has already requested message resources for card %d\n", pdata->msg_req_params.slot));
        kerror(("robin(ioctl,MSG_REG): pdata->dpm_handle[%d] = %d\n", pdata->msg_req_params.slot, pdata->dpm_handle[pdata->msg_req_params.slot]));
        kerror(("robin(ioctl,MSG_REG): pdata->mem_handle[%d] = %d\n", pdata->msg_req_params.slot, pdata->mem_handle[pdata->msg_req_params.slot]));
        up(&pdata->thread_sema);
	return(-RD_ALREADYDONE);
      }
      
      //Get a buffer in the DPM
      ret = getmem(pdata->msg_req_params.slot, 0, pdata->msg_req_params.dpm_size, &pdata->msg_req_params.dpm_base, &pdata->dpm_handle[pdata->msg_req_params.slot]);
      if (ret)
      {
        kerror(("robin(ioctl,MSG_REG): getmem for DPM returned %d\n", ret));
        up(&pdata->thread_sema);
	return(ret);
      } 

      //Add the PCI base address of the DPM
      pdata->msg_req_params.dpm_offset = pdata->msg_req_params.dpm_base;
      pdata->msg_req_params.dpm_base += (robincard[pdata->msg_req_params.slot].pci_memaddr_bus + MESSAGE_RAM_OFFSET);
      kdebug(("robin(ioctl,MSG_REG): getmem returns for DPM: base=0x%08x  handle=%d\n", pdata->msg_req_params.dpm_base, pdata->dpm_handle[pdata->msg_req_params.slot]))
      kdebug(("robin(ioctl,MSG_REG): pdata->msg_req_params.dpm_offset = 0x%08x\n", pdata->msg_req_params.dpm_offset))

      //Get a buffer in the PC MEM
      ret = getmem(pdata->msg_req_params.slot, 1, pdata->msg_req_params.mem_size, &pdata->msg_req_params.mem_base, &pdata->mem_handle[pdata->msg_req_params.slot]);
      if (ret)
      {
        kerror(("robin(ioctl,MSG_REG): getmem for PC MEM returned %d\n", ret));
        up(&pdata->thread_sema);
	return(ret);
      } 
      kdebug(("robin(ioctl,MSG_REG): getmem returns for MEM: base=0x%08x  handle=%d\n", pdata->msg_req_params.mem_base, pdata->mem_handle[pdata->msg_req_params.slot]))

      pdata->msg_req_params.mem_offset = pdata->msg_req_params.mem_base - robincard[pdata->msg_req_params.slot].mem_base;
      kdebug(("robin(ioctl,MSG_REG): pdata->msg_req_params.mem_base   = 0x%08x\n", pdata->msg_req_params.mem_base))
      kdebug(("robin(ioctl,MSG_REG): robincard[%d].mem_base           = 0x%08x\n", pdata->msg_req_params.slot, robincard[pdata->msg_req_params.slot].mem_base))
      kdebug(("robin(ioctl,MSG_REG): pdata->msg_req_params.mem_offset = 0x%08x\n", pdata->msg_req_params.mem_offset))

      //Get FIFO slots
      kdebug(("robin(ioctl,MSG_REG): card=%d   FIFO entries available=%d   FIFO entries requested=%d\n", pdata->msg_req_params.slot, msg_fifo_avail[pdata->msg_req_params.slot], pdata->msg_req_params.n_fifo));
      if (msg_fifo_avail[pdata->msg_req_params.slot] < pdata->msg_req_params.n_fifo)
      {
        kerror(("robin(ioctl,MSG_REG): card=%d   FIFO entries requested=%d   FIFO entries available=%d\n", pdata->msg_req_params.slot, msg_fifo_avail[pdata->msg_req_params.slot], pdata->msg_req_params.n_fifo));
        up(&pdata->thread_sema);
	return(-RD_FIFOEMPTY);
      }
      else
      {
	pdata->fifo_handle[pdata->msg_req_params.slot] = pdata->msg_req_params.n_fifo;
	msg_fifo_avail[pdata->msg_req_params.slot] -= pdata->msg_req_params.n_fifo;
	kdebug(("robin(ioctl,MSG_REG): %d entries in MSG FIFO allocated on card %d\n", pdata->msg_req_params.n_fifo, pdata->msg_req_params.slot))
      }

      if (copy_to_user((void *)arg, &pdata->msg_req_params, sizeof(T_msg_req_params)) != 0)
      {
	kerror(("robin(ioctl,MSG_REG): error from copy_to_user\n"));
        up(&pdata->thread_sema);
	return(-RD_CTU);
      }
      
      up(&pdata->thread_sema);
      break;
    }

    case MSG_FREE:
    {
      kdebug(("robin(ioctl,MSG_FREE): called from process %d\n", current->pid))

      if (copy_from_user(&slot, (void *)arg, sizeof(int)) !=0)
      {
	kerror(("robin(ioctl,MSG_FREE): error from copy_from_user\n"));
	return(-RD_CFU);
      } 
            
      kdebug(("robin(ioctl,MSG_FREE): card = %d\n", slot))
      kdebug(("robin(ioctl,MSG_FREE): Freeing %d entries in the FIFO\n", pdata->fifo_handle[slot]))
      msg_fifo_avail[slot] += pdata->fifo_handle[slot];
      pdata->fifo_handle[slot] = 0;
      
      kdebug(("robin(ioctl,MSG_FREE): Freeing handle %d of the DPM\n", pdata->dpm_handle[slot]))
      ret = retmem(slot, 0, pdata->dpm_handle[slot]);
      if (ret)
      {
        kerror(("robin(ioctl,MSG_FREE): retmem for DPM returned %d\n", ret));
	return(ret);
      } 
      
      kdebug(("robin(ioctl,MSG_FREE): Freeing handle %d of the PC MEM\n", pdata->mem_handle[slot]))
      ret = retmem(slot, 1, pdata->mem_handle[slot]);
      if (ret)
      {
        kerror(("robin(ioctl,MSG_FREE): retmem for PC MEM returned %d\n", ret));
	return(ret);
      } 
    
      pdata->dpm_handle[slot] = -1;
      pdata->mem_handle[slot] = -1;
      kdebug(("robin(ioctl,MSG_FREE): pdata->dpm_handle[%d] = %d\n", slot, pdata->dpm_handle[slot]))
      kdebug(("robin(ioctl,MSG_FREE): pdata->mem_handle[%d] = %d\n", slot, pdata->mem_handle[slot]))

      break;
    }
    
    case GETPROC:
    {
      u_int len;
      kdebug(("robin(ioctl,GETPROC): called from process %d\n", current->pid))

      kerror(("robin(ioctl,GETPROC): This function has been disabled for reasons of lazyness. Contact M. Joos if you need the function to work.\n"));
      len = 0;
      
      //len = fill_proc_read_text();  //MJ: Disabled to facilitate the porting of the proc_read function to seq_files.

      if (copy_to_user((void *)arg, proc_read_text, len) != 0)
      {
	kerror(("robin(ioctl,GETPROC): error from copy_to_user\n"));
	return(-RD_CTU);
      }

      break;
    }
        
    case WAIT_IRQ:
    {
      u_long flags;
      int card;
      T_irq_info local_irq_info;
      
      kdebug(("robin(ioctl,WAIT_IRQ): ioctl called. semaphore = %d\n", robinirq_semaphore.count))
      //robinirq_semaphore is a counting semaphore. If the driver receives interrups when no process is waiting for them
      //the semaphore will get a value > 1. This is problematic because the BELL and MBOX registers only contain
      //meaningfull information about the last interrupt. Therefore we do not want any "history" in the semaphore.
      sema_init(&robinirq_semaphore, 0);    
      kdebug(("robin(ioctl,WAIT_IRQ): ioctl called. semaphore initialized and waiting.....\n"))
      
      ret = down_interruptible(&robinirq_semaphore);	// wait for an irq. Wait forever if necessary

      if (signal_pending(current))
      {						// interrupted system call
        kerror(("robin(ioctl,WAIT_IRQ): Interrupted by signal\n"))
        return(-RD_INTBYSIGNAL);		// or ERESTARTSYS ?
      }

      kdebug(("robin(ioctl,WAIT_IRQ): ioctl called. ........interrupt received\n"))
      kdebug(("robin(ioctl,WAIT_IRQ): ioctl called. After waiting: semaphore = %d\n", robinirq_semaphore.count))

      //We have seen the interrupt. Now lets make sure we don't get a race condition with another interrupt
      local_irq_save(flags);  //MJ: for 2.6 see p274 & p275

      //Copy the IRQ information. This is to reduce the time interrupts are disabled. Apparently Linux does not like a 
      //call to "copy_to_user" when interrupts are disabled.
      
      local_irq_info.ncards = maxcard;
      kdebug(("robin(ioctl,WAIT_IRQ): local_irq_info.ncards = %d\n", local_irq_info.ncards))
      for (card = 0; card < maxcard; card++)
      {    
	local_irq_info.ch0_type[card] = irq_info.ch0_type[card];
        local_irq_info.ch0_id[card]   = irq_info.ch0_id[card];
        local_irq_info.ch1_type[card] = irq_info.ch1_type[card];
        local_irq_info.ch1_id[card]   = irq_info.ch1_id[card];
        local_irq_info.ch2_type[card] = irq_info.ch2_type[card];
        local_irq_info.ch2_id[card]   = irq_info.ch2_id[card];

	irq_info.ch0_type[card] = NO_EVENT;
	irq_info.ch0_id[card]   = 0;
	irq_info.ch1_type[card] = NO_EVENT;
	irq_info.ch1_id[card]   = 0;
	irq_info.ch2_type[card] = NO_EVENT;
	irq_info.ch2_id[card]   = 0;
      
	kdebug(("robin(ioctl,WAIT_IRQ): local_irq_info.ch0_type[%d] = %d\n", card, local_irq_info.ch0_type[card]))
	kdebug(("robin(ioctl,WAIT_IRQ): local_irq_info.ch0_id[%d]   = %d\n", card, local_irq_info.ch0_id[card]))
	kdebug(("robin(ioctl,WAIT_IRQ): local_irq_info.ch1_type[%d] = %d\n", card, local_irq_info.ch1_type[card]))
	kdebug(("robin(ioctl,WAIT_IRQ): local_irq_info.ch1_id[%d]   = %d\n", card, local_irq_info.ch1_id[card]))
	kdebug(("robin(ioctl,WAIT_IRQ): local_irq_info.ch2_type[%d] = %d\n", card, local_irq_info.ch2_type[card]))
	kdebug(("robin(ioctl,WAIT_IRQ): local_irq_info.ch2_id[%d]   = %d\n", card, local_irq_info.ch2_id[card]))
      }  

      //Re-enable interrupts
      local_irq_restore(flags);

      if (copy_to_user((void *)arg, &local_irq_info, sizeof(T_irq_info)))
      {
	kerror(("robin(ioctl,WAIT_IRQ): error from copy_to_user\n"))
	return(-RD_CTU);
      }     
      break;
    }

    default:
    {
      kerror(("robin(ioctl,default): You should not be here. Command 0x%x is not known\n", cmd))
      //kerror(("robin(ioctl,default): LINK     = 0x%x\n", LINK))
      //kerror(("robin(ioctl,default): INIT_DMA = 0x%x\n", INIT_DMA))
      //kerror(("robin(ioctl,default): MSG_REG  = 0x%x\n", MSG_REG))
      //kerror(("robin(ioctl,default): MSG_FREE = 0x%x\n", MSG_FREE))
      //kerror(("robin(ioctl,default): GETPROC  = 0x%x\n", GETPROC))
      //kerror(("robin(ioctl,default): WAIT_IRQ = 0x%x\n", WAIT_IRQ))

      return(-EINVAL);
    }
  }   
  return(0);
}


/*******************************************************************************************************/
static ssize_t robin_proc_write(struct file *file, const char *buffer, size_t count, loff_t *startOffset)
/*******************************************************************************************************/
{
  int len;
  char value[100];

  kdebug(("robin(robin_write_procmem): robin_write_procmem called\n"));

  if(count > 99)
    len = 99;
  else
    len = count;

  if (copy_from_user(value, buffer, len))
  {
    kerror(("robin(robin_write_procmem): error from copy_from_user\n"));
    return(-RD_CFU);
  }

  kdebug(("robin(robin_write_procmem): len = %d\n", len));
  value[len - 1] = '\0';
  kdebug(("robin(robin_write_procmem): text passed = %s\n", value));

  if (!strcmp(value, "debug"))
  {
    debug = 1;
    kdebug(("robin(robin_write_procmem): debugging enabled\n"));
  }

  if (!strcmp(value, "nodebug"))
  {
    kdebug(("robin(robin_write_procmem): debugging disabled\n"));
    debug = 0;
  }

  if (!strcmp(value, "elog"))
  {
    kdebug(("robin(robin_write_procmem): Error logging enabled\n"))
    errorlog = 1;
  }

  if (!strcmp(value, "noelog"))
  {
    kdebug(("robin(robin_write_procmem): Error logging disabled\n"))
    errorlog = 0;
  }
  
  return(len);
}


/**************************************************/
int robin_proc_show(struct seq_file *sfile, void *p)
/**************************************************/
{
  //MJ-SMP: protect this function (preferrably with a spinlock)???
  u_int mbox_data, perr, ret, len, card, fpgadesign, value, v1, v2, v3, v4, v5, v6, loop1, loop2;
  u_short sdata;
  
  kdebug(("robin(robin_proc_show): Creating text....\n"));

  len = 0;
  seq_printf(sfile, "\n\n\nROBIN driver (CS9 ready) for TDAQ release: %s (based on CVS tag %s)\n", RELEASE_NAME, CVSTAG);
  seq_printf(sfile, "\nSupported FPGA F/W: PCI=0x%08x, PCIe=0x%08x\n", DESIGN_ID_P, DESIGN_ID_S);

  seq_printf(sfile, "Number of cards detected = %d\n", maxcard);
  seq_printf(sfile, "\nSome PLX registers\n");

  perr = 0;

  seq_printf(sfile, "card |      MBOX2 |       DMRR |     DMLBAM |     DMPBAM |     INTCSR |   PCI_STATUS|\n");
  for (card = 0; card < maxcard; card++)
  {
    mbox_data = robincard[card].plx_regs[PLX_MBOX2];
    seq_printf(sfile, "%4d |", card);
    seq_printf(sfile, " 0x%08x |", mbox_data);
    seq_printf(sfile, " 0x%08x |", robincard[card].plx_regs[PLX_DMRR]);
    seq_printf(sfile, " 0x%08x |", robincard[card].plx_regs[PLX_DMLBAM]);
    seq_printf(sfile, " 0x%08x |", robincard[card].plx_regs[PLX_DMPBAM]);
    seq_printf(sfile, " 0x%08x |", robincard[card].plx_regs[PLX_INTCSR]);
    ret = pci_read_config_word(robincard[card].pciDevice, PCI_STATUS, &sdata);
    
    if (ret)
    {
      kerror(("robin(robin_proc_show): ERROR from pci_read_config_word\n"));
      seq_printf(sfile, "UNREADABLE\n");
    }
    else
    {
      seq_printf(sfile, "      0x%04x |\n", sdata);
      if (sdata & 0x8000)
      {
	perr = 1;      
        seq_printf(sfile, " Error: Card %d has a parity error\n", card);
      }
    }  
    if (mbox_data != 0x0055aa00)
      seq_printf(sfile, " Error: MBOX2 of card %d is not 0x0055aa00\n", card);
  }
  
  if(perr)
    seq_printf(sfile, "ATTENTION: THERE WAS A PARITY ERROR ON ONE ROBIN\n");
    
  seq_printf(sfile, "\nSome other registers\n");
  seq_printf(sfile, "card | FPGA design ID |       BAR2 |\n");
  for (card = 0; card < maxcard; card++)
  {
    seq_printf(sfile, "%4d |", card);
    ret = fpgaLoaded(card, &fpgadesign);
    if(ret == 0)
    {
      seq_printf(sfile, "     0x%08x |", robincard[card].fpga_regs[0]);
      seq_printf(sfile, " 0x%08x |\n", robincard[card].bar2);
    }
    else if (ret == -1)
    {
      seq_printf(sfile, "Error: robinconfig has not yet been run on this Robin\n");
    }
    else
    {
      seq_printf(sfile, "Error: The FPGA of this card (0x%08x) is out of date\n", fpgadesign);
    }
  }
  
  seq_printf(sfile, "\nS-Link status bits\n");
  seq_printf(sfile, "Card | ROL | Link err. | Link up | Xoff | Link active | Test mode | ROL Emulation |\n");
  for (card = 0; card < maxcard; card++)
  {
    ret = fpgaLoaded(card, &fpgadesign);
    if(ret == 0)
    {
      value = robincard[card].fpga_regs[SLINK_REG_OFFSET >> 2];

      v1 = (value >> 0) & 0x1;
      v2 = (value >> 1) & 0x1;
      v3 = (value >> 2) & 0x1;
      v4 = (value >> 3) & 0x1;
      v5 = (value >> 15) & 0x1;
      v6 = (value >> 12) & 0x1;
      seq_printf(sfile, "   %d |   0 |       %s |     %s |  %s |         %s |       %s |           %s |\n", card, v1?" No":"Yes",  v2?" No":"Yes", v3?" No":"Yes", v4?" No":"Yes", v5?" No":"Yes", v6?"Yes":" No");

      v1 = (value >> 4) & 0x1;
      v2 = (value >> 5) & 0x1;
      v3 = (value >> 6) & 0x1;
      v4 = (value >> 7) & 0x1;
      v5 = (value >> 16) & 0x1;
      v6 = (value >> 13) & 0x1;    
      seq_printf(sfile, "   %d |   1 |       %s |     %s |  %s |         %s |       %s |           %s |\n", card, v1?" No":"Yes",  v2?" No":"Yes", v3?" No":"Yes", v4?" No":"Yes", v5?" No":"Yes", v6?"Yes":" No");

      v1 = (value >> 8) & 0x1;
      v2 = (value >> 9) & 0x1;
      v3 = (value >> 10) & 0x1;
      v4 = (value >> 11) & 0x1;
      v5 = (value >> 17) & 0x1;
      v6 = (value >> 14) & 0x1;
      seq_printf(sfile, "   %d |   2 |       %s |     %s |  %s |         %s |       %s |           %s |\n", card, v1?" No":"Yes",  v2?" No":"Yes", v3?" No":"Yes", v4?" No":"Yes", v5?" No":"Yes", v6?"Yes":" No");
    }
    else if (ret == -1)
      seq_printf(sfile, "Error: robinconfig has not yet been run on this Robin\n");
    else
      seq_printf(sfile, "Error: The FPGA of this card (0x%08x) is out of date\n", fpgadesign);
  }

  seq_printf(sfile, "\nDumping status of Message FIFOs\n");
  for(loop1 = 0; loop1 < maxcard; loop1++)
    seq_printf(sfile,"There are %d FIFO slots available on card %d\n", msg_fifo_avail[loop1], loop1);

  seq_printf(sfile, "\nDumping status of Dual Ported Memory\n");
  seq_printf(sfile,"Card | Index | Status |              Start |       Size\n");
  for(loop1 = 0; loop1 < maxcard; loop1++)
  {
    for(loop2 = 0; loop2 < MAX_BLOCKLETS; loop2++)
    {
      if(blocklets[loop1][0][loop2].used)
      {
        seq_printf(sfile, "%4d |", loop1);
	seq_printf(sfile, "%6d |", loop2);
	seq_printf(sfile, "%7d |", blocklets[loop1][0][loop2].used);
	seq_printf(sfile, " 0x%016lx |", blocklets[loop1][0][loop2].start);
	seq_printf(sfile, " 0x%08x\n", blocklets[loop1][0][loop2].size);
      }
    }
  }

  seq_printf(sfile,"\nDumping status of PC direct-DMA Memory\n");
  seq_printf(sfile,"Card | Index | Status |              Start |       Size\n");
  for(loop1 = 0; loop1 < maxcard; loop1++)
  {
    for(loop2 = 0; loop2 < MAX_BLOCKLETS; loop2++)
    {
      if(blocklets[loop1][1][loop2].used)
      {
        seq_printf(sfile, "%4d |", loop1);
	seq_printf(sfile, "%6d |", loop2);
	seq_printf(sfile, "%7d |", blocklets[loop1][1][loop2].used);
	seq_printf(sfile, " 0x%016lx |", blocklets[loop1][1][loop2].start);
	seq_printf(sfile, " 0x%08x\n", blocklets[loop1][1][loop2].size);
      }
    }
  }

  seq_printf(sfile, " \n");
  seq_printf(sfile, "The command 'echo <action> > /proc/robin', executed as root,\n");
  seq_printf(sfile, "allows you to interact with the driver. Possible actions are:\n");
  seq_printf(sfile, "debug   -> enable debugging\n");
  seq_printf(sfile, "nodebug -> disable debugging\n");
  seq_printf(sfile, "elog    -> Log errors to /var/log/message\n");
  seq_printf(sfile, "noelog  -> Do not log errors to /var/log/message\n");
  kdebug(("robin(robin_proc_show): Number of characters created = %d\n", len));
  return(0);
}


//------------------
// Service functions
//------------------


/******************************************************************/
static int robin_mmap(struct file *file, struct vm_area_struct *vma)
/******************************************************************/
{
  u_long size, offset;  

  kdebug(("robin_mmap: vma->vm_end       = 0x%016lx\n", (u_long)vma->vm_end));
  kdebug(("robin_mmap: vma->vm_start     = 0x%016lx\n", (u_long)vma->vm_start));
  kdebug(("robin_mmap: vma->vm_pgoff     = 0x%016lx\n", (u_long)vma->vm_pgoff));
  kdebug(("robin_mmap: vma->vm_flags     = 0x%08x\n", (u_int)vma->vm_flags));
  kdebug(("robin_mmap: PAGE_SHIFT        = 0x%016lx\n", (u_long)PAGE_SHIFT));
  kdebug(("robin_mmap: PAGE_SIZE         = 0x%016lx\n", (u_long)PAGE_SIZE));

  size = vma->vm_end - vma->vm_start;
  offset = vma->vm_pgoff << PAGE_SHIFT;

  kdebug(("robin_mmap: size                  = 0x%016lx\n", size));
  kdebug(("robin_mmap: physical base address = 0x%016lx\n", offset));

  if (remap_pfn_range(vma, vma->vm_start, vma->vm_pgoff, size, vma->vm_page_prot))
  {
    kerror(("robin_mmap: function remap_pfn_range failed \n"));
    return(-EINVAL);
  }
  kdebug(("robin_mmap: remap_pfn_range OK, vma->vm_start(2) = 0x%016lx\n", (u_long)vma->vm_start));

  vma->vm_ops = &robin_vm_ops;

  return(0);
}


/**************************************************/
static void robin_vclose(struct vm_area_struct *vma)
/**************************************************/
{  
  kdebug(("robin(robin_vclose): Virtual address  = 0x%08x\n",(u_int)vma->vm_start));
  kdebug(("robin(robin_vclose): mmap released.\n"));
}


/****************************************************/
static int initmem(u_int card, u_int size, u_int base)
/****************************************************/
{
  int loop1, loop2;

  kdebug(("robin(initmem): Initializing memory management structures for card %d\n", card));

  for(loop1 = 0; loop1 < BLOCK_TYPES; loop1++)
  {
    for(loop2 = 0; loop2 < MAX_BLOCKLETS; loop2++)
    {
      blocklets[card][loop1][loop2].used = BLOCKLET_UNUSED;
      blocklets[card][loop1][loop2].start = 0;
      blocklets[card][loop1][loop2].size = 0;
    }
  }

  //Initialise the DPM buffer
  blocklets[card][0][0].used = BLOCKLET_FREE;
  blocklets[card][0][0].start = 0;
  blocklets[card][0][0].size = MESSAGE_RAM_SIZE;  

  //Initialise the PC MEM buffer
  blocklets[card][1][0].used = BLOCKLET_FREE;
  blocklets[card][1][0].start = base;
  blocklets[card][1][0].size = size;
  
  msg_fifo_avail[card] = MESSAGE_FIFO_DEPTH;
  kdebug(("robin(initmem):  msg_fifo_avail has been initialised to %d\n", MESSAGE_FIFO_DEPTH));

  return(0);
}


/************************************************************************************/
static int getmem(u_int card, u_int type, u_int size, u_int *start_add, u_int *handle)
/************************************************************************************/
{
  int loop, b1 = -1 , b2 = -1;

  kdebug(("robin(getmem): called with card=%d  type=%d  size=%d\n", card, type, size));

  kdebug(("robin(getmem): looking for space...\n"));
  //look for a large enough free blocklet  
  for(loop = 0; loop < MAX_BLOCKLETS; loop++)
  {
    kdebug(("robin(getmem): blocklets[%d][%d][%d].used = %d\n", card, type, loop, blocklets[card][type][loop].used));
    kdebug(("robin(getmem): blocklets[%d][%d][%d].size = %d\n", card, type, loop, blocklets[card][type][loop].size));
    if (blocklets[card][type][loop].used == BLOCKLET_FREE && blocklets[card][type][loop].size >= size)
    {
      b1 = loop;
      break;
    }  
  }
  
  kdebug(("robin(getmem): looking for free slot...\n"));
  //look for an empty slot in the array to hold the new blocklet
  for(loop = 0; loop < MAX_BLOCKLETS; loop++)
  {
    kdebug(("robin(getmem): blocklets[%d][%d][%d].used = %d\n", card, type, loop, blocklets[card][type][loop].used));
    if (blocklets[card][type][loop].used == BLOCKLET_UNUSED)
    {
      b2 = loop;
      break;
    }  
  } 

  if (b1 == -1)
  {
    kerror(("robin(getmem): failed to find free memory\n"))
    return(-RD_NOMEMORY);
  }
    
  if (b2 == -1)
  {
    kerror(("robin(getmem): failed to find an enpty slot\n"))
    return(-RD_NOSLOT);     
  }
 
  blocklets[card][type][b2].used = BLOCKLET_USED;
  blocklets[card][type][b2].size = size;
  blocklets[card][type][b2].start = blocklets[card][type][b1].start;  

  blocklets[card][type][b1].size -= size;
  if (blocklets[card][type][b1].size == 0)
  {
    blocklets[card][type][b1].used = BLOCKLET_UNUSED;
    blocklets[card][type][b1].start = 0;
  }
  else
    blocklets[card][type][b1].start += size;
  
  *start_add = blocklets[card][type][b2].start;
  *handle = b2;

  return(0);
}


/*****************************************************/
static int retmem(u_int card, u_int type, u_int handle)
/*****************************************************/
{
  int loop, bprev = -1,  bnext = -1;
  
  kdebug(("robin(retmem): called with card=%d  type=%d  handle=%d\n", card, type, handle));
  if (blocklets[card][type][handle].used != BLOCKLET_USED)
    return(-RD_ILLHANDLE);
 
  blocklets[card][type][handle].used = BLOCKLET_FREE;
    
  //Check if the blocklet can be merged with a predecessor   
  for(loop = 0; loop < MAX_BLOCKLETS; loop++)
  {
    if (blocklets[card][type][loop].used == BLOCKLET_FREE && (blocklets[card][type][loop].start + blocklets[card][type][loop].size) == blocklets[card][type][handle].start)
    {
      bprev = loop;
      break;
    }
  }
  
  //Check if the blocklet can be merged with a sucessor   
  for(loop = 0; loop < MAX_BLOCKLETS; loop++)
  {
    if (blocklets[card][type][loop].used == BLOCKLET_FREE && (blocklets[card][type][handle].start + blocklets[card][type][handle].size) == blocklets[card][type][loop].start)
    {
      bnext = loop;
      break;
    }
  }
  
  if (bprev != -1)
  {
    kdebug(("robin(retmem): merging %d with predecessor %d\n", handle, bprev)); 
    blocklets[card][type][handle].start = blocklets[card][type][bprev].start;
    blocklets[card][type][handle].size += blocklets[card][type][bprev].size;
    blocklets[card][type][bprev].used = BLOCKLET_UNUSED;
    blocklets[card][type][bprev].start = 0;
    blocklets[card][type][bprev].size = 0;
  }
  
  if (bnext != -1)
  {
    kdebug(("robin(retmem): merging %d with sucessor %d\n", handle, bnext)); 
    blocklets[card][type][handle].size += blocklets[card][type][bnext].size;
    blocklets[card][type][bnext].used = BLOCKLET_UNUSED;
    blocklets[card][type][bnext].start = 0;
    blocklets[card][type][bnext].size = 0;
  }
  return(0);
}


/**************************************************/
static int fpgaLoaded(u_int card, u_int *fpgadesign)  
/**************************************************/
{
  int value, version, majorRevision, minorRevision;
  int eversion, emajorRevision, eminorRevision;
  u_int did;
  
  kdebug(("robin(fpgaLoaded): function called\n"))
  
  if (robincard[card].card_type == ROBIN_CLASSIC)
    did = DESIGN_ID_P;
  else if (robincard[card].card_type == ROBIN_EXPRESS)
    did = DESIGN_ID_S;
  else
  {
    kerror(("robin(fpgaLoaded): Unknown card type\n"))
    return -1;
  }

  eversion       = did >> 24;
  emajorRevision = (did >> 16) & 0xff;
  eminorRevision = did & 0x0fff;
  
  if ((card < maxcard) && (robincard[card].fpgaConfigured == 1)) 
  {
    value = ioread32((void *) (robincard[card].fpga_regs));
    *fpgadesign = value;
    
    version = value >> 24;
    majorRevision = (value >> 16) & 0xff;
    minorRevision = value & 0x0fff;

    kdebug(("robin(fpgaLoaded): Expected = 0x%08x\n", did));
    kdebug(("robin(fpgaLoaded): eversion: 0x%x\n", eversion));
    kdebug(("robin(fpgaLoaded): emajorRevision: 0x%x\n", emajorRevision));
    kdebug(("robin(fpgaLoaded): eminorRevision: 0x%x\n", eminorRevision));
    kdebug(("robin(fpgaLoaded): Detected = 0x%08x\n", value));
    kdebug(("robin(fpgaLoaded): version: 0x%x\n", version));
    kdebug(("robin(fpgaLoaded): majorRevision: 0x%x\n", majorRevision));
    kdebug(("robin(fpgaLoaded): minorRevision: 0x%x\n", minorRevision));

    if ((eversion != version) || (emajorRevision != majorRevision))
    {
      kerror(("robin(fpgaLoaded): Incompatible FPGA version. Can't continue!\n"));
      return -2;
    } 
    else 
    {
      if (eminorRevision > minorRevision)
      {
        kerror(("robin(fpgaLoaded): Incompatible FPGA revision (too small). Can't continue!\n"));
        return -2;
      }
      if (eminorRevision < minorRevision)
        kdebug(("robin(fpgaLoaded): Actual FPGA revision too high. Not all FPGA features might be exploited\n"));
    }
    return 0;
  }
  else
  { 
    kdebug(("robin(fpgaLoaded): robinconfig has not yet been called on this Robin\n"));
    *fpgadesign = 0;
    return -1;
  }
}


/*********************************************************/
static irqreturn_t robin_irq_handler(int irq, void *dev_id)
/*********************************************************/
{
  u_int boxdata, action, card, bell_data;
  u_int activeisrs;
  
  //It is possible that several interrupts arrive back to back. In such a case we want to handle all
  //of them before we increment the semaphore that unblocks the WAIT_IRQ ioctl. Therefore we have to know
  //if several ISRs are running in parallel. For that purpose we count the number of active ISRs with
  //an atomic integer.
  activeisrs = atomic_inc_return(&isrs_active);
  
  kdebug(("robin(robin_irq_handler): ISR called for irq = %d and activeisrs = %d\n", irq, activeisrs));
  action = 0;
  
  for (card = 0; card < maxcard; card++)
  {    
    if (robincard[card].irqnumber == irq)
    {
      kdebug(("%d robin(robin_irq_handler): IRQ %d is for card %d\n", activeisrs, irq, card));
  
      //1. Read and clear the dorbell register
      bell_data = robincard[card].plx_regs[PLX_L2PDBELL];   
      kdebug(("%d robin(robin_irq_handler): card = %d, bell_data = 0x%08x, INTCSR = 0x%08x\n", activeisrs, card, bell_data, robincard[card].plx_regs[PLX_INTCSR]));

      robincard[card].plx_regs[PLX_L2PDBELL] = bell_data;  //Clear all bits
      kdebug(("%d robin(robin_irq_handler): bell_data reset. bell_data = 0x%08x\n", activeisrs, robincard[card].plx_regs[PLX_L2PDBELL]));

      if (bell_data)      //There was an interrupt from this Robin
      {
	action = 1;      

	//2. Read the mailbox register and store the information for the ioctl call
	if (bell_data & 0x1)       //bit 0
	{
          boxdata = robincard[card].plx_regs[PLX_MBOX4];
          kdebug(("%d robin(robin_irq_handler): card = %d, MBOX4 = 0x%08x\n", activeisrs, card, boxdata));
          irq_info.ch0_type[card] = INMATE_EVENT;
	  irq_info.ch0_id[card]   = boxdata;
	}  

	if (bell_data & 0x2)       //bit 1
	{
          boxdata = robincard[card].plx_regs[PLX_MBOX4];
          kdebug(("%d robin(robin_irq_handler): card = %d, MBOX4 = 0x%08x\n", activeisrs, card, boxdata));
          irq_info.ch0_type[card  ] = GUEST_EVENT;
	  irq_info.ch0_id[card] = boxdata;
	}    

	if (bell_data & 0x100)     //bit 8
	{
          boxdata = robincard[card].plx_regs[PLX_MBOX5];
          kdebug(("%d robin(robin_irq_handler): card = %d, MBOX5 = 0x%08x\n", activeisrs, card, boxdata));
          irq_info.ch1_type[card] = INMATE_EVENT;
          irq_info.ch1_id[card]   = boxdata;
	}

	if (bell_data & 0x200)     //bit 9
	{
          boxdata = robincard[card].plx_regs[PLX_MBOX5];
          kdebug(("%d robin(robin_irq_handler): card = %d, MBOX5 = 0x%08x\n", activeisrs, card, boxdata));
          irq_info.ch1_type[card] = GUEST_EVENT;
          irq_info.ch1_id[card]   = boxdata;
	}    

	if (bell_data & 0x10000)   //bit 16
	{
          boxdata = robincard[card].plx_regs[PLX_MBOX6];
          kdebug(("%d robin(robin_irq_handler): card = %d, MBOX6 = 0x%08x\n", activeisrs, card, boxdata));
          irq_info.ch2_type[card] = INMATE_EVENT;
          irq_info.ch2_id[card]   = boxdata;
	}

	if (bell_data & 0x20000)   //bit 17
	{
          boxdata = robincard[card].plx_regs[PLX_MBOX6];
          kdebug(("%d robin(robin_irq_handler): card = %d, MBOX6 = 0x%08x\n", activeisrs, card, boxdata));
          irq_info.ch2_type[card] = GUEST_EVENT;
          irq_info.ch2_id[card]   = boxdata;
	}    
      } 
      kdebug(("%d robin(robin_irq_handler): card = %d, PLX_INTCSR = 0x%08x\n", activeisrs, card, robincard[card].plx_regs[PLX_INTCSR]));
    }
  }
  
  if (action)
  {
    //just for debug
    for (card = 0; card < maxcard; card++)
    {    
      kdebug(("%d robin(robin_irq_handler): card = %d, irq_info.ch0_type = %d, irq_info.ch0_id   = %d\n", activeisrs, card, irq_info.ch0_type[card], irq_info.ch0_id[card]));
      kdebug(("%d robin(robin_irq_handler): card = %d, irq_info.ch1_type = %d, irq_info.ch1_id   = %d\n", activeisrs, card, irq_info.ch1_type[card], irq_info.ch1_id[card]));
      kdebug(("%d robin(robin_irq_handler): card = %d, irq_info.ch2_type = %d, irq_info.ch2_id   = %d\n", activeisrs, card, irq_info.ch2_type[card], irq_info.ch2_id[card]));
    }  

    //If this is the last active ISR we unblock the ioctl
    if(atomic_dec_and_test(&isrs_active))
    {
      kdebug(("%d robin(robin_irq_handler): incrementing semaphore\n", activeisrs));
      up(&robinirq_semaphore);
    }
    return(IRQ_HANDLED);  //MJ: see p 273   
  }
  else
  {
    kdebug(("%d robin(robin_irq_handler): No action, interrupt was probably coming from soething other than a Robin\n", activeisrs));
    activeisrs = atomic_dec_return(&isrs_active);  //We have to decrement the ISR counter anyway to balance the atomic_inc in the beginning of the function
    if (activeisrs < 0)
      kerror(("robin(robin_irq_handler): activeisrs = %d\n", activeisrs));
    
    return(IRQ_NONE);     //MJ: see p 273
  }
}

#if LINUX_VERSION_CODE < KERNEL_VERSION(5,6,0)

/**************************************************/
static int readFpga(u_int card, u_int addressOffset)
/**************************************************/
{
  u_int fpgadesign;
  int value = 0;
  void *address;
  
  kdebug(("robin(readFpga): function called\n"))

  if ((robincard[card].fpga_regs != NULL) && (addressOffset < robincard[card].pci_memsize_bus)) 
  {
    address = (void *)(robincard[card].fpga_regs + addressOffset);
    if (fpgaLoaded(card, &fpgadesign) == 0) 
    {
      value = ioread32(address);
      kdebug(("robin(readFpga): 0x%08x read from FPGA of card %d at offset 0x%08x\n", value, card, addressOffset))
    }
    return value;
  }
  else 
    return 0;
}


/**************************************************************/
static void writeFpga(u_int card, u_int addressOffset, int data)
/**************************************************************/
{
  u_int fpgadesign;
  void *address;
  
  kdebug(("robin(writeFpga): function called\n"))

  if ((robincard[card].fpga_regs != NULL) && (addressOffset < robincard[card].pci_memsize_bus)) 
  {
    address = (void *)(robincard[card].fpga_regs + addressOffset);
    if (fpgaLoaded(card, &fpgadesign) == 0) 
      iowrite32(data, robincard[card].fpga_regs + addressOffset);
  }
}


/*************************/
static int getCardNum(void) 
/*************************/
{
  return(maxcard);
}

EXPORT_SYMBOL(readFpga);
EXPORT_SYMBOL(writeFpga);
EXPORT_SYMBOL(fpgaLoaded);
EXPORT_SYMBOL(getCardNum);

#endif
