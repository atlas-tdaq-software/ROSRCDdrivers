/************************************************************************/
/*                                                                      */
/* File: test_drivers.c                                                 */
/*                                                                      */
/* This program checks if the tdaq drivers running on a                 */
/* computer are compatible with the TDAQ S/W used for the run           */
/*                                                                      */
/* Author: M, Joos, CERN                                                */
/*                                                                      */
/**************** C 2021 - A nickel program worth a dime ****************/

#include <stdlib.h>
#include <cmdl/cmdargs.h>
#include <tmgr/tmresult.h>
#include <errno.h>
#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include "rcc_error/rcc_error.h"
#include "DFDebug/DFDebug.h"
#include "io_rcc/io_rcc.h"

using namespace std;
using namespace daq::tmgr;


/**************************************************************************/  
int access_to_file(const char* filename, const char* matching_str, int mode)
/**************************************************************************/
{
  int return_access_to_file = 0;
  FILE* driverfile_ptr;

  if (access(filename, R_OK) == -1)
  {
    switch (errno)
    {
      case EACCES:
	cerr << "Error:: File (" << filename << ") does not have read permissions." << endl;
	return_access_to_file = 10;
	break;
      case ENOENT:
	cerr << "Error:: File (" << filename << ") does not exist." << endl;
	return_access_to_file = 10;
	break;
      case EINVAL:
	cerr << "Error:: Invalid argument." << endl;
	return_access_to_file = 10;
	break;
      default:
	cerr << "Error:: Got '" << strerror(errno) << "' error trying to access file ("<< filename << ")." << endl;
	return_access_to_file = 10;
    }
  }
   
  else
  {
    char buff[1024];
    driverfile_ptr = fopen(filename, "r");
    
    if(!driverfile_ptr)
      cerr << "Error: could not open driver file to read" << endl;
      
    while(fgets(buff, sizeof(buff) - 1, driverfile_ptr)) 
    {
      if(strstr(buff, matching_str))
      {
        cout << "The driver installed in (" << filename << ") IS compatible with the TDAQ release " << endl;
        if (mode == 1)
	{
          return_access_to_file = 0;
	  break;
	}  
	else
	{
	  fclose(driverfile_ptr);
          return (0);
	}  
      }
      else
        return_access_to_file = 10;

     if(strstr(buff, "TDAQ"))
     {
       //cout << "proc version: " << buff << endl;
       break;
     }


    }
    
    if(return_access_to_file == 10) 
    {
      cout << "The " << filename << " driver IS NOT compatible with the TDAQ release " << endl;
      cout << "Required version: " << matching_str << endl;
      cout << "Detected version: " << buff << endl;
    }
    
    if (mode == 1)
    {
      //Reopen the file for a second scan
      fclose(driverfile_ptr);	
      driverfile_ptr = fopen(filename, "r");
      if(!driverfile_ptr)
        cerr << "Error: could not re-open driver file for reading" << endl;
	
      while(fgets(buff, sizeof(buff) - 1, driverfile_ptr)) 
      {
	if(strstr(buff, "robinconfig has not"))
	{
          cout << "Problem in robin driver: robinconfig has not been run on all cards " << endl;
          fclose(driverfile_ptr);
          return (10);
	}
	
	if(strstr(buff, "The FPGA of this"))
	{
          cout << "Problem in robin driver: The FPGA of some Robins is out of date " << endl;
          fclose(driverfile_ptr);
          return (10);
	}
      }    
    }    
    
    fclose(driverfile_ptr);	
  }
  
  return return_access_to_file;
}
  

/*********************************/  
int check_bpa(const char* filename)
/*********************************/
{
  FILE *driverfile_ptr;
  char buff[256];
  
  driverfile_ptr = fopen(filename, "r");
  if(!driverfile_ptr)
    cerr << "Error: could not open driver file to read" << endl;

  while(fgets(buff, sizeof(buff) - 1, driverfile_ptr)) 
  {
    if(strstr(buff, "GFPBPA"))
    {
      fclose(driverfile_ptr);
      return (0);
    }
  }    

  cout << "Problem in cmem_rcc driver: There is no GFPBPA buffer" << endl;
  fclose(driverfile_ptr);	  
  return(1);
}


/***************************************/
int find_pci_device(u_int vid, u_int did)
/***************************************/
{
  u_int ret, handle;
  
  ret = IO_Open();
  if (ret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, ret);
    exit(TmFail);
  }

  ret = IO_PCIDeviceLink(vid, did, 1, &handle);
  if (ret != IO_RCC_SUCCESS)
  {
    if(ret != IO_PCI_NOT_FOUND)
      rcc_error_print(stdout, ret);
    return(1);
  }
  else
    return(0);
  
  ret = IO_Close();
  if (ret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, ret);
    exit(TmFail);
  }
}


/************/
int main(void)
/************/
{
  string proc = "/proc/";
  string drv_proc_path;
  int result = TmPass;
  int r_test = TmPass;
  string release = RELEASE_NAME;   // release name as defined at compilation time
  int hn_error;
  char my_hostname[100], *is_swrod;

  hn_error = gethostname(my_hostname, 100);
  if (hn_error)
  {
    cout << "ERROR received from call to gethostname. errno = " << errno << endl;
    exit(-1);
  }
  else
     cout << "This computer is called " << my_hostname << endl;

  //Exclude SW-RODs from the test
  is_swrod = strstr(my_hostname, "swrod");
  if (is_swrod != NULL)
  {
    cout << "Nothing to do on a SW/ROD" << endl;
    return(TmPass); 	
  }

  cout << "Checking if drivers are compatible with release " << release.c_str() << endl;  
   
  //io_rcc and cmem_rcc have to run on all ROS and RCD systems

  //io_rcc 
  cout << "Checking io_rcc driver" << endl; 
  drv_proc_path = proc + "io_rcc";
  r_test = access_to_file(drv_proc_path.c_str(), release.c_str(), 0);
  if (r_test == 10)
    return(TmFail);

  //cmem_rcc 
  cout << "Checking cmem_rcc driver" << endl; 
  drv_proc_path = proc + "cmem_rcc";
  r_test = access_to_file(drv_proc_path.c_str(), release.c_str(), 0);
  if (r_test == 10)
    return(TmFail);
  r_test = check_bpa(drv_proc_path.c_str());
  if (r_test)
    return(TmFail);

  //All other drivers are just checked if we can detect at least one related PCI device
  
  //robin 
  result = find_pci_device(0x10dc, 0x0144);
  if (!result)
  {
    cout << "Checking robin driver" << endl; 
    drv_proc_path = proc + "robin";
    r_test = access_to_file(drv_proc_path.c_str(), release.c_str(), 1);
    if (r_test == 10)
      return(TmFail);
  }

  //robinnp 
  result = find_pci_device(0x10dc, 0x0187);
  if (!result)
  {
    cout << "Checking robinnp driver" << endl; 
    drv_proc_path = proc + "robinnp";
    r_test = access_to_file(drv_proc_path.c_str(), release.c_str(), 1);
    if (r_test == 10)
      return(TmFail);
  }

  //filar 
  result = find_pci_device(0x10dc, 0x0014);
  if (!result)
  {
    cout << "Checking filar driver" << endl; 
    drv_proc_path = proc + "filar";
    r_test = access_to_file(drv_proc_path.c_str(), release.c_str(), 0);
    if (r_test == 10)
      return(TmFail);
  }

  //solar
  result = find_pci_device(0x10dc, 0x0017);
  if (!result)
  {
    cout << "Checking solar driver" << endl; 
    drv_proc_path = proc + "solar";
    r_test = access_to_file(drv_proc_path.c_str(), release.c_str(), 0);
    if (r_test == 10)
      return(TmFail);
  }

  //quest
  result = find_pci_device(0x10dc, 0x001b);
  if (!result)
  {
    cout << "Checking quest driver" << endl; 
    drv_proc_path = proc + "quest";
    r_test = access_to_file(drv_proc_path.c_str(), release.c_str(), 0);
    if (r_test == 10)
      return(TmFail);
  }

  //vme_rcc
  result = find_pci_device(0x10e3, 0x0000);
  if (!result)
  {
    cout << "Checking vme_rcc driver for Universe" << endl; 
    drv_proc_path = proc + "vme_rcc";
    r_test = access_to_file(drv_proc_path.c_str(), release.c_str(), 0);
    if (r_test == 10)
      return(TmFail);
  }

  //vme_rcc_tsi
  result = find_pci_device(0x10e3, 0x0148);
  if (!result)
  {
    cout << "Checking vme_rcc driver for Tsi148" << endl; 
    drv_proc_path = proc + "vme_rcc_tsi";
    r_test = access_to_file(drv_proc_path.c_str(), release.c_str(), 0);
    if (r_test == 10)
      return(TmFail);
  }

  //flx
  //result = find_pci_device(0x10dc, 0x0427);
  //if (!result)
  //{
  //  cout << "Checking flx driver for FLX712" << endl;
  //  drv_proc_path = proc + "flx";
  //  r_test = access_to_file(drv_proc_path.c_str(), release.c_str(), 0);
  //  if (r_test == 10)
  //    return(TmFail);
  //}



  return(TmPass); 	
}
