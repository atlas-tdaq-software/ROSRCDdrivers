/************************************************************************/
/*									*/
/* File: quest_driver.c							*/
/*									*/
/* driver for the QUEST S-Link interface				*/
/*									*/
/* 9. Dec. 03  MAJO  created						*/
/*									*/
/************ C 2007 - The software with that certain something *********/

/************************************************************************/
/*NOTES:								*/
/*- This driver should work on kernels from 2.6.9 onwards		*/
/************************************************************************/

#include <linux/init.h>           //MJ: for 2.6, p30
#include <linux/module.h>         //p30
#include <linux/moduleparam.h>    //MJ: for 2.6, p30
#include <linux/kernel.h>         //e.g. for printk
#include <linux/stat.h>           //MJ: for 2.6, e.g. for module_param
#include <linux/pci.h>            //e.g. for   pci_find_device
#include <linux/errno.h>          //e.g. for EFAULT
#include <linux/fs.h>             //e.g. for register_chrdev
#include <linux/proc_fs.h>        //e.g. for create_proc_entry
#include <linux/mm.h>             //e.g. for vm_operations_struct
#include <linux/spinlock.h>       //For the spin-lock 
#include <linux/slab.h>           //e.g. for kmalloc
#include <linux/cdev.h>           //e.g. for cdev_alloc
#include <linux/delay.h>
#include <linux/version.h>        
#include <linux/interrupt.h>      //e.g. for request_irq
#include <linux/sched.h>          //MJ: for current->pid (first needed with SLC6)
#include <linux/seq_file.h>       //CS9
#include <asm/uaccess.h>          //e.g. for copy_from_user
#include <asm/io.h>               //e.g. for inl
//#include <asm/system.h>           //e.g. for wmb  //MJ: not available in CC7
#include "ROSsolar/quest_driver.h"
#include "ROSRCDdrivers/tdaq_drivers.h"

#ifndef PCI_VENDOR_ID_CERN
  #define PCI_VENDOR_ID_CERN 0x10dc
#endif

/*********/
/*Globals*/
/*********/
static int debug = 0, errorlog = 1;
static u_int maxcards = 0;
static u_int opid, served[MAXQUESTROLS], irqlist[MAXIRQ];
static u_int *infifo, *infifon;
static u_long reqbuf[MAXQUESTCARDS], reqadd[MAXQUESTCARDS];
static u_int infifor[MAXQUESTROLS], req_dma_active[MAXQUESTCARDS];
static u_int order, infifodatasize, fifoptrsize, tsize = 0;
static u_long swfifobase, swfifobasephys;
static char *proc_read_text;
static dev_t major_minor;
static struct cdev *quest_cdev;
static T_quest_card questcard[MAXQUESTCARDS];

//Note: until 24.10.2016 there was an array of spin locks (slock[MAXQUESTCARDS]). The problem is that nowadays spinlocks should be created with DEFINE_SPINLOCK
//      and this macro does not work with arrays. Using a single spin lock for all cards is an easy fix (and therefore implemented) but may have some performance 
//      disadvantages. To be seen.... 
//static spinlock_t slock[MAXQUESTCARDS];
DEFINE_SPINLOCK(slock);

/************/
/*Prototypes*/
/************/
static void quest_cleanup(void);
static void quest_vmaOpen(struct vm_area_struct *vma);
static void quest_vmaClose(struct vm_area_struct *vma);
static void questRemove(struct pci_dev *dev); 
static int questProbe(struct pci_dev *dev, const struct pci_device_id *id);
static int quest_init(void);
int quest_proc_open(struct inode *inode, struct file *file);
int quest_proc_show(struct seq_file *sfile, void *p);
static irqreturn_t quest_irq_handler(int irq, void *dev_id);

/***************************************************************/
/* Use /sbin/modinfo <module name> to extract this information */
/***************************************************************/
MODULE_DESCRIPTION("S-Link QUEST");
MODULE_AUTHOR("Markus Joos, CERN/PH");
MODULE_VERSION("2.2");
MODULE_LICENSE("Private: Contact markus.joos@cern.ch");
module_param (debug, int, S_IRUGO | S_IWUSR);  //MJ: for 2.6 p37
MODULE_PARM_DESC(debug, "1 = enable debugging   0 = disable debugging"); 
module_param (errorlog, int, S_IRUGO | S_IWUSR);  //MJ: for 2.6 p37
MODULE_PARM_DESC(errorlog, "1 = enable error logging   0 = disable error logging");


//MJ: Not actually required. Just for kdebug
struct vm_operations_struct quest_vm_ops =
{       
  .close = quest_vmaClose,      
  .open  = quest_vmaOpen,      //MJ: Note the comma at the end of the list!
};

static struct file_operations fops = 
{
  .owner          = THIS_MODULE,
  .unlocked_ioctl = quest_ioctl,
  .open           = quest_open,    
  .mmap           = quest_mmap,
  .release        = quest_release,
};



//CS9
//Inspiration taken from: https://stackoverflow.com/questions/64931555/how-to-fix-error-passing-argument-4-of-proc-create-from-incompatible-pointer
//===========================================================
#if LINUX_VERSION_CODE >= KERNEL_VERSION(5,6,0)
static const struct proc_ops quest_proc_file_ops = 
{
  .proc_open  = quest_proc_open,
  .proc_write = quest_proc_write,
  .proc_read  = seq_read,    
  .proc_lseek = seq_lseek	
};
#else
static struct file_operations quest_proc_file_ops = 
{
  .owner   = THIS_MODULE,
  .open    = quest_proc_open,
  .write   = quest_proc_write,
  .read    = seq_read,
  .llseek  = seq_lseek,
  .release = single_release
};
#endif
//==============================================================



// PCI driver structures. See p311
static struct pci_device_id questIds[] = 
{
  {PCI_DEVICE(PCI_VENDOR_ID_CERN, QUEST_PCI_DEVICE_ID)},      
  {0,},
};

//PCI operations
static struct pci_driver __refdata questPciDriver = 
{
  .name     = "quest",
  .id_table = questIds,
  .probe    = questProbe,
  .remove   = questRemove,
};


/*****************************/
/* Standard driver functions */
/*****************************/


/*********************************************************/
int quest_proc_open(struct inode *inode, struct file *file) 
/*********************************************************/
{
  return single_open(file, quest_proc_show, NULL);
}


/*************************/
static int quest_init(void)
/*************************/
{
  int tfsize, result;
  u_int channel, card, loop;
  struct page *page_ptr;
  static struct proc_dir_entry *quest_file;

  if (alloc_chrdev_region(&major_minor, 0, 1, "quest")) //MJ: for 2.6 p45
  {
    kerror(("quest(quest_init): failed to obtain device numbers\n"));
    result = -QUEST_EIO;
    goto fail1;
  }
  
  proc_read_text = (char *)kmalloc(MAX_PROC_TEXT_SIZE, GFP_KERNEL);
  if (proc_read_text == NULL)
  {
    kerror(("quest(quest_init): error from kmalloc\n"));
    result = -EFAULT;
    goto fail2;
  }

  quest_file = proc_create("quest", 0644, NULL, &quest_proc_file_ops);
  if (quest_file == NULL)
  {
    kerror(("quest(quest_init): error from call to create_proc_entry\n"));
    result = -EFAULT;
    goto fail3;
  }  

  //Clear the list of used interupts 
  for(loop = 0; loop < MAXIRQ; loop++)
    irqlist[loop] = 0;
      
  kdebug(("quest(quest_init): registering PCIDriver\n"));
  result = pci_register_driver(&questPciDriver);  //MJ: See P312
  if (result < 0)
  {
    kerror(("quest(quest_init): ERROR! no QUEST cards found!\n"));
    result = -EIO;
    goto fail4;
  }
  else 
  {
    kdebug(("quest(quest_init): Found some QUEST cards!\n"));
  }

  // Allocate contiguous memory for the FIFOs and store the base addresses in a global structure
  infifodatasize = MAXQUESTROLS * MAXQUESTINFIFO * sizeof(u_int) * 2;  //infifo
  fifoptrsize = MAXQUESTROLS * sizeof(u_int);                     //infifo counter
  kdebug(("quest(quest_init): 0x%08x bytes needed for the IN FIFOs\n", infifodatasize));
  kdebug(("quest(quest_init): 0x%08x bytes needed for the FIFO counters\n", fifoptrsize));
  
  tsize = infifodatasize + fifoptrsize;
  kdebug(("quest(quest_init): 0x%08x bytes needed for all FIFOs\n", tsize));

  tfsize = (tsize + 4095) & 0xfffff000;  // round up to next 4K boundary
  tfsize = tfsize >> 12;                 // compute number of pages
  kdebug(("quest(quest_init): %d pages needed for the S/W FIFOs\n", tfsize));
 
  order = 0;
  while(tfsize)
  {
    order++;
    tfsize = tfsize >> 1;                 // compute order
  } 
  kdebug(("quest(quest_init): order = %d\n", order));
  
  swfifobase = __get_free_pages((GFP_ATOMIC | GFP_DMA32), order);
  if (!swfifobase)
  {
    kerror(("rquest(quest_init): error from __get_free_pages\n"));
    result = -QUEST_EFAULT;
    goto fail5;
  }
  kdebug(("quest(quest_init): swfifobase = 0x%016lx\n", swfifobase));
   
  // Reserve all pages to make them remapable
  page_ptr = virt_to_page(swfifobase);
  for (loop = (1 << order); loop > 0; loop--, page_ptr++)
    set_bit(PG_reserved, &page_ptr->flags);

  //////swfifobasephys = virt_to_bus((void *) swfifobase);
  swfifobasephys = virt_to_phys((void *) swfifobase);
  kdebug(("quest(quest_init): swfifobasephys = 0x%016lx\n", swfifobasephys));

  // Assign base addresses to the FIFO arrays
  infifo  = (u_int *)swfifobase;
  infifon = (u_int *)(swfifobase + infifodatasize);
  kdebug(("quest(quest_init): infifo   is at 0x%016lx\n", (u_long)infifo));
  kdebug(("quest(quest_init): infifon  is at 0x%016lx\n", (u_long)infifon));

  //Initialize the FIFOs
  for(channel = 0; channel < MAXQUESTROLS; channel++)
  {
    infifor[channel] = 0;
    infifon[channel] = 0;
  }

  //Allocate the buffers for the REQ blocks
  //The max size of such a block is: 4 channels * (1 size word + 126 data words) = 504 words = 2032 bytes
  //It is therefore safe if we get one page (4k) per buffer
  for(card = 0; card < maxcards; card++)
  {
    req_dma_active[card] = 0;
    reqbuf[card] = __get_free_page(GFP_ATOMIC | GFP_DMA32);
    if (!reqbuf[card])
    {
      kerror(("quest(quest_init): error from __get_free_pages for reqbuf\n"));
      result = -QUEST_EFAULT;
      goto fail6;
    }
    //////reqadd[card] = virt_to_bus((void *) reqbuf[card]);
    reqadd[card] = virt_to_phys((void *) reqbuf[card]);
    kdebug(("quest(quest_init): reqbuf[%d] = 0x%016lx (PCI = 0x%016lx)\n", card, reqbuf[card], reqadd[card]));
  }
  
  opid = 0; 
  quest_cdev = (struct cdev *)cdev_alloc();       //MJ: for 2.6 p55
  quest_cdev->ops = &fops;
  result = cdev_add(quest_cdev, major_minor, 1);  //MJ: for 2.6 p56
  if (result)
  {
    kerror(("quest(quest_init): error from call to cdev_add.\n"));
    goto fail7;
  }

  kdebug(("quest(quest_init): driver loaded; major device number = %d\n", MAJOR(major_minor)));
  return(0);

  fail7:
    for(card = 0; card < maxcards; card++)
      free_page((u_long)reqbuf[card]);
   
  fail6:
    page_ptr = virt_to_page(swfifobase);
    for (loop = (1 << order); loop > 0; loop--, page_ptr++)
      clear_bit(PG_reserved, &page_ptr->flags);
    free_pages(swfifobase, order);

  fail5:
    pci_unregister_driver(&questPciDriver);

  fail4:
    remove_proc_entry("io_rcc", NULL);
    
  fail3:
    kfree(proc_read_text);

  fail2:
    unregister_chrdev_region(major_minor, 1); //MJ: for 2.6 p45

  fail1:
    return(result);  
}


/*****************************/
static void quest_cleanup(void)
/*****************************/
{
  u_int card, loop;
  struct page *page_ptr;
      
  cdev_del(quest_cdev);                     //MJ: for 2.6 p56
  pci_unregister_driver(&questPciDriver);

  // unreserve all pages used for the S/W FIFOs
  page_ptr = virt_to_page(swfifobase);
  for (loop = (1 << order); loop > 0; loop--, page_ptr++)
    clear_bit(PG_reserved, &page_ptr->flags);

  // free the area 
  free_pages(swfifobase, order);
  
  // Return the buffers for the REQ blocks
  for(card = 0; card < maxcards; card++)
    free_page(reqbuf[card]);
  
  remove_proc_entry("quest", NULL);
  kfree(proc_read_text);
  unregister_chrdev_region(major_minor, 1); //MJ: for 2.6 p45

  kdebug(("quest(quest_cleanup): driver removed\n"));
}


module_init(quest_init);    //MJ: for 2.6 p16
module_exit(quest_cleanup); //MJ: for 2.6 p16


/**********************************************************/
static int quest_open(struct inode *ino, struct file *filep)
/**********************************************************/
{
  kdebug(("quest(quest_open): nothing to be done\n"));
  return(0);
}


/*************************************************************/
static int quest_release(struct inode *ino, struct file *filep)
/*************************************************************/
{
  kdebug(("quest(quest_release): (almost) nothing to be done\n"));
  opid = 0;
  return(0);
}


/***************************************************************/
static long quest_ioctl(struct file *file, u_int cmd, u_long arg)
/***************************************************************/
{
  switch (cmd)
  {    
    case QUEST_OPEN:
    {
      QUEST_open_t params;
      
      kdebug(("quest(ioctl,QUEST_OPEN): called from process %d\n", current->pid))
      if (!opid)
        opid = current->pid;
      else
      {
        kerror(("quest(ioctl,QUEST_OPEN): The QUEST cards are already used by process %d\n", opid))
  	return(-QUEST_USED);
      }
      
      params.size = tsize;
      params.insize = infifodatasize;
      params.physbase = swfifobasephys;
      kdebug(("quest(ioctl,QUEST_OPEN): params.size     = 0x%08x\n", params.size));
      kdebug(("quest(ioctl,QUEST_OPEN): params.physbase = 0x%lx\n", params.physbase));
      kdebug(("quest(ioctl,QUEST_OPEN): params.insize   = 0x%08x\n", params.insize));
  
      if (copy_to_user((void *)arg, &params, sizeof(QUEST_open_t)))
      {
	kerror(("quest(ioctl, QUEST_OPEN): error from copy_to_user\n"));
	return(-QUEST_EFAULT);
      }      
      return(0);
      break;
    }
    
    case QUEST_CLOSE:
    {
      kdebug(("quest(ioctl,QUEST_CLOSE): called from process %d\n", current->pid))
      opid = 0;
  
      return(0);
      break;
    }
    
    case QUEST_CONFIG:
    { 
      QUEST_config_t params;
      u_int channel, card, data;
      
      if (copy_from_user(&params, (void *)arg, sizeof(QUEST_config_t)))
      {
	kerror(("quest(ioctl,QUEST_CONFIG): error from copy_from_user\n"));
	return(-QUEST_EFAULT);
      }
      kdebug(("quest(ioctl,QUEST_CONFIG): \n"));        
      kdebug(("quest(ioctl,QUEST_CONFIG): params.bswap  = %d\n", params.bswap));
      kdebug(("quest(ioctl,QUEST_CONFIG): params.wswap  = %d\n", params.wswap));
      kdebug(("quest(ioctl,QUEST_CONFIG): params.scw    = 0x%08x\n", params.scw));
      kdebug(("quest(ioctl,QUEST_CONFIG): params.ecw    = 0x%08x\n", params.ecw));

      for(card = 0; card < maxcards; card++)
      {
        data = 0;  
        questcard[card].regs->opctl = (params.wswap << 2) + (params.bswap << 1);
        questcard[card].regs->sctl  = params.scw & 0xfffffffc;
        questcard[card].regs->ectl  = params.ecw & 0xfffffffc;
        
        //The LFF and LDOWN interrupts are not yet supported
	questcard[card].regs->intctl = 0xFBEFBE05; // threshold fixed at 62 for all channels, clear any outstanding interrupts
	questcard[card].regs->intmask = 0x1;
      }

      for(channel = 0; channel < MAXQUESTROLS; channel++)
        served[channel] = 0;
	
      break;
    }
    
    case QUEST_RESET:
    {
      u_int rol, data, card, channel;
      
      if (copy_from_user(&card, (void *)arg, sizeof(int)))
      {
	kerror(("quest(ioctl,QUEST_RESET): error from copy_from_user\n"));
	return(-QUEST_EFAULT);
      }
     
      kdebug(("quest(ioctl,QUEST_RESET): Resetting card %d\n", card));
      data = questcard[card].regs->opctl;
      data |= 0x1;
      questcard[card].regs->opctl = data;
      udelay(2);    // Delay for at least one us
      data &= 0xfffffffe;
      questcard[card].regs->opctl = data;
      req_dma_active[card] = 0;

      for (channel = 0; channel < MAXQUESTCHANNELS; channel++) //reset the FIFOs
      {
        rol = (card << 2) + channel;     
        infifor[rol] = 0;
        infifon[rol] = 0;
      }
      
      return(0);
      break;
    }
    
    case QUEST_LINKRESET:
    {   
      u_int data1, data2, waited = 0, card, channel;
 
      if (copy_from_user(&channel, (void *)arg, sizeof(int)))
      {
	kerror(("quest(ioctl,QUEST_LINKRESET): error from copy_from_user\n"));
	return(-QUEST_EFAULT);
      }

      card = channel >> 2;      
      channel &= 0x3;
      kdebug(("quest(ioctl,QUEST_LINKRESET): Resetting channel %d of card %d\n", channel, card));

      data1 = 1 << (16 + channel * 4);
      data2 = 1 << (4 + channel * 7);
      kdebug(("quest(ioctl,QUEST_LINKRESET): data 1 = 0x%08x\n", data1));
      kdebug(("quest(ioctl,QUEST_LINKRESET): data 2 = 0x%08x\n", data2));
      questcard[card].regs->opctl |= data1;        // Set the URESET bit
      udelay(2000);                                // Delay for at least two ms
     
      while(questcard[card].regs->opstat & data2)  // Now wait for LDOWN to come up again
      {
        kdebug(("quest(ioctl,QUEST_LINKRESET): opstat = 0x%08x\n", questcard[card].regs->opstat));
        udelay(10000);                              // Wait a bit
        waited++;
        if (waited > 100)
        {
          kerror(("quest(ioctl,QUEST_LINKRESET): channel %d on card %d does not come up again\n", channel, card));
          questcard[card].regs->opctl &= ~data1;   // Reset the URESET bit
	  return(-QUEST_STUCK);
        }          
      }

      questcard[card].regs->opctl &= ~data1;       // Reset the URESET bit
      served[4* card + channel] = 0;
      return(0);
      break;
    }
    
    case QUEST_LINKSTATUS:
    {              
      u_long flags;
      u_int data, card;
      QUEST_status_t params;
      
      if (copy_from_user(&params, (void *)arg, sizeof(QUEST_status_t)))
      {
	kerror(("quest(ioctl, QUEST_LINKSTATUS): error from copy_from_user\n"));
	return(-QUEST_EFAULT);
      }
       
      card = params.channel >> 2;
      data = 1 << (4 + (params.channel & 0x3) * 7);
      if (questcard[card].regs->opstat & data)
        params.ldown = 1;
      else
        params.ldown = 0;
      
      //We have to make sure that this code does not get interrupted. 
      local_irq_save(flags);  //MJ: for 2.6 see p274 & p275
      params.infifo = infifon[params.channel];
      params.reqfifo = REQFIFODEPTH - ((questcard[card].regs->fstat >> (8 * (params.channel & 0x3))) & 0xff); 
      kdebug(("quest(ioctl, QUEST_LINKSTATUS): params.ldown = %d\n", params.ldown));
      kdebug(("quest(ioctl, QUEST_LINKSTATUS): params.infifo = %d\n", params.infifo));
      kdebug(("quest(ioctl, QUEST_LINKSTATUS): params.reqfifo = %d\n", params.reqfifo));
      local_irq_restore(flags);  //MJ: for 2.6 see p274 & p275

      if (copy_to_user((void *)arg, &params, sizeof(QUEST_status_t)))
      {
	kerror(("quest(ioctl, QUEST_LINKSTATUS): error from copy_to_user\n"));
	return(-QUEST_EFAULT);
      } 
      
      return(0);
      break;
    }

    case QUEST_INFO:
    {
      QUEST_info_t info;
      u_int data, card, channel;
            
      info.ncards = maxcards;  
      kdebug(("quest(ioctl, QUEST_INFO): maxcards = %d\n", maxcards));
      
      for(card = 0; card < MAXQUESTCARDS; card++)
      {
        if (card < maxcards)
	  info.cards[card] = 1;
	else  
	  info.cards[card] = 0;
        kdebug(("quest(ioctl, QUEST_INFO): info.cards[%d] = %d\n", card, info.cards[card]));
      }
      
      for(channel = 0; channel < MAXQUESTROLS; channel++)
      {
        card = channel >> 2;
	if (info.cards[card] == 1)
	{
	  data = questcard[card].regs->opstat & (1 << (10 + 7 * (channel & 0x3))); 
	  if (data)
	    info.channel[channel] = 0;
	  else
	    info.channel[channel] = 1;
	}
	else
	  info.channel[channel] = 0;
        kdebug(("quest(ioctl, QUEST_INFO): info.channel[%d] = %d\n", channel, info.channel[channel]));
      } 

      if (copy_to_user((void *)arg, &info, sizeof(QUEST_info_t)))
      {
	kerror(("quest(ioctl, QUEST_INFO): error from copy_to_user\n"));
	return(-QUEST_EFAULT);
      }      
      return(0);
      break;
    }
    
    case QUEST_FIFOIN:
    { 
      u_int channel, qcard;
      u_long irq_flags;

      if (copy_from_user(&channel, (void *)arg, sizeof(int)) !=0)
      {
	kerror(("quest(ioctl,QUEST_FIFOIN): error from copy_from_user\n"));
	return(-QUEST_EFAULT);
      }
      kdebug(("quest(ioctl, QUEST_FIFOIN): channel = %d\n", channel));

      //Try to post a new request block on the card of this channel
      //The function refill_req_fifo_dma is also called from the IRQ handler. On SMP kernels this can lead
      //to race conditions. The spinlock guarantees that only one copy of refill_req_fifo_dma is running at any time
      //As we want to avoid deadlocks with the IRQ handler we have to disable the IRQs temporarily
      
      qcard = channel >> 2;
      
      kdebug(("quest(ioctl, QUEST_FIFOIN): requesting lock\n"));
      spin_lock_irqsave(&slock, irq_flags); 
      kdebug(("quest(ioctl, QUEST_FIFOIN): lock obtained\n"));
      
      refill_req_fifo_dma(channel >> 2);
      
      kdebug(("quest(ioctl, QUEST_FIFOIN): releasing lock \n"));
      spin_unlock_irqrestore(&slock, irq_flags); 

      return(0);
      break;
    }
    
    default:
    {
      kerror(("quest(ioctl,default): You should not be here\n"))
      return(-EINVAL);
    }
  }   
  return(0);
}


/***************************************************/
static void quest_vmaOpen(struct vm_area_struct *vma)
/***************************************************/
{
  kdebug(("quest(quest_vmaOpen): Called\n"));
}


/****************************************************/
static void quest_vmaClose(struct vm_area_struct *vma)
/****************************************************/
{  
  kdebug(("quest(quest_vmaClose): Virtual address  = 0x%016lx\n",(u_long)vma->vm_start));
  kdebug(("quest(quest_vmaClose): mmap released\n"));
}


/******************************************************************/
static int quest_mmap(struct file *file, struct vm_area_struct *vma)
/******************************************************************/
{
  u_long size, offset;  

  kdebug(("quest_mmap: vma->vm_end       = 0x%016lx\n", (u_long)vma->vm_end));
  kdebug(("quest_mmap: vma->vm_start     = 0x%016lx\n", (u_long)vma->vm_start));
  kdebug(("quest_mmap: vma->vm_pgoff     = 0x%016lx\n", (u_long)vma->vm_pgoff));
  kdebug(("quest_mmap: vma->vm_flags     = 0x%08x\n", (u_int)vma->vm_flags));
  kdebug(("quest_mmap: PAGE_SHIFT        = 0x%016lx\n", (u_long)PAGE_SHIFT));
  kdebug(("quest_mmap: PAGE_SIZE         = 0x%016lx\n", (u_long)PAGE_SIZE));

  size = vma->vm_end - vma->vm_start;
  offset = vma->vm_pgoff << PAGE_SHIFT;

  kdebug(("quest_mmap: size                  = 0x%016lx\n", size));
  kdebug(("quest_mmap: physical base address = 0x%016lx\n", offset));

  if (remap_pfn_range(vma, vma->vm_start, vma->vm_pgoff, size, vma->vm_page_prot))
  {
    kerror(("quest_mmap: function remap_pfn_range failed \n"));
    return(-EINVAL);
  }
  kdebug(("quest_mmap: remap_pfn_range OK, vma->vm_start(2) = 0x%016lx\n", (u_long)vma->vm_start));

  vma->vm_ops = &quest_vm_ops;

  return(0);
}


/*******************************************************************************************************/
static ssize_t quest_proc_write(struct file *file, const char *buffer, size_t count, loff_t *startOffset)
/*******************************************************************************************************/
{
  int len;
  char value[100];

  kdebug(("quest(quest_write_procmem): quest_write_procmem called\n"));

  if(count > 99)
    len = 99;
  else
    len = count;

  if (copy_from_user(value, buffer, len))
  {
    kerror(("quest(quest_write_procmem): error from copy_from_user\n"));
    return(-QUEST_EFAULT);
  }

  kdebug(("quest(quest_write_procmem): len = %d\n", len));
  value[len - 1] = '\0';
  kdebug(("quest(quest_write_procmem): text passed = %s\n", value));

  if (!strcmp(value, "debug"))
  {
    debug = 1;
    kdebug(("quest(quest_write_procmem): debugging enabled\n"));
  }

  if (!strcmp(value, "nodebug"))
  {
    kdebug(("quest(quest_write_procmem): debugging disabled\n"));
    debug = 0;
  }
 
  if (!strcmp(value, "elog"))
  {
    kdebug(("quest(quest_write_procmem): error logging enabled\n"))
    errorlog = 1;
  }

  if (!strcmp(value, "noelog"))
  {
    kdebug(("quest(quest_write_procmem): error logging disabled\n"))
    errorlog = 0;
  }
  
  return len;
}


/**************************************************/
int quest_proc_show(struct seq_file *sfile, void *p)
/**************************************************/
{
  u_int card, channel, fstat, ocr, osr, osr2, imask, estat, value, xoff1, xoff2;

  kdebug(("quest(quest_proc_show): Creating text....\n"));
  seq_printf(sfile, "QUEST driver (CS9 ready) for TDAQ release %s (based on SVN tag %s, quest.c revision %s)\n", RELEASE_NAME, CVSTAG, QUEST_TAG);

  if (opid)
    seq_printf(sfile, "The QUEST(s) are currently used by process %d\n", opid);

  seq_printf(sfile, "================================================================================\n");
  seq_printf(sfile, "Card|IRQ line|  REQ IRQ| DONE IRQ|revision|PCI MEM addr.|wswap|bswap|Temperature\n");
  seq_printf(sfile, "----|--------|---------|---------|--------|-------------|-----|-----|-----------\n");
  for(card = 0; card < maxcards; card++)
  {
    ocr = questcard[card].regs->opctl;
    osr = questcard[card].regs->opstat;
    imask = questcard[card].regs->intmask;
    estat = questcard[card].regs->estat;
    seq_printf(sfile, "   %d|", card);
    seq_printf(sfile, "     %3d|", questcard[card].irq_line);
    seq_printf(sfile, " %s|", (imask & 0x4)?" enabled":"disabled"); 
    seq_printf(sfile, " %s|", (imask & 0x1)?" enabled":"disabled"); 
    seq_printf(sfile, "    0x%02x|", questcard[card].pci_revision); 
    seq_printf(sfile, "   0x%08x|", questcard[card].pci_memaddr);
    seq_printf(sfile, "  %s|", (ocr & 0x4)?"yes":" no");
    seq_printf(sfile, "  %s|", (ocr & 0x2)?"yes":" no");
    seq_printf(sfile, "%9d C\n", estat & 0xff);
  }

  for(card = 0; card < maxcards; card++)
  {
    seq_printf(sfile, "\nCard %d:\n", card);
    seq_printf(sfile, "=======\n");
    seq_printf(sfile, "Channel|present|LDOWN|  XOFF|#REQ slots avail.|#fragments sent|#fragment in S/W FIFO\n");
    seq_printf(sfile, "-------|-------|-----|------|-----------------|---------------|---------------------\n");

    osr =  questcard[card].regs->opstat;
    osr2 =  questcard[card].regs->opstat;

    fstat = questcard[card].regs->fstat;
    for(channel  = 0; channel < MAXQUESTCHANNELS; channel++)
    {
      seq_printf(sfile, "      %1d|", channel);
      value = osr & (1 << (10 + channel * 7));
      seq_printf(sfile, "    %s|", value?" No":"Yes");
      value = osr & (1 << (4 + channel * 7));
      seq_printf(sfile, "  %s|", value?"Yes":" No");  

      xoff1 = osr & (1 << (5 + channel * 7));
      xoff2 = osr2 & (1 << (5 + channel * 7));
      if (xoff1 && xoff2)  
  	seq_printf(sfile, " Is ON|");  
      else if (xoff1 && !xoff2)  
  	seq_printf(sfile, "Was ON|");  
      else
  	seq_printf(sfile, "Is OFF|");  

      value = (fstat >> (8 * channel)) & 0xff;
      seq_printf(sfile, "%17d|", value);
      seq_printf(sfile, "%15d|", served[(card << 2) + channel]); 
      seq_printf(sfile, "%21d\n", infifon[card * MAXQUESTCHANNELS + channel]);

    }
  }

  seq_printf(sfile, " \n");
  seq_printf(sfile, "The command 'echo <action> > /proc/quest', executed as root,\n");
  seq_printf(sfile, "allows you to interact with the driver. Possible actions are:\n");
  seq_printf(sfile, "debug   -> enable debugging\n");
  seq_printf(sfile, "nodebug -> disable debugging\n");
  seq_printf(sfile, "elog    -> Log errors to /var/log/messages\n");
  seq_printf(sfile, "noelog  -> Do not log errors to /var/log/messages\n");

  return(0);
}


/*********************************************************/
static irqreturn_t quest_irq_handler(int irq, void *dev_id)
/*********************************************************/
{
  u_int card;

  // Note: the QUEST card(s) may have to share an interrupt line with other PCI devices.
  // It is therefore important to exit quickly if we are not concerned with an interrupt.
  // This is done by looking at DONE_SPACE bit in the OPSTAT register. 
  
  kdebug(("quest(quest_irq_handler): Interrupt received\n"));
  kdebug(("quest(quest_irq_handler): irq = %d\n", irq));
  kdebug(("quest(quest_irq_handler): maxcards = %d\n", maxcards));

  for(card = 0; card < maxcards; card++)
  {
    kdebug(("quest(quest_irq_handler): Card %d uses IRQ %d\n", card, questcard[card].irq_line));
    kdebug(("quest(quest_irq_handler): Card %d has OPSTAT 0x%08x\n", card, questcard[card].regs->opstat));

    if ((questcard[card].irq_line == (u_int)irq) && questcard[card].regs->opstat & 0x1)
    {
      questcard[card].regs->intctl |= 0x5;   //Acknowledge (and clear) the interrupt
      req_dma_active[card] = 0;
      
      //Use a spinlock to avoid race conditions with the ioctl routine QUEST_FIFOIN

      kdebug(("quest(quest_irq_handler): requesting spinlock\n"));
      spin_lock(&slock);
      kdebug(("quest(quest_irq_handler): spinlock obtained\n"));

      refill_req_fifo_dma(card); 

      kdebug(("quest(quest_irq_handler): releasing lock\n"));
      spin_unlock(&slock); 

      return(IRQ_HANDLED);                   //MJ: for 2.6 see p273
    }
    kdebug(("quest(quest_irq_handler): Card %d: done\n", card));
  }  
 
  kdebug(("quest(quest_irq_handler): This was not a quest interrupt\n"));
  return(IRQ_NONE);     
}


/*****************************/
/* PCI driver functions      */
/*****************************/

/************************************************************************/
static int questProbe(struct pci_dev *dev, const struct pci_device_id *id)
/************************************************************************/
{
  int i, card;
  u_long flags;
  u_int ret, size;

  kdebug(("quest(questProbe): called for device: 0x%04x / 0x%04x\n", dev->vendor, dev->device));
  kdebug(("quest(questProbe): Bus: 0x%04x / Devfn 0x%04x\n", dev->bus->number, dev->devfn));

  /* Find a new "slot" for the card */
  card = -1;
  for (i = 0; i < MAXQUESTCARDS; i++) 
  {
    if (questcard[i].pciDevice == NULL) 
    {
      card = i;
      questcard[card].pciDevice = dev;
      break;
    }
  }

  if (card == -1) 
  {
    kerror(("quest(questProbe): Could not find space in the questcard array for this card."));
    return(-QUEST_TOOMANYCARDS);
  }
  
  flags = pci_resource_flags(dev, BAR0);  //MJ: See p317
  if ((flags & IORESOURCE_MEM) != 0) 
  {
    questcard[card].pci_memaddr = pci_resource_start(dev, BAR0) & PCI_BASE_ADDRESS_MEM_MASK;        //MJ: See p317
    size = pci_resource_end(dev, BAR0) - pci_resource_start(dev, BAR0);                             //MJ: See p317
    kdebug(("quest(questProbe): Mapping %d bytes at PCI address 0x%08x\n", size, questcard[card].pci_memaddr));
    
    questcard[card].regs = (T_quest_regs *)ioremap(questcard[card].pci_memaddr, size);
    if (questcard[card].regs == NULL)
    {
      kerror(("quest(questProbe): error from ioremap for questcard[%d].regs\n", card));
      questcard[card].pciDevice = NULL;
      return(-QUEST_IOREMAP);
    }
    kdebug(("quest(questProbe): questcard[%d].regs is at 0x%016lx\n", card, (u_long)questcard[card].regs));
  }
  else 
  {
    kerror(("quest(questProbe): Bar 0 is not a memory resource"));
    return(-QUEST_EIO);
  }  

  // get revision directly from the QUEST
  pci_read_config_byte(questcard[card].pciDevice, PCI_REVISION_ID, &questcard[card].pci_revision);
  kdebug(("quest(questProbe): revision = %x \n", questcard[card].pci_revision));
  if (questcard[card].pci_revision < MINREV)
  {
    kerror(("quest(questProbe): Illegal quest Revision\n"));
    return(-QUEST_ILLREV);
  }  
  
  
  //Enable the device (See p314) and get the interrupt line
  ret = pci_enable_device(dev);
  questcard[card].irq_line = dev->irq;
  kdebug(("quest(questProbe): interrupt line = %d \n", questcard[card].irq_line));

  //Several quest cards may use the same interrupt but we want to install the driver only once
  if (irqlist[questcard[card].irq_line] == 0) 
  {
    if(request_irq(questcard[card].irq_line, quest_irq_handler, IRQF_SHARED, "quest", dev))	
    {
      kerror(("quest(questProbe): request_irq failed on IRQ = %d\n", questcard[card].irq_line));
      return(-QUEST_REQIRQ);
    }
    irqlist[questcard[card].irq_line]++;
    kdebug(("quest(questProbe): Interrupt %d registered\n", questcard[card].irq_line)); 
  }  
 
  maxcards++;
  return(0);
}


/******************************************/
static void questRemove(struct pci_dev *dev) 
/******************************************/
{
  int i, card;

  /* Find the "slot" of the card */
  card = -1;
  for (i = 0; i < MAXQUESTCARDS; i++) 
  {
    if (questcard[i].pciDevice == dev) 
    {
      card = i;
      questcard[i].pciDevice = 0;
      break;
    }  
  }
  
  if (card == -1) 
  {
    kerror(("quest(questRemove): Could not find device to remove."));
    return;
  }

  questcard[card].regs->intctl |= 0x5; // clear any outstanding interrupts
  questcard[card].regs->intmask = 0;   // mask the interrupts
 
  if (--irqlist[questcard[card].irq_line] == 0)
  {
    kdebug(("quest(questRemove): removing handler for interrupt %d", questcard[card].irq_line));
    free_irq(questcard[card].irq_line, dev);	
  }
  
  iounmap((void *)questcard[card].regs);
  kdebug(("quest(questRemove): Card %d removed", card));
  maxcards--;
}




/*********************/
/* Service functions */
/*********************/

/*************************************************************/
static int in_fifo_pop(int channel, u_int *data1, u_int *data2)
/*************************************************************/
{
  int index;
  if (infifon[channel] == 0)
  {
    kerror(("quest(in_fifo_pop): The IN FIFO is empty\n"));
    return(-1);
  }
  
  index = QUEST_IN_INDEX(channel, infifor[channel]);
  *data1 = infifo[index];
  *data2 = infifo[index + 1];
  kdebug(("quest(in_fifo_pop): index=%d data1=0x%x  data2=0x%x\n", index, *data1, *data2));

  infifor[channel]++;

  if (infifor[channel] > (MAXQUESTINFIFO - 1))
    infifor[channel] = 0;

  infifon[channel]--;

  return(0);
}


/***************************************/
static void refill_req_fifo_dma(int card)
/***************************************/
{
  int ret, channel;
  int reqlen = 2; // 4 32-bit words for the entry count -> 2 64-bit words
  int rol = 4 * card;
  u_int *reqptr = (u_int *) reqbuf[card];
  u_int fstat = questcard[card].regs->fstat;
     
  if (req_dma_active[card]) 
  {
    kdebug(("quest(refill_req_fifo_dma): No action will be taken because req_dma_active is still set for this card.\n"));
    kdebug(("quest(refill_req_fifo_dma): REQBLK_DONE = %d\n", (questcard[card].regs->opstat >> 2) & 0x1));
    kdebug(("quest(refill_req_fifo_dma): DONE_SPACE  = %d\n", questcard[card].regs->opstat & 0x1));   
    return;
  }
  
  for (channel = 0; channel < MAXQUESTCHANNELS; channel++)
  {
    int loop;
    int numfree = fstat & 0xff;
    int numreq = infifon[rol];
    int numcopy = (numfree < numreq) ? numfree : numreq;
    kdebug(("quest(refill_req_fifo_dma): channel=%d: numfree=%d,  numreq=%d,  numcopy=%d\n", channel, numfree, numreq, numcopy));
    
    *reqptr++ = numcopy << 1; // number of requests x 2
    
    for (loop = 0; loop < numcopy; loop++)
    {
      u_int addr = 0, size = 0;
      ret = in_fifo_pop(rol, &addr, &size);
      if (ret)
        kerror(("quest(refill_req_fifo_dma): PANIC!!!\n"));

      kdebug(("quest(refill_req_fifo_dma): writing addr=0x%x and ctrl/size=0x%x to reqptr\n", addr, size));
      *reqptr++ = addr; // address of the data block
      *reqptr++ = size; // block size and control bits
    }
    
    served[rol] += numcopy;
    reqlen += numcopy;
    fstat >>= 8;
    rol++;
  }
  
  // Post the request
  if (reqlen > 2) // at least one request
  {
    req_dma_active[card] = 1;
    kdebug(("quest(refill_req_fifo_dma): Posting request: blklen=0x%08x  reqadd=0x%016lx\n", reqlen, reqadd[card]));
    questcard[card].regs->blklen = reqlen; // reqlen is in 64 bit words
    
    //Just to be sure that we have a 32bit PCI address
    if(reqadd[card] > 0xffffffff)
      kerror(("quest(refill_req_fifo_dma): PANIC!!! reqadd[%d] is 0x%016lx (i.e. above 4 GB)\n", card, reqadd[card]));
    
    questcard[card].regs->reqadd = reqadd[card];
  }  
  
  kdebug(("quest(refill_req_fifo_dma): job done\n"));   
}


