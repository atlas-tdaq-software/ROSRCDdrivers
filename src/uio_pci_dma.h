/**
 * @author Dominic Eschweiler <eschweiler@fias.uni-frankfurt.de>
 * @date 2013-04-18
 *
 * @section LICENSE
 * Copyright (C) 2012 Dominic Eschweiler
 * This code is no free software. Please read the LICENSE file in the root of
 * the base directory to get all terms and conditions. Don't use this code if
 * you had not contacted the author before.
 */

#ifndef UIO_PCI_DMA_H
#define UIO_PCI_DMA_H

/** Uncomment this for SLC6 */

#ifdef LINUX_VERSION_CODE
    #undef LINUX_VERSION_CODE
#endif
#define LINUX_VERSION_CODE KERNEL_VERSION(2,6,35)


#define UIO_PCI_DMA_VERSION "0.1.0"
#define UIO_PCI_GENERIC_VERSION "0.01.0"

#ifndef __KERNEL__
#include <limits.h>

#if ( __WORDSIZE == 64 )
   #define BUILD_64   1
#else
   #define BUILD_32
#endif /* __WORDSIZE */

struct kobject
{
    char name[1024];
};

    #ifdef BUILD_64
typedef uint64_t dma_addr_t;
    #else
typedef uint32_t dma_addr_t;
    #endif /* dma_addr_t */
#endif /** #ifndef __KERNEL__ */

struct
__attribute__((__packed__))
uio_pci_dma_private
{
    char      name[1024];
    size_t    size;
    uint8_t   dma_direction;

    /* Kernel internal stuff */
    struct    kobject kobj;

    char     *buffer;
    struct    scatterlist *sg;

    struct    scatter *scatter;

    uint64_t  length;
    struct    device *device;

};

struct scatter
{
    unsigned long page_link;
    unsigned int offset;
    unsigned int length;
    dma_addr_t dma_address;
};

#ifdef __KERNEL__

/** Kernel versions >= 3.8.0  don't have the __devinit flag anymore */
    #if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 8, 0)
        #ifndef __devinit
            #define __devinit
        #endif /** __devinit */
    #endif

/** Kernel versions <= 2.6.34 don't get a file pointer passed to the
 *  sysfs-callbacks
 */
    #if LINUX_VERSION_CODE <= KERNEL_VERSION(2, 6, 34)
    #define BIN_ATTR_WRITE_CALLBACK( name )                                     \
    ssize_t                                                                     \
    uio_pci_dma_sysfs_ ## name                                                  \
    (                                                                           \
        struct kobject       *kobj,                                             \
        struct bin_attribute *attr,                                             \
        char                 *buffer,                                           \
        loff_t offset,                                                          \
        size_t count                                                            \
    )

    #define BIN_ATTR_READ_CALLBACK( name )                                      \
    ssize_t                                                                     \
    uio_pci_dma_sysfs_ ## name                                                  \
    (                                                                           \
        struct kobject       *kobj,                                             \
        struct bin_attribute *attr,                                             \
        char                 *buffer,                                           \
        loff_t offset,                                                          \
        size_t count                                                            \
    )

    #define BIN_ATTR_MAP_CALLBACK( name )                                       \
    int                                                                         \
    uio_pci_dma_sysfs_ ## name                                                  \
    (                                                                           \
        struct kobject        *kobj,                                            \
        struct bin_attribute  *attr,                                            \
        struct vm_area_struct *vma                                              \
    )
    #endif

    #if LINUX_VERSION_CODE > KERNEL_VERSION(2, 6, 34)
    #define BIN_ATTR_WRITE_CALLBACK( name )                                     \
    ssize_t                                                                     \
    uio_pci_dma_sysfs_ ## name                                                  \
    (                                                                           \
        struct file          *file,                                             \
        struct kobject       *kobj,                                             \
        struct bin_attribute *attr,                                             \
        char                 *buffer,                                           \
        loff_t offset,                                                          \
        size_t count                                                            \
    )

    #define BIN_ATTR_READ_CALLBACK( name )                                      \
    ssize_t                                                                     \
    uio_pci_dma_sysfs_ ## name                                                  \
    (                                                                           \
        struct file          *file,                                             \
        struct kobject       *kobj,                                             \
        struct bin_attribute *attr,                                             \
        char                 *buffer,                                           \
        loff_t offset,                                                          \
        size_t count                                                            \
    )

    #define BIN_ATTR_MAP_CALLBACK( name )                                       \
    int                                                                         \
    uio_pci_dma_sysfs_ ## name                                                  \
    (                                                                           \
        struct file           *file,                                            \
        struct kobject        *kobj,                                            \
        struct bin_attribute  *attr,                                            \
        struct vm_area_struct *vma                                              \
    )
    #endif

/** Kernel versions <= 2.6.31 don't have certain defines for PCI config space
 *  adressing */
    #if LINUX_VERSION_CODE <= KERNEL_VERSION(2, 6, 31)
        #ifndef PCI_STATUS_INTERRUPT
            #define PCI_STATUS_INTERRUPT 0x08
        #endif
    #endif

/** Generic stuff */
    #ifdef UIO_PDA_DEBUG
        #define UIO_DEBUG_PRINTF( __F_STRING__, ... )                              \
    {                                                                              \
        printk(DRIVER_NAME " : line %d " __F_STRING__, __LINE__, ## __VA_ARGS__ ); \
    }
    #endif

    #ifndef UIO_PDA_DEBUG
        #define UIO_DEBUG_PRINTF( __F_STRING__, ... )
    #endif

    #ifdef UIO_PDA_DEBUG_SG
        #define UIO_DEBUG_SG( __N_STRING__, __SG, __LENGTH )                    \
    {                                                                           \
        printk( __N_STRING__ );                                                 \
        for(i = 0; i < (__LENGTH); i++)                                         \
        {                                                                       \
            printk("a %llu l %u\n", __SG[i].dma_address, __SG[i].length);       \
        }                                                                       \
    }
    #endif /* UIO_PDA_DEBUG_SG */

    #ifndef UIO_PDA_DEBUG_SG
        #define UIO_DEBUG_SG( __N_STRING__, __SG, __LENGTH )
    #endif

    #define KSET_FIND( _kset, _name, _ret )                                     \
    {                                                                           \
        struct kobject *k;                                                      \
        spin_lock( &_kset->list_lock );                                         \
        list_for_each_entry(k, &_kset->list, entry)                             \
        {                                                                       \
            if(kobject_name(k) && !strcmp(kobject_name(k), _name) )             \
            {                                                                   \
                _ret = kobject_get(k); break;                                   \
            }                                                                   \
        }                                                                       \
        spin_unlock( &_kset->list_lock );                                       \
    }

    #define PRINT_SG( _sg, _length, _str)                                       \
    {                                                                           \
        struct scatterlist *sgp;                                                \
        uint64_t            i;                                                  \
        for_each_sg(_sg, sgp, _length - 1, i)                                   \
        {                                                                       \
            printk( "%s %llu: %lx\n", _str, i, sgp->page_link );                \
        }                                                                       \
    }

    #define BIN_ATTR_PDA(_name, _size, _mode, _read, _write, _mmap)                   \
    struct bin_attribute *attr_bin_ ## _name =                                    \
        (struct bin_attribute*)kmalloc(sizeof(struct bin_attribute), GFP_KERNEL); \
    attr_bin_ ## _name->attr.name = __stringify(_name);                           \
    attr_bin_ ## _name->attr.mode = _mode;                                        \
    attr_bin_ ## _name->size  = _size;                                            \
    attr_bin_ ## _name->read  = _read;                                            \
    attr_bin_ ## _name->write = _write;                                           \
    attr_bin_ ## _name->mmap  = _mmap;

/** Attribute callback definitions */
BIN_ATTR_WRITE_CALLBACK( request_buffer_write );
BIN_ATTR_WRITE_CALLBACK( delete_buffer_write );

BIN_ATTR_MAP_CALLBACK( map );
BIN_ATTR_MAP_CALLBACK( map_sg );

#endif /* __KERNEL__ */

#endif
