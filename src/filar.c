// u_long irq_flags;
// spin_lock_irqsave(&slock, irq_flags); 
// spin_unlock_irqrestore(&slock, irq_flags); 



/************************************************************************/
/*									*/
/* File: filar_driver.c							*/
/*									*/
/* driver for the FILAR S-Link interface				*/
/*									*/
/* 25. Jun. 02  MAJO  created						*/
/*									*/
/************ C 2015 - The software with that certain something *********/


/************************************************************************/
/*NOTES:								*/
/*- This driver should work on kernels from 2.6.9 onwards		*/
/*- This driver should work on 32bit and 64bit kernels			*/
/************************************************************************/

/************************************************************************/
/*Open issues:								*/
/*- The ISR can have errors from the FIFOS (unlikely). It is not clear 	*/
/*  how such errors would be reported. For now they are just logged	*/
/*- The start and end control word patters passed via the INIT ioctl    */
/*  are not yet used in the ISR for consistency checking.		*/
/*- The ioctl fuctions copy large arrays into the driver and back to	*/
/*  the user application. This is so because the amount of data in the	*/
/*  arrays is not known. If possible one should only copy the valid 	*/
/*  data. I don't know yet how to do this				*/ 
/************************************************************************/


#include <linux/init.h>           //MJ: for 2.6, p30
#include <linux/module.h>
#include <linux/moduleparam.h>    //MJ: for 2.6, p30
#include <linux/kernel.h>
#include <linux/stat.h>           //MJ: for 2.6, e.g. for module_param
#include <linux/pci.h>
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/mm.h>
#include <linux/cdev.h>           //e.g. for cdev_alloc
#include <linux/slab.h>           //e.g. for kmalloc
#include <linux/delay.h>          //e.g. for udelay
#include <linux/interrupt.h>      //e.g. for request_irq
#include <linux/version.h>        
#include <linux/sched.h>          //MJ: for current->pid (first needed with SLC6)
#include <linux/spinlock.h>       //For the spin-lock 
#include <linux/seq_file.h>       //CS9
#include <asm/io.h>
#include <asm/uaccess.h>
#include "ROSfilar/filar_driver.h"
#include "ROSfilar/filar_ioctl.h"
#include "ROSRCDdrivers/tdaq_drivers.h"

#ifndef PCI_VENDOR_ID_CERN
  #define PCI_VENDOR_ID_CERN 0x10dc
#endif


/*********/
/*Globals*/
/*********/
static int debug = 0, errorlog = 1;
static char *proc_read_text;
static u_int fuse = 1, scw, ecw;
static u_int maxcard = 0, channels[MAXCARDS][MAXCHANNELS], served[MAXCARDS][MAXCHANNELS];
static u_int irqlist[MAXIRQ], cmask[MAXCARDS], hwfifo[MAXROLS][MAXHWFIFO];
static u_int opid, hwfifow[MAXROLS], hwfifor[MAXROLS], hwfifon[MAXROLS];
static u_int infifor[MAXROLS], outfifow[MAXROLS];
static u_int ccw, order, outfifodatasize, infifodatasize, fifoptrsize, tsize = 0;
static u_int ackdata[MAXROLS][MAXOUTFIFO][4], acknum[MAXROLS]; 
static u_int *outfifo, *outfifon, *infifo, *infifon;
static u_long swfifobase, swfifobasephys;
static struct cdev *filar_cdev;
static T_filar_card filarcard[MAXCARDS];
static dev_t major_minor;
static DEFINE_SPINLOCK(slock);


/************/
/*Prototypes*/
/************/
static int hw_fifo_level(int ro, int *nfree);
static int filarProbe(struct pci_dev *dev, const struct pci_device_id *id);
static int filar_init(void);
static void disable_input(void);
static void enable_input(void);
static void filar_cleanup(void);
static void filar_vmaOpen(struct vm_area_struct *vma);
static void filar_vmaClose(struct vm_area_struct *vma);
static void filarRemove(struct pci_dev *dev); 
static irqreturn_t filar_irq_handler(int irq, void *dev_id); 
int filar_proc_open(struct inode *inode, struct file *file);
int filar_proc_show(struct seq_file *sfile, void *p);

MODULE_DESCRIPTION("S-Link FILAR");
MODULE_AUTHOR("Markus Joos, CERN/PH");
MODULE_VERSION("4.0");
MODULE_LICENSE("Private: Contact markus.joos@cern.ch");
module_param (debug, int, S_IRUGO | S_IWUSR);  //MJ: for 2.6 p37
MODULE_PARM_DESC(debug, "1 = enable debugging   0 = disable debugging"); 
module_param (errorlog, int, S_IRUGO | S_IWUSR);  //MJ: for 2.6 p37
MODULE_PARM_DESC(errorlog, "1 = enable debugging   0 = disable debugging"); 


// memory handler functions
static struct vm_operations_struct filar_vm_ops = 
{
  .close = filar_vmaClose,   //mmap-close
  .open  = filar_vmaOpen,    //MJ: Note the comma at the end of the list!
};

// Standard file operations
static struct file_operations fops = 
{
  .owner          = THIS_MODULE,
  .unlocked_ioctl = filar_ioctl,
  .open           = filar_open,    
  .mmap           = filar_mmap,
  .release        = filar_release,  
};


//CS9
//Inspiration taken from: https://stackoverflow.com/questions/64931555/how-to-fix-error-passing-argument-4-of-proc-create-from-incompatible-pointer
//===========================================================
#if LINUX_VERSION_CODE >= KERNEL_VERSION(5,6,0)
static const struct proc_ops filar_proc_file_ops = 
{
  .proc_open    = filar_proc_open,
  .proc_write   = filar_proc_write,
  .proc_read    = seq_read,    
  .proc_lseek   = seq_lseek,	
  .proc_release = single_release
};
#else
static struct file_operations filar_proc_file_ops = 
{
  .owner   = THIS_MODULE,
  .open    = filar_proc_open,
  .write   = filar_proc_write,
  .read    = seq_read,
  .llseek  = seq_lseek,
  .release = single_release
};
#endif
//==============================================================




// PCI driver structures. See p311
static struct pci_device_id filarIds[] = 
{
  {PCI_DEVICE(PCI_VENDOR_ID_CERN, 0x14)},      
  {0,},
};

//PCI operations
static struct pci_driver __refdata filarPciDriver = 
{
  .name     = "filar",
  .id_table = filarIds,
  .probe    = filarProbe,
  .remove   = filarRemove,
};


/*****************************/
/* Standard driver functions */
/*****************************/


/*********************************************************/
int filar_proc_open(struct inode *inode, struct file *file) 
/*********************************************************/
{
  return single_open(file, filar_proc_show, NULL);
}


/*************************/
static int filar_init(void)
/*************************/
{
  int rol, tfsize, result;
  u_int loop;
  struct page *page_ptr;
  static struct proc_dir_entry *filar_file;

  kerror(("FILAR (SC) driver version 4.0\n")); 

  if (alloc_chrdev_region(&major_minor, 0, 1, "filar")) //MJ: for 2.6 p45
  {
    kdebug(("filar(filar_init): failed to obtain device numbers\n"));
    result = -FILAR_EIO;
    goto fail1;
  }
  
  proc_read_text = (char *)kmalloc(MAX_PROC_TEXT_SIZE, GFP_KERNEL);
  if (proc_read_text == NULL)
  {
    kdebug(("filar(filar_init): error from kmalloc\n"));
    result = -EFAULT;
    goto fail2;
  }

  filar_file = proc_create("filar", 0644, NULL, &filar_proc_file_ops);
  if (filar_file == NULL)
  {
    kerror(("filar(filar_init): error from call to create_proc_entry\n"));
    result = -EFAULT;
    goto fail3;
  }  

  //Clear the list of used interupts 
  for(loop = 0; loop < MAXIRQ; loop++)
    irqlist[loop] = 0;

  kdebug(("filar(filar_init): registering PCIDriver\n"));
  result = pci_register_driver(&filarPciDriver);  //MJ: See P312
  if (result < 0)
  {
    kerror(("filar(filar_init): ERROR! no FILAR cards found!\n"));
    result = -EIO;
    goto fail4;
  }
  else 
  {
    kerror(("filar(filar_init): Found some FILAR cards!\n"));
  }

  // Allocate contiguous memory for the FIFOs and store the base addresses in a global structure
  outfifodatasize = MAXROLS * MAXOUTFIFO * OUTFIFO_ELEMENTS * sizeof(u_int);    //outfifo
  infifodatasize  = MAXROLS * MAXINFIFO * INFIFO_ELEMENTS * sizeof(u_int);      //infifo
  fifoptrsize     = MAXROLS * sizeof(u_int);                                    //outfifo(w/r/n) + infifo(w/r/n)
  kdebug(("filar(filar_init): 0x%08x bytes needed for the OUT FIFO\n", outfifodatasize));
  kdebug(("filar(filar_init): 0x%08x bytes needed for the IN FIFO\n", infifodatasize));
  kdebug(("filar(filar_init): 0x%08x bytes needed for the FIFO pointers\n", fifoptrsize));
  
  tsize = outfifodatasize + infifodatasize + 2 * fifoptrsize;
  kdebug(("filar(filar_init): 0x%08x bytes needed for all FIFOs\n", tsize));

  tfsize = (tsize + 4095) & 0xfffff000;  // round up to next 4K boundary
  tfsize = tfsize >> 12;                 // compute number of pages
  kdebug(("filar(filar_init): %d pages needed for the S/W FIFOs\n", tfsize));
 
  order = 0;
  while(tfsize)
  {
    order++;
    tfsize = tfsize >> 1;                 // compute order
  } 
  kdebug(("filar(filar_init): order = %d\n", order));
  
  swfifobase = __get_free_pages(GFP_ATOMIC, order);
  if (!swfifobase)
  {
    kerror(("filar(filar_init): error from __get_free_pages\n"));
    result = -FILAR_EFAULT;
    goto fail5;
  }
  kdebug(("filar(filar_init): swfifobase = 0x%016lx\n", swfifobase));
   
  // Reserve all pages to make them remapable
  page_ptr = virt_to_page(swfifobase);

  for (loop = (1 << order); loop > 0; loop--, page_ptr++)
    set_bit(PG_reserved, &page_ptr->flags);                   //MJ: have a look at the kernel book

  //////swfifobasephys = virt_to_bus((void *)swfifobase);           //MJ: Is virt_to_bus the correct function also for modern kernels???
  swfifobasephys = virt_to_phys((void *)swfifobase);           //MJ: Is virt_to_bus the correct function also for modern kernels???
  kdebug(("filar(filar_init): swfifobasephys = 0x%016lx\n", swfifobasephys));

  // Assign base addresses to the FIFO arrays
  outfifo  = (u_int *)swfifobase;
  infifo   = (u_int *)(swfifobase + outfifodatasize);
  outfifon = (u_int *)(swfifobase + outfifodatasize + infifodatasize);
  infifon  = (u_int *)(swfifobase + outfifodatasize + infifodatasize + fifoptrsize);

  kdebug(("filar(filar_init): outfifo  is at 0x%016lx\n", (u_long)outfifo));
  kdebug(("filar(filar_init): infifo   is at 0x%016lx\n", (u_long)infifo));
  kdebug(("filar(filar_init): outfifon is at 0x%016lx\n", (u_long)outfifon));
  kdebug(("filar(filar_init): infifon  is at 0x%016lx\n", (u_long)infifon));

  //Initialize the FIFOs
  for(rol = 0; rol < MAXROLS; rol++)
  {
    infifor[rol] = 0;
    infifon[rol] = 0;
    hwfifow[rol] = 0;
    hwfifor[rol] = 0;
    hwfifon[rol] = 0;
    outfifow[rol] = 0;
    outfifon[rol] = 0;
  }

  opid = 0;
  
  filar_cdev = (struct cdev *)cdev_alloc();       //MJ: for 2.6 p55
  filar_cdev->ops = &fops;
  result = cdev_add(filar_cdev, major_minor, 1);  //MJ: for 2.6 p56
  if (result)
  {
    kdebug(("filar(filar_init): error from call to cdev_add.\n"));
    goto fail6;
  }
  
  kdebug(("filar(filar_init): driver loaded; major device number = %d\n", MAJOR(major_minor)));
  return(0);  
  
  fail6:
    page_ptr = virt_to_page(swfifobase);
    for (loop = (1 << order); loop > 0; loop--, page_ptr++)
      clear_bit(PG_reserved, &page_ptr->flags);
    free_pages(swfifobase, order);

  fail5:
    pci_unregister_driver(&filarPciDriver);

  fail4:
    remove_proc_entry("io_rcc", NULL);
    
  fail3:
    kfree(proc_read_text);

  fail2:
    unregister_chrdev_region(major_minor, 1); //MJ: for 2.6 p45

  fail1:
    return(result);  
}


/*****************************/
static void filar_cleanup(void)
/*****************************/
{
  u_int loop;
  struct page *page_ptr;
      
  cdev_del(filar_cdev);                     //MJ: for 2.6 p56
  pci_unregister_driver(&filarPciDriver);
  page_ptr = virt_to_page(swfifobase);      // unreserve all pages used for the S/W FIFOs

  for (loop = (1 << order); loop > 0; loop--, page_ptr++)
    clear_bit(PG_reserved, &page_ptr->flags);

  free_pages(swfifobase, order);            // free the area 
  remove_proc_entry("filar", NULL);
  kfree(proc_read_text);
  unregister_chrdev_region(major_minor, 1); //MJ: for 2.6 p45

  kdebug(("filar(filar_cleanup): driver removed\n"));
}


module_init(filar_init);    //MJ: for 2.6 p16
module_exit(filar_cleanup); //MJ: for 2.6 p16


/**********************************************************/
static int filar_open(struct inode *ino, struct file *filep)
/**********************************************************/
{
  kdebug(("filar(filar_open): nothing to be done\n"));
  return(0);
}


/*************************************************************/
static int filar_release(struct inode *ino, struct file *filep)
/*************************************************************/
{
  kdebug(("filar(filar_release): (alomst) nothing to be done\n"));
  opid = 0;
  return(0);
}


/***************************************************************/
static long filar_ioctl(struct file *file, u_int cmd, u_long arg)
/***************************************************************/
{
  switch (cmd)
  {    
    case OPEN:
    {
      FILAR_open_t params;
      
      kdebug(("filar(ioctl,OPEN): called from process %d\n", current->pid))
      if (!opid)
        opid = current->pid;
      else
      {
	kerror(("filar(ioctl, OPEN): The FILAR is already used by process %d\n", opid));
    	return(-FILAR_USED);
      }
	
      params.size = tsize;
      params.physbase = swfifobasephys;
      params.outsize = outfifodatasize;
      params.insize = infifodatasize;
      params.ptrsize = fifoptrsize;
      kdebug(("filar(ioctl, OPEN):  tsize           = 0x%08x\n", tsize));
      kdebug(("filar(ioctl, OPEN):  swfifobasephys  = 0x%016lx\n", swfifobasephys));
      kdebug(("filar(ioctl, OPEN):  outfifodatasize = 0x%08x\n", outfifodatasize));
      kdebug(("filar(ioctl, OPEN):  infifodatasize  = 0x%08x\n", infifodatasize));
      kdebug(("filar(ioctl, OPEN):  fifoptrsize     = 0x%08x\n", fifoptrsize));
            
      if (copy_to_user((void *)arg, &params, sizeof(FILAR_open_t)) != 0)
      {
	kerror(("filar(ioctl, OPEN): error from copy_to_user\n"));
	return(-FILAR_EFAULT);
      }      
      return(0);
      break;
    }
    
    case CLOSE:
    {
      kdebug(("filar(ioctl, CLOSE): called from process %d\n", current->pid))
      opid = 0;
  
      return(0);
      break;
    }
    
    case CONFIG:
    { 
      FILAR_config_t params;
      u_int card, channel, data, rol;
      
      if (copy_from_user(&params, (void *)arg, sizeof(FILAR_config_t)) !=0)
      {
	kerror(("filar(ioctl, CONFIG): error from copy_from_user\n"));
	return(-FILAR_EFAULT);
      }
        
      scw = params.scw;
      ecw = params.ecw;
      ccw = params.ccw;
      kdebug(("filar(ioctl, CONFIG): params.bswap = %d\n", params.bswap));
      kdebug(("filar(ioctl, CONFIG): params.wswap = %d\n", params.wswap));
      kdebug(("filar(ioctl, CONFIG): params.psize = %d\n", params.psize));
      kdebug(("filar(ioctl, CONFIG): params.ccw = %d\n", params.ccw));

      for(card = 0; card < maxcard; card++)
      {
        data = 0;
        for(channel = 0; channel < MAXCHANNELS; channel++)
        { 
          served[card][channel] = 0;
          rol = (card << 2) + channel;          //works only for 4 channels per card
          kdebug(("filar(ioctl, CONFIG): params.enable[%d] = %d\n", rol, params.enable[rol]));
          if (params.enable[rol])
            channels[card][channel] = 1; 
          else
          {
            data += (1 << (6 * channel + 9));
            channels[card][channel] = 0;
          }
          kdebug(("filar(ioctl, CONFIG): channels[%d][%d] = %d\n", card, channel, channels[card][channel]));
          kdebug(("filar(ioctl, CONFIG): data = 0x%08x\n", data));
        }
        data += (params.bswap << 1) + (params.wswap << 2) + (params.psize << 3);
        kdebug(("filar(ioctl, CONFIG): Writing 0x%08x to the OCR\n", data));
        filarcard[card].regs->ocr = data;
        cmask[card] = data;                    //remember which channels were enabled
        
        if (params.interrupt)
          filarcard[card].regs->imask |= 0x2;  //Enable the ACK FIFO almost full interrupt 
      }
      break;
    }
    
    case RESET:
    {
      u_int channel, data, card, rol;
      
      if (copy_from_user(&card, (void *)arg, sizeof(int)) !=0)
      {
	kerror(("filar(ioctl, RESET): error from copy_from_user\n"));
	return(-FILAR_EFAULT);
      }

      for(channel = 0; channel < MAXCHANNELS; channel++)
        served[card][channel] = 0;
      
      if (card <= maxcard)
      {
        kdebug(("filar(ioctl, RESET): Resetting card %d\n", card));
        data = filarcard[card].regs->ocr;
        data |= 0x1;
        filarcard[card].regs->ocr = data;
        udelay(2);  // Delay for at least one us
        data &= 0xfffffffe;
        filarcard[card].regs->ocr = data;

        //reset the FIFOs
        for(channel = 0; channel < MAXCHANNELS; channel++)
        {
          rol = (card << 2) + channel;
          infifor[rol] = 0;
          infifon[rol] = 0;
          hwfifow[rol] = 0;
          hwfifor[rol] = 0;
          hwfifon[rol] = 0;
          outfifow[rol] = 0;
          outfifon[rol] = 0;
        }
      }
      else
      {
       	kerror(("filar(ioctl,RESET): This card is unknown\n"));
	return(-FILAR_NOCARD); 
      }
      
      return(0);
      break;
    }
    
    case LINKRESET:
    {   
      u_int waited, rol, data, card, channel;
 
      if (copy_from_user(&rol, (void *)arg, sizeof(int)) !=0)
      {
	kerror(("filar(ioctl,RESET): error from copy_from_user\n"));
	return(-FILAR_EFAULT);
      }
      
      card = rol >> 2;      
      if (card <= maxcard)
      {
        channel = rol & 0x3;
        kdebug(("filar(ioctl,LINKRESET): Resetting card %d, channel %d\n", card, channel));
        served[card][channel] = 0;

        data = 0x1 << (8 + channel * 6);       // Set the URESET bit
        filarcard[card].regs->ocr |= data;
        data = 0x1 << (16 + channel * 4);      // Now wait for LDOWN to come up again
        waited = 0;
        while(filarcard[card].regs->osr & data)
        {
	  udelay(100);                         // wait for 100 us
          waited++;
          if (waited > 1000)                   // wait for a total of 100 ms
          {
            kerror(("filar(ioctl,LINKRESET): channel %d of card %d does not come up again\n", channel, card));
            data = ~(0x1 << (8 + channel * 6));  // Reset the URESET bit
            filarcard[card].regs->ocr &= data;
	    return(-FILAR_STUCK);
          }          
        }
        data = ~(0x1 << (8 + channel * 6));  // Reset the URESET bit
        filarcard[card].regs->ocr &= data;
      }
      else
      {
       	kerror(("filar(ioctl,LINKRESET): This card is unknown\n"));
	return(-FILAR_NOCARD); 
      }
      
      kdebug(("filar(ioctl,LINKRESET): Done\n"));
      return(0);
      break;
    }
    
    case LINKSTATUS:
    {   
      u_int isup, data, card, rol, channel;
      
      if (copy_from_user(&rol, (void *)arg, sizeof(int)) !=0)
      {
	kerror(("filar(ioctl, RESET): error from copy_from_user\n"));
	return(-FILAR_EFAULT);
      }
      
      card = rol >> 2;
      if (card <= maxcard)
      {
        channel = rol & 0x3;
        data = 0x1 << (16 + channel * 4);
        if (filarcard[card].regs->osr & data)
          isup = 0;
        else
          isup = 1;
          
        if (copy_to_user((void *)arg, &isup, sizeof(int)) != 0)
        {
	  kerror(("filar(ioctl, LINKSTATUS): error from copy_to_user\n"));
	  return(-FILAR_EFAULT);
        } 
      }
      else
      {
       	kerror(("filar(ioctl, LINKSTATUS): This card is unknown\n"));
	return(-FILAR_NOCARD); 
      }
      return(0);
      break;
    }

    case INFO:
    {
      FILAR_info_t info;
      u_int rol, card, data;
     
      //Initialise the array to 0
      for(rol = 0; rol < MAXROLS; rol++)
      {
        info.channels[rol] = 0;
        info.enabled[rol] = 0;
      }
      info.nchannels = 0;
      
      for(card = 0; card < maxcard; card++)
      {
        data = filarcard[card].regs->osr;
        kdebug(("filar(ioctl, INFO): 0x%08x read from OSR\n", data));
        if (data & 0x00080000)  info.channels[(card << 2)] = 0; 
        else {info.channels[(card << 2)] = 1; info.nchannels++;}
        
        if (data & 0x00800000)  info.channels[(card << 2) + 1] = 0; 
        else {info.channels[(card << 2) + 1] = 1; info.nchannels++;}
        
        if (data & 0x08000000)  info.channels[(card << 2) + 2] = 0; 
        else {info.channels[(card << 2) + 2] = 1; info.nchannels++;}
        
        if (data & 0x80000000)  info.channels[(card << 2) + 3] = 0; 
        else {info.channels[(card << 2) + 3] = 1; info.nchannels++;}
                
        data = filarcard[card].regs->ocr;
        kdebug(("filar(ioctl, INFO): 0x%08x read from OCR\n", data));
        
        if (data & 0x00000200) info.enabled[(card << 2)] = 0;     else info.enabled[(card << 2)] = 1;
        if (data & 0x00008000) info.enabled[(card << 2) + 1] = 0; else info.enabled[(card << 2) + 1] = 1;        
        if (data & 0x00200000) info.enabled[(card << 2) + 2] = 0; else info.enabled[(card << 2) + 2] = 1;        
        if (data & 0x08000000) info.enabled[(card << 2) + 3] = 0; else info.enabled[(card << 2) + 3] = 1;       
      }  

      if (copy_to_user((void *)arg, &info, sizeof(FILAR_info_t)) != 0)
      {
	kerror(("filar(ioctl, INFO): error from copy_to_user\n"));
	return(-FILAR_EFAULT);
      }      
      return(0);
      break;
    }
    
    case FIFOIN:
    { 
      u_int card, channel, rol, loop, nfree, navail, min, data;
      u_long flags;
      u_long irq_flags;
            
      spin_lock_irqsave(&slock, irq_flags); 
      kdebug(("filar(ioctl,FIFOIN): spinlock obtained\n"));

      if (copy_from_user(&rol, (void *)arg, sizeof(int)) !=0)
      {
	kerror(("filar(ioctl,FIFOIN): error from copy_from_user\n"));
	return(-FILAR_EFAULT);
      }
      card = rol >> 2;
      channel = rol & 0x3; 
      kdebug(("filar(ioctl,FIFOIN): channel = %d\n", channel));
      kdebug(("filar(ioctl,FIFOIN): card = %d\n", card));

      //We have to make sure that this code does not get interrupted. Otherwhise there will be
      //overflows in the IN FIFO which lead to incorrect PCI addresses being written to the FILAR
      local_irq_save(flags);  //MJ: for 2.6 see p274 & p275

      //Is there space in the FILAR
      hw_fifo_level(rol, &nfree);
      kdebug(("filar(ioctl,FIFOIN): There are %d slots available in the HW FIFO of ROL %d\n", nfree, rol));

      //How many entries are in the IN FIFO
      navail = infifon[rol]; 
      kdebug(("filar(ioctl,FIFOIN): There are %d entries in the IN FIFO\n", navail));

      if (nfree < navail)
        min = nfree;
      else
        min = navail;
              
      kdebug(("filar(ioctl,FIFOIN): min = %d\n", min));
                 
      for(loop = 0; loop < min; loop++)
      {
        kdebug(("filar(ioctl,FIFOIN): in loop: loop = %d, min = %d\n", loop, min));
        // We have checked how many entries are free / available in the FIFOS
        // Therefore we can waive the return values
        in_fifo_pop(rol, &data);
	
        kdebug(("filar(ioctl,FIFOIN): calling hw_fifo_push\n"));
        hw_fifo_push(rol, data);

        // I don't check if there is space as this should be guaranteed by
        // the coupling between the FILAR FIFO and the H/W FIFO which has the same depth
             if (channel == 0) filarcard[card].regs->req1 = data;
        else if (channel == 1) filarcard[card].regs->req2 = data;
        else if (channel == 2) filarcard[card].regs->req3 = data;
        else                   filarcard[card].regs->req4 = data; 
	
        kdebug(("filar(ioctl,FIFOIN): PCI addr. 0x%08x written to card %d, channel %d\n", data, card, channel));
      }

      local_irq_restore(flags);
      spin_unlock_irqrestore(&slock, irq_flags);       
      kdebug(("filar(ioctl,FIFOIN): spinlock released\n"));

      return(0);
      break;
    }
    
    case FLUSH:
    { 
      flush_cards();
      break;
    }

    default:
    {
      kerror(("filar(ioctl,default): You should not be here\n"))
      return(-EINVAL);
    }
  }   
  return(0);
}


/***************************************************/
static void filar_vmaOpen(struct vm_area_struct *vma)
/***************************************************/
{ 
  kdebug(("filar(filar_vmaOpen): Called\n"));
}


/****************************************************/
static void filar_vmaClose(struct vm_area_struct *vma)
/****************************************************/
{  
  kdebug(("filar(filar_vmaClose): Virtual address  = 0x%016lx\n",(u_long)vma->vm_start));
  kdebug(("filar(filar_vmaClose): mmap released\n"));
}


/******************************************************************/
static int filar_mmap(struct file *file, struct vm_area_struct *vma)
/******************************************************************/
{
  u_long size, offset;  

  kdebug(("filar_mmap: vma->vm_end       = 0x%016lx\n", (u_long)vma->vm_end));
  kdebug(("filar_mmap: vma->vm_start     = 0x%016lx\n", (u_long)vma->vm_start));
  kdebug(("filar_mmap: vma->vm_pgoff     = 0x%016lx\n", (u_long)vma->vm_pgoff));
  kdebug(("filar_mmap: vma->vm_flags     = 0x%08x\n", (u_int)vma->vm_flags));
  kdebug(("filar_mmap: PAGE_SHIFT        = 0x%016lx\n", (u_long)PAGE_SHIFT));
  kdebug(("filar_mmap: PAGE_SIZE         = 0x%016lx\n", (u_long)PAGE_SIZE));

  size = vma->vm_end - vma->vm_start;
  offset = vma->vm_pgoff << PAGE_SHIFT;

  kdebug(("filar_mmap: size                  = 0x%016lx\n", size));
  kdebug(("filar_mmap: physical base address = 0x%016lx\n", offset));

  if (remap_pfn_range(vma, vma->vm_start, vma->vm_pgoff, size, vma->vm_page_prot))
  {
    kerror(("filar_mmap: function remap_pfn_range failed \n"));
    return(-EFAULT);
  }
  kdebug(("filar_mmap: remap_pfn_range OK, vma->vm_start(2) = 0x%016lx\n", (u_long)vma->vm_start));

  vma->vm_ops = &filar_vm_ops;

  return(0);
}


/*******************************************************************************************************/
static ssize_t filar_proc_write(struct file *file, const char *buffer, size_t count, loff_t *startOffset)
/*******************************************************************************************************/
{
  int len;
  u_int card;
  char value[100];

  kdebug(("filar(filar_write_procmem): filar_write_procmem called\n"));

  if(count > 99)
    len = 99;
  else
    len = count;

  if (copy_from_user(value, buffer, len))
  {
    kerror(("filar(filar_write_procmem): error from copy_from_user\n"));
    return(-FILAR_EFAULT);
  }

  kdebug(("filar(filar_write_procmem): len = %d\n", len));
  value[len - 1] = '\0';
  kdebug(("filar(filar_write_procmem): text passed = %s\n", value));

  if (!strcmp(value, "debug"))
  {
    debug = 1;
    kdebug(("filar(filar_write_procmem): debugging enabled\n"));
  }

  if (!strcmp(value, "nodebug"))
  {
    kdebug(("filar(filar_write_procmem): debugging disabled\n"));
    debug = 0;
  }
  
  if (!strcmp(value, "elog"))
  {
    kdebug(("filar(filar_write_procmem): Error logging enabled\n"))
    errorlog = 1;
  }

  if (!strcmp(value, "noelog"))
  {
    kdebug(("filar(filar_write_procmem): Error logging disabled\n"))
    errorlog = 0;
  }

  if (!strcmp(value, "disable"))
  {
    for(card = 0; card < maxcard; card++)
      filarcard[card].regs->ocr |= 0x08208200;
    kdebug(("filar(filar_write_procmem): All channels disabled\n"));
  }
  
  if (!strcmp(value, "reset"))
  {
    reset_cards(1);
    kdebug(("filar(filar_write_procmem): All cards reset\n"));
  }
    
  if (!strcmp(value, "flush"))
  {
    flush_cards();
    kdebug(("filar(filar_write_procmem): All cards flushed\n"));
  }
  
  return len;
}


/**************************************************/
int filar_proc_show(struct seq_file *sfile, void *p)
/**************************************************/
{
  u_int rol, card, ocr[MAXROLS], osr[MAXROLS], osr2[MAXROLS], imask[MAXROLS], fifostat[MAXROLS], value, value2, channel;
  unsigned long ret = 0;

  kdebug(("filar(filar_proc_show): Creating text....\n"));

  seq_printf(sfile, "FILAR driver (single cycle  protocol, CS9 ready) for TDAQ release %s (based on tag %s, filar.c revision %s)\n", RELEASE_NAME, CVSTAG, FILAR_TAG);

  if (opid)
    seq_printf(sfile, "The drivers is currently used by PID %d\n", opid);

  seq_printf(sfile, "=========================================================================\n");
  seq_printf(sfile, "Card|IRQ line| ACK IRQ|revision|PCI MEM addr.|         page size|wswap|bswap|temperature\n");
  seq_printf(sfile, "----|--------|--------|--------|-------------|------------------|-----|-----|-----------\n");

  //Read the registers
  for(card = 0; card < maxcard; card++)
  {
    ocr[card]   = filarcard[card].regs->ocr;
    osr[card]   = filarcard[card].regs->osr;
    osr2[card]  = filarcard[card].regs->osr;
    imask[card] = filarcard[card].regs->imask;
    fifostat[card] = filarcard[card].regs->fifostat;
  }

  for(card = 0; card < maxcard; card++)
  {
    seq_printf(sfile, "   %d|", card);
    seq_printf(sfile, "     %3d|", filarcard[card].irq_line);
    seq_printf(sfile, "%s|", (imask[card] & 0x2)?" enabled":"disabled"); 
    seq_printf(sfile, "    0x%02x|", filarcard[card].pci_revision); 
    seq_printf(sfile, "   0x%08x|", filarcard[card].pci_memaddr);
    value = (ocr[card] >> 3) & 0x7;
    if (value == 0) seq_printf(sfile, "         256 bytes|");
    if (value == 1) seq_printf(sfile, "           1 Kbyte|");
    if (value == 2) seq_printf(sfile, "          2 Kbytes|");
    if (value == 3) seq_printf(sfile, "          4 Kbytes|");
    if (value == 4) seq_printf(sfile, "         16 Kbytes|");
    if (value == 5) seq_printf(sfile, "         64 Kbytes|");
    if (value == 6) seq_printf(sfile, "        256 Kbytes|");
    if (value == 7) seq_printf(sfile, "4 MBytes - 8 Bytes|");
    seq_printf(sfile, "  %s|", (ocr[card] & 0x4)?"yes":" no");
    seq_printf(sfile, "  %s|", (ocr[card] & 0x2)?"yes":" no");
    seq_printf(sfile, " %3d deg. C\n", (osr[card] >>8) & 0xff);
  }

  for(card = 0; card < maxcard; card++)
  {
    seq_printf(sfile, "\nCard %d:\n", card);
    seq_printf(sfile, "=======\n");
    seq_printf(sfile, "       |       |       |          |free      |     |        |       |          |free      |\n");
    seq_printf(sfile, "       |       |       |entries in|entries in|     |        |       |entries in|entries in|fragments\n");
    seq_printf(sfile, "Channel|present|enabled|ACK FIFO  |REQ FIFO  |LDOWN|Overflow|   XOFF|OUT FIFO  |IN FIFO   |served\n");
    seq_printf(sfile, "-------|-------|-------|----------|----------|-----|--------|-------|----------|----------|---------\n");

    for(channel = 0; channel < MAXCHANNELS; channel++)
    {
      seq_printf(sfile, "      %d|", channel);

      value = osr[card] & (1 << (19 + channel * 4));
      seq_printf(sfile, "    %s|", value?" no":"yes");

      value = ocr[card] & (1 << (9 + channel * 6));
      seq_printf(sfile, "    %s|", value?" no":"yes");

      value = (fifostat[card] >> (channel * 8)) & 0xf;
      if (value < 15)
        seq_printf(sfile, "        %2d|", value);
      else
        seq_printf(sfile, "       >14|");

      value = (fifostat[card] >> ( 4 + channel * 8)) & 0xf;
      if (value < 15)
        seq_printf(sfile, "        %2d|", value);
      else
        seq_printf(sfile, "       >14|");

      value = osr[card] & (1 << (16 + channel * 4));
      seq_printf(sfile, "  %s|", value?"yes":" no"); 

      value = osr[card] & (1 << (17 + channel * 4));
      seq_printf(sfile, "     %s|", value?"yes":" no");

      value = osr[card] & (1 << (18 + channel * 4));
      value2 = osr2[card] & (1 << (18 + channel * 4));
      if (!value && !value2)
        seq_printf(sfile, " is off|");
      else if (value && value2)
        seq_printf(sfile, "  is on|");
      else if (value && !value2)
        seq_printf(sfile, " was on|");
      else 
        seq_printf(sfile, "went on|");

      rol = (card << 2) + channel;
      ret = out_fifo_level(rol, &value);
      seq_printf(sfile, "      %4d|", MAXOUTFIFO - value);

      ret = in_fifo_level(rol, &value);
      seq_printf(sfile, "      %4d|", value);

      seq_printf(sfile, " %8d\n", served[card][channel]);     
    }
  }

  seq_printf(sfile, " \n");
  seq_printf(sfile, "The command 'echo <action> > /proc/filar', executed as root,\n");
  seq_printf(sfile, "allows you to interact with the driver. Possible actions are:\n");
  seq_printf(sfile, "debug   -> enable debugging\n");
  seq_printf(sfile, "nodebug -> disable debugging\n");
  seq_printf(sfile, "elog    -> Log errors to /var/log/message\n");
  seq_printf(sfile, "noelog  -> Do not log errors to /var/log/message\n");
  seq_printf(sfile, "disable -> disable all FILAR channels\n");
  seq_printf(sfile, "reset   -> Reset all FILAR channels\n");
  seq_printf(sfile, "flush   -> Flush all FILAR channels\n");
  
  return(0);
}


/*********************************************************/
static irqreturn_t filar_irq_handler(int irq, void *dev_id)    //MJ: for 2.6 see p272 & p273 
/*********************************************************/
{
  u_int card;

  // Note: the FILAR card(s) may have to share an interrupt line with other PCI devices
  // It is therefore important to exit quickly if we are not concerned with an interrupt
  
  for(card = 0; card < maxcard; card++)
  {
    if ((filarcard[card].irq_line == (u_int)irq) && (filarcard[card].regs->osr & 0x2))
    {
      if (fuse)
        read_ack_and_write_out(card);
      else
      {    
        filarcard[card].regs->imask = 0;  //Disable all interrupts
        kerror(("filar(filar_irq_handler): CATASTROPHY: Interrupt received from card %d with fuse=0\n", card));
      }
      return(IRQ_HANDLED);  //MJ: for 2.6 see p273
    }
    else
    {
      kdebug(("filar(filar_irq_handler): This was not a FILAR interrupt\n"));
    }
  }
  return(IRQ_NONE);     
}  



/*****************************/
/* PCI driver functions      */
/*****************************/

/************************************************************************/
static int filarProbe(struct pci_dev *dev, const struct pci_device_id *id)  
/************************************************************************/
{
  int i, card;
  u_long flags;
  u_int ret, size;

  kdebug(("filar(filarProbe): called for device: 0x%04x / 0x%04x\n", dev->vendor, dev->device));
  kdebug(("filar(filarProbe): Bus: 0x%04x / Devfn 0x%04x\n", dev->bus->number, dev->devfn));

  /* Find a new "slot" for the card */
  card = -1;
  for (i = 0; i < MAXCARDS; i++) 
  {
    if (filarcard[i].pciDevice == NULL) 
    {
      card = i;
      filarcard[card].pciDevice = dev;
      break;
    }
  }

  if (card == -1) 
  {
    kerror(("filar(filarProbe): Could not find space in the filarcard array for this card."));
    return(-FILAR_TOOMANYCARDS);
  }
  
  flags = pci_resource_flags(dev, BAR0);  //MJ: See p317
  if ((flags & IORESOURCE_MEM) != 0) 
  {
    filarcard[card].pci_memaddr = pci_resource_start(dev, BAR0) & PCI_BASE_ADDRESS_MEM_MASK;        //MJ: See p317
    size = pci_resource_end(dev, BAR0) - pci_resource_start(dev, BAR0);                             //MJ: See p317
    kdebug(("filar(filarProbe): Mapping %d bytes at PCI address 0x%08x\n", size, filarcard[card].pci_memaddr));
    
    filarcard[card].regs = (T_filar_regs *)ioremap(filarcard[card].pci_memaddr, size);
    if (filarcard[card].regs == NULL)
    {
      kerror(("filar(filarProbe): error from ioremap for filarcard[%d].regs\n", card));
      filarcard[card].pciDevice = NULL;
      return(-FILAR_IOREMAP);
    }
    kdebug(("filar(filarProbe): filarcard[%d].regs is at 0x%016lx\n", card, (u_long)filarcard[card].regs));
  }
  else 
  {
    kerror(("filar(filarProbe): Bar 0 is not a memory resource"));
    return(-FILAR_EIO);
  }

  // get revision directly from the Filar
  pci_read_config_byte(filarcard[card].pciDevice, PCI_REVISION_ID, &filarcard[card].pci_revision);
  kdebug(("filar(filarProbe): revision = %x \n", filarcard[card].pci_revision));
  if (filarcard[card].pci_revision < MINREV)
  {
    kerror(("filar(filarProbe): Illegal Filar Revision\n"));
    return(-FILAR_ILLREV);
  }

  //Enable the device (See p314) and get the interrupt line
  ret = pci_enable_device(dev);
  filarcard[card].irq_line = dev->irq;
  kdebug(("filar(filarProbe): interrupt line = %d \n", filarcard[card].irq_line));

  //Several FILAR cards may use the same interrupt but we want to install the driver only once
  if (irqlist[filarcard[card].irq_line] == 0) 
  {
    if(request_irq(filarcard[card].irq_line, filar_irq_handler, IRQF_SHARED, "filar", dev))	
    {
      kdebug(("filar(filarProbe): request_irq failed on IRQ = %d\n", filarcard[card].irq_line));
      return(-FILAR_REQIRQ);
    }
    irqlist[filarcard[card].irq_line]++;
    kdebug(("filar(filarProbe): Interrupt %d registered\n", filarcard[card].irq_line)); 
  }  
 
  maxcard++;
  return(0);
}


/******************************************/
static void filarRemove(struct pci_dev *dev) 
/******************************************/
{
  int i, card;

  /* Find the "slot" of the card */
  card = -1;
  for (i = 0; i < MAXCARDS; i++) 
  {
    if (filarcard[i].pciDevice == dev) 
    {
      card = i;
      filarcard[i].pciDevice = 0;
      break;
    }  
  }
  
  if (card == -1) 
  {
    kerror(("filar(filarRemove): Could not find device to remove."));
    return;
  }

  if (--irqlist[filarcard[card].irq_line] == 0)
  {
    kerror(("filar(filarRemove): removing handler for interrupt %d", filarcard[card].irq_line));
    free_irq(filarcard[card].irq_line, dev);	
  }
  
  iounmap((void *)filarcard[card].regs);
  kerror(("filar(filarRemove): Card %d removed", card));
  maxcard--;
}



/*********************/
/* Service functions */
/*********************/

/****************************************/
static int hw_fifo_push(int rol, int data)
/****************************************/
{
  kdebug(("filar(hw_fifo_push): Function called with rol = %d , hwfifon[rol] = %d and data = 0x%08x\n", rol, hwfifon[rol], data));
  if (hwfifon[rol] == MAXHWFIFO)
  {
    kerror(("filar(hw_fifo_push): The HW FIFO is full. MAXHWFIFO = %d\n", MAXHWFIFO));
    blow_fuse();
    return(-1);
  }
  hwfifo[rol][hwfifow[rol]] = data;
  hwfifow[rol]++;
  if (hwfifow[rol] == MAXHWFIFO)
    hwfifow[rol] = 0;
  
  hwfifon[rol]++;
  return(0);
}


/****************************************/
static int hw_fifo_pop(int rol, int *data)
/****************************************/
{
  if (hwfifon[rol] == 0)
  {
    kerror(("filar(hw_fifo_pop): The HW FIFO is empty\n"));
    blow_fuse();
    return(-1);
  }
  *data = hwfifo[rol][hwfifor[rol]];
  hwfifor[rol]++;
  if (hwfifor[rol] == MAXHWFIFO)
    hwfifor[rol] = 0;
  
  hwfifon[rol]--;
  return(0);
}


/*******************************************/
static int hw_fifo_level(int rol, int *nfree)
/*******************************************/
{
  kdebug(("filar(hw_fifo_level): Function called. hwfifon[rol] = %d\n", hwfifon[rol]));
  
  *nfree = MAXHWFIFO - hwfifon[rol];
  return(0);
}


/****************************************/
static int in_fifo_pop(int rol, int *data)
/****************************************/
{
  if (infifon[rol] == 0)
  {
    kdebug(("filar(in_fifo_pop): The IN FIFO is empty\n"));
    return(-1);
  }
  *data = infifo[IN_INDEX(rol, infifor[rol])];

  infifor[rol]++;
  if (infifor[rol] == MAXINFIFO)
    infifor[rol] = 0;

  infifon[rol]--;

  return(0);
}


/*******************************************/
static int in_fifo_level(int rol, int *nfree)
/*******************************************/
{
  *nfree = MAXINFIFO - infifon[rol];
  return(0);
}


/******************************************************************************************/
static int out_fifo_push(int rol, int pciaddr, int fragsize, int fragstat, int scw, int ecw)
/******************************************************************************************/
{
  int oo;
  
  oo = OUT_INDEX(rol, outfifow[rol]);
  kdebug(("filar(out_fifo_push): oo = %d\n", oo));
  
  if (outfifon[rol] == MAXOUTFIFO)
  {
    kerror(("filar(out_fifo_push): The OUT FIFO is full\n"));
    return(-1);
  }
  
  outfifo[oo]     = pciaddr;
  outfifo[oo + 1] = fragsize;
  outfifo[oo + 2] = fragstat;
  outfifo[oo + 3] = scw;
  outfifo[oo + 4] = ecw;

  outfifow[rol]++;
  if (outfifow[rol] == MAXOUTFIFO)
    outfifow[rol] = 0;
    
  barrier();
    
  outfifon[rol]++;
  return(0);
}


/********************************************/
static int out_fifo_level(int rol, int *nfree)
/********************************************/
{
  *nfree = MAXOUTFIFO - outfifon[rol];
  return(0);
}


/*****************************/
static void disable_input(void)
/*****************************/
{
  u_int card, data;

  for(card = 0; card < maxcard; card++)
  {
    data = filarcard[card].regs->ocr;
    data |= 0x08208200;
    filarcard[card].regs->ocr = data;   //disable all channels
  }
}


/****************************/
static void enable_input(void)
/****************************/
{
  u_int card;

  for(card = 0; card < maxcard; card++)
    filarcard[card].regs->ocr = cmask[card];  //reset cannels to initial state
}


/******************************************/
static void read_ack_and_write_out(int card)
/******************************************/
{
  u_int data2, fragstat, nfree, pciaddr, ret, dindex, rol, loop, nacks, channel;
  volatile u_int data, *ackptr, *reqptr;
  u_long irq_flags;

  spin_lock_irqsave(&slock, irq_flags); 
  kdebug(("filar(read_ack_and_write_out): spinlock obtained\n"));

  //---> Part 1: read / write FILAR registers
  disable_input();
  for(channel = 0; channel < MAXCHANNELS; channel++)
  {
    kdebug(("filar(read_ack_and_write_out): channels[%d][%d] = %d\n", card, channel, channels[card][channel]));
    if (channels[card][channel])
    {
      dindex = 0;
      rol = (card << 2) + channel;     
      reqptr = (u_int *) (&filarcard[card].regs->req1 + 4 * channel);
      ackptr = (u_int *) reqptr + 1;
      out_fifo_level(rol, &nfree); 

      while(1)
      {   
        nacks = (filarcard[card].regs->fifostat >> (channel * 8)) & 0xf;
        kdebug(("filar(read_ack_and_write_out): card=%d  channel=%d  rol=%d  nacks=%d\n", card, channel, rol, nacks));
        if (!nacks)
          break;

        for(loop = 0; loop < nacks; loop++)
        {  
	  ret = hw_fifo_pop(rol, &pciaddr);
          if (ret)
          {
            kerror(("filar(read_ack_and_write_out): error from hw_fifo_pop\n"));
	    blow_fuse();
            return;
          }
          kdebug(("filar(read_ack_and_write_out): Read PCI address 0x%08x from HW FIFO\n", pciaddr));

	  data = *ackptr;
          kdebug(("filar(read_ack_and_write_out): Read 0x%08x from ACK FIFO\n", data));
          ackdata[rol][dindex][0] = data;
          if ((data & 0x40000000) || ccw)
            ackdata[rol][dindex][1] = filarcard[card].regs->scw;
          if ((data & 0x10000000) || ccw)
            ackdata[rol][dindex][2] = filarcard[card].regs->ecw;
	    
          ackdata[rol][dindex][3] = pciaddr;
          dindex++;
	  nfree--;
	  if (!nfree)
          {
            kerror(("filar(read_ack_and_write_out): nfree=0 , dindex=%d-> blowing fuse\n", dindex));
            blow_fuse();
            return;
          }
        }
      }
      
      acknum[rol] = dindex;             
      kdebug(("filar(read_ack_and_write_out): Received %d fragments from card=%d, channel=%d\n", dindex, card, channel));

      for(loop = 0; loop < dindex; loop++)
      {
        ret = in_fifo_pop(rol, &pciaddr);
        if (!ret)
        {
          kdebug(("filar(read_ack_and_write_out): calling hw_fifo_push\n"));
	  ret = hw_fifo_push(rol, pciaddr);
          if (ret)
          {
            kerror(("filar(read_ack_and_write_out): error from hw_fifo_push\n"));
            blow_fuse();
            return;
          }
          *reqptr = pciaddr;
          kdebug(("filar(read_ack_and_write_out): Wrote PCI address 0x%08x to REQ FIFO\n", pciaddr));
	}  
      }
    }
  }
  enable_input();
  
  //---> Part 2: process data
  for(channel = 0; channel < MAXCHANNELS; channel++)
  {
    if (channels[card][channel])
    {
      rol = (card << 2) + channel;
      for(loop = 0; loop < acknum[rol]; loop++)
      {        
        fragstat = 0;
        data = ackdata[rol][loop][0];
        kdebug(("filar(read_ack_and_write_out): Analyzing card=%d  channel=%d  fragment=%d  ack=0x%08x\n", card, channel, loop, data));
        if (data & 0x80000000)
        {
          fragstat |= NO_SCW;
          kerror(("filar(read_ack_and_write_out): NO_SCW on card=%d, channel=%d, ack-data=0x%08x\n", card, channel, data));
        }
        else 
        {
          if (data & 0x40000000)
          {
            fragstat |= BAD_SCW;
            kerror(("filar(read_ack_and_write_out): BAD_SCW on card=%d, channel=%d, ack-data=0x%08x\n", card, channel, data));

            data2 = ackdata[rol][loop][1];
            kerror(("filar(read_ack_and_write_out): Start Control Word              = 0x%08x\n", data2));

            if (data2 & 0x1)
            {
              kerror(("filar(read_ack_and_write_out): Data transmission error in SCW on card=%d, channel=%d, ack-data=0x%08x\n", card, channel, data));
              fragstat |= SCW_DTE;
            }

            if (data2 & 0x2)
            {
              kerror(("filar(read_ack_and_write_out): Control word transmission error in SCW on card=%d, channel=%d, ack-data=0x%08x\n", card, channel, data));
              fragstat |= SCW_CTE;
            }
          }
        }

        if (data & 0x20000000)
        {
          fragstat |= NO_ECW;
          kerror(("filar(read_ack_and_write_out): NO_ECW on card=%d, channel=%d, ack-data=0x%08x\n", card, channel, data));
        }
        else
        {
          if (data & 0x10000000)
          {
            fragstat |= BAD_ECW;
            kerror(("filar(read_ack_and_write_out): BAD_ECW on card=%d, channel=%d, ack-data=0x%08x\n", card, channel, data));

            data2 = ackdata[rol][loop][2];  
            kerror(("filar(read_ack_and_write_out): End Control Word                = 0x%08x\n", data2));

            if (data2 & 0x1)
            {
              kerror(("filar(read_ack_and_write_out): Data transmission error in ECW on card=%d, channel=%d, ack-data=0x%08x\n", card, channel, data));
              fragstat |= ECW_DTE;
            }

            if (data2 & 0x2)
            {
              kerror(("filar(read_ack_and_write_out): Control word transmission error in ECW on card=%d, channel=%d, ack-data=0x%08x\n", card, channel, data));
              fragstat |= ECW_CTE;
            } 
          }
        }

        if (ccw)
          ret = out_fifo_push(rol, ackdata[rol][loop][3], data & 0x000fffff, fragstat, ackdata[rol][loop][1], ackdata[rol][loop][2]);
	else
          ret = out_fifo_push(rol, ackdata[rol][loop][3], data & 0x000fffff, fragstat, 0 ,0);
    
        if (ret)
        {
          kerror(("filar(read_ack_and_write_out): error from out_fifo_push\n"));
          blow_fuse();
	  return;
        }        
      }
      served[card][channel] += acknum[rol];
    }
  } 
  spin_unlock_irqrestore(&slock, irq_flags); 
  kdebug(("filar(read_ack_and_write_out): spinlock released\n"));
}


/*************************/
static void blow_fuse(void)
/*************************/
{   
  // If this fuse blows one has to re-configure the FILAR (to re-enable the interrupt)
  u_int data, card, channel, rol, nacks, nfree;

  fuse = 0;
  kerror(("filar(blow_fuse): There was an internal error in the driver\n"));
  kerror(("filar(blow_fuse): All interrupts have been disabled\n"));
  kerror(("filar(blow_fuse): You have to reload the driver\n"));

  for(card = 0; card < MAXCARDS; card++)
  {
    for(channel = 0; channel < MAXCHANNELS; channel++)
    {
      if (channels[card][channel])
      {
        rol = (card << 2) + channel;
        nacks = (filarcard[card].regs->fifostat >> (channel * 8)) & 0xf;
        out_fifo_level(rol, &nfree); 
        kerror(("filar(blow_fuse): rol=%d  nacks=%d  nfree=%d\n", rol, nacks, nfree));
      }
    }
  } 

  for(card = 0; card < maxcard; card++)
  {
    filarcard[card].regs->imask = 0;  //Disable all interrupts
    
    data = filarcard[card].regs->ocr;
    data |= 0x1;
    filarcard[card].regs->ocr = data;
    udelay(2);  // Delay for at least one us
    data &= 0xfffffffe;
    filarcard[card].regs->ocr = data;
  }
}


/*******************************/
static void reset_cards(int mode)
/*******************************/
{
  u_int card, rol, channel, data;

  kdebug(("filar(reset_cards): maxcard = %d   mode = %d\n", maxcard, mode));
  for(card = 0; card < maxcard; card++)
  {
    data = filarcard[card].regs->ocr;
    data |= 0x1;
    filarcard[card].regs->ocr = data;
    // Delay for at least one us
    udelay(10);
    data &= 0xfffffffe;
    filarcard[card].regs->ocr = data;
    //reset the FIFOs
    if (mode)
    {
      for(channel = 0; channel < MAXCHANNELS; channel++)
      {
        served[card][channel] = 0;
        rol = (card << 2) + channel;
        infifor[rol] = 0;
        infifon[rol] = 0;
        outfifow[rol] = 0;
        outfifon[rol] = 0;
        hwfifow[rol] = 0;
        hwfifor[rol] = 0;
        hwfifon[rol] = 0;
      }
    }
  }
  kdebug(("filar(reset_cards): Done\n"));
}


/***************************/
static void flush_cards(void)
/***************************/
{
  u_long flags;
  u_int card;

  //We have to make sure that this code does not get interrupted.
  //Otherwhise there can be a race condition.
  local_irq_save(flags);
  for(card = 0; card < maxcard; card++)
  {
    kdebug(("filar(flush_cards): Calling read_ack_and_write_out for card %d\n", card));
    read_ack_and_write_out(card);
  }

  local_irq_restore(flags);
}

