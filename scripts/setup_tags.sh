#TDAQ_RELEASE is hardcoded and 
export TDAQ_RELEASE="tdaq-12-00-00"
echo "HARDCODED TDAQ RELEASE NAME: $TDAQ_RELEASE"

echo "=== BUILD TAGS ==="

rtag="ROSRCDdrivers-99-00-00"
# CI_COMMIT_TAG is an automatic git CI variable.
# It is set to the tag name, if present, of the current commit.
if [ ! -z $CI_COMMIT_TAG ]; then
    rtag=$CI_COMMIT_TAG
fi

export RELEASE_TAG=$rtag

export CMEM_TAG=$(cd ../cmem_rcc && git describe --always --dirty)
echo "cmem_rcc library:       $CMEM_TAG"

export IO_TAG=$(cd ../io_rcc && git describe --always --dirty)
echo "io_rcc library:         $IO_TAG"

export ROBINNP_TAG=$(cd ../ROSRobinNP && git describe --always --dirty)
echo "ROSRobinNP library:     $ROBINNP_TAG"

export FILAR_TAG=$(cd ../ROSfilar && git describe --always --dirty)
echo "ROSfilar library:       $FILAR_TAG"

export SOLAR_TAG=$(cd ../ROSsolar && git describe --always --dirty)
echo "ROSsolar library:       $SOLAR_TAG"

export VME_TAG=$(cd ../vme_rcc && git describe --always --dirty)
echo "vme_rcc library:        $VME_TAG"

echo "=================="
