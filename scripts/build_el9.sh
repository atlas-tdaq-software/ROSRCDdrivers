#!/bin/bash

NUM_KERNELS=2
ALL_KERNELS=$(dnf list --showduplicates kernel | grep kernel.x86_64 | awk '{print $2 ".x86_64"}' | tail -n ${NUM_KERNELS})

for KERNEL in ${ALL_KERNELS}; do
    dnf install -y kernel-${KERNEL} kernel-devel-${KERNEL} grubby
    source setup_kernel.sh ${KERNEL}
    cd ../src
    make clean
    make
    make
    if [ $? -ne 0 ]; then
        echo "Error: The make command exited with a non-zero status code."
        exit 1
    fi
    cd ../scripts
done
